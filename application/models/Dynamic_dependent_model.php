<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dynamic_dependent_model extends CI_Model
{


	function fetch_state($PROVINCE_ID)
	{
		$this->db->where('Strategic_id', $PROVINCE_ID);
		$this->db->order_by('Goal_id', 'desc');
		$query = $this->db->get('goal');
		$output = '<option value="">--- กรุณาเลือกเป้าประสงค์---</option>';
		$i = 1;
		foreach ($query->result() as $row) {
			$output .= ' <option value="' . $row->Goal_id . '">' . $i++ . '.' . $row->Goal_name . '</option> ';
		}
		return $output;
	}

	function fetch_city($AMPHUR_ID)
	{
		$this->db->where('Goal_id', $AMPHUR_ID);
		$this->db->order_by('Tactic_id', 'ASC');
		$query = $this->db->get('tactic');
		$output = '<option value=""> --- กรุณาเลือกสาขา --- </option> ';
		$e = 1;
		foreach ($query->result() as $row) {
			$output .= '<option value="' . $row->Tactic_id . '">' . $e++ . '.' . $row->Tactic_name . '</option>';
		}
		return $output;
	}

	// function list()
	// {
	// 	$this->db->select('*,tbl_thesis.id AS idT');
	// 	$this->db->from('tbl_thesis');
	// 	$this->db->join('tbl_board', 'tbl_board.id = tbl_thesis.board', 'left');
	// 	$this->db->join('tbl_subject', 'tbl_subject.id = tbl_thesis.subject', 'left');
	// 	$this->db->join('tbl_branch', 'tbl_branch.id = tbl_thesis.branch', 'left');

	// 	$data = $this->db->get();

	// 	return $data->result();
	// }
}

/* End of file dynamic_dependent_model.php */
/* Location: ./application/models/dynamic_dependent_model.php */
