<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Login model class
 */
class Search_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function project_list($year)
    {
        $this->db->select('*');
        $this->db->from('project');
        if ($year != '') {
            $this->db->where('Year', $year);
        }

        $this->db->order_by('Time', 'desc');
        $data = $this->db->get();

        return $data->result_array();
    }

    public function search_All($year, $Department, $Status)
    {
        $this->db->select('*');
        $this->db->from('project');
        if ($year != '') {
            $this->db->where('Year', $year);
        }
        if ($Department != '') {
            $this->db->where('Department_id', $Department);
        }
        if ($Status != '') {
            $this->db->where('Status', $Status);
        } else {
            $this->db->where('Status !=', 0);
            $this->db->where('Status !=', 9);
        }
        $this->db->order_by('Time', 'desc');

        $data = $this->db->get();

        return $data->result_array();
    }

    public function search_All_supplies($year, $Status)
    {
        $this->db->select('*');
        $this->db->from('project');
        if ($year != '') {
            if ($year !== 'all_year') {
                $this->db->where('Year', $year);
            }
        }
        if ($Status != '') {
            $this->db->where('Status', $Status);
        } else {
            $this->db->where('Tor', 1);
            $this->db->where('Status !=', 0);
            $this->db->where('Status !=', 9);
        }
        $this->db->order_by('Time', 'desc');

        $data = $this->db->get();

        return $data->result_array();
    }

    public function project_list_manager($year)
    {
        $this->db->select('*');
        $this->db->from('project');
        if ($year != '') {
            // $this->db->where('Status', 1);
            $this->db->where('Year', $year);
        }
        // $this->db->where('Status', 1);
        $this->db->order_by('Time', 'desc');
        $data = $this->db->get();

        return $data->result_array();
    }

    public function search_All_manager($year, $Department, $Status)
    {
        $this->db->select('*');
        $this->db->from('project');
        if ($year != '') {
            if ($year !== 'all_year') {
                $this->db->where('Year', $year);
            }
        }
        if ($Department != '') {
            $this->db->where('Department_id', $Department);
        }
        if ($Status != '') {
            $this->db->where('Status', $Status);
        } else {
            $this->db->where('Status !=', 0);
            $this->db->where('Status !=', 9);
        }
        $this->db->order_by('Time', 'desc');

        $data = $this->db->get();

        return $data->result_array();
    }

    public function project_list_supervisor($year, $chk_user)
    {
        $this->db->select('*');
        $this->db->from('project');
        if ($year != '') {
            // $this->db->where('Status', 2);
            $this->db->where('Year', $year);
        }
        $this->db->where('Department_id', $chk_user);
        // $this->db->where('Status', 2);
        $this->db->order_by('Project_id', 'DESC');
        $data = $this->db->get();

        return $data->result_array();
    }

    public function search_All_supervisor($year, $Department, $Status)
    {
        $this->db->select('*');
        $this->db->from('project');
        if ($year != '') {
            $this->db->where('Year', $year);
        }
        if ($Department != '') {
            $this->db->where('Department_id', $Department);
        }
        if ($Status != '') {
            $this->db->where('Status', $Status);
        }
        $this->db->where('Status !=', '0');
        $this->db->where('Status !=', '9');
        $this->db->order_by('Project_id', 'DESC');

        $data = $this->db->get();

        return $data->result_array();
    }

    public function search_ME($year, $ME, $Status)
    {
        $this->db->select('*');
        $this->db->from('project');
        $this->db->where('Account_id', $ME);
        if ($year != '') {
            $this->db->where('Year', $year);
        }
        if ($Status != '') {
            $this->db->where('Status', $Status);
        }
        $this->db->where('Status !=', 0);
        $this->db->order_by('Time', 'desc');
        $data = $this->db->get();

        return $data->result_array();
    }

    public function project_listME($ME, $year)
    {
        $this->db->select('*');
        $this->db->from('project');
        // $this->db->join('comment', 'comment.Project_id = project.Project_id','left');
        $this->db->where('Status !=', 0);
        if ($year != '') {
            $this->db->where('Year', $year);
        }
        $this->db->where('Account_id', $ME);
        $this->db->order_by('Time', 'desc');
        $data = $this->db->get();

        return $data->result_array();
    }

    public function project_notAppAll($year)
    {
        $this->db->select('*');
        $this->db->from('project');
        $this->db->where('Status', 0);
        if ($year != '') {
            $this->db->where('Year', $year);
        }
        $this->db->order_by('Time', 'desc');

        $data = $this->db->get();

        return $data->result_array();
    }

    public function project_notAppAll_search($Department, $year)
    {
        $this->db->select('*');
        $this->db->from('project');
        $this->db->where('Year', $year);
        if ($Department != '') {
            $this->db->where('Department_id', $Department);
        }
        $this->db->where('Status', 0);

        $data = $this->db->get();

        return $data->result_array();
    }

    public function project_notApp($account)
    {
        $this->db->select('*');
        $this->db->from('project');
        $this->db->where('Status', 0);
        $this->db->where('Account_id', $account);
        $this->db->order_by('Time', 'desc');

        $data = $this->db->get();

        return $data->result_array();
    }

    public function project_notApp_search($year, $account)
    {
        $this->db->select('*');
        $this->db->from('project');
        $this->db->where('Status', 0);
        $this->db->where('Account_id', $account);
        $this->db->where('Year', $year);
        $this->db->order_by('Time', 'desc');

        $data = $this->db->get();

        return $data->result_array();
    }
}
