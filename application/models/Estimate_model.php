<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Login model class
 */
class Estimate_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function estimate($PID)
    {
        $this->db->select('*,project_strategic_plane.Project_id as str_plane_pid');
        $this->db->from('project_strategic_plane');
        $this->db->join('strategic_plane', 'project_strategic_plane.Strategic_Plan_id = strategic_plane.Strategic_Plan_id');
        $this->db->join('project', 'project.Project_id = project_strategic_plane.Project_id');
        $this->db->join('department', 'project.Department_id = department.Department_id');
        $this->db->join('strategic', 'strategic.Strategic_Plan_id = project_strategic_plane.Strategic_Plan_id');
        $this->db->join('goal', 'goal.Goal_id = project_strategic_plane.Goal_id');
        $this->db->join('indic_project', 'indic_project.Indic_project_id = project_strategic_plane.Indic_project_id');
        $this->db->where('project_strategic_plane.Project_id', $PID);
        $data = $this->db->get();

        return $data->row_array();
    }

    public function estimate_view($PID)
    {
        $this->db->select('*');
        $this->db->from('estimate');
        $this->db->where('Project_id', $PID);
        $data = $this->db->get();

        return $data->row_array();
    }
}
