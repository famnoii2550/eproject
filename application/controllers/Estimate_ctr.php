<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("Asia/Bangkok");
class Estimate_ctr extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Estimate_model');
    }

    public function index()
    {
        $PID = base64_decode($this->input->get('PID'));
        $data['PID'] = $PID;
        $data['project'] = $this->db->get_where('project', ['Project_id' => $PID])->row_array();
        $data['estimate'] = $this->Estimate_model->estimate($PID);
        $this->load->view('option/header');
        $this->load->view('estimate', $data);
        $this->load->view('option/footer');
    }

    function estimate_review()
    {
        $PID = base64_decode($this->input->get('PID'));
        $URI = base64_decode($this->input->get('uri'));
        $data['URI'] = $URI;
        $data['PID'] = $PID;
        $data['project'] = $this->db->get_where('project', ['Project_id' => $PID])->row_array();
        $data['estimate'] = $this->Estimate_model->estimate_view($PID);
        $this->load->view('option/header');
        $this->load->view('estimate_review', $data);
        $this->load->view('option/footer');
    }

    function estimate_review_approved()
    {
        $pid = $this->input->get('project_id');
        $st_approved = $this->input->get('status_approved');

        $data = array(
            'Flag_close'    => $st_approved,
        );
        $this->db->where('Project_id', $pid);
        if ($this->db->update('estimate', $data)) {
            $user = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            $comment = $this->input->get('note');
            if (empty($comment)) {
                $comm = 'อนุมัติขอปิดโครงการโดย' . ' (' . $user['Fname'] . ' ' . $user['Lname'] . ')';
            } else {
                $comm = $this->input->get('note');
            }

            $data = array(
                'Project_id'    => $pid,
                'Account_id'    => $user['Account_id'],
                'Comment'       => $comm,
                'Time'          => date('Y-m-d H:i:s'),
                'Role'          => 'Manager',
            );
            $result = $this->db->insert('comment', $data);
        }

        if ($result) {
            echo "success";
        } else {
            echo "false";
        }
    }

    function estimate_review_notapproved()
    {
        $pid = $this->input->get('project_id');

        $this->db->where('Project_id', $pid);
        if ($result = $this->db->delete('estimate')) {
            $user = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            $data = array(
                'Project_id'    => $pid,
                'Account_id'    => $user['Account_id'],
                'Comment'       => 'ถูกปฎิเสธขอปิดโครงการโดย' . ' (' . $user['Fname'] . ' ' . $user['Lname'] . ' )',
                'Time'          => date('Y-m-d H:i:s'),
                'Role'          => 'Manager',
            );
            $result = $this->db->insert('comment', $data);
        }

        if ($result) {
            echo "success";
        } else {
            echo "false";
        }
    }

    function estimate_review_approved_director()
    {
        $pid = $this->input->get('project_id');
        $st_approved = $this->input->get('status_approved');

        $data = array(
            'Flag_close'    => $st_approved,
        );
        $this->db->where('Project_id', $pid);
        if ($this->db->update('estimate', $data)) {
            $this->db->where('Project_id', $pid);
            $this->db->update('project', ['Status' => 4]);
            $user = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            $data = array(
                'Project_id'    => $pid,
                'Account_id'    => $user['Account_id'],
                'Comment'       => 'อนุมัติขอปิดโครงการโดย' . ' (' . $user['Fname'] . ' ' . $user['Lname'] . ' )',
                'Time'          => date('Y-m-d H:i:s'),
                'Role'          => 'Director',
            );
            $result = $this->db->insert('comment', $data);
        }

        if ($result) {
            echo "success";
        } else {
            echo "false";
        }
    }

    function estimate_review_notapproved_director()
    {
        $pid = $this->input->get('project_id');
        $comment = $this->input->get('comment');
        $quarter = $this->input->get('quarter');

        $this->db->where('Project_id', $pid);
        if ($result = $this->db->delete('estimate')) {
            $user = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            $data = array(
                'Project_id'    => $pid,
                'Account_id'    => $user['Account_id'],
                'Comment'       => $comment,
                'Time'          => date('Y-m-d H:i:s'),
                'Role'          => 'Director',
            );
            $result = $this->db->insert('comment', $data);
            $last_report = $this->db->order_by('Project_id', 'DESC')->get_where('project_report', ['Project_id' => $pid, 'quarter' => $quarter])->row_array();
            $this->db->where('Project_id', $last_report['Project_id']);
            $this->db->where('quarter', $quarter);
            $this->db->delete('project_report');
        }

        if ($result) {
            echo "success";
        } else {
            echo "false";
        }
    }
}
