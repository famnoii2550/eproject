<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("Asia/Bangkok");
class List_project_ctr extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Search_model');
    }

    public function index()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $year           = $this->input->get('year');
            $Status         = $this->input->get('Status');
            $account        = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            $ME             = $account['Account_id'];
            // echo $year . ' ' . $ME . ' ' . $Status;
            // exit();
            // if ($year == '' && $Status == '') {
            //     $data['project']        = $this->Search_model->project_listME($ME, $year);
            // } else {
                $data['project']        = $this->Search_model->search_ME($year, $ME, $Status);
            //}
            $this->load->view('option/header');
            $this->load->view('list_project', $data);
            $this->load->view('option/footer');
        }
    }
}
