<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("Asia/Bangkok");
class Workplan_ctr extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Pdf');
    }

    public function index()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $valueStrategic_plan        = base64_decode($this->input->get('stp'));
            $strategic_plane_first      = $this->db->order_by('Strategic_Plan_id', 'ASC')->get('strategic_plane')->row_array();
            $data['strategic_plane']    = $this->db->get('strategic_plane')->result();
            $data['stp'] = $valueStrategic_plan;
            if (empty($valueStrategic_plan)) {
                $data['workplan_list']      = $this->db->get_where('strategic', ['Strategic_Plan_id' => $strategic_plane_first['Strategic_Plan_id']])->result();
            } else {
                $data['workplan_list']      = $this->db->get_where('strategic', ['Strategic_Plan_id' => $valueStrategic_plan])->result();
            }

            $this->load->view('option/header');
            $this->load->view('workplan_list', $data);
            $this->load->view('option/footer');
        }
    }

    public function workplan()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['SID'] = base64_decode($this->input->get('Strategic_id'));
            $this->load->view('option/header');
            $this->load->view('workplan', $data);
            $this->load->view('option/footer');
        }
    }

    public function workplan_report()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $this->load->view('option/header');
            $this->load->view('workplan_report');
            $this->load->view('option/footer');
        }
    }

    public function workplan_report_pdf()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['wpy'] = $this->input->get('wpy');
            $this->load->view('workplan_report_pdf', $data);
        }
    }

    function workplan_add()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $strategic_plan             = $this->input->post('strategic_plan');
            $strategic_plan_id          = $this->input->post('strategic_plan_id');
            // $fiscalyear                 = $this->input->post('fiscalyear');
            // $director_of_time           = $this->input->post('director_of_time');
            // $director_of_date           = $this->input->post('director_of_date');
            // $ref_of_time                = $this->input->post('ref_of_time');
            // $ref_of_date                = $this->input->post('ref_of_date');

            $data = array(
                'Strategic_name'        => $strategic_plan,
                'Strategic_Plan_id'     => $strategic_plan_id
                // 'Fiscalyear'            => $fiscalyear,
                // 'Director_of_time'      => $director_of_time,
                // 'Director_of_date'      => $director_of_date,
                // 'Ref_of_time'           => $ref_of_time,
                // 'Ref_of_date'           => $ref_of_date,
                // 'Status'                => 1
            );

            if ($this->db->insert('strategic', $data)) {
                echo "<script>";
                echo "alert('แผนยุทธ์ศาสตร์ได้ทำการบันทึกเรียบร้อยแล้ว.');";
                echo "window.location='workplan';";
                echo "</script>";
            } else {
                echo "<script>";
                echo "alert('เกิดข้อผิดพลาดในการบันทึกแผนยุทธ์ศาสตร์ กรุณาลองใหม่อีกครั้ง !!');";
                echo "window.location='workplan';";
                echo "</script>";
            }
        }
    }

    public function strategic_edit()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $strategic_id = $this->input->post('Strategic_id');
            $strategic_name = $this->input->post('Strategic_name');

            $data = array(
                'Strategic_name'    => $strategic_name,
            );
            $this->db->where('Strategic_id', $strategic_id);
            $result = $this->db->update('strategic', $data);
            if ($result) {
                $this->session->set_flashdata('save_ss2', 'Successfully ');
            } else {
                $this->session->set_flashdata('del_ss2', 'Not Successfully ');
            }
            redirect('workplan');
        }
    }

    public function budget()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['budget'] = $this->db->order_by('Fiscalyear', 'DESC')->get('strategic_plane')->result_array();

            $this->load->view('option/header');
            $this->load->view('budget', $data);
            $this->load->view('option/footer');
        }
    }

    public function budget_add_com()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data = array(
                'Strategic_Plan'         => $this->input->post('Strategic_Plan'),
                'Status'                 => 1,
            );
            $resultsedit = $this->db->insert('strategic_plane', $data);
            if ($resultsedit > 0) {
                $this->session->set_flashdata('save_ss2', 'Successfully ');
            } else {
                $this->session->set_flashdata('del_ss2', 'Not Successfully ');
            }
            return redirect('plane_list');
        }
    }

    public function budget_edit_com()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $Strategic_Plan_id = $this->input->post('Strategic_Plan_id');
            $data = array(
                'Strategic_Plan'      => $this->input->post('Strategic_Plan'),
                'Director_of_time'    => $this->input->post('Director_of_time'),
                'Director_of_date'    => $this->input->post('Director_of_date'),
                'Ref_of_time'         => $this->input->post('Ref_of_time'),
                'Ref_of_date'         => $this->input->post('Ref_of_date')
            );
            $this->db->where('Strategic_Plan_id', $Strategic_Plan_id);
            $resultsedit = $this->db->update('strategic_plane', $data);

            if ($resultsedit > 0) {
                $this->session->set_flashdata('save_ss2', 'Successfully update ');
            } else {
                $this->session->set_flashdata('del_ss2', 'Not Successfully update ');
            }
            return redirect('plane_list');
        }
    }

    public function status()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $id = $this->input->get('id');
            $c_id = $this->input->get('c_id');

            $this->db->where('Strategic_Plan_id', $id);
            $resultsedit = $this->db->update('strategic_plane', ['Status' => $c_id]);

            if ($resultsedit > 0) {
                $this->session->set_flashdata('save_ss2', ' Successfully updated status information !!.');
            } else {
                $this->session->set_flashdata('del_ss2', 'Not Successfully updated status information');
            }
            return redirect('plane_list');
        }
    }

    public function delete()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $id = $this->input->get('id');
            $strategic_plane = $this->db->get_where('project_strategic_plane', ['Strategic_Plan_id' => $id])->row_array();
            if ($strategic_plane) {
                $this->session->set_flashdata('del_ss2', 'มีโครงการกำลังใช้งานอยู่แผนยุทธ์ศาสตร์นี้อยู่ !!');
                return redirect('plane_list');
            } else {
                $this->db->where('Strategic_Plan_id', $id);
                $resultsedit = $this->db->delete('strategic_plane', ['Strategic_Plan_id' => $id]);

                if ($resultsedit > 0) {
                    $this->session->set_flashdata('save_ss2', ' Successfully delete  information !!.');
                } else {
                    $this->session->set_flashdata('del_ss2', 'Not Successfully delete  information');
                }
                return redirect('plane_list');
            }
        }
    }

    public function search_workplan_list()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $valueStrategic_plan = $this->input->get('valueStrategic_plan');
            $strategic = $this->db->get_where('strategic', ['Strategic_Plan_id' => $valueStrategic_plan])->result();
            $strategic_plan = $this->db->get_where('strategic_plane', ['Strategic_Plan_id' => $valueStrategic_plan])->row();

            $result = [];
            $result['successfully'] = true;
            $result['strategic_plan'] = $strategic_plan->Strategic_Plan;
            $result['strategicList'] = $strategic;
            echo json_encode($result);
        }
    }

    public function plane_list()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['budget'] = $this->db->order_by('Fiscalyear', 'DESC')->get('strategic_plane')->result_array();
            $this->load->view('option/header');
            $this->load->view('plane_list', $data);
            $this->load->view('option/footer');
        }
    }

    public function set_year()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $Fiscalyear = $this->input->post('Fiscalyear');

            $user_data = array(
                'budget_year' => $Fiscalyear,
                'update_at'   => date('Y-m-d H:i:s'),
            );
            $this->db->where('id', 1);
            $this->db->update('budget_year', $user_data);

            $this->session->set_flashdata('save_ss2', 'Successfully ');
            return redirect('budget');
        }
    }
}
