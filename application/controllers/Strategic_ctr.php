<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("Asia/Bangkok");
class Strategic_ctr extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['workplan_list'] = $this->db->get('strategic_plane')->result();

            $this->load->view('option/header');
            $this->load->view('workplan_list', $data);
            $this->load->view('option/footer');
        }
    }

    public function strategic_add()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['stp'] = base64_decode($this->input->get('stp'));
            $data['id'] = base64_decode($this->input->get('id'));
            $this->load->view('option/header');
            $this->load->view('strategic_add', $data);
            $this->load->view('option/footer');
        }
    }

    function strategic_delete()
    {
        $id = $this->input->get('id');

        $this->db->where('Strategic_id', $id);
        if ($this->db->delete('strategic')) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function indicproject_add()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['id'] = base64_decode($this->input->get('id'));
            $this->load->view('option/header');
            $this->load->view('indicproject_add', $data);
            $this->load->view('option/footer');
        }
    }

    // function workplan_add()
    // {
    //     $strategic_plan             = $this->input->post('strategic_plan');
    //     // $fiscalyear                 = $this->input->post('fiscalyear');
    //     // $director_of_time           = $this->input->post('director_of_time');
    //     // $director_of_date           = $this->input->post('director_of_date');
    //     // $ref_of_time                = $this->input->post('ref_of_time');
    //     // $ref_of_date                = $this->input->post('ref_of_date');

    //     $data = array(
    //         'Strategic_Plan'        => $strategic_plan,
    //         // 'Fiscalyear'            => $fiscalyear,
    //         // 'Director_of_time'      => $director_of_time,
    //         // 'Director_of_date'      => $director_of_date,
    //         // 'Ref_of_time'           => $ref_of_time,
    //         // 'Ref_of_date'           => $ref_of_date,
    //         // 'Status'                => 1
    //     );

    //     if ($this->db->insert('strategic_plane', $data)) {
    //         echo "<script>";
    //         echo "alert('แผนยุทธ์ศาสตร์ได้ทำการบันทึกเรียบร้อยแล้ว.');";
    //         echo "window.location='workplan';";
    //         echo "</script>";
    //     } else {
    //         echo "<script>";
    //         echo "alert('เกิดข้อผิดพลาดในการบันทึกแผนยุทธ์ศาสตร์ กรุณาลองใหม่อีกครั้ง !!');";
    //         echo "window.location='workplan';";
    //         echo "</script>";
    //     }
    // }

    public function goal_success()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $id = $this->input->post('id');
            $goal_name = $this->input->post('goal_name');
            $success = 0;
            foreach ($goal_name as $goalName) {
                $data = [
                    'Strategic_id' => $id,
                    'Goal_name' => $goalName
                ];
                $this->db->insert('goal', $data);
                $success += 1;
            }

            if ($success > 0) {
                echo "<script>";
                echo "alert('เป้าประสงค์ได้ทำการบันทึกเรียบร้อยแล้ว.');";
                echo "window.location='workplan';";
                echo "</script>";
            } else {
                echo "<script>";
                echo "alert('เกิดข้อผิดพลาดในการบันทึกเป้าประสงค์ กรุณาลองใหม่อีกครั้ง !!');";
                echo "window.location='workplan';";
                echo "</script>";
            }
        }
    }

    public function indicproject_success()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $id = $this->input->post('id');
            $id = base64_encode($id);
            $goal_id = $this->input->post('goal_id');
            $indic_name = $this->input->post('indic_name');
            $unit = $this->input->post('unit');
            $cost = $this->input->post('cost');

            foreach ($indic_name as $key => $indicName) {
                $data = [
                    'Indic_project' => $indicName,
                    'Unit' => $unit[$key],
                    'Cost' => $cost[$key],
                    'Goal_id' => $goal_id
                ];
                $success = $this->db->insert('indic_project', $data);
            }

            if ($success > 0) {
                echo "<script>";
                echo "alert('ตัวชี้วัดได้ทำการบันทึกเรียบร้อยแล้ว.');";
                echo "window.location='indicproject_add?id=$id';";
                echo "</script>";
            } else {
                echo "<script>";
                echo "alert('เกิดข้อผิดพลาดในการบันทึกตัวชี้วัด กรุณาลองใหม่อีกครั้ง !!');";
                echo "window.location='indicproject_add?id=$id';";
                echo "</script>";
            }
        }
    }

    public function strategic_add_process()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $dataList = $this->input->post('dataList');
            $Strategic_id = $this->input->post('Strategic_id');
            // เป้าประสงค์
            foreach ($dataList as $dataList) {
                $insert_goal['Strategic_id'] = $Strategic_id;
                $insert_goal['Goal_name'] = $dataList['goal_name'];
                $this->db->insert('goal', $insert_goal);
                $goal_id = $this->db->insert_id();
                // ตัวชี้วัด
                foreach ($dataList['indic_list'] as $indic_list) {
                    $insert_indic['Indic_project'] = $indic_list['indic_project'];
                    $insert_indic['Unit'] = $indic_list['unit'];
                    $insert_indic['Cost'] = $indic_list['cost'];
                    $insert_indic['Goal_id'] = $goal_id;
                    // $insert_indic['Tactic_name'] = $indic_list['tactic'];
                    $this->db->insert('indic_project', $insert_indic);
                }
                // กลยุทธ์
                foreach ($dataList['tactic_list'] as $tactic_list) {
                    $insert_tactic['Goal_id']       = $goal_id;
                    $insert_tactic['Tactic_name']   = $tactic_list;
                    $this->db->insert('tactic', $insert_tactic);
                }
            }

            $result = [];
            $result['successfully'] = true;
            $result['message'] = "add success";
            $result['dataList'] = $dataList;

            echo json_encode($result);
        }
    }

    public function strategic_edit()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['stp'] = base64_decode($this->input->get('stp'));
            $goalList = $this->db->get_where('goal', ['Strategic_id' => $this->input->get('Strategic_id')])->result_array();
            $arr = [];
            foreach ($goalList as $key => $goalDetail) {
                $arr[$key]['goal_id'] = $goalDetail['Goal_id'];
                $arr[$key]['goal'] = $goalDetail['Goal_name'];
                $indic_arr = [];
                $indic_projectList = $this->db->get_where('indic_project', ['Goal_id' => $goalDetail['Goal_id']])->result_array();
                foreach ($indic_projectList as $key_indic => $indic_projectDetail) {
                    $indic_arr[$key_indic]['Indic_project_id'] = $indic_projectDetail['Indic_project_id'];
                    $indic_arr[$key_indic]['indic_project'] = $indic_projectDetail['Indic_project'];
                    $indic_arr[$key_indic]['unit'] = $indic_projectDetail['Unit'];
                    $indic_arr[$key_indic]['cost'] = $indic_projectDetail['Cost'];
                    $indic_arr[$key_indic]['tactic'] = $indic_projectDetail['Tactic_name'];
                }
                $arr[$key]['indic_arr'] = $indic_arr;
            }

            $data['strategic'] = $arr;

            // echo "<pre>";
            // echo json_encode($arr);
            // echo "</pre>";
            $this->load->view('option/header');
            $this->load->view('strategic_edit', $data);
            $this->load->view('option/footer');
        }
    }

    public function strategic_edit_process()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $edit_data = $this->input->post();

            $dataList = $edit_data['dataList'];
            $remove_goal = [];
            if (isset($edit_data['remove_goal'])) {
                $remove_goal = $edit_data['remove_goal'];
            }
            $remove_indic = [];
            if (isset($edit_data['remove_indic'])) {
                $remove_indic = $edit_data['remove_indic'];
            }
            $Strategic_id = $edit_data['Strategic_id'];
            $goal_status = [];

            foreach ($dataList as $dataList) {
                $insert_goal['Strategic_id'] = $Strategic_id;
                $insert_goal['Goal_name'] = $dataList['goal_name'];
                if (!empty($dataList['goal_id'])) {
                    array_push($goal_status, "update");
                    $this->db->where('Goal_id', intval($dataList['goal_id']));
                    $this->db->update('goal', $insert_goal);

                    foreach ($dataList['indic_list'] as $indic_list) {
                        $insert_indic['Indic_project'] = $indic_list['indic_project'];
                        $insert_indic['Unit'] = $indic_list['unit'];
                        $insert_indic['Cost'] = $indic_list['cost'];
                        $insert_indic['Goal_id'] = $dataList['goal_id'];

                        if (!empty($indic_list['indic_project_id'])) {
                            $indic_id = intval($indic_list['indic_project_id']);
                            $this->db->where('Indic_project_id', $indic_id);
                            $this->db->update('indic_project', $insert_indic);
                        } else {
                            $this->db->insert('indic_project', $insert_indic);
                        }
                    }
                    $this->db->where('Goal_id', intval($dataList['goal_id']));
                    $this->db->delete('tactic');
                    // กลยุทธ์
                    foreach ($dataList['tactic_list'] as $tactic_list) {
                        $insert_tactic['Goal_id']       = intval($dataList['goal_id']);
                        $insert_tactic['Tactic_name']   = $tactic_list;
                        $this->db->insert('tactic', $insert_tactic);
                    }
                } else {
                    array_push($goal_status, "insert");
                    $this->db->insert('goal', $insert_goal);
                    $goal_id = $this->db->insert_id();

                    foreach ($dataList['indic_list'] as $indic_list) {
                        $insert_indic['Indic_project'] = $indic_list['indic_project'];
                        $insert_indic['Unit'] = $indic_list['unit'];
                        $insert_indic['Cost'] = $indic_list['cost'];
                        $insert_indic['Goal_id'] = $goal_id;
                        // $insert_indic['Tactic_name'] = $indic_list['tactic'];
                        $this->db->insert('indic_project', $insert_indic);
                    }
                }
            }

            // ลบ Indic
            foreach ($remove_indic as $removeIndic) {
                $this->db->where('Indic_project_id', intval($removeIndic));
                $this->db->delete('indic_project');
            }
            // ลบ Goal

            foreach ($remove_goal as $removeGoal) {
                $this->db->where('Goal_id', intval($removeGoal));
                $this->db->delete('goal');
            }




            $result = [];
            $result['successfully'] = true;
            $result['status'] = $remove_indic;
            $result['message'] = "edit success";
            $result['dataList'] = $edit_data;
            echo json_encode($result);
        }
    }
}
