<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home_ctr extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $this->load->view('option/header');
            $this->load->view('home');
            $this->load->view('option/footer');
        }
    }
}
