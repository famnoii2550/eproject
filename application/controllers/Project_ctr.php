<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("Asia/Bangkok");
class Project_ctr extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Dynamic_dependent_model');
        $this->load->model('Search_model');
    }

    // ผู้บริหาร
    public function project_director()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_list_director', $data);
            $this->load->view('option/footer');
        }
    }

    // นักวิชาการพัสดุ
    public function project_supplies()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $year           = $this->input->get('year');
            $Status         = $this->input->get('Status');

            $data['projects']       = $this->Search_model->search_All_supplies($year, $Status);
            $y_budget = $this->db->get('budget_year')->row_array();
            $data['y_budget'] = $y_budget['budget_year'];
            if ($year == 'all_year') {
                $data['y_budget'] = 'all_year';
            }

            $this->load->view('option/header');
            $this->load->view('project_supplies', $data);
            $this->load->view('option/footer');
        }
    }

    public function purchase_supplies()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID = $this->input->post('project_id');
            $purchase = $this->input->post('purchase');

            $data = array(
                'purchase'  => $purchase,
            );
            $this->db->where('Project_id', $PID);
            if ($this->db->update('project', $data)) {
                echo "<script>";
                echo "alert('ทำรายการเรียบร้อยแล้ว');";
                echo "window.location='project_supplies'";
                echo "</script>";
            } else {
                echo "<script>";
                echo "alert('ไม่สามารถทำรายการได้ กรุณาลองใหม่อีกครั้ง!!');";
                echo "window.location='project_supplies'";
                echo "</script>";
            }
        }
    }

    public function project_doc_director()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_doc_director', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_consider_director()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_consider_director', $data);
            $this->load->view('option/footer');
        }
    }

    // เจ้าหน้าที่แผน
    public function project_manager()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $year           = $this->input->get('year');
            $Department     = $this->input->get('Department_id');
            $Status         = $this->input->get('Status');

            $data['projects']       = $this->Search_model->search_All_manager($year, $Department, $Status);
            $y_budget = $this->db->get('budget_year')->row_array();
            $data['y_budget'] = $y_budget['budget_year'];
            if ($year == 'all_year') {
                $data['y_budget'] = 'all_year';
            }

            $this->load->view('option/header');
            $this->load->view('project_manager', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_list_manager()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_list_manager', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_doc_manager()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_doc_manager', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_consider_manager()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_consider_manager', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_supervisor()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $year           = $this->input->get('year');
            $Department     = $this->input->get('Department_id');
            $Status         = $this->input->get('Status');
            $chk_user       = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            // if ($Department == '' && $Status == '') {
            //     $data['projects']       = $this->Search_model->project_list_supervisor($year, $chk_user['Department_id']);
            // } else {
            $data['projects']       = $this->Search_model->search_All_supervisor($year, $Department, $Status);
            //}

            $this->load->view('option/header');
            $this->load->view('project_supervisor', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_list_supervisor()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_list_supervisor', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_consider_supervisor()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_consider_supervisor', $data);
            $this->load->view('option/footer');
        }
    }

    public function index()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $year           = $this->input->get('year');
            $Department     = $this->input->get('Department_id');
            $Status         = $this->input->get('Status');

            // if ($year == '' && $Department == '' && $Status == '') {
            //     $data['projects']       = $this->Search_model->project_list($year);
            // } else {
            $data['projects']       = $this->Search_model->search_All($year, $Department, $Status);
            //}

            $this->load->view('option/header');
            $this->load->view('project', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_only()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $this->load->view('option/header');
            $this->load->view('project_only');
            $this->load->view('option/footer');
        }
    }

    public function project_list()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_list', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_NotApp_director()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $year                   = $this->input->get('year');
            $acc                    = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            $account                = $acc['Account_id'];
            if ($year == '') {
                $data['projects']       = $this->Search_model->project_notApp($account);
            } else {
                $data['projects']       = $this->Search_model->project_notApp_search($year, $account);
            }
            $this->load->view('option/header');
            $this->load->view('project_notApp_director', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_NotAppAll_manager()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $year                   = $this->input->get('year');
            $acc                    = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            $account                = $acc['Account_id'];
            if ($year == '') {
                $data['projects']       = $this->Search_model->project_notApp($account);
            } else {
                $data['projects']       = $this->Search_model->project_notApp_search($year, $account);
            }
            $this->load->view('option/header');
            $this->load->view('project_notAppAll_manager', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_NotApp()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $year                   = $this->input->get('year');
            $acc                    = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            $account                = $acc['Account_id'];
            if ($year == '') {
                $data['projects']       = $this->Search_model->project_notApp($account);
            } else {
                $data['projects']       = $this->Search_model->project_notApp_search($year, $account);
            }
            $this->load->view('option/header');
            $this->load->view('project_notApp', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_notAppAll()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $Department             = $this->input->get('department');
            $year                   = $this->input->get('year');
            // echo ($Department);
            // exit();
            if ($Department == '') {
                $data['projects']       = $this->Search_model->project_notAppAll($year);
            } else {
                $data['projects']       = $this->Search_model->project_notAppAll_search($Department, $year);
            }

            $this->load->view('option/header');
            $this->load->view('project_notAppAll', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $this->load->view('option/header');
            $this->load->view('project_app');
            $this->load->view('option/footer');
        }
    }

    public function edit_project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $project_id = base64_decode($this->input->get('id'));

            $data = [];
            $data['project']         = $this->db->get_where('project', ['Project_id' => $project_id])->row_array();

            $project_strategic_plane = $this->db->get_where('project_strategic_plane', ['Project_id' => $project_id])->result_array();

            $strategic_planeList     = $this->db->get('strategic_plane')->result_array();

            $objective_list          = $this->db->get_where('objective', ['Project_id' => $project_id])->result_array();

            $work_step_list          = $this->db->get_where('work_step', ['Project_id' => $project_id])->result_array();

            $charges_main_list       = $this->db->get_where('charges_main', ['Project_id' => $project_id])->result_array();

            $charges_main_list_sub   = $this->db->get_where('charges_main', ['Project_id' => $project_id])->result_array();

            $benefit_list            = $this->db->get_where('benefit', ['Project_id' => $project_id])->result_array();


            $data['project_strategic_plane']    = $project_strategic_plane;
            $data['strategic_planeList']        = $strategic_planeList;
            $data['objective_list']             = $objective_list;
            $data['work_step_list']             = $work_step_list;
            $data['charges_main_list']          = $charges_main_list;
            $data['charges_main_list_sub']      = $charges_main_list_sub;
            $data['benefit_list']               = $benefit_list;

            $this->load->view('option/header');
            $this->load->view('edit_project_app', $data, $project_id);
            $this->load->view('option/footer');
        }
    }

    public function project_doc()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_doc', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_doc_tor()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID    = $this->input->post('Project_id');
            $uri    = $this->input->post('uri');
            // Set preference
            $config['upload_path']     = 'uploads/tor/';
            $config['allowed_types']   = '*';
            $config['max_size']        = '99999'; // max_size in kb
            $config['file_name']       = time() . $_FILES['file_name']['name'];

            //Load upload library
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload('file_name');


            // Get data about the file
            $uploadData = $this->upload->data();
            $data = array(
                'Project_id'        => $PID,
                'File_name'         => $uploadData['file_name'],
                'Full_path'         => 'uploads/tor/' . $uploadData['file_name'],
                'Check_type_tor'    => '1',
            );

            if ($this->db->insert('file', $data)) {
                $this->session->set_flashdata('save_ss2', 'เอกสาร TOR ได้บันทึกเรียบร้อยแล้ว.');
                redirect('project_doc?PID=' . base64_encode($PID) . '&uri=' . $uri);
            } else {
                $this->session->set_flashdata('del_ss2', 'เกิดข้อผิดพลาดในการเพิ่มเอกสาร TOR กรุณาลองใหม่อีกครั้ง 1 !!.');
                redirect('project_doc?PID=' . base64_encode($PID) . '&uri=' . $uri);
            }
        }
    }

    public function project_doc_doc()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID    = $this->input->post('Project_id');
            $uri    = $this->input->post('uri');
            // Set preference
            $config['upload_path']     = 'uploads/doc/';
            $config['allowed_types']   = '*';
            $config['max_size']        = '99999'; // max_size in kb
            $config['file_name']       = time() . $_FILES['file_name']['name'];

            //Load upload library
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload('file_name');


            // Get data about the file
            $uploadData = $this->upload->data();
            $data = array(
                'Project_id'        => $PID,
                'File_name'         => $uploadData['file_name'],
                'Full_path'         => 'uploads/doc/' . $uploadData['file_name'],
                'Check_type_tor'    => '2',
            );

            if ($this->db->insert('file', $data)) {
                $this->session->set_flashdata('save_ss2', 'เอกสาร TOR ได้บันทึกเรียบร้อยแล้ว.');
                redirect('project_doc?PID=' . base64_encode($PID) . '&uri=' . $uri);
            } else {
                $this->session->set_flashdata('del_ss2', 'เกิดข้อผิดพลาดในการเพิ่มเอกสาร TOR กรุณาลองใหม่อีกครั้ง 1 !!.');
                redirect('project_doc?PID=' . base64_encode($PID) . '&uri=' . $uri);
            }
        }
    }

    public function project_estimate_doc()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID    = $this->input->post('Project_id');
            $uri    = $this->input->post('uri');
            // Set preference
            $config['upload_path']     = 'uploads/doc/';
            $config['allowed_types']   = '*';
            $config['max_size']        = '99999'; // max_size in kb
            $config['file_name']       = time() . $_FILES['file_name']['name'];

            //Load upload library
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload('file_name');


            // Get data about the file
            $uploadData = $this->upload->data();
            $data = array(
                'Project_id'        => $PID,
                'File_name'         => $uploadData['file_name'],
                'Full_path'         => 'uploads/doc/' . $uploadData['file_name'],
                'Check_type_tor'    => '2',
            );

            if ($this->db->insert('file', $data)) {
                $this->session->set_flashdata('save_ss2', 'เอกสาร TOR ได้บันทึกเรียบร้อยแล้ว.');
                redirect($uri);
            } else {
                $this->session->set_flashdata('del_ss2', 'เกิดข้อผิดพลาดในการเพิ่มเอกสาร TOR กรุณาลองใหม่อีกครั้ง !!.');
                redirect($uri);
            }
        }
    }

    public function project_consider()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_consider', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_review()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_review', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_review_supervisor()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_review_supervisor', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_review_manager()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_review_manager', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_review_director()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_review_director', $data);
            $this->load->view('option/footer');
        }
    }

    function fetch_state()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            if ($this->input->post('PROVINCE_ID')) {
                echo $this->Dynamic_dependent_model->fetch_state($this->input->post('PROVINCE_ID'));
            }
        }
    }

    function fetch_city()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            if ($this->input->post('AMPHUR_ID')) {
                echo $this->Dynamic_dependent_model->fetch_city($this->input->post('AMPHUR_ID'));
            }
        }
    }

    public function search_strategic_planeList()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $strategic_plane = $this->input->get('valueStrategic_plane');
            $strategics = $this->db->get_where('strategic', ['Strategic_Plan_id' => $strategic_plane])->result_array();
            $strategicList = [];
            foreach ($strategics as $key => $strategic) {
                $strategicList[$key]['Strategic_id'] = $strategic['Strategic_id'];
                $strategicList[$key]['Strategic_name'] = $strategic['Strategic_name'];
            }

            $result = [];
            $result['successfully'] = true;
            $result['strategicList'] = $strategicList;
            echo json_encode($result);
        }
    }

    public function search_strategicList()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $strategic = $this->input->get('valueStrategic');
            $goals = $this->db->get_where('goal', ['Strategic_id' => $strategic])->result_array();
            $goalList = [];
            foreach ($goals as $key => $goal) {
                $goalList[$key]['Goal_id'] = $goal['Goal_id'];
                $goalList[$key]['Goal_name'] = $goal['Goal_name'];
            }

            $result = [];
            $result['successfully'] = true;
            $result['goalList'] = $goalList;
            echo json_encode($result);
        }
    }

    public function search_tacticList()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $goal = $this->input->get('valueGoal');
            $indic_projects = $this->db->get_where('indic_project', ['Goal_id' => $goal])->result_array();
            $indic_projectList = [];
            foreach ($indic_projects as $key => $indic_project) {
                $indic_projectList[$key]['Indic_project_id'] = $indic_project['Indic_project_id'];
                $indic_projectList[$key]['Tactic_name'] = $indic_project['Tactic_name'];
            }

            $result = [];
            $result['successfully'] = true;
            $result['indic_projectList'] = $indic_projectList;
            echo json_encode($result);
        }
    }

    public function success_project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data = $this->input->post();
            $account_id = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            $insert = [];
            foreach ($data['data'] as $dataDetail) {
                $insert['Year'] = $dataDetail['year'];
                $insert['Project_name'] = $dataDetail['project_name'];
                $insert['Type'] = $dataDetail['type'];
                $insert['Integra_name'] = $dataDetail['integra_name'];
                if ($dataDetail['integra_name'] == "อื่นๆ") {
                    $insert['Integra_name'] = $dataDetail['integra_name_text'];
                }
                $insert['Integra_detail'] = $dataDetail['integra_detail'];

                $insert['Rationale'] = $dataDetail['rationale'];
                $insert['Target_group'] = $dataDetail['target_group'];
                $insert['Source'] = $dataDetail['charges_main'];
                if ($dataDetail['charges_main'] == "งบอื่นๆ") {
                    $insert['Source'] = $dataDetail['charges_main_text'];
                }
                $insert['Butget'] = $dataDetail['butget'];
                $insert['Butget_char'] = $dataDetail['butget_text'];
                if ($dataDetail['butget_text'] == "บาทถ้วน") {
                    $insert['Butget_char'] = "";
                }
                $insert['Workplan_id'] = $dataDetail['work_plan'];
                if ($dataDetail['tor'] == "มี") {
                    $insert['Tor'] = 1;
                } else {
                    $insert['Tor'] = 0;
                }
                $insert['Account_id'] = $account_id['Account_id'];
                $insert['Department_id'] = $dataDetail['department_id'];

                $project = $this->db->insert('project', $insert);
                $project_id = $this->db->insert_id();
            }
            // ผู้รับผิดชอบโครงการ
            foreach ($data['data'][0]['accountList'] as $account) {
                $data_user = [
                    'Account_id' => $account,
                    'Project_id' =>  $project_id
                ];
                $checkuser = $this->db->get_where('user', $data_user)->row_array();
                if (!empty($checkuser)) {
                    continue;
                }
                $this->db->insert('user', $data_user);
            }
            //ชื่อแผนยุทธ์ศาสตร์
            foreach ($data['data'][0]['strategic_planeList'] as $strategic_plane) {
                $Strategic_id = $this->db->get_where('strategic', ['Strategic_Plan_id' => $strategic_plane['strategic_plane']])->row_array();
                $data_strategic_plane = [
                    'Project_id' =>  $project_id,
                    'Strategic_Plan_id' => $strategic_plane['strategic_plane'],
                    'Strategic_id' => $Strategic_id['Strategic_id'],
                    'Goal_id' => $strategic_plane['goal'],
                    // 'Indic_project_id' => $strategic_plane['indic']
                ];
                $this->db->insert('project_strategic_plane', $data_strategic_plane);
            }
            //วัตถุประสงค์
            foreach ($data['data'][0]['objective_nameList'] as $objective_name) {
                $data_objective = [
                    'Project_id' =>  $project_id,
                    'Objective_name' => $objective_name
                ];
                $this->db->insert('objective', $data_objective);
            }
            // กลยุทธ์
            foreach ($data['data'][0]['titles'] as $titles) {
                $data_tactic = [
                    'Tactic_id'     => $titles,
                    'Project_id'    => $project_id,
                ];
                $this->db->insert('strategics_project', $data_tactic);
            }
            //ตัวชี้วัดความสำเร็จระดับโครงการ
            $this->db->where('Project_id', $project_id);
            $this->db->delete('project_indic_success');
            foreach ($data['data'][0]['dataAll_indic_unit_cost'] as $dataIndic_unit_cost) {
                $data_indic_unit_cost = [
                    'Project_id' => $project_id,
                    'Indic_success' => $dataIndic_unit_cost['indic_project'],
                    'Unit' => $dataIndic_unit_cost['unit'],
                    'Cost' => $dataIndic_unit_cost['cost'],
                ];

                $this->db->insert('project_indic_success', $data_indic_unit_cost);
            }
            //ขั้นตอนการดำเนินการ
            foreach ($data['data'][0]['stepList'] as $data_stepList) {
                $data_step = [
                    'Step_name' => $data_stepList['step'],
                    'Start' => $data_stepList['start'],
                    'Stop' => $data_stepList['stop'],
                    'Project_id' => $project_id,
                ];
                $this->db->insert('work_step', $data_step);
            }
            //ประเภทการใช้จ่าย
            if ($data['data'][0]['charges_main'] != "ไม่ได้ใช้งบประมาณ") {

                foreach ($data['data'][0]['charges_sub_list'] as $charges) {
                    $data_charges = [
                        'Charges_Main' => $insert['Source'],
                        'Project_id' => $project_id,
                    ];
                    $this->db->insert('charges_main', $data_charges);
                    $charges_id = $this->db->insert_id();
                    foreach ($charges['income_butgetList'] as $income_butget) {
                        $data_charges_sub = [
                            'Charges_Sub'       => $income_butget['income_butget'],
                            'Quarter_one'       => $income_butget['quarter_one'],
                            'Quarter_two'       => $income_butget['quarter_two'],
                            'Quarter_three'     => $income_butget['quarter_three'],
                            'Quarter_four'      => $income_butget['quarter_four'],
                            'Charges_Main_id'   => $charges_id,
                        ];
                        $this->db->insert('charges_sub', $data_charges_sub);
                    }
                }
            }

            //ประโยชน์ที่คาดว่าจะได้รับ
            foreach ($data['data'][0]['benefit_nameList'] as $benefit_name) {
                $data_benefit_name = [
                    'Project_id' => $project_id,
                    'Benefit_name' => $benefit_name
                ];
                $this->db->insert('benefit', $data_benefit_name);
            }

            $result = [];
            $result['successfully'] = true;
            $result['dataList'] = $data;

            echo json_encode($result);
        }
    }

    public function success_project_app_copy()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data = $this->input->post();
            $account_id = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            $insert = [];
            foreach ($data['data'] as $dataDetail) {
                $insert['Year'] = $dataDetail['year'];
                $insert['Project_name'] = $dataDetail['project_name'];
                $insert['Type'] = $dataDetail['type'];
                $insert['Integra_name'] = $dataDetail['integra_name'];
                if ($dataDetail['integra_name'] == "อื่นๆ") {
                    $insert['Integra_name'] = $dataDetail['integra_name_text'];
                }
                $insert['Integra_detail'] = $dataDetail['integra_detail'];

                $insert['Rationale'] = $dataDetail['rationale'];
                $insert['Target_group'] = $dataDetail['target_group'];
                $insert['Source'] = $dataDetail['charges_main'];
                if ($dataDetail['charges_main'] == "งบอื่นๆ") {
                    $insert['Source'] = $dataDetail['charges_main_text'];
                }
                $insert['Butget'] = $dataDetail['butget'];
                $insert['Butget_char'] = $dataDetail['butget_text'];
                if ($dataDetail['butget_text'] == "บาทถ้วน") {
                    $insert['Butget_char'] = "";
                }
                $insert['Workplan_id'] = $dataDetail['work_plan'];
                if ($dataDetail['tor'] == "มี") {
                    $insert['Tor'] = 1;
                } else {
                    $insert['Tor'] = 0;
                }
                $insert['Account_id'] = $account_id['Account_id'];
                $insert['Department_id'] = $dataDetail['department_id'];
                $insert['Status'] = 9;

                $project = $this->db->insert('project', $insert);
                $project_id = $this->db->insert_id();
            }
            // ผู้รับผิดชอบโครงการ
            foreach ($data['data'][0]['accountList'] as $account) {
                $data_user = [
                    'Account_id' => $account,
                    'Project_id' =>  $project_id
                ];
                $checkuser = $this->db->get_where('user', $data_user)->row_array();
                if (!empty($checkuser)) {
                    continue;
                }
                $this->db->insert('user', $data_user);
            }
            //ชื่อแผนยุทธ์ศาสตร์
            foreach ($data['data'][0]['strategic_planeList'] as $strategic_plane) {
                $Strategic_id = $this->db->get_where('strategic', ['Strategic_Plan_id' => $strategic_plane['strategic_plane']])->row_array();
                $data_strategic_plane = [
                    'Project_id' =>  $project_id,
                    'Strategic_Plan_id' => $strategic_plane['strategic_plane'],
                    'Strategic_id' => $Strategic_id['Strategic_id'],
                    'Goal_id' => $strategic_plane['goal'],
                    // 'Indic_project_id' => $strategic_plane['indic']
                ];
                $this->db->insert('project_strategic_plane', $data_strategic_plane);
            }
            //วัตถุประสงค์
            foreach ($data['data'][0]['objective_nameList'] as $objective_name) {
                $data_objective = [
                    'Project_id' =>  $project_id,
                    'Objective_name' => $objective_name
                ];
                $this->db->insert('objective', $data_objective);
            }
            // กลยุทธ์
            foreach ($data['data'][0]['titles'] as $titles) {
                $data_tactic = [
                    'Tactic_id'     => $titles,
                    'Project_id'    => $project_id,
                ];
                $this->db->insert('strategics_project', $data_tactic);
            }
            //ตัวชี้วัดความสำเร็จระดับโครงการ
            $this->db->where('Project_id', $project_id);
            $this->db->delete('project_indic_success');
            foreach ($data['data'][0]['dataAll_indic_unit_cost'] as $dataIndic_unit_cost) {
                $data_indic_unit_cost = [
                    'Project_id' => $project_id,
                    'Indic_success' => $dataIndic_unit_cost['indic_project'],
                    'Unit' => $dataIndic_unit_cost['unit'],
                    'Cost' => $dataIndic_unit_cost['cost'],
                ];

                $this->db->insert('project_indic_success', $data_indic_unit_cost);
            }
            //ขั้นตอนการดำเนินการ
            foreach ($data['data'][0]['stepList'] as $data_stepList) {
                $data_step = [
                    'Step_name' => $data_stepList['step'],
                    'Start' => $data_stepList['start'],
                    'Stop' => $data_stepList['stop'],
                    'Project_id' => $project_id,
                ];
                $this->db->insert('work_step', $data_step);
            }
            //ประเภทการใช้จ่าย
            if ($data['data'][0]['charges_main'] != "ไม่ได้ใช้งบประมาณ") {

                foreach ($data['data'][0]['charges_sub_list'] as $charges) {
                    $data_charges = [
                        'Charges_Main' => $insert['Source'],
                        'Project_id' => $project_id,
                    ];
                    $this->db->insert('charges_main', $data_charges);
                    $charges_id = $this->db->insert_id();
                    foreach ($charges['income_butgetList'] as $income_butget) {
                        $data_charges_sub = [
                            'Charges_Sub'       => $income_butget['income_butget'],
                            'Quarter_one'       => $income_butget['quarter_one'],
                            'Quarter_two'       => $income_butget['quarter_two'],
                            'Quarter_three'     => $income_butget['quarter_three'],
                            'Quarter_four'      => $income_butget['quarter_four'],
                            'Charges_Main_id'   => $charges_id,
                        ];
                        $this->db->insert('charges_sub', $data_charges_sub);
                    }
                }
            }

            //ประโยชน์ที่คาดว่าจะได้รับ
            foreach ($data['data'][0]['benefit_nameList'] as $benefit_name) {
                $data_benefit_name = [
                    'Project_id' => $project_id,
                    'Benefit_name' => $benefit_name
                ];
                $this->db->insert('benefit', $data_benefit_name);
            }

            $result = [];
            $result['successfully'] = true;
            $result['dataList'] = $data;

            echo json_encode($result);
        }
    }

    public function project_edit_manager()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $project_id = base64_decode($this->input->get('PID'));


            $data = [];
            $data['project']         = $this->db->get_where('project', ['Project_id' => $project_id])->row_array();
            $project_strategic_plane = $this->db->get_where('project_strategic_plane', ['Project_id' => $project_id])->result_array();
            $strategic_planeList     = $this->db->get('strategic_plane')->result_array();
            $data['project_strategic_plane']    = $project_strategic_plane;
            $data['strategic_planeList']        = $strategic_planeList;
            $data['PID'] = base64_decode($this->input->get('PID'));
            $this->load->view('option/header');
            $this->load->view('project_edit_manager', $data);
            $this->load->view('option/footer');
        }
    }

    public function success_project_edit_manager()
    {
        $data = $this->input->post();
        // ชื่อแผนยุทธ์ศาสตร์
        $this->db->where('Project_id', $data['data'][0]['id_main']);
        $strategic_plane_delete = $this->db->delete('project_strategic_plane');

        if ($strategic_plane_delete) {
            foreach ($data['data'][0]['strategic_planeList'] as $strategic_plane) {
                $Strategic_id = $this->db->get_where('strategic', ['Strategic_Plan_id' => $strategic_plane['strategic_plane']])->row_array();
                if ($strategic_plane['indic'] == "") {
                    $indic = 0;
                } else {
                    $indic = $strategic_plane['indic'];
                }
                $data_strategic_plane = [
                    'Project_id'        =>  $data['data'][0]['id_main'],
                    'Strategic_Plan_id' => $strategic_plane['strategic_plane'],
                    'Strategic_id'      => $strategic_plane['strategic_id'],
                    'Goal_id'           => $strategic_plane['goal'],
                    'Indic_project_id'  => $indic,
                ];
                $this->db->insert('project_strategic_plane', $data_strategic_plane);
            }
        }
        // กลยุทธ์
        if ($data['data'][0]['titles'] == false) {
        } else {
            $this->db->where('Project_id', $data['data'][0]['id_main']);
            $tactic_plane_delete = $this->db->delete('strategics_project');
            foreach ($data['data'][0]['titles'] as $titles) {
                $data_tactic = [
                    'Tactic_id'     => $titles,
                    'Project_id'    =>  $data['data'][0]['id_main'],
                ];
                $this->db->insert('strategics_project', $data_tactic);
            }
        }

        $chk_acc = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
        $data2 = array(
            'Project_id'    =>  $data['data'][0]['id_main'],
            'Account_id'    => $chk_acc['Account_id'],
            'Comment'       => "แก้ไขโครงการ (" . '' . $chk_acc['Fname'] . ' ' . $chk_acc['Lname'] . ')',
            'Role'          => "Manager",
            'Time'          => date('Y-m-d H:i:s'),
        );
        $this->db->insert('comment', $data2);
        $result = [];
        $result['successfully'] = true;
        $result['dataList'] = 'success';

        echo json_encode($result);
    }

    public function success_project_edit()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data = $this->input->post();
            $account_id = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            $insert = [];
            foreach ($data['data'] as $dataDetail) {
                $insert['Year'] = $dataDetail['year'];
                $insert['Project_name'] = $dataDetail['project_name'];
                $insert['Type'] = $dataDetail['type'];
                $insert['Integra_name'] = $dataDetail['integra_name'];
                if ($dataDetail['integra_name'] == "อื่นๆ") {
                    $insert['Integra_name'] = $dataDetail['integra_name_text'];
                }
                $insert['Integra_detail'] = $dataDetail['integra_detail'];

                $insert['Rationale'] = $dataDetail['rationale'];
                $insert['Target_group'] = $dataDetail['target_group'];
                $insert['Source'] = $dataDetail['charges_main'];
                if ($dataDetail['charges_main'] == "งบอื่นๆ") {
                    $insert['Source'] = $dataDetail['charges_main_text'];
                }
                $insert['Butget'] = $dataDetail['butget'];
                $insert['Butget_char'] = $dataDetail['butget_text'];
                if ($dataDetail['butget_text'] == "บาทถ้วน") {
                    $insert['Butget_char'] = "";
                }
                $insert['Workplan_id'] = $dataDetail['work_plan'];
                if ($dataDetail['tor'] == "มี") {
                    $insert['Tor'] = 1;
                } else {
                    $insert['Tor'] = 0;
                }
                $insert['Account_id'] = $account_id['Account_id'];
                $insert['Department_id'] = $dataDetail['department_id'];
                $insert['Status'] = 1;

                $this->db->where('Project_id', $dataDetail['id_main']);
                $project = $this->db->update('project', $insert);
                // $project_id = $this->db->insert_id();
            }

            // ผู้รับผิดชอบโครงการ
            $this->db->where('Project_id', $data['data'][0]['id_main']);
            $user_delete = $this->db->delete('user');

            if ($user_delete) {
                foreach ($data['data'][0]['accountList'] as $account) {
                    $data_user = [
                        'Account_id' => $account,
                        'Project_id' =>  $data['data'][0]['id_main']
                    ];
                    $checkuser = $this->db->get_where('user', $data_user)->row_array();
                    if (!empty($checkuser)) {
                        continue;
                    }
                    $this->db->insert('user', $data_user);
                }
            }

            // ชื่อแผนยุทธ์ศาสตร์
            $this->db->where('Project_id', $data['data'][0]['id_main']);
            $strategic_plane_delete = $this->db->delete('project_strategic_plane');

            if ($strategic_plane_delete) {
                foreach ($data['data'][0]['strategic_planeList'] as $strategic_plane) {
                    $Strategic_id = $this->db->get_where('strategic', ['Strategic_Plan_id' => $strategic_plane['strategic_plane']])->row_array();
                    $data_strategic_plane = [
                        'Project_id'        =>  $data['data'][0]['id_main'],
                        'Strategic_Plan_id' => $strategic_plane['strategic_plane'],
                        'Strategic_id'      => $strategic_plane['strategic_id'],
                        'Goal_id'           => $strategic_plane['goal'],
                        'Indic_project_id'  => $strategic_plane['indic']
                    ];
                    $this->db->insert('project_strategic_plane', $data_strategic_plane);
                }
            }

            //วัตถุประสงค์
            $this->db->where('Project_id', $data['data'][0]['id_main']);
            $objective_delete = $this->db->delete('objective');
            if ($objective_delete) {
                foreach ($data['data'][0]['objective_nameList'] as $objective_name) {
                    $data_objective = [
                        'Project_id'        =>  $data['data'][0]['id_main'],
                        'Objective_name'    => $objective_name
                    ];
                    $this->db->insert('objective', $data_objective);
                }
            }

            //ตัวชี้วัดความสำเร็จระดับโครงการ
            $this->db->where('Project_id', $data['data'][0]['id_main']);
            $indic_success_delete = $this->db->delete('project_indic_success');
            if ($indic_success_delete) {
                foreach ($data['data'][0]['dataAll_indic_unit_cost'] as $dataIndic_unit_cost) {
                    $data_indic_unit_cost = [
                        'Project_id'    => $data['data'][0]['id_main'],
                        'Indic_success' => $dataIndic_unit_cost['indic_project'],
                        'Unit'          => $dataIndic_unit_cost['unit'],
                        'Cost'          => $dataIndic_unit_cost['cost'],
                    ];

                    $this->db->insert('project_indic_success', $data_indic_unit_cost);
                }
            }

            //ขั้นตอนการดำเนินการ
            $this->db->where('Project_id', $data['data'][0]['id_main']);
            $work_step_delete = $this->db->delete('work_step');

            if ($work_step_delete) {
                foreach ($data['data'][0]['stepList'] as $data_stepList) {
                    $data_step = [
                        'Step_name'     => $data_stepList['step'],
                        'Start'         => $data_stepList['start'],
                        'Stop'          => $data_stepList['stop'],
                        'Project_id'    => $data['data'][0]['id_main'],
                    ];
                    $this->db->insert('work_step', $data_step);
                }
            }

            //ประเภทการใช้จ่าย
            if ($data['data'][0]['charges_main'] != "ไม่ได้ใช้งบประมาณ") {

                $result_cm = $this->db->get_where('charges_main', ['Project_id' => $data['data'][0]['id_main']])->result_array();

                foreach ($result_cm as $key => $result_cm) {
                    $this->db->where('Charges_Main_id', $result_cm['Charges_Main_id']);
                    $this->db->delete('charges_sub');
                }

                $this->db->where('Project_id', $data['data'][0]['id_main']);
                $charges_main_delete = $this->db->delete('charges_main');

                if ($charges_main_delete) {
                    foreach ($data['data'][0]['charges_sub_list'] as $charges) {

                        $data_charges = [
                            'Charges_Main'  => $charges['charges_sub'],
                            'Project_id'    => $data['data'][0]['id_main'],
                        ];
                        $this->db->insert('charges_main', $data_charges);
                        $charges_id = $this->db->insert_id();

                        foreach ($charges['income_butgetList'] as $income_butget) {
                            $data_charges_sub = [
                                'Charges_Sub'       => $income_butget['income_butget'],
                                'Quarter_one'       => $income_butget['quarter_one'],
                                'Quarter_two'       => $income_butget['quarter_two'],
                                'Quarter_three'     => $income_butget['quarter_three'],
                                'Quarter_four'      => $income_butget['quarter_four'],
                                'Charges_Main_id'   => $charges_id,
                            ];
                            $this->db->insert('charges_sub', $data_charges_sub);
                        }
                    }
                }
            }

            //ประโยชน์ที่คาดว่าจะได้รับ
            $this->db->where('Project_id', $data['data'][0]['id_main']);
            $benefit_delete = $this->db->delete('benefit');
            if ($benefit_delete) {

                foreach ($data['data'][0]['benefit_nameList'] as $benefit_name) {
                    $data_benefit_name = [
                        'Project_id'    => $data['data'][0]['id_main'],
                        'Benefit_name'  => $benefit_name
                    ];
                    $this->db->insert('benefit', $data_benefit_name);
                }
            }


            $result = [];
            $result['successfully'] = true;
            $result['dataList'] = 'success';

            echo json_encode($result);
        }
    }

    public function success_project_edit_copy()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data = $this->input->post();
            $account_id = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            $insert = [];
            foreach ($data['data'] as $dataDetail) {
                $insert['Year'] = $dataDetail['year'];
                $insert['Project_name'] = $dataDetail['project_name'];
                $insert['Type'] = $dataDetail['type'];
                $insert['Integra_name'] = $dataDetail['integra_name'];
                if ($dataDetail['integra_name'] == "อื่นๆ") {
                    $insert['Integra_name'] = $dataDetail['integra_name_text'];
                }
                $insert['Integra_detail'] = $dataDetail['integra_detail'];

                $insert['Rationale'] = $dataDetail['rationale'];
                $insert['Target_group'] = $dataDetail['target_group'];
                $insert['Source'] = $dataDetail['charges_main'];
                if ($dataDetail['charges_main'] == "งบอื่นๆ") {
                    $insert['Source'] = $dataDetail['charges_main_text'];
                }
                $insert['Butget'] = $dataDetail['butget'];
                $insert['Butget_char'] = $dataDetail['butget_text'];
                if ($dataDetail['butget_text'] == "บาทถ้วน") {
                    $insert['Butget_char'] = "";
                }
                $insert['Workplan_id'] = $dataDetail['work_plan'];
                if ($dataDetail['tor'] == "มี") {
                    $insert['Tor'] = 1;
                } else {
                    $insert['Tor'] = 0;
                }
                $insert['Account_id'] = $account_id['Account_id'];
                $insert['Department_id'] = $dataDetail['department_id'];
                $insert['Status'] = 9;

                $this->db->where('Project_id', $dataDetail['id_main']);
                $project = $this->db->update('project', $insert);
                // $project_id = $this->db->insert_id();
            }

            // ผู้รับผิดชอบโครงการ
            $this->db->where('Project_id', $data['data'][0]['id_main']);
            $user_delete = $this->db->delete('user');

            if ($user_delete) {
                foreach ($data['data'][0]['accountList'] as $account) {
                    $data_user = [
                        'Account_id' => $account,
                        'Project_id' =>  $data['data'][0]['id_main']
                    ];
                    $checkuser = $this->db->get_where('user', $data_user)->row_array();
                    if (!empty($checkuser)) {
                        continue;
                    }
                    $this->db->insert('user', $data_user);
                }
            }

            // ชื่อแผนยุทธ์ศาสตร์
            $this->db->where('Project_id', $data['data'][0]['id_main']);
            $strategic_plane_delete = $this->db->delete('project_strategic_plane');

            if ($strategic_plane_delete) {
                foreach ($data['data'][0]['strategic_planeList'] as $strategic_plane) {
                    $Strategic_id = $this->db->get_where('strategic', ['Strategic_Plan_id' => $strategic_plane['strategic_plane']])->row_array();
                    $data_strategic_plane = [
                        'Project_id'        =>  $data['data'][0]['id_main'],
                        'Strategic_Plan_id' => $strategic_plane['strategic_plane'],
                        'Strategic_id'      => $strategic_plane['strategic_id'],
                        'Goal_id'           => $strategic_plane['goal'],
                        'Indic_project_id'  => $strategic_plane['indic']
                    ];
                    $this->db->insert('project_strategic_plane', $data_strategic_plane);
                }
            }

            //วัตถุประสงค์
            $this->db->where('Project_id', $data['data'][0]['id_main']);
            $objective_delete = $this->db->delete('objective');
            if ($objective_delete) {
                foreach ($data['data'][0]['objective_nameList'] as $objective_name) {
                    $data_objective = [
                        'Project_id'        =>  $data['data'][0]['id_main'],
                        'Objective_name'    => $objective_name
                    ];
                    $this->db->insert('objective', $data_objective);
                }
            }

            //ตัวชี้วัดความสำเร็จระดับโครงการ
            $this->db->where('Project_id', $data['data'][0]['id_main']);
            $indic_success_delete = $this->db->delete('project_indic_success');
            if ($indic_success_delete) {
                foreach ($data['data'][0]['dataAll_indic_unit_cost'] as $dataIndic_unit_cost) {
                    $data_indic_unit_cost = [
                        'Project_id'    => $data['data'][0]['id_main'],
                        'Indic_success' => $dataIndic_unit_cost['indic_project'],
                        'Unit'          => $dataIndic_unit_cost['unit'],
                        'Cost'          => $dataIndic_unit_cost['cost'],
                    ];

                    $this->db->insert('project_indic_success', $data_indic_unit_cost);
                }
            }

            //ขั้นตอนการดำเนินการ
            $this->db->where('Project_id', $data['data'][0]['id_main']);
            $work_step_delete = $this->db->delete('work_step');

            if ($work_step_delete) {
                foreach ($data['data'][0]['stepList'] as $data_stepList) {
                    $data_step = [
                        'Step_name'     => $data_stepList['step'],
                        'Start'         => $data_stepList['start'],
                        'Stop'          => $data_stepList['stop'],
                        'Project_id'    => $data['data'][0]['id_main'],
                    ];
                    $this->db->insert('work_step', $data_step);
                }
            }

            //ประเภทการใช้จ่าย
            if ($data['data'][0]['charges_main'] != "ไม่ได้ใช้งบประมาณ") {

                $result_cm = $this->db->get_where('charges_main', ['Project_id' => $data['data'][0]['id_main']])->result_array();

                foreach ($result_cm as $key => $result_cm) {
                    $this->db->where('Charges_Main_id', $result_cm['Charges_Main_id']);
                    $this->db->delete('charges_sub');
                }

                $this->db->where('Project_id', $data['data'][0]['id_main']);
                $charges_main_delete = $this->db->delete('charges_main');

                if ($charges_main_delete) {
                    foreach ($data['data'][0]['charges_sub_list'] as $charges) {

                        $data_charges = [
                            'Charges_Main'  => $charges['charges_sub'],
                            'Project_id'    => $data['data'][0]['id_main'],
                        ];
                        $this->db->insert('charges_main', $data_charges);
                        $charges_id = $this->db->insert_id();

                        foreach ($charges['income_butgetList'] as $income_butget) {
                            $data_charges_sub = [
                                'Charges_Sub'       => $income_butget['income_butget'],
                                'Quarter_one'       => $income_butget['quarter_one'],
                                'Quarter_two'       => $income_butget['quarter_two'],
                                'Quarter_three'     => $income_butget['quarter_three'],
                                'Quarter_four'      => $income_butget['quarter_four'],
                                'Charges_Main_id'   => $charges_id,
                            ];
                            $this->db->insert('charges_sub', $data_charges_sub);
                        }
                    }
                }
            }

            //ประโยชน์ที่คาดว่าจะได้รับ
            $this->db->where('Project_id', $data['data'][0]['id_main']);
            $benefit_delete = $this->db->delete('benefit');
            if ($benefit_delete) {

                foreach ($data['data'][0]['benefit_nameList'] as $benefit_name) {
                    $data_benefit_name = [
                        'Project_id'    => $data['data'][0]['id_main'],
                        'Benefit_name'  => $benefit_name
                    ];
                    $this->db->insert('benefit', $data_benefit_name);
                }
            }


            $result = [];
            $result['successfully'] = true;
            $result['dataList'] = 'success';

            echo json_encode($result);
        }
    }

    public function project_delete_app()
    {
        $id = base64_decode($this->input->get('id_project'));
        // ลบโครงการ
        $this->db->where('Project_id', $id);
        $success = $this->db->delete('project');
        // ลบผู้รับผิดชอบโครงการ
        $this->db->where('Project_id', $id);
        $this->db->delete('user');
        // ลบแผนยุทธ์ศาสตร์
        $this->db->where('Project_id', $id);
        $this->db->delete('project_strategic_plane');
        // ลบวัตถุประสงค์
        $this->db->where('Project_id', $id);
        $this->db->delete('objective');
        // ลบตัวชี้วัดโครงการ
        $this->db->where('Project_id', $id);
        $this->db->delete('project_indic_success');
        // ลบขั้นตอนการดำเนินการ
        $this->db->where('Project_id', $id);
        $this->db->delete('work_step');
        // ลบประเภทการใช้จ่าย
        $this->db->where('Charges_Main_id', $id);
        $this->db->delete('charges_sub');
        // ลบประโยชน์ที่คาดว่าจะได้รับ
        $this->db->where('Project_id', $id);
        $this->db->delete('benefit');

        if ($success) {
            echo "SUCCESS";
        } else {
            echo "ERROR";
        }
    }

    public function project_status_pass_director()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID        = $this->input->post('Project_id');
            $data = array(
                'Status'        => 11,
            );

            $this->db->where('Project_id', $PID);
            if ($this->db->update('project', $data)) {
                $Account_id = $this->input->post('Account_id');
                $chk_acc = $this->db->get_where('account', ['Account_id' => $Account_id])->row_array();
                $data2 = array(
                    'Project_id'    => $PID,
                    'Account_id'    => $Account_id,
                    'Comment'       => "อนุมัติโครงการโดย (" . '' . $chk_acc['Fname'] . ' ' . $chk_acc['Lname'] . ')',
                    'Role'          => "Director",
                    'Time'          => date('Y-m-d H:i:s'),
                );
                $success = $this->db->insert('comment', $data2);
                echo $success;
            }
        }
    }

    public function project_status_padding_director()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID            = $this->input->post('PID');
            $Comment        = $this->input->post('Comment');
            $Account_id     = $this->input->post('Account_id');
            $BPID = base64_encode($PID);
            $data = array(
                'Status'        => 5,

            );

            $this->db->where('Project_id', $PID);
            if ($this->db->update('project', $data)) {
                $data2 = array(
                    'Project_id'    => $PID,
                    'Comment'       => $Comment,
                    'Account_id'    => $Account_id,
                    'Time'          => date('Y-m-d H:i:s'),
                    'Role'          => "Director",
                );
                $success = $this->db->insert('comment', $data2);
                if ($success > 0) {
                    echo "<script>";
                    echo "alert('ทำรายการเรียบร้อยแล้ว');";
                    echo "window.location='project'";
                    echo "</script>";
                } else {
                    echo "<script>";
                    echo "alert('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !!');";
                    echo "window.location='project'";
                    echo "</script>";
                }
            }
        }
    }


    public function project_status_pass_supervisor()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID        = $this->input->post('Project_id');
            $Account_id = $this->input->post('Account_id');
            $chk_acc = $this->db->get_where('account', ['Account_id' => $Account_id])->row_array();

            $data = array(
                'Status'        => 2,
            );

            $this->db->where('Project_id', $PID);
            if ($this->db->update('project', $data)) {
                $data2 = array(
                    'Project_id'    => $PID,
                    'Comment'       => "อนุมัติโครงการโดย (" . '' . $chk_acc['Fname'] . ' ' . $chk_acc['Lname'] . ')',
                    'Account_id'    => $Account_id,
                    'Time'          => date('Y-m-d H:i:s'),
                    'Role'          => "Supervisor",
                );
                $success = $this->db->insert('comment', $data2);
            }

            echo $success;
        }
    }

    public function project_doc_supervisor()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_doc_supervisor', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_status_pass()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID        = $this->input->post('Project_id');
            $Account_id        = $this->input->post('Account_id');
            $chk_acc = $this->db->get_where('account', ['Account_id' => $Account_id])->row_array();
            $data = array(
                'Status'        => 3,
            );

            $this->db->where('Project_id', $PID);
            if ($this->db->update('project', $data)) {
                $data2 = array(
                    'Project_id'    => $PID,
                    'Comment'       => "อนุมัติโครงการโดย (" . '' . $chk_acc['Fname'] . ' ' . $chk_acc['Lname'] . ')',
                    'Account_id'    => $Account_id,
                    'Time'          => date('Y-m-d H:i:s'),
                    'Role'          => "Manager"
                );

                $success = $this->db->insert('comment', $data2);
            }

            echo $success;
        }
    }

    public function project_status_not_director()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID            = $this->input->post('PID');
            $Comment        = $this->input->post('Comment');
            $Account_id     = $this->input->post('Account_id');
            $data = array(
                'Status'        => 0,

            );

            $this->db->where('Project_id', $PID);
            if ($this->db->update('project', $data)) {
                $data2 = array(
                    'Project_id'    => $PID,
                    'Comment'       => $Comment,
                    'Account_id'    => $Account_id,
                    'Time'          => date('Y-m-d H:i:s'),
                    'Role'          => "Director",
                );
                $success = $this->db->insert('comment', $data2);
                if ($success > 0) {
                    echo "<script>";
                    echo "alert('ทำรายการเรียบร้อยแล้ว');";
                    echo "window.location='project'";
                    echo "</script>";
                } else {
                    echo "<script>";
                    echo "alert('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !!');";
                    echo "window.location='project'";
                    echo "</script>";
                }
            }
        }
    }

    public function project_status_not()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID            = $this->input->post('PID');
            $Comment        = $this->input->post('Comment');
            $Account_id     = $this->input->post('Account_id');
            $data = array(
                'Status'        => 0,

            );

            $this->db->where('Project_id', $PID);
            if ($this->db->update('project', $data)) {
                $data2 = array(
                    'Project_id'    => $PID,
                    'Comment'       => $Comment,
                    'Account_id'    => $Account_id,
                    'Time'          => date('Y-m-d H:i:s'),
                    'Role'          => "Supervisor",
                );
                $success = $this->db->insert('comment', $data2);
                if ($success > 0) {
                    echo "<script>";
                    echo "alert('ทำรายการเรียบร้อยแล้ว');";
                    echo "window.location='project_supervisor'";
                    echo "</script>";
                } else {
                    echo "<script>";
                    echo "alert('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !!');";
                    echo "window.location='project_supervisor'";
                    echo "</script>";
                }
            }
        }
    }

    public function project_status_padding()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID            = $this->input->post('PID');
            $Comment        = $this->input->post('Comment');
            $Account_id     = $this->input->post('Account_id');
            $BPID = base64_encode($PID);
            $data = array(
                'Status'        => 3,

            );

            $this->db->where('Project_id', $PID);
            if ($this->db->update('project', $data)) {
                $data2 = array(
                    'Project_id'    => $PID,
                    'Comment'       => $Comment,
                    'Account_id'    => $Account_id,
                    'Time'          => date('Y-m-d H:i:s'),
                    'Role'          => 'Manager',
                );
                $success = $this->db->insert('comment', $data2);
                if ($success > 0) {
                    echo "<script>";
                    echo "alert('ทำรายการเรียบร้อยแล้ว');";
                    echo "window.location='project_consider_manager?PID=$BPID';";
                    echo "</script>";
                } else {
                    echo "<script>";
                    echo "alert('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !!');";
                    echo "window.location='project_consider_manager?PID=$BPID';";
                    echo "</script>";
                }
            }
        }
    }

    public function project_status_padding_supervisor()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID            = $this->input->post('PID');
            $Comment        = $this->input->post('Comment');
            $Account_id     = $this->input->post('Account_id');
            $BPID = base64_encode($PID);
            $data = array(
                'Status'        => 5,

            );

            $this->db->where('Project_id', $PID);
            if ($this->db->update('project', $data)) {
                $data2 = array(
                    'Project_id'    => $PID,
                    'Comment'       => $Comment,
                    'Account_id'    => $Account_id,
                    'Time'          => date('Y-m-d H:i:s'),
                );
                $success = $this->db->insert('comment', $data2);
                if ($success > 0) {
                    echo "<script>";
                    echo "alert('ทำรายการเรียบร้อยแล้ว');";
                    echo "window.location='project_consider_supervisor?PID=$BPID';";
                    echo "</script>";
                } else {
                    echo "<script>";
                    echo "alert('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !!');";
                    echo "window.location='project_consider_supervisor?PID=$BPID';";
                    echo "</script>";
                }
            }
        }
    }

    public function project_delete()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $D_PID  = base64_decode($this->input->get('D_PID'));

            $this->db->where('Project_id', $D_PID);
            if ($this->db->delete('project')) {
                echo "<script>";
                echo "alert('คุณได้ทำการลบโครงการเรียบร้อยแล้ว');";
                echo "window.location='project';";
                echo "</script>";
            } else {
                echo "<script>";
                echo "alert('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !!');";
                echo "window.location='project';";
                echo "</script>";
            }
        }
    }

    public function strategic_plane_project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $buttonType = $this->input->get('buttonType');
            $arrStr_all = [];
            if (isset($_GET['arrStr_all'])) {
                $arrStr_all = $this->input->get('arrStr_all');
            }

            $data['type'] = $buttonType;
            $strategic_planeList = $this->db->get_where('strategic_plane', ['Status' => 1])->result_array();
            foreach ($strategic_planeList as $key => $strategicPlane) {
                foreach ($arrStr_all as $strKey => $arrStr) {

                    if (intval($strategicPlane['Strategic_Plan_id']) == intval($arrStr)) {
                        unset($strategic_planeList[$key]);
                    }
                }
            }
            $data['strategic_planeList'] = $strategic_planeList;
            $data['test'] = $arrStr_all;
            $this->load->view('option/strategic_plane_project_app', $data);
        }
    }

    public function strategic_plane_project_edit()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $buttonType = $this->input->get('buttonType');
            $arrStr_all = [];
            if (isset($_GET['arrStr_all'])) {
                $arrStr_all = $this->input->get('arrStr_all');
            }

            $data['type'] = $buttonType;
            $strategic_planeList = $this->db->get_where('strategic_plane', ['Status' => 1])->result_array();
            foreach ($strategic_planeList as $key => $strategicPlane) {
                foreach ($arrStr_all as $strKey => $arrStr) {

                    if (intval($strategicPlane['Strategic_Plan_id']) == intval($arrStr)) {
                        unset($strategic_planeList[$key]);
                    }
                }
            }
            $data['strategic_planeList'] = $strategic_planeList;
            $data['test'] = $arrStr_all;
            $this->load->view('option/strategic_plane_project_edit', $data);
        }
    }

    public function strategic_project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $strategicGet = $this->input->get('strategicGet');
            $strategicGetList = [];
            if (isset($strategicGet) && !empty($strategicGet) && $strategicGet != "false") {
                $strategicGetList = $this->db->get_where('strategic', ['Strategic_Plan_id' => $strategicGet])->result_array();
            }
            $data['strategicGet'] = $strategicGet;
            $data['strategicGetList'] = $strategicGetList;
            $this->load->view('option/strategic_project_app', $data);
        }
    }

    public function strategic_project_edit()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $strategicGet = $this->input->get('strategicGet');
            $strategicGetList = [];
            if (isset($strategicGet) && !empty($strategicGet) && $strategicGet != "false") {
                $strategicGetList = $this->db->get_where('strategic', ['Strategic_Plan_id' => $strategicGet])->result_array();
            }
            $data['strategicGet'] = $strategicGet;
            $data['strategicGetList'] = $strategicGetList;
            $this->load->view('option/strategic_project_edit', $data);
        }
    }

    public function goal_project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $goalGet = $this->input->get('goalGet');
            $goalGetList = [];
            if (isset($goalGet) && !empty($goalGet) && $goalGet != "false") {
                $goalGetList = $this->db->get_where('goal', ['Strategic_id' => $goalGet])->result_array();
            }
            $data['goalGet'] = $goalGet;
            $data['goalGetList'] = $goalGetList;
            $this->load->view('option/goal_project_app', $data);
        }
    }

    public function goal_project_edit()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $goalGet = $this->input->get('goalGet');
            $goalGetList = [];
            if (isset($goalGet) && !empty($goalGet) && $goalGet != "false") {
                $goalGetList = $this->db->get_where('goal', ['Strategic_id' => $goalGet])->result_array();
            }
            $data['goalGet'] = $goalGet;
            $data['goalGetList'] = $goalGetList;
            $this->load->view('option/goal_project_edit', $data);
        }
    }

    public function indic_project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $indicGet = $this->input->get('indicGet');
            $goalGet = $this->input->get('goalGet');
            $indicGetList = [];
            if (isset($indicGet) && !empty($indicGet) && $indicGet != "false") {
                $indicGetList = $this->db->get_where('indic_project', ['Goal_id' => $goalGet])->result_array();
            }
            $data['indicGet'] = $indicGet;
            $data['indicGetList'] = $indicGetList;
            $this->load->view('option/indic_project_app', $data);
        }
    }



    public function tactic_project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $tacticGet = $this->input->get('tacticGet');
            $tacticGetList = [];
            if (isset($tacticGet) && !empty($tacticGet) && $tacticGet != "false") {
                $tacticGetList = $this->db->get_where('tactic', ['Goal_id' => $tacticGet])->result_array();
            }
            $data['tacticGet'] = $tacticGet;
            $data['tacticGetList'] = $tacticGetList;
            $this->load->view('option/tactic_project_app', $data);
        }
    }

    public function tactic_project_edit()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $tacticGet = $this->input->get('tacticGet');
            $tacticGetList = [];
            if (isset($tacticGet) && !empty($tacticGet) && $tacticGet != "false") {
                $tacticGetList = $this->db->get_where('tactic', ['Goal_id' => $tacticGet])->result_array();
            }
            $data['tacticGet'] = $tacticGet;
            $data['tacticGetList'] = $tacticGetList;
            $this->load->view('option/tactic_project_edit', $data);
        }
    }

    public function project_report()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID = base64_decode($this->input->get('PID'));
            $data['PID'] = $PID;
            $data['project'] = $this->db->get_where('project', ['Project_id' => $PID])->row_array();
            $data['work_step'] = $this->db->get_where('work_step', ['Project_id' => $PID])->result_array();
            $this->load->view('option/header');
            $this->load->view('project_report', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_report_save()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data = $this->input->post('data');
            $Used = $this->input->post('Used');
            $quarter = $this->input->post('quarter');
            $per_check = $this->input->post('per_check');
            $project_id = $this->input->post('project_id');

            //ตารางรายงานความก้าวหน้า
            $report = array(
                'Used'          => $Used,
                'Quarter'       => $quarter,
                'Period_check'  => $per_check,
                'Project_id'    => $project_id,
            );
            $this->db->insert('project_report', $report);
            $report_last = $this->db->insert_id();

            //ตารางผลตามตัวชี้วัดความสำเร็จ
            foreach ($data as $dataDetail) {
                $id = [];
                foreach ($dataDetail['Indic_project'] as $indic_project) {
                    $this->db->insert('indic_project_report', ['Indic_project_id' => $indic_project]);
                    $id[] = $this->db->insert_id();
                }
                foreach ($dataDetail['Result'] as $key => $result) {
                    $this->db->where('Indic_project_report_id', $id[$key]);
                    $this->db->update('indic_project_report', ['Result' => $result]);
                }
                foreach ($dataDetail['achieve'] as $key => $achieve) {
                    $this->db->where('Indic_project_report_id', $id[$key]);
                    $this->db->update('indic_project_report', ['Achieve' => $achieve, 'Report_id' => $report_last]);
                }
                foreach ($dataDetail['detail_data'] as $key => $Detail) {
                    $this->db->insert('detail', ['Detail' => $Detail, 'Report_id' => $report_last]);
                }
                foreach ($dataDetail['problem_data'] as $key => $problem) {
                    $this->db->insert('problem', ['Problem' => $problem, 'Report_id' => $report_last]);
                }
            }

            echo "SUCCESS";
        }
    }

    public function project_report_quar_view()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID = base64_decode($this->input->get('PID'));
            $data['PID'] = $PID;
            $data['project'] = $this->db->get_where('project', ['Project_id' => $PID])->row_array();
            $data['work_step'] = $this->db->get_where('work_step', ['Project_id' => $PID])->result_array();
            $this->load->view('option/header');
            $this->load->view('project_report_view', $data);
            $this->load->view('option/footer');
        }
    }

    function project_report_estimate_save()
    {
        $pid                = $this->input->post('pid');
        $explanation        = $this->input->post('explanation');
        $Conducting         = $this->input->post('Conducting');
        $objective_check    = $this->input->post('objective_check');
        $Result             = $this->input->post('Result');
        $Benefits           = $this->input->post('Benefits');
        $Problem            = $this->input->post('Problem');
        $Improvement        = $this->input->post('Improvement');
        $Performance        = $this->input->post('Performance');
        $quarter            = $this->input->post('quarter');
        $Used               = $this->input->post('Used');

        $data = array(
            'Project_id'    => $pid,
            'Explanation'   => $explanation,
            'Conducting'    => $Conducting,
            'Objective' => $objective_check,
            'Result'        => $Result,
            'Benefits'      => $Benefits,
            'Problem'       => $Problem,
            'Improvement'   => $Improvement,
            'Performance'   => $Performance,
            'quarter' => $quarter,
            'Flag_close'    => 0,
        );
        $this->db->insert('estimate', $data);
        $this->db->where('Project_id', $pid);
        $this->db->update('project', ['Status' => '11']);
        $data_report = array(
            'Project_id'    => $pid,
            'Period_check'  => 0,
            'Quarter'       => $quarter,
            'Used'          => $Used,
        );
        if ($this->db->insert('project_report', $data_report)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function strategic_search_ajax_edit()
    {
        $search              = $this->input->post('search');
        $search_strategic_id = $this->input->post('search_strategic_id');

        $this->db->where('Strategic_Plan_id', $search);
        $search_strategic = $this->db->get('strategic')->result_array();

        echo $this->strategic_search_table_edit($search_strategic, $search_strategic_id);
    }

    function strategic_search_table_edit($search_strategic, $search_strategic_id)
    {
        $output = '';
        $output .= '<option value="" disabled> กรุณาเลือกประเด็นยุทธศาสตร์ </option>';
        foreach ($search_strategic as $search_strategic) {
            if ($search_strategic_id == $search_strategic['Strategic_id']) {
                $output .= '<option value="' . $search_strategic['Strategic_id'] . '"  "selected" >' . $search_strategic['Strategic_name'] . '</option>';
            } else {
                $output .= '<option value="' . $search_strategic['Strategic_id'] . '" >' . $search_strategic['Strategic_name'] . '</option>';
            }
        }

        return $output;
    }

    public function goal_search_ajax_edit()
    {
        $search_goal    = $this->input->post('search_goal');
        $search_goal_id = $this->input->post('search_goal_id');

        $this->db->where('Goal_id', $search_goal_id);
        $result_goal = $this->db->get('goal')->result_array();

        echo $this->goal_search_table_edit($result_goal, $search_goal_id);
    }

    function goal_search_table_edit($result_goal, $search_goal_id)
    {
        $output = '';
        $output .= '<option value="" disabled> กรุณาเลือกเป้าประสงค์ </option>';
        foreach ($result_goal as $result_goal) {
            if ($search_goal_id == $result_goal['Goal_id']) {
                $output .= '<option value="' . $result_goal['Goal_id'] . '"  "selected" >' . $result_goal['Goal_name'] . '</option>';
            } else {
                $output .= '<option value="' . $result_goal['Goal_id'] . '" >' . $result_goal['Goal_name'] . '</option>';
            }
        }

        return $output;
    }

    public function tactic_search_ajax_edit()
    {
        $search_tactic    = $this->input->post('search_tactic');
        $search_tactic_id = $this->input->post('search_tactic_id');

     
        $this->db->where('Tactic_id', $search_tactic);
        $result_tactic = $this->db->get('tactic')->result_array();

        echo $this->tactic_search_table_edit($result_tactic, $search_tactic_id);
    }

    function tactic_search_table_edit($result_tactic, $search_tactic_id)
    {
        $output = '';
        $output .= '<option value="" disabled> กรุณาเลือกกลยุทธ์ </option>';
        foreach ($result_tactic as $result_tactic) {
            if ($search_tactic_id == $result_tactic['Tactic_id']) {
                $output .= '<option value="' . $result_tactic['Tactic_id'] . '"  "selected" >' . $result_tactic['Tactic_name'] . '</option>';
            } else {
                $output .= '<option value="' . $result_tactic['Tactic_id'] . '" >' . $result_tactic['Tactic_name'] . '</option>';
            }
        }

        return $output;
    }
    function check_account()
    {
        $accountList = $this->input->post('accountList');
        foreach ($accountList as $accountList) {
            $this->db->where('Account_id !=', $accountList);
        }
        $account = $this->db->get('account')->result_array();
        //ผู้รับผิดชอบโครงการ
        $accountText = '<div class="row container form-group input_add" id="remove_account">';
        $accountText .= '<label class="col-3"></label><div class="col-7">';
        $accountText .= '<select name="account[]" id="" class="form-control" required>';
        $accountText .= '<option selected disabled>เลือกผู้รับผิดชอบโครงการ</option>';
        foreach ($account as $account) {
            $accountText .= '<option value="' . $account['Account_id'] . '">' . $account['Fname'] . ' '  . $account['Lname'] . '</option>';
        }
        $accountText .= '</select>';
        $accountText .= '</div>';
        $accountText .= '<div class="col-2">';
        $accountText .= '<button type="button" class="btn btn-danger" onClick="remove_account(this);"><i class="fa fa-times" aria-hidden="true"></i> ลบ</button>';
        $accountText .= '</div>';
        $accountText .= '</div>';
        echo $accountText;
    }
}
