<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("Asia/Bangkok");
class Authorization_ctr extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if ($this->session->userdata('Username') == "") {
			redirect('login');
		} else {
			$data['department'] = $this->db->get('department')->result_array();
			$data['account'] = $this->db->get('account')->result_array();

			$this->load->view('option/header');
			$this->load->view('authorization', $data);
			$this->load->view('option/footer');
		}
	}

	public function add_user()
	{
		if ($this->session->userdata('Username') == "") {
			redirect('login');
		} else {

			$username = $this->input->post('username');
			// $password = $this->input->post('password');
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$email = $this->input->post('email');
			$department_one = $this->input->post('department_one');
			if (empty($first_name)) {
				echo "<script>";
				echo "alert('กรุณาระบุ username ให้ถูกต้องด้วยค่ะ !!!');";
				echo "window.location='authorization';";
				echo "</script>";
				exit();
			}

			if (empty($last_name)) {
				echo "<script>";
				echo "alert('กรุณาระบุ username ให้ถูกต้องด้วยค่ะ !!!');";
				echo "window.location='authorization';";
				echo "</script>";
				exit();
			}

			if (empty($email)) {
				echo "<script>";
				echo "alert('กรุณาระบุ username ให้ถูกต้องด้วยค่ะ !!!');";
				echo "window.location='authorization';";
				echo "</script>";
				exit();
			}



			$director_responsible = $this->input->post('director_responsible');
			$manager = $this->input->post('manager');
			$supervisor = $this->input->post('supervisor');
			$supplies = $this->input->post('supplies');
			$admin = $this->input->post('admin');

			if ($director_responsible == "director") {
				$director = 1;
				$responsible = 0;
				$manager = 0;
				$supervisor = 0;
				$supplies = 0;
				$admin = 0;
			} else {
				$director = 0;
				$responsible = 1;
				if ($manager == "") {
					$manager = 0;
				} else {
					$manager = 1;
				}

				if ($supervisor == "") {
					$supervisor = 0;
				} else {
					$supervisor = 1;
				}

				if ($supplies == "") {
					$supplies = 0;
				} else {
					$supplies = 1;
				}

				if ($admin == "") {
					$admin = 0;
				} else {
					$admin = 1;
				}
			}


			if ($department_one == "nodata") {
				echo "<script>";
				echo "alert('กรุณาเลือกฝ่าย ด้วยค่ะ !!!');";
				echo "window.location='authorization';";
				echo "</script>";
				exit();
			}


			$checkUsername = $this->db->get_where('account', ['Username' => $username])->row_array();
			if (!empty($checkUsername)) {
				echo "<script>";
				echo "alert('มีผู้ใช้ Username นี้แล้ว !!!');";
				echo "window.location='authorization';";
				echo "</script>";
				exit();
			}
			$checkEmail = $this->db->get_where('account', ['Email' => $email])->row_array();
			if (!empty($checkEmail)) {
				echo "<script>";
				echo "alert('มีผู้ใช้ Email นี้แล้ว !!!');";
				echo "window.location='authorization';";
				echo "</script>";
				exit();
			}
			$checkFirst_last = $this->db->get_where('account', ['Fname' => $first_name, 'Lname' => $last_name])->row_array();
			if (!empty($checkFirst_last)) {
				echo "<script>";
				echo "alert('มีผู้ใช้ ชื่อ-นามสกุล นี้แล้ว !!!');";
				echo "window.location='authorization';";
				echo "</script>";
				exit();
			}

			$data = [
				'Department_id' => $department_one,
				'Username'      => $username,
				// 'Password'      => md5($password),
				'Fname'         => $first_name,
				'Lname'         => $last_name,
				'Email'         => $email,
				'Director'      => $director,
				'Manager'       => $manager,
				'Supervisor'    => $supervisor,
				'Supplies'      => $supplies,
				'Responsible'   => $responsible,
				'Admin'         => $admin,
			];

			$this->db->insert('account', $data);
			echo "<script>";
			echo "alert('สำเร็จ !!! เพิ่มบัญชีผู้ใช้งานเรียบร้อยแล้ว');";
			echo "window.location='authorization';";
			echo "</script>";
			exit();
		}
	}

	public function edit_user()
	{
		if ($this->session->userdata('Username') == "") {
			redirect('login');
		} else {
			$account_id = $this->input->post('account_id');
			$department_one = $this->input->post('department_one');

			$director_responsible = $this->input->post('director_responsible');
			$manager = $this->input->post('manager');
			$supervisor = $this->input->post('supervisor');
			$supplies = $this->input->post('supplies');
			$admin = $this->input->post('admin');

			if ($director_responsible == "director") {
				$director = 1;
				$responsible = 0;
				$manager = 0;
				$supervisor = 0;
				$supplies = 0;
				$admin = 0;
			} else {
				$director = 0;
				$responsible = 1;
				if ($manager == "") {
					$manager = 0;
				} else {
					$manager = 1;
				}

				if ($supervisor == "") {
					$supervisor = 0;
				} else {
					$supervisor = 1;
				}

				if ($supplies == "") {
					$supplies = 0;
				} else {
					$supplies = 1;
				}

				if ($admin == "") {
					$admin = 0;
				} else {
					$admin = 1;
				}
			}



			if ($department_one == "nodata") {
				echo "<script>";
				echo "alert('กรุณาเลือกสิทธิผู้ใช้งาน ทั้ง 2 ด้วยค่ะ !!!');";
				echo "window.location='authorization';";
				echo "</script>";
				exit();
			}

			$data = [
				'Department_id' => $department_one,
				'Director'      => $director,
				'Manager'       => $manager,
				'Supervisor'    => $supervisor,
				'Supplies'      => $supplies,
				'Responsible'   => $responsible,
				'Admin'         => $admin,
			];
			$this->db->where('Account_id', $account_id);
			$this->db->update('account', $data);
			echo "<script>";
			echo "alert('สำเร็จ !!! แก้ไขบัญชีผู้ใช้งานเรียบร้อยแล้ว');";
			echo "window.location='authorization';";
			echo "</script>";
			exit();
		}
	}

	public function delete_user()
	{
		if ($this->session->userdata('Username') == "") {
			redirect('login');
		} else {
			$account_id = $this->input->post('account_id');
			$this->db->where('Account_id', $account_id);
			$this->db->delete('account');
			echo "<script>";
			echo "alert('สำเร็จ !!! ลบบัญชีผู้ใช้งานเรียบร้อยแล้ว');";
			echo "window.location='authorization';";
			echo "</script>";
			exit();
		}
	}

	public function manager_authorization()
	{
		if ($this->session->userdata('Username') == "") {
			redirect('login');
		} else {

			$data['department'] = $this->db->get('department')->result_array();

			$this->load->view('option/header');
			$this->load->view('manager_authorization', $data);
			$this->load->view('option/footer');
		}
	}

	public function add_department()
	{
		if ($this->session->userdata('Username') == "") {
			redirect('login');
		} else {
			$department = $this->input->post('department');
			$data = [
				'Department' => $department
			];
			$this->db->insert('department', $data);
			echo "<script>";
			echo "alert('สำเร็จ !!! เพิ่มสิทธิผู้ใช้งานเรียบร้อยแล้ว');";
			echo "window.location='manager_authorization';";
			echo "</script>";
			exit();
		}
	}

	public function edit_department()
	{
		if ($this->session->userdata('Username') == "") {
			redirect('login');
		} else {
			$department_id = $this->input->post('department_id');
			$department = $this->input->post('department');
			$data = [
				'Department' => $department
			];
			$this->db->where('Department_id', $department_id);
			$this->db->update('department', $data);
			echo "<script>";
			echo "alert('สำเร็จ !!! แก้ไขสิทธิผู้ใช้งานเรียบร้อยแล้ว');";
			echo "window.location='manager_authorization';";
			echo "</script>";
			exit();
		}
	}

	public function delete_department()
	{
		if ($this->session->userdata('Username') == "") {
			redirect('login');
		} else {
			$department_id = $this->input->post('department_id');

			$this->db->where('Department_id', $department_id);
			$this->db->delete('account');

			$this->db->where('Department_id', $department_id);
			$this->db->delete('department');


			echo "<script>";
			echo "alert('สำเร็จ !!! ลบสิทธิผู้ใช้งานเรียบร้อยแล้ว');";
			echo "window.location='manager_authorization';";
			echo "</script>";
			exit();
		}
	}

	public function authorization_department()
	{
		if ($this->session->userdata('Username') == "") {
			redirect('login');
		} else {
			$department_id = $this->input->get('department_id');
			$accounts = $this->db->get('account')->result_array();
			$accountList = array();
			foreach ($accounts as $account) {
				$check = explode(',', $account['Department_id']);
				if (in_array($department_id, $check)) {
					$accountList[] = $account;
				}
			}
			$data['account'] = $accountList;
			$data['department_id'] = $department_id;

			$this->load->view('option/header');
			$this->load->view('authorization_department', $data);
			$this->load->view('option/footer');
		}
	}

	public function search_department_list()
	{
		if ($this->session->userdata('Username') == "") {
			redirect('login');
		} else {
			$valueDepartment = $this->input->get('id');

			$data['department'] = $this->db->get('department')->result_array();
			$data['account'] = $this->db->get_where('account', ['Department_id' => $valueDepartment])->result_array();
			$data['valueDepartment'] = $valueDepartment;
			$this->load->view('option/header');
			$this->load->view('authorization_search_department', $data);
			$this->load->view('option/footer');
		}
	}

	public function search_user()
	{
		// if ($this->session->userdata('Username') == "") {
		//     redirect('login');
		// } else {
		//     $userValue = $this->input->post('userValue');

		//     $access_token = 'nK6p0wT-8NVHUwB8p0e9QSYBSaIZGp9D'; // <----- API - Access Token Here
		//     $username   = $userValue; // <----- Username for search

		//     $api_url = 'https://api.account.kmutnb.ac.th/api/account-api/user-info'; // <----- API URL

		//     $ch = curl_init();
		//     curl_setopt($ch, CURLOPT_URL, $api_url);
		//     curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		//     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		//     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//     curl_setopt($ch, CURLOPT_POST, true);
		//     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $access_token));
		//     curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		//     curl_setopt($ch, CURLOPT_POSTFIELDS, array('username' => $username));

		//     if (($response = curl_exec($ch)) === false) {
		//         echo 'Curl error: ' . curl_errno($ch) . ' - ' . curl_error($ch);
		//     } else {
		//         $json_data = json_decode($response, true);
		//         if (!isset($json_data['api_status'])) {
		//             echo 'API Error ' . print_r($response, true);
		//         } elseif ($json_data['api_status'] == 'success') {

		//             // $user = [
		//             //     'username' => $json_data['userInfo']['username'],
		//             //     'displayname' => $json_data['userInfo']['displayname'],
		//             //     'firstname_en' => $json_data['userInfo']['firstname_en'],
		//             //     'lastname_en' => $json_data['userInfo']['lastname_en'],
		//             //     'email' => $json_data['userInfo']['email'],
		//             //     'account_type' => $json_data['userInfo']['account_type'],

		//             // ];
		//             $cutDisplay = explode(" ", $json_data['userInfo']['displayname']);
		//             $user = [
		//                 'username' => $json_data['userInfo']['username'],
		//                 'displayname' => $json_data['userInfo']['displayname'],
		//                 'firstname_en' => $cutDisplay[0],
		//                 'lastname_en' => $cutDisplay[1],
		//                 'email' => $json_data['userInfo']['email'],
		//                 'account_type' => $json_data['userInfo']['account_type'],

		//             ];

		//             $result = [];
		//             $result['successfully'] = true;
		//             $result['dataList'] = $user;
		//             echo json_encode($result);
		//         } elseif ($json_data['api_status'] == 'fail') {
		//             $user = [
		//                 'username' => null,
		//                 'displayname' => null,
		//                 'firstname_en' => null,
		//                 'lastname_en' => null,
		//                 'email' => null,
		//                 'account_type' => null,

		//             ];
		//             $result = [];
		//             $result['successfully'] = false;
		//             $result['dataList'] = [];
		//             echo json_encode($result);
		//         } else {
		//             echo "Internal Error";
		//         }
		//     }
		//     curl_close($ch);
		// }

		if ($this->session->userdata('Username') == "") {
			redirect('login');
		} else {
			$userValue = $this->input->post('userValue');

			$access_token = 'nK6p0wT-8NVHUwB8p0e9QSYBSaIZGp9D'; // <----- API - Access Token Here
			$username   = $userValue; // <----- Username for search

			$api_url = 'https://api.account.kmutnb.ac.th/api/account-api/user-info';
			$params = array('username' => $username);
			$header[] = 'Content-type: application/x-www-form-urlencoded';
			$header[] = 'Authorization: Bearer ' . $access_token;
			$options = array(
				'http' => array(
					'header'  => $header,
					'method'  => 'POST',
					'content' => http_build_query($params),
					'timeout' => 10,  // Seconds
				),
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
				),
			);

			$context = stream_context_create($options);
			$result  = @file_get_contents($api_url, false, $context);

			if ($result === false) {
				$error = error_get_last();
				$error = explode(': ', $error['message']);
				echo 'Error: ' . trim($error[2]);
			} else {
				$json_data = json_decode($result, true);
				if (!isset($json_data['api_status'])) {
					echo 'API Error ' . print_r($result, true);
				} elseif ($json_data['api_status'] == 'success') {
					$cutDisplay = explode(" ", $json_data['userInfo']['displayname']);
					$user = [
						'username' => $json_data['userInfo']['username'],
						'displayname' => $json_data['userInfo']['displayname'],
						'firstname_en' => $cutDisplay[0],
						'lastname_en' => $cutDisplay[1],
						'email' => $json_data['userInfo']['email'],
						'account_type' => $json_data['userInfo']['account_type'],

					];

					$result = [];
					$result['successfully'] = true;
					$result['dataList'] = $user;
					echo json_encode($result);
				} elseif ($json_data['api_status'] == 'fail') {
					$user = [
						'username' => null,
						'displayname' => null,
						'firstname_en' => null,
						'lastname_en' => null,
						'email' => null,
						'account_type' => null,

					];
					$result = [];
					$result['successfully'] = false;
					$result['dataList'] = [];
					echo json_encode($result);
				} else {
					echo "Internal Error";
				}
			}
		}
	}
}
