<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pdf_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Pdf');
    }

    public function print_pdf()
    {
        // load excel library
        $this->load->view('project_proposal_form');
    }
}
