<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_ctr extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->session->userdata('Username') == "") {
            $this->load->view('login');
        } else {
            redirect('list_project');
        }
    }

    public function loginMe()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('Username', 'Username', 'required');
        // $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run()) {
            $Username = $this->input->post('Username');
            // $password = md5($this->input->post('password'));
            $password = $this->input->post('password');
            $this->load->model('Login_model');

            if ($this->Login_model->login_admin($Username)) {
                $user_data = array(
                    'Username' => $Username,
                    'Year' => date('Y')
                );
                $this->session->set_userdata($user_data);
                $this->session->set_flashdata('save_ss2', 'ล็อกอินเรียบร้อยแล้ว');
                redirect('list_project');
            } else {
                $userAuth = $this->callAPI_userAuthen($Username, $password);

                if ($userAuth == "success") {

                    $user_data = array(
                        'Username' => $Username,
                        'Year' => date('Y')
                    );

                    $this->session->set_userdata($user_data);
                    $this->session->set_flashdata('save_ss2', 'ล็อกอินเรียบร้อยแล้ว');
                    redirect('list_project');
                } elseif ($userAuth == "api error" || $userAuth == "Internal Error") {
                    $this->session->set_flashdata('del_ss2', 'ตอนนี้ระบบกำลังมีปัญหา ขออภัยในความไม่สะดวก!!');
                    redirect('login', 'refresh');
                } else {
                    $this->session->set_flashdata('del_ss2', 'รหัสผ่านผิดกรุณา ตรวจสอบ!!');
                    redirect('login', 'refresh');
                }
            }
        }
    }

    public function logout()
    {
        $this->session->sess_destroy(); //ล้างsession

        redirect('login'); //กลับไปหน้า Login
    }

    private function callAPI_userAuthen($username, $password)
    {
        $access_token = 'nK6p0wT-8NVHUwB8p0e9QSYBSaIZGp9D';
        $scopes = 'personel,student,templecturer,retirement,exchange_student,alumni,guest,kiosk,event';     // <----- Scopes for search account type
        // $username = '';
        // $password = '';

        $api_url = 'https://api.account.kmutnb.ac.th/api/account-api/user-authen'; // <----- API URL
        $post_data = array('scopes' => $scopes, 'username' => $username, 'password' => $password);
        $options = array(
            'http' => array(
                'header'  => array(
                    'Content-type: application/x-www-form-urlencoded',
                    'Authorization: Bearer ' . $access_token,
                ),
                'method'  => 'POST',
                'content' => http_build_query($post_data),
                'timeout' => 10,  // Seconds
            ),
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
            ),
        );
        $context = stream_context_create($options);
        $response  = @file_get_contents($api_url, false, $context);
        $data = array();
        if ($response === false) {
            // $error = error_get_last();
            // $error = explode(': ', $error['message']);
            //echo 'Error: ' . trim($error[2]);
            return 'Error';
        } else {
            $json_data = json_decode($response, true);
            if (!isset($json_data['api_status'])) {
                // echo 'API Error ' . print_r($response, true);
                return 'api error';
            } elseif ($json_data['api_status'] == 'success') {
                // echo 'Login success';
                // echo "<br />=============================";
                // echo "<br />Username: " . $json_data['userInfo']['username'];
                // echo "<br />Displayname: " . $json_data['userInfo']['displayname'];
                // echo "<br />Firstname EN: " . $json_data['userInfo']['firstname_en'];
                // echo "<br />Lirstname EN: " . $json_data['userInfo']['lastname_en'];
                // echo "<br />pid: " . $json_data['userInfo']['pid'];
                // echo "<br />Email: " . $json_data['userInfo']['email'];
                // echo "<br />Birthdate: " . $json_data['userInfo']['birthdate'];
                // echo "<br />Account type: " . $json_data['userInfo']['account_type'];
                return 'success';
            } elseif ($json_data['api_status'] == 'fail') {
                // echo "API Error: " . $json_data['api_status_code'] . ' - ' . $json_data['api_message'];
                return "fail";
            } else {
                return "Internal Error";
            }
        }
    }
}
