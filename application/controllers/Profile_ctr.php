<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("Asia/Bangkok");
class Profile_ctr extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function edit_profile()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
			$id = $this->input->post('id');
			
			$director_responsible = $this->input->post('director_responsible');
			$manager = $this->input->post('manager');
			$supervisor = $this->input->post('supervisor');
			$supplies = $this->input->post('supplies');
			$admin = $this->input->post('admin');

			$department_id = $this->input->post('department_id');

			if ($director_responsible == "director") {
				$director = 1;
				$responsible = 0;
				$manager = 0;
				$supervisor = 0;
				$supplies = 0;
				$admin = 0;
			} else {
				$director = 0;
				$responsible = 1;
				if ($manager == "") {
					$manager = 0;
				} else {
					$manager = 1;
				}

				if ($supervisor == "") {
					$supervisor = 0;
				} else {
					$supervisor = 1;
				}

				if ($supplies == "") {
					$supplies = 0;
				} else {
					$supplies = 1;
				}

				if ($admin == "") {
					$admin = 0;
				} else {
					$admin = 1;
				}
			}

            // $first_name = $this->input->post('first_name');
            // $last_name = $this->input->post('last_name');
            //$tel = $this->input->post('tel');
            //$email = $this->input->post('email');

            $data = [
                // 'Fname'         => $first_name,
                // 'Lname'         => $last_name,
                //'Tel'           => $tel,
				//'Email'         => $email,
				'Director'      => $director,
				'Manager'       => $manager,
				'Supervisor'    => $supervisor,
				'Supplies'      => $supplies,
				'Responsible'   => $responsible,
				'Admin'         => $admin,
				'Department_id' => $department_id,

            ];
            $this->db->where('Account_id', $id);
            $this->db->update('account', $data);
            echo "<script>";
            echo "alert('สำเร็จ !!! แก้ไขบัญชีผู้ใช้งานเรียบร้อยแล้ว');";
            echo "window.location='home';";
            echo "</script>";
            exit();
        }
    }

    public function edit_profile_password()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $id = $this->input->post('id');
            $password = $this->input->post('password');
            $new_password = $this->input->post('new_password');
            $confirm_password = $this->input->post('confirm_password');
            $account = $this->db->get_where('account', ['Account_id', $id])->row_array();
            $passwordCheck = md5($password);

            if ($account['Password'] != $passwordCheck) {
                echo "<script>";
                echo "alert('รหัสผ่านเก่าของคุณไม่ถูกต้อง');";
                echo "window.location='home';";
                echo "</script>";
                exit();
            }

            if ($new_password != $confirm_password) {
                echo "<script>";
                echo "alert('รหัสผ่านใหม่และยืนยันรหัสผ่านใหม่ของคุณไม่ตรงกัน');";
                echo "window.location='home';";
                echo "</script>";
                exit();
            }

            $passwordConfirm = md5($confirm_password);

            $data = [
                'Password'         => $passwordConfirm
            ];
            $this->db->where('Account_id', $id);
            $this->db->update('account', $data);
            $this->session->sess_destroy(); //ล้างsession
            echo "<script>";
            echo "alert('สำเร็จ !!! แก้ไขรหัสผ่านของคุณเรียบร้อยแล้ว');";
            echo "window.location='login';";
            echo "</script>";
            exit();
        }
    }
}
