<div class="row container" style="    padding: 25px 0 0 0px;">
    <div class="col-3"> </div>
    <label class="col-2 text-right" for="">กลยุทธ์ :</label>
    <div class="col-4">
		
		<select class="form-control" name="tactic">
            <option value=""  selected disabled>กรุณาเลือกกลยุทธ์</option>
            <?php foreach ($tacticGetList as $tacticGetDetail) { ?>
            <option value="<?php echo $tacticGetDetail['Indic_project_id']; ?>"><?php echo $tacticGetDetail['Tactic_name']; ?></option>
            <?php } ?>
		</select>
		
    </div>
</div>
<div id="indicList"></div> 

<script>
    $("select[name='tactic']").change(function(){
        getIndicList($(this));
    });
     function getIndicList(event){
        let indicGet = false;
        if (event) {
            indicGet = event.val();
        }
        $.ajax({
            url:"<?php echo base_url('Project_ctr/indic_project_app'); ?>",
            data:{indicGet:indicGet},
            success:function(response){
                event.parents('#tacticList').find('#indicList').html(response);
            }
        });
    }
</script>
