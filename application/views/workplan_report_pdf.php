<?php $wpy = $this->input->get('wpy'); ?>
<?php
function DateThai($strDate)
{
    $strYear = date("Y", strtotime($strDate)) + 543;
    $strMonth = date("n", strtotime($strDate));
    $strDay = date("j", strtotime($strDate));
    $strHour = date("H", strtotime($strDate));
    $strMinute = date("i", strtotime($strDate));
    $strSeconds = date("s", strtotime($strDate));
    $strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
    $strMonthThai = $strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
}
function fetch_data()
{
    $output = '';
    $output .= '';
    return $output;
}
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$obj_pdf->SetTitle("แบบเสนอโครงการ");
$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('thsarabun');
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, '25', PDF_MARGIN_RIGHT);
$obj_pdf->setPrintHeader(false);
$obj_pdf->setPrintFooter(false);
$obj_pdf->SetAutoPageBreak(TRUE, 10);
$obj_pdf->SetFont('thsarabun', '', 16);
$obj_pdf->AddPage('L', 'A4'); // L แนวนอน P แนวตั้ง
$content = '';
$content .= '<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>';
$content .= '
    <div style="text-align:center;">
        <p align="center">แผนปฏิบัติการประจําปี ' . ($wpy + 543) . ' สํานักคอมพิวเตอร์และเทคโนโลยีสารสนเทศ</p>
    </div>';
$content .= '<table id="table" class="table table-bordered table-striped">';
$content .= '
        <thead style="text-align:center;" align="center">
            <tr role="row">
              <th rowspan="3" colspan="0">ลำดับ</th>
              <th rowspan="3" colspan="1">โครงการ/กิจกรรม</th>
              <th rowspan="3" colspan="1">งบประมาณ</th>
              <th colspan="12" rowspan="1">ระยะเวลาดำเนินงาน</th>
              <th rowspan="3" colspan="1">ผู้รับผิดชอบ</th>
            </tr>
            <tr role="row">
              <th colspan="3">2563</th>
              <th colspan="9">2564</th>
            </tr>
        </thead>';
$content .= '</table>';

$content .= fetch_data();
$obj_pdf->writeHTML($content);
$obj_pdf->Output('file.pdf', 'I');
