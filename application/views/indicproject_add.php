  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
          <div class="container-fluid">
              <div class="row mb-2">
                  <div class="col-sm-6">
                      <h1>ตัวชี้วัด</h1>
                  </div>
                  <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                          <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                      </ol>
                  </div>
              </div>
          </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
          <!-- Default box -->
          <div class="card">
              <div class="card-header">
                  <h3 class="card-title">จัดการตัวชี้วัดใหม่</h3>

                  <div class="card-tools">
                      <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> -->
                  </div>
              </div>
              <div class="card-body">
                  <table id="table" class="table table-bordered table-striped" style="text-align:center;">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>ชื่อเป้าประสงค์</th>

                              <th>เครื่องมือ</th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php $strategicList = $this->db->get_where('goal', ['Strategic_id' => $id])->result_array();

                            ?>
                          <?php $n = 1;
                            $ii = 0;
                            ?>
                          <?php foreach ($strategicList as $key => $data) {
                                $ii += 1;
                            ?>
                              <tr>
                                  <td><?php echo $n++; ?></td>
                                  <td><?php echo $data['Goal_name']; ?></td>

                                  <td>
                                      <?php $check_indic_project = $this->db->get_where('indic_project',['Goal_id'=> $data['Goal_id']])->row_array(); 
                                        if (empty($check_indic_project)) {
                                      ?>
                                       <a href="" data-toggle="modal" data-target="#modal-default<?php echo $data['Goal_id']; ?>" style="display: inline-block;"><button class="btn btn-success">เพิ่มตัวชี้วัด</button></a>
                                        
                                      <?php }else{ ?>
                                        <a style="display: inline-block;"><button class="btn btn-success" disabled>เพิ่มตัวชี้วัด</button></a>
                                      <?php } ?>
                                      <div class="modal fade" id="modal-default<?php echo $data['Goal_id']; ?>" style="display: none;" aria-hidden="true">
                                        
                              <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title"><?php echo $data['Goal_name']; ?></h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>

                                        <form action="indicproject_success" method="POST">
                                          <div class="modal-body">
                                                <input type="hidden" name="id" value="<?php echo $id; ?>">
                                                <input type="hidden" name="goal_id" value="<?php echo $data['Goal_id']; ?>">
                                                <div class="row my-2 mx-2 mb-4" id="gold<?php echo $ii; ?>">


                                                    <div class="form-group" style="text-align:right; width:100%;">
                                                        <button type="button" class="btn btn-info btn_indic" id="indic_<?php echo $ii; ?>" style="color:#fff;"><i class="fa fa-plus"></i> เพิ่มตัวชี้วัด</button>
                                                    </div>

                                                    <div class="form-group" style="width:100%;">
                                                        <p for="" class="col-form-label" style="display:inline-block; width: 15%;">ตัวชี้วัด :</p>
                                                        <div class="" style="display:inline-block; width: 80%;">
                                                            <input type="text" id="indic_name<?php echo $ii; ?>" name="indic_name[]" class="form-control" required>
                                                        </div>

                                                    </div>

                                                    <div class="form-group" style="width:100%;">
                                                        <p for="" class="col-form-label" style="display:inline-block; width: 15%;">หน่วยนับ :</p>
                                                        <div class="" style="display:inline-block; width: 80%;">
                                                            <input type="text" id="unit<?php echo $ii; ?>" name="unit[]" class="form-control" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group" style="width:100%;">
                                                        <p for="" class="col-form-label" style="display:inline-block; width: 20%;">ค่าเป้าหมาย :</p>
                                                        <div class="" style="display:inline-block; width: 75%;">
                                                            <input type="text" id="cost<?php echo $ii; ?>" name="cost[]" class="form-control" required>
                                                        </div>
                                                    </div>

                                                </div>
                                          </div>
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                                                    <button type="submit" class="btn btn-warning">บันทึก</button>
                                                </div>
                                          </form>



                                     
                                    </div>
                                </div>
                            </div>
                                  </td>

                              </tr>
                              

                              
                             


                              <script type="text/javascript">
                                  //เป้าประสงค์
                                  $(document).ready(function() {
                                      $("#indic_<?php echo $ii; ?>").click(function() {
                                          $('#gold<?php echo $ii; ?>').append('<div class="row my-2 mx-2 mb-4 indic_project1_remove<?php echo $ii; ?>"><div class="form-group" style="text-align:right; width:100%;"><button type="button" class="btn btn-danger btn_indic" id="indic_remove_<?php echo $ii; ?>" style="color:#fff;"><i class="fa fa-times"></i> ลบตัวชี้วัด</button></div><div class="form-group" style="width:100%;"><p for="" class="col-form-label" style="display:inline-block; width: 15%;">ตัวชี้วัด :</p><div class="" style="display:inline-block; width: 80%;"><input type="text" id="indic_name<?php echo $ii; ?>" name="indic_name[]" class="form-control" required></div></div><div class="form-group" style="width:100%;"><p for="" class="col-form-label" style="display:inline-block; width: 15%;">หน่วยนับ :</p><div class="" style="display:inline-block; width: 80%;"><input type="text" id="unit<?php echo $ii; ?>" name="unit[]" class="form-control" required></div></div><div class="form-group" style="width:100%;"><p for="" class="col-form-label" style="display:inline-block; width: 20%;">ค่าเป้าหมาย :</p><div class="" style="display:inline-block; width: 75%;"><input type="text" id="cost<?php echo $ii; ?>" name="cost[]" class="form-control" required></div></div></div>');
                                      });

                                      $("body").on("click", "#indic_remove_<?php echo $ii; ?>", function(e) {
                                          $(this).parents('.indic_project1_remove<?php echo $ii; ?>').remove();
                                      });

                                  });
                              </script>
                          <?php } ?>
                      </tbody>
                  </table>
              </div>
              <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </section>
      <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



  <script type="text/javascript">
      $(document).ready(function() {

          $("#stra1").click(function() {
              $('.modal .modal-dialog .modal-content form .modal-body #straBT').append('<?php $this->load->view('option/stra1'); ?>');
          });

      });
  </script>
  <script type="text/javascript">
      $(document).ready(function() {

          $("#btn1").click(function() {
              $('.modal .modal-dialog .modal-content form .modal-body #meas').append('<?php $this->load->view('option/measure1'); ?>');
          });

      });
  </script>