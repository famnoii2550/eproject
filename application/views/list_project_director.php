  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<section class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1>จัดการข้อมูลโครงการ</h1>
  				</div>
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
  					</ol>
  				</div>
  			</div>
  		</div><!-- /.container-fluid -->
  	</section>

  	<!-- Main content -->
  	<section class="content">

  		<!-- Default box -->
  		<div class="card">
  			<div class="card-header">
  				<h3 class="card-title">รายชื่อโครงการ</h3>
  				<? echo $data; ?>
  				<div class="card-tools">
  					<!-- <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> -->
  				</div>
  			</div>
  			<div class="card-body">
  				<div class="row">
  					<div class="col-6 text-right row">
  						เลือกปีงบประมาณ :
  						<select class="form-control">
  							<option value="2563">2563</option>
  							<option value="2562">2562</option>
  							<option value="2561">2561</option>
  							<option value="2560">2560</option>
  							<option value="2559">2559</option>
  							<option value="2558">2558</option>
  							<option value="2557">2557</option>
  						</select>
  					</div>
  					<div class="col-6 text-right row">สถานะ :
  						<select class="form-control">
  							<option value="0" selected="">ทั้งหมด</option>
  							<option value="1">รอหัวหน้าฝ่ายพิจารณา</option>
  							<option value="2">รอเจ้าหน้าที่แผนตรวจสอบ</option>
  							<option value="3">รออนุมัติจากผู้บริหาร</option>
  							<option value="4">อนุมัติ</option>
  							<option value="10">รอเจ้าหน้าที่แผนตรวจสอบปิดโครงการ</option>
  							<option value="11">รอผู้บริหารอนุมัติปิดโครงการ</option>
  							<option value="12">ปิดโครงการ/เสร็จตามระยะเวลา</option>
  							<option value="13">ปิดโครงการ/ไม่เป็นไปตามระยะเวลา</option>
  							<option value="14">ปิดโครงการ/ขอเลื่อน</option>
  							<option value="15">ปิดโครงการ/ขอยกเลิก</option>
  							<option value="20">แก้ไขโครงการ</option>
  							<option value="21">แก้ไขเอกสารประเมินโครงการ</option>
  						</select>
  					</div>
  				</div>
  				<table class="table table-striped table-bordered table-hover">
  					<thead>
  						<tr role="row">
  							<th rowspan="2" colspan="1" style="width: 15px;">#</th>
  							<th rowspan="2" colspan="1" style="width: 200px;">ชื่อโครงการ</th>
  							<th rowspan="2" colspan="1" style="width: 140px;">สถานะ</th>
  							<th rowspan="2" colspan="1" style="width: 102px;">สถานะการจัดซื้อจัดจ้าง</th>
  							<th rowspan="1" colspan="1">
  							</th>
  							<th colspan="4" rowspan="1">ดูรายงานความก้าวหน้า</th>
  						</tr>
  						<tr role="row">
  							<th rowspan="1" colspan="1" style="width: 102px;">ไตรมาส 1</th>
  							<th rowspan="1" colspan="1" style="width: 102px;">ไตรมาส 2</th>
  							<th rowspan="1" colspan="1" style="width: 102px;">ไตรมาส 3</th>
  							<th rowspan="1" colspan="1" style="width: 102px;">ไตรมาส 4</th>
  						</tr>
  					</thead>
  					<tbody id="project">
  						<tr class="odd">
  							<td class=" ">1</td>
  							<td style="text-align:left" class=" "><a href="<?= base_url(); ?>/home/list_project_detail_director">เปิด FINN Space</a></td>
  							<td class=" ">
  								<span class="label label-default">รอเจ้าหน้าที่แผนตรวจสอบ</span>
  							</td>
  							<td class=" ">
  								<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-default"><i class="fas fa-eye"> ดูสถานะ</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class="">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class="">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  						</tr>
  						<tr class="even">
  							<td class=" ">2</td>
  							<td style="text-align:left" class=" "><a href="<?= base_url(); ?>/home/list_project_detail_director">โครงการส่งเสริมสุขภาพและความสัมพันธ์ของบุคลากร</a></td>
  							<td class=" ">
  								<span class="label label-default">รออนุมัติจากผู้บริหาร</span>
  							</td>
  							<td class=" ">

  								<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-default"><i class="fas fa-eye"> ดูสถานะ</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  						</tr>
  						<tr class="odd">
  							<td class=" ">3</td>
  							<td style="text-align:left" class=" "><a href="<?= base_url(); ?>/home/list_project_detail_director">โครงการปรับปรุงและเพิ่มประสิทธิภาพระบบรักษาความปลอดภัยเว็บไซต์</a></td>
  							<td class=" ">
  								<span class="label label-default">รออนุมัติจากผู้บริหาร</span>
  							</td>
  							<td class=" ">

  								<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-default"><i class="fas fa-eye"> ดูสถานะ</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  						</tr>
  						<tr class="even">
  							<td class=" ">4</td>
  							<td style="text-align:left" class=" "><a href="<?= base_url(); ?>/home/list_project_detail_director">อบรม ISO 9001:2015 Internal Auditor</a></td>
  							<td class=" ">
  								<span class="label label-primary">รอเจ้าหน้าที่แผนตรวจสอบปิดโครงการ</span>
  							</td>
  							<td class=" ">

  								<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-default"><i class="fas fa-eye"> ดูสถานะ</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<a href="<?= base_url(); ?>home/data_estimate_project_director" class="btn btn-warning btn-xs"><i class="fas fa-eye"></i> ดูรายงาน </a>
  							</td>
  						</tr>
  						<tr class="odd">
  							<td class=" ">5</td>
  							<td style="text-align:left" class=" "><a href="<?= base_url(); ?>/home/list_project_detail_director">อบรม ISO 9001:2015 Requirement &amp; Implementation</a></td>
  							<td class=" ">
  								<span class="label label-primary">รอเจ้าหน้าที่แผนตรวจสอบปิดโครงการ</span>
  							</td>
  							<td class=" ">

  								<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-default"><i class="fas fa-eye"> ดูสถานะ</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  						</tr>
  						<tr class="even">
  							<td class=" ">6</td>
  							<td style="text-align:left" class=" "><a href="<?= base_url(); ?>/home/list_project_detail_director">โครงการปรับปรุงอุปกรณ์จ่ายกระแสไฟฟ้าห้องเครือข่าย มจพ.วิทยาเขตระยอง</a></td>
  							<td class=" ">
  								<span class="label label-info">อนุมัติ</span>
  							</td>
  							<td class=" ">

  								<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#ModalAddComment" data-comment="งานพัสดุได้ออกใบสั่งซื้อ เลขที่ 102/06 ลงวันที่ 27 พฤศจิกายน 2562 ครบกำหนดส่งมอบวันที่ 26 มกราคม 2563" data-project_id="220"><i class="fas fa-eye"> ดูสถานะ</i></button>
  							</td>
  							<td class=" ">
  								<a href="<?= base_url(); ?>home/view_report_director" class="btn btn-warning btn-xs"><i class="fas fa-eye"></i> ดูรายงาน </a>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  						</tr>
  						<tr class="odd">
  							<td class=" ">7</td>
  							<td style="text-align:left" class=" "><a href="<?= base_url(); ?>/home/list_project_detail_director">โครงการปรับปรุงประสิทธิภาพเครื่องคอมพิวเตอร์แม่ข่าย สำหรับการให้บริการระบบยืนยันตัวตน ICIT Account</a></td>
  							<td class=" ">
  								<span class="label label-info">อนุมัติ</span>
  							</td>
  							<td class=" ">

  								<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#ModalAddComment" data-comment="เปิดซองและพิจารณาผลวันที่ 18 ธันวาคม 2562" data-project_id="219"><i class="fas fa-eye"> ดูสถานะ</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  						</tr>
  						<tr class="even">
  							<td class=" ">8</td>
  							<td style="text-align:left" class=" "><a href="<?= base_url(); ?>/home/list_project_detail_director">โครงการผู้บริหารพบผู้ปฏิบัติงาน</a></td>
  							<td class=" ">
  								<span class="label label-primary">รอเจ้าหน้าที่แผนตรวจสอบปิดโครงการ</span>
  							</td>
  							<td class=" ">

  								<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-default"><i class="fas fa-eye"> ดูสถานะ</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  						</tr>
  						<tr class="odd">
  							<td class=" ">9</td>
  							<td style="text-align:left" class=" "><a href="<?= base_url(); ?>/home/list_project_detail_director">โครงการการจัดการความรู้ (KM) ของสำนักคอมพิวเตอร์ฯ</a></td>
  							<td class=" ">
  								<span class="label label-info">อนุมัติ</span>
  							</td>
  							<td class=" ">

  								<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-default"><i class="fas fa-eye"> ดูสถานะ</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  						</tr>
  						<tr class="even">
  							<td class=" ">10</td>
  							<td style="text-align:left" class=" "><a href="<?= base_url(); ?>/home/list_project_detail_director">โครงการประเมินและตรวจติดตามคุณภาพภายในสำนักคอมพิวเตอร์ฯ</a></td>
  							<td class=" ">
  								<span class="label label-info">อนุมัติ</span>
  							</td>
  							<td class=" ">

  								<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-default"><i class="fas fa-eye"> ดูสถานะ</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  							<td class=" ">
  								<button class="btn btn-default btn-xs" disabled=""><i class="fas fa-eye"> ดูรายงาน</i></button>
  							</td>
  						</tr>
  					</tbody>
  				</table>
  			</div>
  			<!-- /.card-body -->
  		</div>
  		<!-- /.card -->

  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
  	<div class="modal-dialog">
  		<div class="modal-content">
  			<div class="modal-header">
  				<h4 class="modal-title">สถานะการจัดซื้อจัดจ้าง</h4>
  				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  					<span aria-hidden="true">×</span>
  				</button>
  			</div>
  			<div class="modal-body">
  				<div class="row my-2 mx-2 mb-4">
  					<div class="col-12">
  						<div class="form-group">
  							<textarea class="form-control" placeholder="สถานะการจัดซื้อจัดจ้าง" style="height:150px;" disabled=""></textarea>
  						</div>
  					</div>
  				</div>
  			</div>
  			<div class="modal-footer justify-content-end">
  				<button type="button" class="btn btn-warning" data-dismiss="modal">ปิด</button>
  			</div>
  		</div>
  		<!-- /.modal-content -->
  	</div>
  	<!-- /.modal-dialog -->
  </div>