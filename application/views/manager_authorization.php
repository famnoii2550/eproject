  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>หน่วยงาน</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">ข้อมูลจัดการหน่วยงาน</h3>
          <div class="card-tools">
            <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> -->
          </div>
        </div>
        <div class="card-body">

          <div class="text-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addUserModal">เพิ่มหน่วยงาน</button>
          </div>
          <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="exampleModalLabel">เพิ่มหน่วยงาน</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form action="add_department" method="POST">
                  <div class="modal-body">

                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">หน่วยงาน: <span style="color:red;">*</span></label>
                      <input type="text" name="department" class="form-control" id="recipient-name" required>
                    </div>

                  </div>
                  <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                    <button type="submit" class="btn btn-warning">บันทึก</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <table id="table" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>หน่วยงานทั้งหมด</th>
                <th>เครื่องมือ</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($department as $departmentDetail) {

              ?>
                <tr>
                  <td><?php echo $departmentDetail['Department']; ?></td>
                  <td>
                    <a href="" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modal-default<?php echo $departmentDetail['Department_id']; ?>"><i class="fas fa-pen"></i> แก้ไข </a>
                    <a href="" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-default-delete<?php echo $departmentDetail['Department_id']; ?>"><i class="fas fa-trash"></i> ลบ </a>
                  </td>
                </tr>

                <div class="modal fade" id="modal-default<?php echo $departmentDetail['Department_id']; ?>" style="display: none;" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel">แก้ไขหน่วยงาน</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <form action="edit_department" method="POST">
                        <div class="modal-body">
                          <input type="hidden" name="department_id" value="<?php echo $departmentDetail['Department_id']; ?>">
                          <div class="form-group">
                            <label for="recipient-name" class="col-form-label">หน่วยงาน: <span style="color:red;">*</span></label>
                            <input type="text" name="department" class="form-control" id="recipient-name" value="<?php echo $departmentDetail['Department']; ?>" required>
                          </div>

                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                          <button type="submit" class="btn btn-warning">บันทึก</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="modal-default-delete<?php echo $departmentDetail['Department_id']; ?>" style="display: none;" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">ลบหน่วยงาน</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>
                      <form action="delete_department" method="post">
                        <input type="hidden" name="department_id" value="<?php echo $departmentDetail['Department_id']; ?>">
                        <div class="modal-body">
                          <div class="row my-2 mx-2 mb-4">

                            <div class="col-12">
                              <div class="form-group">
                                <label class="text-center" style="display: block;">ท่านต้องการลบข้อมูล ใช่ไหม ?</label>

                              </div>
                            </div>

                          </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                          <button type="submit" class="btn btn-danger">ลบ</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>

              <?php } ?>

            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->