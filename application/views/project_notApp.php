  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
          <div class="container-fluid">
              <div class="row mb-2">
                  <div class="col-sm-6">
                      <h1>โครงการที่ไม่อนุมัติ</h1>
                  </div>
                  <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                          <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                      </ol>
                  </div>
              </div>
          </div><!-- /.container-fluid -->
      </section>
      <!-- Main content -->
      <section class="content">
          <!-- Default box -->
          <div class="card">
              <div class="card-header">
                  <!-- <button class="btn btn-info" style="float:right;" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus-circle"></i> สร้างประเด็นยุทธ์ศาสตร์ใหม่</button> -->
                  <div style="background-color: rgba(0,0,0,.05); padding:15px;">
                      <form action="project_NotApp" method="get">
                          <div class="row">
                              <div class="col-5">
                                  <span>เลือกปีงบประมาณ</span>
                                  <select name="year" class="form-control" style="width:35%; display: inline-block;">
                                      <?php
                                        $Date = date('Y-m-d');
                                        $Date10 = date('Y', strtotime($Date . ' + 10 years'));
                                        for ($x = date('Y'); $x <= $Date10; $x++) { ?>
                                          <option value="<?php echo $x; ?>"> <?php echo $x + 543; ?></option>
                                      <?php } ?>
                                  </select>
                                  <button type="submit" class="btn btn-primary" style="margin-bottom: 3px;"><i class="fa fa-search"></i> ค้นหา</button>
                              </div>
                          </div>
                      </form>
                  </div>
                  <div class="card-tools">

                  </div>
              </div>
              <div class="card-body">
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                          <tr>
                              <th class="text-center">ชื่อโครงการ</th>
                              <th class="text-center">ชื่อผู้ไม่อนุมัติโครงการ</th>
							  <th class="text-center">สิทธิ์</th>
                              <th class="text-center">ฝ่ายงาน</th>
                              <th class="text-center">สถานะ</th>
                              <th class="text-center">เนื่องจาก</th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php foreach ($projects as $project) { ?>
                              <?php $comm = $this->db->order_by('Comment_id', 'DESC')->get_where('comment', ['Project_id' => $project['Project_id']])->row_array(); ?>
                              <?php $ac = $this->db->get_where('account', ['Account_id' => $comm['Account_id']])->row_array(); ?>
                              <?php $dpm = $this->db->get_where('department', ['Department_id' => $ac['Department_id']])->row_array(); ?>
                              <tr>
                                  <td><a href="project_list?PID=<?php echo base64_encode($project['Project_id']); ?>"><?php echo $project['Project_name']; ?></a></td>
                                  <td><?= $ac['Fname'] . $ac['Lname']; ?></td>
								  <td>
                                      <?php if ($comm['Role'] == "Manager") { ?>
                                          เจ้าหน้าที่แผน
                                      <?php } elseif ($comm['Role'] == "Supervisor") { ?>
                                          หัวหน้าฝ่าย
                                      <?php } elseif ($comm['Role'] == "Director") { ?>
                                          ผู้บริหาร
                                      <?php } ?>
                                  </td>
                                  <td><?= $dpm['Department']; ?></td>
                                  <td class="text-center"><span class="badge badge-danger" style="font-size: 13px;">ไม่อนุมัติ</span></td>
                                  <?php if (!empty($comm)) { ?>
                                      <td class="text-center"><?php echo $comm['Comment']; ?></td>
                                  <?php } else { ?>
                                      <td class="text-center">-</td>
                                  <?php } ?>
                              </tr>
                          <?php } ?>
                      </tbody>
                  </table>
              </div>
              <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </section>
      <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
      $(document).ready(function() {
          $('#example').DataTable();
      });
  </script>