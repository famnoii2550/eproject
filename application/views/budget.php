<?php
$month = array(
    '01'  => 'มกราคม', '02'  => 'กุมภาพันธ์', '03'  => 'มีนาคม',
    '04'  => 'เมษายน', '05'  => 'พฤษภาคม', '06'  => 'มิถุนายน',
    '07'  => 'กรกฎาคม', '08'  => 'สิงหาคม', '09'  => 'กันยายน',
    '10'  => 'ตุลาคม', '11'  => 'พฤศจิกายน', '12'  => 'ธันวาคม',
);
function thaiDate($date)
{
    list($date) = explode(' ', $date); // แยกวันที่ กับ เวลาออกจากกัน
    list($Y, $m, $d) = explode('-', $date); // แยกวันเป็น ปี เดือน วัน
    $Y = $Y + 543; // เปลี่ยน ค.ศ. เป็น พ.ศ.
    switch ($m) {
        case "01":
            $m = "ม.ค.";
            break;
        case "02":
            $m = "ก.พ.";
            break;
        case "03":
            $m = "มี.ค.";
            break;
        case "04":
            $m = "เม.ย.";
            break;
        case "05":
            $m = "พ.ค.";
            break;
        case "06":
            $m = "มิ.ย.";
            break;
        case "07":
            $m = "ก.ค.";
            break;
        case "08":
            $m = "ส.ค.";
            break;
        case "09":
            $m = "ก.ย.";
            break;
        case "10":
            $m = "ต.ค.";
            break;
        case "11":
            $m = "พ.ย.";
            break;
        case "12":
            $m = "ธ.ค.";
            break;
    }
    return $d . " " . $m . " " . $Y;
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>จัดการข้อมูลปีงบประมาน</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <!-- <button class="btn btn-info" style="float:right;" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus-circle"></i> สร้างประเด็นยุทธ์ศาสตร์ใหม่</button> -->
                <div style="background-color: rgba(0,0,0,.05); padding:15px;">
                    <form action="set_year" method="post">
                        <div style="display: inline-block; width:100%;">

                            <div style="display: inline-block; width: 50%;">
                                <span>เพิ่มปีงบประมาณ</span>
                                <select name="Fiscalyear" class="form-control" style="width:50%; display: inline-block;">
                                    <?php
                                    $Date = date('Y-m-d');
                                    $Date10 = date('Y', strtotime($Date . ' + 10 years'));
                                    $y_budget = $this->db->get('budget_year')->row_array();
                                    for ($x = date('Y'); $x <= $Date10; $x++) { ?>
                                        <option value="<?php echo $x; ?>" <?php if ($y_budget['budget_year'] == $x) {
                                                                                echo 'selected';
                                                                            } ?>> <?php echo $x + 543; ?></option>
                                    <?php } ?>
                                </select>
                                <button type="submit" class="btn btn-primary" style="margin-bottom: 3px;"><i class="fa fa-download"></i> ตั้งค่าเริ่มต้น</button>
                                <!-- <input type="text" name="Fiscalyear" class="form-control" style="width:50%; display: inline-block;" id="" placeholder="ตัวอย่าง 2559"> -->
                            </div>


                        </div>


                    </form>
                </div>
                <div class="card-tools">
                    <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> -->

                </div>
            </div>

            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ข้อมูลปีงบประมาณ</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <form action="budget_edit_com" method="post">
                    <!-- <div class="row my-2 mx-2 mb-4"> -->
                    <div class="form-group">
                        <input type="hidden" name="Strategic_Plan_id" value="<?php echo $budget['Strategic_Plan_id']; ?>">
                        <label for="recipient-name" class="col-form-label">ปีงบประมาณ: <span><?php echo $budget['Fiscalyear']; ?></span></label>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">ชื่อแผนงาน</label>
                        <input type="text" class="form-control" name="Strategic_Plan" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Strategic_Plan']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">ผ่านมติกรรมการบริหาย</label>
                        <div>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <th>ครั้งที่</th>
                                    <th>วันที่</th>
                                </thead>
                                <tbody>
                                    <td><input type="text" class="form-control" name="Director_of_time" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Director_of_time']; ?>"></td>
                                    <td><input type="date" class="form-control" name="Director_of_date" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Director_of_date']; ?>"></td>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">ผ่านมติกรรมการประจำ</label>
                        <div>
                            <table class="table table-striped table-bordered text-center">
                                <thead>
                                    <th>ครั้งที่</th>
                                    <th>วันที่</th>
                                </thead>
                                <tbody>
                                    <td><input type="text" class="form-control" name="Ref_of_time" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Ref_of_time']; ?>"></td>
                                    <td><input type="date" class="form-control" name="Ref_of_date" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Ref_of_date']; ?>"></td>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <!-- </div> -->

            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                <button type="submit" class="btn btn-warning">บันทึก</button>
            </div>
        </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">แผนยุทธศาสตร์</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="workplan_add" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">ชื่อแผนยุทธ์ศาสตร์</label>
                        <input type="text" class="form-control" name="strategic_plan">
                    </div>
                    <div id="straBT">
                        <div class="form-group">
                            <label for="">ประเด็นยุทธ์ศาสตร์</label>
                            <input type="text" class="form-control" name="strategic_name">
                        </div>
                        <div class="form-group">
                            <label for="">เป้าประสงค์ที่</label>
                            <input type="text" class="form-control" name="goal_name">
                        </div>
                        <div id="meas">
                            <div class="form-group">
                                <label for="">ตัวชี้วัด</label>
                                <input type="text" class="form-control" name="#">
                            </div>
                        </div>
                        <div class="form-group" style="text-align:center;">
                            <button type="button" class="btn btn-info" id="btn1" style="color:#fff;"><i class="fa fa-plus"></i> เพิ่มตัวชี้วัด</button>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">หน่วยนับ</label>
                                    <input type="text" class="form-control" name="#">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">ค่าเป้าหมาย</label>
                                    <input type="text" class="form-control" name="#">
                                </div>
                            </div>
                        </div>
                        <div id="goal">
                            <div class="form-group">
                                <label for="">กลยุทธ์</label>
                                <input type="text" class="form-control" name="tactic_name">
                            </div>
                        </div>
                        <div class="form-group" style="text-align:center;">
                            <button type="button" class="btn btn-success" style="color:#fff;" id="btn7"><i class="fa fa-plus"></i> เพิ่มกลยุทธ์</button>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group" style="text-align:center;">
                        <button type="button" class="btn btn-info" style="color:#fff;" id="stra1"><i class="fa fa-plus"></i> เพิ่มเป้าประสงค์</button>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times-circle"></i> ยกเลิก</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-download"></i> จัดเก็บ</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>