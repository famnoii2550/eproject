  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>กำหนดสิทธิผู้ใช้งาน</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">ข้อมูลผู้ใช้งาน</h3>
          <div class="card-tools">
            <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> -->
          </div>
        </div>
        <div class="card-body">
          <div class="text-left" style="display: -webkit-inline-box;">
            <select class="form-control" id="search_department" style="margin-right:5px;" onClick="search_department(this);">
              <option value="">ทั้งหมด</option>
              <?php foreach ($department as $departmentDetail) { ?>
                <option value="<?php echo $departmentDetail['Department_id']; ?>"><?php echo $departmentDetail['Department']; ?></option>
              <?php } ?>
            </select>
            <?php if (!empty($department)) { ?>
              <a href="search_department_list?id=<?php echo $department[0]['Department_id']; ?>" id="search_department_list"><button type="button" class="btn btn-success">ค้นหา</button></a>
            <?php } else { ?>
              <a href="" id="search_department_list"><button type="button" class="btn btn-success">ค้นหา</button></a>
            <?php } ?>
          </div>
          <div class="text-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addUserModal">เพิ่มบัญชีผู้ใช้งาน</button>
          </div>
          <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="exampleModalLabel">เพิ่มบัญชีผู้ใช้งาน</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>

                <form action="add_user" method="POST">
                  <div class="modal-body">

                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Username: <span style="color:red;">*</span></label>
                      <input type="text" name="username" class="form-control" id="search_user" required>
                    </div>

                    <!-- <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Password: <span style="color:red;">*</span></label>
                      <input type="password" name="password" class="form-control" id="recipient-name" required>
                    </div> -->

                    <div class="form-group text-center" id="check_notData" style="color:red; display:none;">
                      ** ไม่พบรายชื่อ **
                    </div>

                    <div class="form-group text-center" id="check_notData_loading" style="display:none;">

                    </div>

                    <div class="form-group" id="check_first_name" style="display:none;">
                      <label for="recipient-name" class="col-form-label">ชื่อ: <span style="color:red;">*</span></label>
                      <div class="form-control" id="recipient_first_name"></div>
                      <input type="hidden" name="first_name" class="form-control" id="recipient-name">
                    </div>

                    <div class="form-group" id="check_last_name" style="display:none;">
                      <label for="recipient-name" class="col-form-label">นามสกุล: <span style="color:red;">*</span></label>
                      <div class="form-control" id="recipient_last_name"></div>
                      <input type="hidden" name="last_name" class="form-control" id="recipient-name">
                    </div>

                    <div class="form-group" id="check_email" style="display:none;">
                      <label for="recipient-name" class="col-form-label">Email: <span style="color:red;">*</span></label>
                      <div class="form-control" id="recipient_email"></div>
                      <input type="hidden" name="email" class="form-control" id="recipient-name">
                    </div>

                    <hr>

                    <div class="form-group" id="depart_hide">
                      <label for="recipient-name" class="col-form-label"> หน่วยงานของผู้ใช้งาน: <span style="color:red;">*</span></label>
                      <select class="form-control" name="department_one" required>
                        <option value="nodata">--เลือก--</option>
                        <?php foreach ($department as $departmentDetail) { ?>
                          <option value="<?php echo $departmentDetail['Department_id']; ?>"><?php echo $departmentDetail['Department']; ?></option>
                        <?php } ?>
                      </select>
                    </div>



                    <div class="col-12">
                      <div>
                        <label>กำหนดสิทธิให้กับผู้ใช้งาน</label>
                      </div>
                    </div>

                    <div class="col-12">
                      <div style="">
                        <div class="col-12">
                          <input type="radio" name="director_responsible" id="add_director" onChange="add_checkDirector(this);" value="director">
                          <div style="display: inline-block;">ผู้บริหาร</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-12">
                      <div style="">
                        <div class="col-12">
                          <input type="radio" name="director_responsible" id="add_responsible" onChange="add_checkResponsible(this);" checked value="responsible">
                          <div style="display: inline-block;">สิทธิ์ของผู้ใช้ในหน่วยงาน</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-12 add_none">
                      <div style="display:flex; justify-content: space-around;">
                        <div class="col-4">
                          <input type="checkbox" class="single-checkbox" name="manager" id="add_manager">
                          <div style="display: inline-block;">เจ้าหน้าที่แผน</div>
                        </div>

                        <div class="col-4">
                          <input type="checkbox" class="single-checkbox" name="supervisor" id="add_supervisor">
                          <div style="display: inline-block;">หัวหน้าฝ่าย</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-12 add_none">
                      <div style="display:flex; justify-content: space-around;">
                        <div class="col-4">
                          <input type="checkbox" class="single-checkbox" name="supplies" id="add_supplies">
                          <div style="display: inline-block;">เจ้าหน้าที่พัสดุ</div>
                        </div>
                        <div class="col-4">
                          <input type="checkbox" class="single-checkbox" name="admin" id="add_admin">
                          <div style="display: inline-block;">ผู้ดูแลระบบ</div>
                        </div>
                      </div>
                    </div>

                  </div>
                  <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                    <button type="submit" class="btn btn-warning" id="submitUser">บันทึก</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <table id="table" class="table table-bordered table-striped text-center">
            <thead>
              <tr>
                <td>ลำดับ</td>
                <td>username</td>
                <th>ชื่อ-สกุล</th>
                <th>หน่วยงานของผู้ใช้งาน</th>
                <th>สิทธิ</th>
                <th>เครื่องมือ</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($account as $accountDetail) {

                $department_account = $this->db->get_where('department', ['Department_id' => $accountDetail['Department_id']])->row_array();
              ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?php echo $accountDetail['Username']; ?></td>
                  <td><?php echo $accountDetail['Fname'] . " " . $accountDetail['Lname']; ?></td>
                  <td><?php echo $department_account['Department']; ?></td>
                  <td>
                    <?php if ($accountDetail['Director'] == 1) { ?>
                      ผู้บริหาร /
                    <?php } ?>

                    <?php if ($accountDetail['Manager'] == 1) { ?>
                      เจ้าหน้าที่แผน /
                    <?php } ?>

                    <?php if ($accountDetail['Supervisor'] == 1) { ?>
                      หัวหน้าฝ่าย /
                    <?php } ?>

                    <?php if ($accountDetail['Supplies'] == 1) { ?>
                      เจ้าหน้าที่พัสดุ /
                    <?php } ?>

                    <?php if ($accountDetail['Admin'] == 1) { ?>
                      ผู้ดูแลระบบ /
                    <?php } ?>
                  </td>
                  <td>
                    <a href="" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modal-default<?php echo $accountDetail['Account_id']; ?>"><i class="fas fa-pen"></i> แก้ไข </a>
                    <a href="" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-default-delete<?php echo $accountDetail['Account_id']; ?>"><i class="fas fa-trash"></i> ลบ </a>
                  </td>
                </tr>

                <div class="modal fade" id="modal-default<?php echo $accountDetail['Account_id']; ?>" style="display: none;" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">แก้ไขสิทธิผู้ใช้งาน</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>
                      <form action="edit_user" method="post">
                        <input type="hidden" name="account_id" value="<?php echo $accountDetail['Account_id']; ?>">
                        <div class="modal-body">
                          <div class="row my-2 mx-2 mb-4">
                            <div class="col-12">
                              <div class="form-group" id="hidden_ab<?php echo $accountDetail['Account_id']; ?>" style="<?php echo $accountDetail['Director'] == 1 ? 'display:none' : ''; ?>">
                                <label>หน่วยงานของผู้ใช้งาน</label>
                                <select class="form-control" name="department_one" required>
                                  <?php foreach ($department as $departmentDetail) { ?>
                                    <option value="<?php echo $departmentDetail['Department_id']; ?>" <?php if ($departmentDetail['Department_id'] == $accountDetail['Department_id']) {
                                                                                                        echo "selected";
                                                                                                      } ?>><?php echo $departmentDetail['Department']; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>



                            <div class="col-12">
                              <div>
                                <label>กำหนดสิทธิให้กับผู้ใช้งาน</label>
                              </div>
                            </div>

                            <div class="col-12">
                              <div style="">
                                <div class="col-12">
                                  <input type="radio" name="director_responsible" id="add_director" onload="checkResponsible<?php echo $accountDetail['Account_id']; ?>();" onChange="add_checkDirector<?php echo $accountDetail['Account_id']; ?>(this);" <?php if ($accountDetail['Director'] == 1) {
                                                                                                                                                                                                                                                              echo "checked";
                                                                                                                                                                                                                                                            } ?> value="director">
                                  <div style="display: inline-block;">ผู้บริหาร</div>
                                </div>
                              </div>
                            </div>

                            <div class="col-12">
                              <div style="">
                                <div class="col-12">
                                  <input type="radio" name="director_responsible" id="add_responsible" onChange="add_checkResponsible<?php echo $accountDetail['Account_id']; ?>(this);" <?php if ($accountDetail['Responsible'] == 1) {
                                                                                                                                                                                            echo "checked";
                                                                                                                                                                                          } ?> value="responsible">
                                  <div style="display: inline-block;">สิทธิ์ของผู้ใช้ในหน่วยงาน</div>
                                </div>
                              </div>
                            </div>

                            <div class="col-12 add_none<?php echo $accountDetail['Account_id']; ?>">
                              <div style="display:flex; justify-content: space-around;">
                                <div class="col-4">
                                  <input type="checkbox" class="single-checkbox<?php echo $accountDetail['Account_id']; ?>" name="manager" id="add_manager" <?php if ($accountDetail['Manager'] == 1) {
                                                                                                                                                              echo "checked";
                                                                                                                                                            } ?>>
                                  <div style="display: inline-block;">เจ้าหน้าที่แผน</div>
                                </div>

                                <div class="col-4">
                                  <input type="checkbox" class="single-checkbox<?php echo $accountDetail['Account_id']; ?>" name="supervisor" id="add_supervisor" <?php if ($accountDetail['Supervisor'] == 1) {
                                                                                                                                                                    echo "checked";
                                                                                                                                                                  } ?>>
                                  <div style="display: inline-block;">หัวหน้าฝ่าย</div>
                                </div>
                              </div>
                            </div>

                            <div class="col-12 add_none<?php echo $accountDetail['Account_id']; ?>">
                              <div style="display:flex; justify-content: space-around;">
                                <div class="col-4">
                                  <input type="checkbox" class="single-checkbox<?php echo $accountDetail['Account_id']; ?>" name="supplies" id="add_supplies" <?php if ($accountDetail['Supplies'] == 1) {
                                                                                                                                                                echo "checked";
                                                                                                                                                              } ?>>
                                  <div style="display: inline-block;">เจ้าหน้าที่พัสดุ</div>
                                </div>
                                <div class="col-4">
                                  <input type="checkbox" class="single-checkbox<?php echo $accountDetail['Account_id']; ?>" name="admin" id="add_admin" <?php if ($accountDetail['Admin'] == 1) {
                                                                                                                                                          echo "checked";
                                                                                                                                                        } ?>>
                                  <div style="display: inline-block;">ผู้ดูแลระบบ</div>
                                </div>
                              </div>
                            </div>

                          </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                          <button type="submit" class="btn btn-warning">บันทึก</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>

                <script type="text/javascript">
                  function add_checkDirector<?php echo $accountDetail['Account_id']; ?>(check) {
                    if (check.checked == true) {
                      $("#add_director").attr('checked', true);
                      $(".add_none<?php echo $accountDetail['Account_id']; ?>").css('display', 'none');
                      $("#add_manager").prop('checked', false);
                      $("#add_supervisor").prop('checked', false);
                      $("#add_supplies").prop('checked', false);
                      $("#add_admin").prop('checked', false);
                      $('#hidden_ab<?php echo $accountDetail['Account_id']; ?>').css('display', 'none');
                    }

                  }

                  function add_checkResponsible<?php echo $accountDetail['Account_id']; ?>(check) {
                    if (check.checked == true) {
                      $("#add_responsible").attr('checked', true);
                      $(".add_none<?php echo $accountDetail['Account_id']; ?>").css('display', 'block');
                      $('#hidden_ab<?php echo $accountDetail['Account_id']; ?>').css('display', 'block');

                    }
                  }

                  <?php if ($accountDetail['Director'] == 1) { ?>
                    $(".add_none<?php echo $accountDetail['Account_id']; ?>").css('display', 'none');
                  <?php } ?>

                  $("input.single-checkbox<?php echo $accountDetail['Account_id']; ?>").click(function() {
                    var bol = $("input.single-checkbox<?php echo $accountDetail['Account_id']; ?>:checked").length >= 2;
                    $("input.single-checkbox<?php echo $accountDetail['Account_id']; ?>").not(":checked").attr("disabled", bol);
                  });

                  $(document).ready(function() {
                    var bolt = $("input.single-checkbox<?php echo $accountDetail['Account_id']; ?>:checked").length >= 2;
                    $("input.single-checkbox<?php echo $accountDetail['Account_id']; ?>").not(":checked").attr("disabled", bolt);
                  });
                </script>


                <div class="modal fade" id="modal-default-delete<?php echo $accountDetail['Account_id']; ?>" style="display: none;" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">ลบสิทธิผู้ใช้งาน</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>
                      <form action="delete_user" method="post">
                        <input type="hidden" name="account_id" value="<?php echo $accountDetail['Account_id']; ?>">
                        <div class="modal-body">
                          <div class="row my-2 mx-2 mb-4">

                            <div class="col-12">
                              <div class="form-group">
                                <label class="text-center" style="display: block;">ท่านต้องการลบข้อมูล ใช่ไหม ?</label>

                              </div>
                            </div>

                          </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                          <button type="submit" class="btn btn-danger">ลบ</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>

              <?php } ?>

            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
    function add_checkDirector(check) {
      if (check.checked == true) {
        $("#add_director").attr('checked', true);
        $(".add_none").css('display', 'none');
        $("#add_manager").prop('checked', false);
        $("#add_supervisor").prop('checked', false);
        $("#add_supplies").prop('checked', false);
        $("#add_admin").prop('checked', false);
        $("#depart_hide").css('display', 'none');
      }

    }

    function add_checkResponsible(check) {
      if (check.checked == true) {
        $("#add_responsible").attr('checked', true);
        $(".add_none").css('display', 'block');
        $("#depart_hide").css('display', 'block');
      }
    }



    $("input.single-checkbox").click(function() {
      var bol = $("input.single-checkbox:checked").length >= 2;
      $("input.single-checkbox").not(":checked").attr("disabled", bol);
    });

    function search_department(value) {
      let valueDepartment = $("#search_department").val();
      $('#search_department_list').replaceWith('<a href="search_department_list?id=' + valueDepartment + '" id="search_department_list"><button type="button" class="btn btn-success">ค้นหา</button></a>');

    }

    function search_user() {
      $("#search_user").keyup(function(e) {
        $('#check_notData').css('display', 'none');
        $('#check_notData_loading').css('display', 'none');
        $('#check_email').css('display', 'none');
        $('#check_first_name').css('display', 'none');
        $('#check_last_name').css('display', 'none');

        $('#recipient_first_name').html(null);
        $('#recipient_last_name').html(null);
        $('#recipient_email').html(null);

        $('input[name="first_name"]').val(null);
        $('input[name="lastname"]').val(null);
        $('input[name="email"]').val(null);

        let userValue = e.target.value;
        $.ajax({
          url: "search_user",
          method: "POST",
          data: {
            userValue: userValue
          },
          beforeSend: function() {
            $('#check_notData_loading').css('display', 'block').html('<i class="fas fa-sync-alt fa-spin" aria-hidden="true"></i> Loading...');
          },
          success: function(getData) {
            const result = JSON.parse(getData);
            $('#check_notData_loading').css('display', 'none');
            if (result.successfully === false) {
              $('#check_notData').css('display', 'block');

              $('#check_email').css('display', 'none');
              $('#check_first_name').css('display', 'none');
              $('#check_last_name').css('display', 'none');

              $('#recipient_first_name').html(null);
              $('#recipient_last_name').html(null);
              $('#recipient_email').html(null);

              $('input[name="first_name"]').val(null);
              $('input[name="lastname"]').val(null);
              $('input[name="email"]').val(null);
            }

            if (result.successfully === true) {
              $('#check_notData').css('display', 'none');

              $('#check_first_name').css('display', 'block');
              $('#check_last_name').css('display', 'block');
              $('#check_email').css('display', 'block');

              $('#recipient_first_name').html(result.dataList.firstname_en);
              $('#recipient_last_name').html(result.dataList.lastname_en);
              $('#recipient_email').html(result.dataList.email);

              $('input[name="first_name"]').val(result.dataList.firstname_en);
              $('input[name="last_name"]').val(result.dataList.lastname_en);
              $('input[name="email"]').val(result.dataList.email);
            }


          }
        });
      });
    }
    search_user();
  </script>