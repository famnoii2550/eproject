<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ระบบบริหารและประเมินผลโครงการ</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="assets/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <style>
        .login-logo img {
            width: 155px;
            height: 155px;
        }

        .login-logo a {
            font-size: 22px;
            font-weight: 900;
        }

        .btn-primary {
            background-color: #f77100;
            border-color: #f77100;
        }

        .btn-primary:hover,
        .btn-primary:focus,
        .btn-primary:active,
        .btn-primary.active,
        .open>.dropdown-toggle.btn-primary {
            background-color: #d56000;
            border-color: #d56000;
        }

        .btn-primary:not(:disabled):not(.disabled).active,
        .btn-primary:not(:disabled):not(.disabled):active,
        .show>.btn-primary.dropdown-toggle {
            background-color: #ff9742;
            border-color: #ff9742;
        }

        textarea:focus,
        input[type="text"]:focus,
        input[type="password"]:focus,
        input[type="datetime"]:focus,
        input[type="datetime-local"]:focus,
        input[type="date"]:focus,
        input[type="month"]:focus,
        input[type="time"]:focus,
        input[type="week"]:focus,
        input[type="number"]:focus,
        input[type="email"]:focus,
        input[type="url"]:focus,
        input[type="search"]:focus,
        input[type="tel"]:focus,
        input[type="color"]:focus,
        .uneditable-input:focus {
            border-color: #f77100;
            /* box-shadow: 0 1px 1px #f77100 inset, 0 0 8px #f77100;
      outline: 1 none; */
        }

        .login-card-body .input-group .form-control:focus~.input-group-append .input-group-text,
        .register-card-body .input-group .form-control:focus~.input-group-append .input-group-text {
            border-color: #f77100;
        }

        a {
            color: #f77100;
            text-decoration: none;
            background-color: transparent;
        }

        a:hover {
            color: #d56000;
            text-decoration: none;
        }

        .about-modal p {
            font-size: 14px;
            margin-top: 0;
            margin-bottom: 0;
            padding-right: 10px;
            padding-left: 10px;
        }

        .indent {
            text-indent: 2.0rem;
        }

        li {
            list-style-type: none;
        }

        .login-modal img {
            width: 125px;
            height: 125px;
        }
    </style>
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <div class="row justify-content-center">
                <img src="assets/img/logo.png" class="img-responsive">
            </div>
            <a href="#">ระบบบริหารและประเมินผลโครงการ</a>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">สำหรับบุคลากรและผู้บริหารในส่วนงาน</p>

                <form action="LoginMe" method="post" class="pb-2">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="Username" placeholder="ชื่อผู้ใช้งาน" required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" name="password" placeholder="รหัสผ่าน" required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block">เข้าสู่ระบบ</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <!-- /.social-auth-links -->
                <hr class="pb-2">
                <div class="row">
                    <p class="col mb-1">
                        <a href="" data-toggle="modal" data-target="#about">เกี่ยวกับ</a>
                    </p>
                    <p class="col d-flex justify-content-end mb-0">
                        <a href="#" class="text-center" data-toggle="modal" data-target="#contact">ติดต่อ</a>
                    </p>
                </div>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->

    <div class="modal fade" id="about">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">เกี่ยวกับ</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body about-modal">
                    <div class="login-modal">
                        <div class="row justify-content-center">
                            <img src="assets/img/logo.png" class="img-responsive">
                        </div>
                    </div>
                    <h3 class="text-center"><b>นโยบายการดำเนินงาน</b></h3>
                    <h4 class="text-center pt-4">กองแผนงาน</h4>
                    <p class="text-center"><strong>ปรัชญา : </strong>พัฒนาคน พัฒนาเทคโนโลยีสารสนเทศ</p>
                    <p class="text-center text-truncate"><strong>วิสัยทัศน์ : </strong>มุ่งมั่นบริการ สร้างสรรค์เทคโนโลยีสารสนเทศ สู่มาตรฐานสากล</p>
                    <h4 class="text-center pt-3">พันธกิจ</h4>
                    <li class="text-center">การให้บริการคอมพิวเตอร์และระบบเทคโนโลยีสารสนเทศเพื่อการเรียนการสอน และงานวิจัย</li>
                    <li class="text-center">การพัฒนาระบบงานคอมพิวเตอร์และสารสนเทศเพื่อการบริหารงานของสถาบัน</li>
                    <li class="text-center">การบริการวิชาการด้านคอมพิวเตอร์และเทคโนโลยีสารสนเทศ</li>
                    <h4 class="text-center pt-3">ปณิธาน</h4>
                    <p class="text-center">มุ่งเน้นการบริการทางด้านเทคโนโลยีสารสนเทศ
                        เพื่อตอบสนองความต้องการของนักศึกษา ตลอดจนบุคลากรของสถาบันในการสนับสนุน
                        การพัฒนาวิทยาศาสตร์และเทคโนโลยีให้อยู่ในระดับแนวหน้าของประเทศ
                        และเป็นที่ยอมรับในระดับชาติ</p>
                    <h4 class="text-center pt-3">อัตลักษณ์</h4>
                    <p class="text-center">ผู้นำการบริการด้านเทคโนโลยีสารสนเทศ</p>
                </div>
                <div class="modal-footer justify-content-end">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">ปิดหน้าต่าง</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="contact">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">ข้อมูลการติดต่อ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body about-modal">
                    <div class="login-modal">
                        <div class="row justify-content-center">
                            <img src="assets/img/logo.png" class="img-responsive">
                        </div>
                    </div>
                    <h3 class="text-center"><b>ข้อมูลการติดต่อ</b></h3>
                    <h4 class="text-center pt-4">สำนักคอมพิวเตอร์และเทคโนโลยีสารสนเทศ มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าพระนครเหนือ</h4>
                    <p class="text-center pt-3">1518 ถนนพิบูลสงคราม แขวงบางซื่อ เขตบางซื่อ กรุงเทพฯ 10800<br><br>
                        โทรศัพท์ (+662) 555 2000 ต่อ 2205<br><br>
                        โทรสาร (+662) 585 7945<br><br>
                        สำนักงาน ชั้น 5 อาคารอเนกประสงค์<br><br>
                        E-mail : anuchitc@kmutnb.ac.th<br><br>
                    </p>
                </div>
                <div class="modal-footer justify-content-end">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">ปิดหน้าต่าง</button>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="assets/js/adminlte.min.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    <?php if ($suss = $this->session->flashdata('save_ss2')) : ?>
        swal("Good job!", '<?php echo $suss; ?>', "success");
    <?php endif; ?>
    <?php if ($error = $this->session->flashdata('del_ss2')) : ?>
        swal("Fail !", '<?php echo $error; ?>', "error");
    <?php endif; ?>
</script>

</body>

</html>