<?php $quarter = base64_decode($this->input->get('quarter')); ?>
<script src="assets/js/thaibath.js" type="text/javascript" charset="utf-8"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>เขียนรายงานความก้าวหน้า ผู้รับผิดชอบโครงการ</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right"></ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <form action="">
            <div class="card">
                <div class="card-header">
                    <a href="#">เขียนรายงานความก้าวหน้า</a> /
                    <a href="estimate?PID=<?= base64_encode($PID); ?>&quarter=<?= base64_encode($quarter); ?>"> ปิดโครงการ</a>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <h5>เขียนรายงานความก้าวหน้า ไตรมาส <?= $quarter; ?></h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row container">
                        <label class="col-3" for="">ชื่อโครงการ : </label>
                        <div class="col-9">
                            <p><?= $project['Project_name']; ?></p>
                        </div>
                    </div>
                    <div class="row container">
                        <label class="col-3" for="">งบประมาณที่จัดสรร : </label>
                        <div class="col-9">
                            <p><?= number_format($project['Butget']); ?> (<?= $project['Butget_char']; ?>)</p>
                        </div>
                    </div>
                    <?php if ($quarter != 1) { ?>
                        <?php $sum_last = $this->db->get_where('project_report', ['Project_id' => $PID, 'Quarter <' => $quarter])->result_array(); ?>
                        <?php $emp = $project['Butget']; ?>
                        <?php foreach ($sum_last as $sum_last) { ?>
                            <?php $emp -= $sum_last['Used']; ?>
                        <?php } ?>
                        <div class="row container">
                            <label class="col-3" for="">งบประมาณที่จัดสรรที่เหลือใช้ : </label>
                            <div class="col-9">
                                <p><?= number_format($emp); ?> บาท</p>
                            </div>
                        </div>
                    <?php } ?>
                    <?php $charges_main = $this->db->get_where('charges_main', ['Project_id' => $PID])->row_array(); ?>
                    <?php $charges_sub = $this->db->get_where('charges_sub', ['Charges_Main_id' => $charges_main['Charges_Main_id']])->row_array(); ?>
                    <div class="row container">
                        <label class="col-3" for="">งบไตรมาสที่ <?= $quarter; ?> : </label>
                        <div class="col-9">
                            <?php if ($quarter == 1) { ?>
                                <p><?= (empty($charges_sub['Quarter_one'])) ? "0" : $charges_sub['Quarter_one']; ?></p>
                            <?php } elseif ($quarter == 2) { ?>
                                <p><?= (empty($charges_sub['Quarter_two'])) ? "0" : $charges_sub['Quarter_two']; ?></p>
                            <?php } elseif ($quarter == 3) { ?>
                                <p><?= (empty($charges_sub['Quarter_three'])) ? "0" : $charges_sub['Quarter_three']; ?></p>
                            <?php } else { ?>
                                <p><?= (empty($charges_sub['Quarter_four'])) ? "0" : $charges_sub['Quarter_four']; ?></p>
                            <?php } ?>
                        </div>
                    </div>
                    <div>
                        <div class="row container input_add form-group">
                            <label class="col-3" for="">ผลการใช้จ่าย : </label>
                            <div class="col-9">
                                <input type="number" class="form-control" value="" id="Used" name="Used" required>
                            </div>
                        </div>
                    </div>
                    <div class="row container">
                        <label class="col-3" for="">ผลตามตัวชี้วัด</label>
                        <div class="col-9">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">ตัวชี้วัด</th>
                                        <th class="text-center">เป้า</th>
                                        <th class="text-center">ผล</th>
                                        <th class="text-center">บรรลุตามตัวชี้วัด</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $Indic = $this->db->get_where('project_indic_success', ['Project_id' => $PID])->result_array(); ?>
                                    <?php foreach ($Indic as $indic) { ?>
                                        <tr>
                                            <td><?= $indic['Indic_success']; ?></td>
                                            <td class="text-center"><?= $indic['Cost']; ?></td>
                                            <td class="text-center">
                                                <input type="hidden" class="form-control" name="Indic_project_id[]" value="<?= $indic['project_indic_success_id']; ?>">
                                                <input type="text" class="form-control" name="Result[]" value="" required>
                                            </td>
                                            <td class="text-center"><input type="checkbox" name="achieve[]"></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row container">
                        <label class="col-3" for="">ขั้นตอนการดำเนินการ</label>
                        <div class="col-9">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width:50%">ขั้นตอนการดำเนินการ/รายการกิจกรรม</th>
                                        <th class="text-center">เริ่มต้น</th>
                                        <th class="text-center">สิ้นสุด</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($work_step as $work_step) { ?>
                                        <tr>
                                            <td>
                                                <p><?= $work_step['Step_name']; ?></p>
                                            </td>
                                            <td class="text-center">
                                                <div style="position: relative;">
                                                    <p><?= DateThai($work_step['Start']); ?></p>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div style="position: relative;">
                                                    <p><?= DateThai($work_step['Stop']); ?></p>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row container">
                        <label class="col-3" for="">มีการดำเนินงานตามระยะเวลาที่กำหนด :</label>
                        <div class="col-5">
                            <input type="checkbox" name="period_check" id="period_check" value="0">
                        </div>
                    </div><br>
                    <div class="row container detail_step">
                        <label class="col-3" for="">รายละเอียดความก้าวหน้า :</label>
                        <div class="col-5">
                            <input type="text" class="form-control detail1" name="detail[]" placeholder="รายละเอียดความก้าวหน้า">
                        </div>
                        <div class="col-4">
                            <button type="button" class="btn btn-primary" onclick="detail_name_add();"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มรายละเอียดความก้าวหน้า</button>
                            <!-- <button type="button" id="getButtonValue">Check</button> -->
                        </div>
                    </div>
                    <br>
                    <div class="row container problem_step">
                        <label class="col-3" for="">ปัญหา/อุปสรรค :</label>
                        <div class="col-5">
                            <input type="text" class="form-control problem1" name="problem[]" placeholder="ปัญหา / อุปสรรค">
                        </div>
                        <div class="col-4">
                            <button type="button" class="btn btn-primary" onclick="problem_name_add();"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มปัญหา/อุปสรรค</button>
                            <!-- <button type="button" id="getButtonValue2">Check</button> -->
                        </div>
                    </div>
                    <div class="row container" style="padding: 10px 0 0 0px;">
                        <div class="col-4">
                            <button type="button" onClick="send_data(this);" class="btn btn-info"><i class="fa fa-download" aria-hidden="true"></i> ส่งรายงานความก้าวหน้า</button>
                        </div>
                    </div>
                    <hr>

                </div>
                <!-- /.card-body -->
            </div>
        </form>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    var counter = 2;

    function detail_name_add() {

        let detail_step = '<div class="row container detail_step" id="detail_stepRemove">';
        detail_step += '<label class="col-3" for=""></label>';
        detail_step += '<div class="col-5"><br><input type="text" class="form-control detail' + counter + '" name="detail[]" placeholder="รายละเอียดความก้าวหน้า"></div>';
        detail_step += '<div class="col-4">';
        detail_step += '<br><button type="button" class="btn btn-danger" onClick="detail_step_remove(this);"><i class="fa fa-times" aria-hidden="true"></i> ลบ</button>';
        detail_step += '</div>';
        // detail_step += ' <td><button type="button" class="btn btn-danger" onClick="work_step_remove(this);"><i class="fa fa-times" aria-hidden="true"></i> ลบ</button></td>';


        $('.detail_step').last().after(detail_step);
        counter++;
    }

    // $("#getButtonValue").click(function() {
    //     var titles = $('input[name^=detail]').map(function(idx, elem) {
    //         return $(elem).val();
    //     }).get();
    //     console.log(titles);
    //     event.preventDefault();
    // });

    function detail_step_remove(e) {
        $(e).parents('#detail_stepRemove').remove();
    }

    var counters = 2;

    function problem_name_add() {

        let problem_step = '<div class="row container problem_step" id="problem_stepRemove">';
        problem_step += '<label class="col-3" for=""></label>';
        problem_step += '<div class="col-5"><br><input type="text" class="form-control problem' + counters + '" name="problem[]" placeholder="ปัญหา / อุปสรรค"></div>';
        problem_step += '<div class="col-4">';
        problem_step += '<br><button type="button" class="btn btn-danger" onClick="problem_step_remove(this);"><i class="fa fa-times" aria-hidden="true"></i> ลบ</button>';
        problem_step += '</div>';

        $('.problem_step').last().after(problem_step);
        counters++;
    }

    function problem_step_remove(e) {
        $(e).parents('#problem_stepRemove').remove();
    }
</script>
<script type="text/javascript">
    // let Used = [];
    let Result = [];
    let achieve = [];
    // let period_check = [];
    let Indic_project = [];
    let detail_data = [];
    let problem_data = [];
    let data = [];

    function send_data(e) {
        // Used = [];
        var Used = $('#Used').val();
        // Used.push(send_used);
        // console.log(Used);

        Result = [];
        let send_result = $('input[name^=Result]').map(function(idx, elem) {
            Result.push($(elem).val())
        }).get();
        // console.log(Result);

        achieve = [];
        let chk_achieve = document.getElementsByName('achieve[]');
        for (var i = 0; i < chk_achieve.length; i++) {
            if (chk_achieve[i].checked) {
                achieve.push(1);
            } else {
                achieve.push(0);
            }
        }
        // console.log(achieve);

        // period_check = [];
        let chk_period = document.getElementById("period_check").checked;
        if (chk_period == true) {
            var per_check = '1';
            // period_check.push(1);
        } else {
            var per_check = '0';
            // period_check.push(0);
        }
        // console.log(period_check);

        Indic_project = [];
        let send_indic = $('input[name^=Indic_project_id]').map(function(idx, elem) {
            Indic_project.push($(elem).val())
        }).get();
        // console.log(Indic_project);

        detail_data = [];
        let detail_input = $('.detail1').val();
        let send_detail = $('input[name^=detail]').map(function(idx, elem) {
            detail_data.push($(elem).val())
        }).get();
        // console.log(detail_data);

        problem_data = [];
        let problem1 = $('.problem1').val();
        let send_problem = $('input[name^=problem]').map(function(idx, elem) {
            problem_data.push($(elem).val())
        }).get();
        // console.log(problem_data);
        if (Result == '') {
            swal("คำเตือน!", 'กรุณากรอกผลตามตัวชี้วัด', "warning");
            return false;
        } else if (detail_input == '') {
            swal("คำเตือน!", 'กรุณากรอกรายละเอียดความก้าวหน้า', "warning");
            return false;
        } else {
            data = [];
            data.push({
                // Used: Used,
                Result: Result,
                achieve: achieve,
                // period_check: period_check,
                Indic_project: Indic_project,
                detail_data: detail_data,
                problem_data: problem_data,
            });
            // console.log(data);

            $.ajax({
                url: "project_report_save",
                method: "POST",
                data: {
                    data: data,
                    Used: Used,
                    quarter: '<?= $quarter; ?>',
                    per_check: per_check,
                    project_id: '<?= $PID; ?>',

                },
                success(getData) {
                    if (getData == "SUCCESS") {
                        swal("Good job!", 'successfully', "success");
                        setTimeout(function() {
                            window.location.href = "list_project";
                        }, 1000);
                    }
                }
            });
        }

    }
</script>