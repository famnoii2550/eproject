<?php $PID = base64_decode($this->input->get('PID')); ?>
<?php $project = $this->db->get_where('project', ['Project_id' => $PID])->row(); ?>
<?php $project_y = $project->Year + 543; ?>
<?php $account_detail = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row(); ?>
<?php $department = $this->db->get_where('department', ['Department_id' => $account_detail->Department_id])->row(); ?>
<?php $users = $this->db->get_where('user', ['Project_id' => $PID])->result(); ?>
<?php $i = 0; ?>
<?php $vv = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; ?>
<?php $v = '&nbsp;&nbsp;&nbsp;'; ?>
<?php $project_strategic_planeList = $this->db->get_where('project_strategic_plane', ['Project_id' => $PID])->result(); ?>
<?php $work_steps = $this->db->get_where('work_step', ['Project_id' => $PID])->result(); ?>
<?php
$o = 0;
$objects = $this->db->get_where('objective', ['Project_id' => $PID])->result(); ?>
<?php $project_indic_success = $this->db->get_where('project_indic_success', ['Project_id' => $PID])->result(); ?>
<?php $workplan = $this->db->get_where('tbl_workplan', ['Workplan_id' => $project->Workplan_id])->row(); ?>
<?php $charges_mains = $this->db->get_where('charges_main', ['Project_id' => $PID])->result(); ?>
<?php $benefits = $this->db->get_where('benefit', ['Project_id' => $PID])->result(); ?>
<?php $b = 0; ?>
<?php
function DateThai($strDate)
{
    $strYear = date("Y", strtotime($strDate)) + 543;
    $strMonth = date("n", strtotime($strDate));
    $strDay = date("j", strtotime($strDate));
    $strHour = date("H", strtotime($strDate));
    $strMinute = date("i", strtotime($strDate));
    $strSeconds = date("s", strtotime($strDate));
    $strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
    $strMonthThai = $strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
}
function fetch_data()
{
    $output = '';
    $output .= '';
    return $output;
}
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$obj_pdf->SetTitle("แบบเสนอโครงการ");
$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('thsarabun');
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, '25', PDF_MARGIN_RIGHT);
$obj_pdf->setPrintHeader(false);
$obj_pdf->setPrintFooter(false);
$obj_pdf->SetAutoPageBreak(TRUE, 10);
$obj_pdf->SetFont('thsarabun', '', 16);
$obj_pdf->AddPage();
$content = '';
$content .= '
    <div style="text-align:center;">
        <img src="assets/img/logo.png" style="width:75px;">
        <h4 align="center">แบบเสนอโครงการ  ประจำปีงบประมาณ พ.ศ. ' . $project_y . '</h4>
        <h4 align="center">มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าพระนครเหนือ</h4>
    </div>
    <div>
        <span style="font-weight:bold;">1. ชื่อโครงการ :</span> <span>' . $project->Project_name . '</span><br>
        <span style="font-weight:bold;">2. หน่วยงานที่รับผิดชอบโครงการ :</span> <span>' . $department->Department . '</span><br>
        <span>' . $vv . ' ผู้รับผิดชอบโครงการ :</span> ';

foreach ($users as $user) {
    $i += 1;
    $accountUser = $this->db->get_where('account', ['Account_id' => $user->Account_id])->row();
    $content .= '<span>' . $accountUser->Fname . ' ' . $accountUser->Lname . '</span><br>';
}
$content .= '<span style="font-weight:bold;">3. ความเชื่อมโยงสอดคล้องกับ </span>';
foreach ($project_strategic_planeList as $project_strategic_plane) {
    $strategic_plane = $this->db->get_where('strategic_plane', ['Strategic_Plan_id' => $project_strategic_plane->Strategic_Plan_id])->row();
    $content .= '<span>' . $strategic_plane->Strategic_Plan;
    '</span>';
}
$strategic = $this->db->get_where('strategic', ['Strategic_id' => $project_strategic_plane->Strategic_id])->row();
$content .= '<br>' . $v . $vv . '<span>ประเด็นยุทธ์ศาสตร์ : ' . $strategic->Strategic_name . '</span><br>';
$goal = $this->db->get_where('goal', ['Goal_id' => $project_strategic_plane->Goal_id])->row();
$content .= $v . $vv . '<span>เป้าประสงค์ : ' . $goal->Goal_name . '</span><br>';
$indic_project = $this->db->get_where('indic_project', ['Indic_project_id' => $project_strategic_plane->Indic_project_id])->row();
$tactic = $this->db->get_where('tactic', ['Goal_id' => $project_strategic_plane->Goal_id])->result();
foreach ($tactic as $tactic) {
    $content .= $v . $vv . '<span>กลยุทธ์ : ' . $tactic->Tactic_name . '</span><br>';
}

$content .= '<span style="font-weight:bold;">4. ลักษณะโครงการ :</span> <span>' . $project->Type . '</span><br>';
if ($project->Type == "โครงการใหม่") {
    $content .= $vv . $v . '<img src="assets/img/icons8-checked-checkbox-48.png" style="width:16px;">' . ' โครงการใหม่
    <img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' โครงการต่อเนื่อง
    <img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' งานประจำ
    <img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' งานพัฒนา';
} elseif ($project->Type == "โครงการต่อเนื่อง") {
    $content .= $vv . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' โครงการใหม่
    <img src="assets/img/icons8-checked-checkbox-48.png" style="width:16px;">' . ' โครงการต่อเนื่อง
    <img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' งานประจำ
    <img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' งานพัฒนา';
} elseif ($project->Type == "งานประจำ") {
    $content .= $vv . $v . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' โครงการใหม่
    <img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' โครงการต่อเนื่อง
    <img src="assets/img/icons8-checked-checkbox-48.png" style="width:16px;">' . ' งานประจำ
    <img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' งานพัฒนา';
} else {
    $content .= $vv . $v . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' โครงการใหม่
    <img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' โครงการต่อเนื่อง
    <img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' งานประจำ
    <img src="assets/img/icons8-checked-checkbox-48.png" style="width:16px;">' . ' งานพัฒนา';
}
$content .= '<br><span style="font-weight:bold;">5. การบูรณาการโครงการ :</span><br>';
if ($project->Integra_name == "บูรณาการกับการเรียนการสอน") {
    $content .= $vv . $v . '<img src="assets/img/icons8-checked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับการเรียนการสอน<br>
    ' . $vv . $v . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับงานวิจัย<br>
    ' . $vv . $v . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับงานบริการวิชาการ<br>
    ' . $vv . $v . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับงานทำนุบำรุงศิลปวัฒนธรรม<br>';
} elseif ($project->Integra_name == "บูรณาการกับงานวิจัย") {
    $content .= $vv . $v . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับการเรียนการสอน<br>
    ' . $vv . $v . '<img src="assets/img/icons8-checked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับงานวิจัย<br>
    ' . $vv . $v . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับงานบริการวิชาการ<br>
    ' . $vv . $v . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับงานทำนุบำรุงศิลปวัฒนธรรม<br>';
} elseif ($project->Integra_name == "บูรณาการกับงานบริการวิชาการ") {
    $content .= $vv . $v . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับการเรียนการสอน<br>
    ' . $vv . $v . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับงานวิจัย<br>
    ' . $vv . $v . '<img src="assets/img/icons8-checked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับงานบริการวิชาการ<br>
    ' . $vv . $v . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับงานทำนุบำรุงศิลปวัฒนธรรม<br>';
} elseif ($project->Integra_name == "บูรณาการกับงานทำนุบำรุงศิลปวัฒนธรรม") {
    $content .= $vv . $v . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับการเรียนการสอน<br>
    ' . $vv . $v . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับงานวิจัย<br>
    ' . $vv . $v . '<img src="assets/img/icons8-unchecked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับงานบริการวิชาการ<br>
    ' . $vv . $v . '<img src="assets/img/icons8-checked-checkbox-48.png" style="width:16px;">' . ' บูรณาการกับงานทำนุบำรุงศิลปวัฒนธรรม<br>';
}
$content .= '<span style="font-weight:bold;">6. หลักการและเหตุผลของโครงการ </span><br>
' . $vv . $v . $project->Rationale . '<br>';
$content .= '<span style="font-weight:bold;">7. วัตถุประสงค์ </span><br>';
foreach ($objects as $object) {
    $o += 1;
    $content .= $vv . $v . $o . '.' . $object->Objective_name . '<br>';
}
$content .= '<span style="font-weight:bold;">8. ตัวชี้วัดความสำเร็จระดับโครงการ (Output/Outcome) และ ค่าเป้าหมาย (ระบุหน่วยนับ)</span><br>';
$content .= '
<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>
<table>
    <tr>
        <th style="font-weight:bold;text-align:center;width:60%;">ตัวชี้วัดความสำเร็จ</th>
        <th style="font-weight:bold;text-align:center;width:20%;">หน่วยนับ</th>
        <th style="font-weight:bold;text-align:center;width:20%;">ค่าเป้าหมาย</th>
    </tr>';
foreach ($project_indic_success as $project_indic_success_detail) {
    $content .= '
    <tr>
        <td>' . $project_indic_success_detail->Indic_success . '</td>
        <td style="text-align:center;">' . $project_indic_success_detail->Unit . '</td>
        <td style="text-align:center;">' . $project_indic_success_detail->Cost . '</td>
    </tr>';
}
$content .= '</table><br><br>';
$content .= '<span style="font-weight:bold;">9. กลุ่มเป้าหมาย (ระบุกลุ่มเป้าหมายและจำนวนกลุ่มเป้าหมายที่เข้าร่วมโครงการ)</span><br>'
    . $vv . $v . $project->Target_group;
$content .= '<br><span style="font-weight:bold;">10. ขั้นตอนการดำเนินการ</span><br>';
$content .= '<table>
    <tr>
        <th style="font-weight:bold;text-align:center;width:60%">ขั้นตอนการดำเนินการ/รายการกิจกรรม</th>
        <th style="font-weight:bold;text-align:center;width:20%">เริ่มต้น</th>
        <th style="font-weight:bold;text-align:center;width:20%">สิ้นสุด</th>
    </tr>';
foreach ($work_steps as $work_step) {
    $content .= '
<tr>
    <td>' . $work_step->Step_name . '</td>
    <td style="text-align:center;">' . DateThai($work_step->Start) . '</td>
    <td style="text-align:center;">' . DateThai($work_step->Stop) . '</td>
</tr>
';
}
$content .= '</table>';
$content .= '<br><br><span style="font-weight:bold;">11. แหล่งเงิน/ประเภทงบประมาณที่ใช้ : </span>' . $project->Source . '<span></span><br>';
$content .= '<span style="font-weight:bold;">12. ปริมาณการงบประมาณที่ใช้ : </span>' . number_format($project->Butget) . ' บาท (' . $project->Butget_char . ')<span></span><br>';
if ($workplan == true) {
    $content .= '<span style="font-weight:bold;">13. แผนงาน : </span>' . $workplan->Workplan_name . '<span></span><br>';
}
$content .= '<span style="font-weight:bold;">14. ประมาณการค่าใช้จ่าย : ( หน่วย : บาท )</span><span></span><br>';
$content .= '<table>
    <tr>
        <th style="border-bottom:none;"></th>
        <th style="text-align:center;">ไตรมาส 1</th>
        <th style="text-align:center;">ไตรมาส 2</th>
        <th style="text-align:center;">ไตรมาส 3</th>
        <th style="text-align:center;">ไตรมาส 4</th>
    </tr>
    <tr>
        <th class="text-center" style="border-top:none;text-align:center">ประเภทรายจ่าย</th>
        <th style="text-align:center;">แผนการใช้จ่าย</th>
        <th style="text-align:center;">แผนการใช้จ่าย</th>
        <th style="text-align:center;">แผนการใช้จ่าย</th>
        <th style="text-align:center;">แผนการใช้จ่าย</th>
    </tr>';
foreach ($charges_mains as $charges_main) {
    $content .= '
    <tr>
        <td>' . $charges_main->Charges_Main . '</td>
        <td style="text-align:center;"></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>';
    $charges_subs = $this->db->get_where('charges_sub', ['Charges_Main_id' => $charges_main->Charges_Main_id])->result();
    foreach ($charges_subs as $charges_sub) {
        $content .= '
    <tr>
        <td>' . $charges_sub->Charges_Sub . '</td>
        <td style="text-align:center;">' . $charges_sub->Quarter_one . '</td>
        <td style="text-align:center;">' . $charges_sub->Quarter_two . '</td>
        <td style="text-align:center;">' . $charges_sub->Quarter_three . '</td>
        <td style="text-align:center;">' . $charges_sub->Quarter_four . '</td>
    </tr>';
    }
}
$content .= '</table><br><br>';
$content .= '<span style="font-weight:bold;">15. ประโยชน์ที่คาดว่าจะได้รับ</span><br>';
foreach ($benefits as $benefit) {
    $b += 1;
    $content .= $vv . $v . $b . '.' . $benefit->Benefit_name . '<br>';
}
$content .= '</div>';
$content .= '<br><br>';
$content .= '<div style="text-align:right;">';
$content .= '<p>ลงชื่อ .............................................</p>';
$content .= '<p>( ' . $accountUser->Fname . ' ' . $accountUser->Lname . ' )  &nbsp;  &nbsp;  &nbsp;</p>';
$content .= '<p>ผู้รับผิดชอบโครงการ &nbsp;  &nbsp;  &nbsp;</p>';
$content .= '<p>วันที่ .............../.............../...............</p>';
$content .= '</div>';
$content .= fetch_data();
$obj_pdf->writeHTML($content);
$obj_pdf->Output('file.pdf', 'I');
