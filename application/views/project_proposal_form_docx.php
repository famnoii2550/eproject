<?php 
function DateThai($strDate)
{
    $strYear = date("Y", strtotime($strDate)) + 543;
    return "$strYear";
}
$PID = base64_decode($this->input->get('PID'));
$project = $this->db->get_where('project',['Project_id' => $PID])->row_array();
$project_strategic_planeList = $this->db->get_where('project_strategic_plane', ['Project_id' => $PID])->result_array();
		$PHPWord = $this->word; // New Word Document
        $section = $PHPWord->createSection(); // New portrait section
        // Add text elements
        $section->addImage('assets/img/logo-eproject.jpg',array('width'=>'72','height'=>'72','align'=>'center'));
        // $section->addTextBreak(2);
        $PHPWord->addFontStyle('titleStyle', array('bold'=>true,'size'=>16));
        $PHPWord->addFontStyle('detailStyle', array('size'=>15));
        $PHPWord->addParagraphStyle('paragraptitleStyle', array('align'=>'center', 'spaceAfter'=>100));
        $PHPWord->addParagraphStyle('paragrapdetailStyle', array('spaceBefore'=>200));
        // Save File / Download (Download dialog, prompt user to save or simply open it)
		$section->addText('แบบเสนอโครงการที่ตอบสนองยุทธศาสตร์การพัฒนามหาวิทยาลัย', 'titleStyle', 'paragraptitleStyle');
		$section->addText('ประจำปีงบประมาณ พ.ศ. '.DateThai($project['Year']), 'titleStyle', 'paragraptitleStyle');
		$section->addText('มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าพระนครเหนือ', 'titleStyle', 'paragraptitleStyle');
        $section->addTextBreak(2);
        $section->addText('1.ชื่อโครงการ : '.$project['Project_name'], array('size'=>15));
        $section->addText('2.หน่วยงานที่รับผิดชอบโครงการ  : '.$project['Integra_detail'], array('size'=>15));
        foreach ($project_strategic_planeList as $project_strategic_plane) {
        $strategic_plane = $this->db->get_where('strategic_plane', ['Strategic_Plan_id' => $project_strategic_plane['Strategic_Plan_id']])->row_array();
        $strategic = $this->db->get_where('strategic', ['Strategic_id' => $project_strategic_plane['Strategic_id']])->row_array();
        $goal = $this->db->get_where('goal', ['Goal_id' => $project_strategic_plane['Goal_id']])->row_array();
        $indic_project = $this->db->get_where('strategics_project', ['Project_id' => $PID])->result_array();
        $section->addText('3.'.$strategic_plane['Strategic_Plan'], array('size'=>15));
        $section->addText('ประเด็นยุทธศาสตร์ : '.$strategic['Strategic_name'], 'detailStyle','paragrapdetailStyle');
        foreach ($indic_project as $tactic_project) {
        $tactic_project = $this->db->get_where('tactic', ['Tactic_id' => $tactic_project['Tactic_id']])->row_array();
        $section->addText('กลยุทธ์ : '.$tactic_project['Tactic_name'], 'detailStyle','paragrapdetailStyle');
        }
        // $section->addText('4.ลักษณะโครงการ/กิจกรรม : '.$project['Type'], array('size'=>15));<img src="assets/img/icons8-checked-checkbox-48.png" style="width:16px;">
        $section->addImage('assets/img/icons8-checked-checkbox-48.png',$project['Type'],array('width'=>'16'));

        }

        $filename='just_some_random_name.docx'; //save our document as this file name
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
        $objWriter->save('php://output');

;?>