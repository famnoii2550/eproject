<script src="assets/js/thaibath.js" type="text/javascript" charset="utf-8"></script>
<style>
    #ic,
    #passport {
        display: none;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 style="display: contents;">ข้อมูลโครงการ</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <?php $uri = base64_decode($this->input->get('uri')); ?>
    <?php $ac = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array(); ?>
    <?php $status = $this->db->get_where('project', ['Project_id' => $PID])->row_array(); ?>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <div class="row" style="padding: 4px 0px 10px 8px;">
                    <div class="col-10">
                        <u> <a>รายละเอียดโครงการ</a> </u> <span style="padding: 0px 10px;"> / </span>
                        <a href="project_doc_supervisor?PID=<?php echo base64_encode($PID); ?>&uri=<?= base64_encode($uri); ?>">เอกสารโครงการ</a> <span style="padding: 0px 10px;"> / </span>
                   
                            <a href="project_consider_supervisor?PID=<?php echo base64_encode($PID); ?>&uri=<?= base64_encode($uri); ?>">พิจารณาโครงการ</a> <span style="padding: 0px 10px;"> / </span>
                  
                        <a href="project_review_supervisor?PID=<?php echo base64_encode($PID); ?>&uri=<?= base64_encode($uri); ?>">ความคิดเห็น</a>
                    </div>
                    <div class="col-2 text-right">
                        <a href="<?= $uri; ?>">กลับหน้าโครงการ</a>
                    </div>
                </div>
                <!-- <button class="btn btn-info" style="float:right;" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus-circle"></i> สร้างประเด็นยุทธ์ศาสตร์ใหม่</button> -->
                <div style="background-color: rgba(0,0,0,.05); padding:15px;">
                    <div class="row">
                        <div class="col-3">
                            <a href="project_proposal?PID=<?= base64_encode($PID); ?>" target="_bank"><img src="assets/img/pdf.png" alt="" style="width: 50px;"> แบบเสนอโครงการ</a>
                        </div>
                    </div>
                </div>
                <div class="card-tools"></div>
            </div>
            <?php $project = $this->db->get_where('project', ['Project_id' => $PID])->row_array(); ?>
            <div class="card-body">
                <div class="row container">
                    <label class="col-3" for="">ปีงบประมาณ : </label>
                    <div class="col-9">
                        <span><?php echo $project['Year'] + 543; ?></span>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">ชื่อโครงการ : </label>
                    <div class="col-9">
                        <span><?php echo $project['Project_name']; ?></span>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">หน่วยงานที่รับผิดชอบโครงการ : </label>
                    <div class="col-9">
                        <?php
                        $account_detail = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
                        $department = $this->db->get_where('department', ['Department_id' => $account_detail['Department_id']])->row_array();
                        $users = $this->db->get_where('user', ['Project_id' => $PID])->result_array();
                        ?>
                        <span><?php echo $department['Department']; ?></span>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">ผู้รับผิดชอบโครงการ : </label>
                    <div class="col-9">
                        <?php
                        $i = 0;
                        foreach ($users as $user) {
                            $i += 1;
                            $accountUser = $this->db->get_where('account', ['Account_id' => $user['Account_id']])->row_array();
                        ?>
                            <span><?php echo $i . '.' . $accountUser['Fname'] . ' ' . $accountUser['Lname']; ?><br></span>
                        <?php } ?>
                    </div>
                </div>
                <hr>
                <?php $project_strategic_planeList = $this->db->get_where('project_strategic_plane', ['Project_id' => $PID])->result_array();
                foreach ($project_strategic_planeList as $project_strategic_plane) {
                ?>
                    <div class="row container">
                        <label class="col-3" for="">ชื่อแผนยุทธ์ศาสตร์ : </label>

                        <div class="col-7">
                            <?php
                            $strategic_plane = $this->db->get_where('strategic_plane', ['Strategic_Plan_id' => $project_strategic_plane['Strategic_Plan_id']])->row_array();
                            echo $strategic_plane['Strategic_Plan'];
                            ?>
                        </div>
                        <div class="col-2">

                        </div>
                    </div>
                    <div class="row container" style="    padding: 25px 0 0 0px;">
                        <div class="col-3"> </div>
                        <label class="col-2 text-right" for="">ประเด็นยุทธ์ศาสตร์ :</label>
                        <div class="col-4">
                            <?php $strategic = $this->db->get_where('strategic', ['Strategic_id' => $project_strategic_plane['Strategic_id']])->row_array();
                            echo $strategic['Strategic_name'];
                            ?>
                        </div>
                    </div>
                    <div class="row container" style="    padding: 25px 0 0 0px;">
                        <div class="col-3"> </div>
                        <label class="col-2 text-right" for="">เป้าประสงค์ :</label>
                        <div class="col-4">
                            <?php $goal = $this->db->get_where('goal', ['Goal_id' => $project_strategic_plane['Goal_id']])->row_array();
                            echo $goal['Goal_name'];
                            ?>
                        </div>
                    </div>
                    <?php $indic_project = $this->db->get_where('strategics_project', ['Project_id' => $PID])->result_array(); ?>
                    <?php foreach ($indic_project as $tactic_project) { ?>
                        <div class="row container" style="    padding: 25px 0 0 0px;">
                            <div class="col-3"> </div>
                            <label class="col-2 text-right" for="">กลยุทธ์ :</label>
                            <div class="col-4">
                                <?php $tactic_project = $this->db->get_where('tactic', ['Tactic_id' => $tactic_project['Tactic_id']])->row_array(); ?>
                                <?= $tactic_project['Tactic_name'];; ?>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">ลักษณะโครงการ :</label>
                    <div class="col-5">
                        <?php echo $project['Type']; ?>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">การบูรณาการโครงการ :</label>
                    <div class="col-5">
                        <?php echo $project['Integra_name']; ?>

                        <?php if ($project['Integra_name'] != "ไม่มี") { ?>
                            <textarea name="integra_detail" style="margin-top: 15px;" id="" cols="30" rows="5" class="form-control" disabled><?php echo $project['Integra_detail']; ?></textarea>
                        <?php } ?>
                    </div>
                </div>
                <div class="row container">
                    <label class="col-3" for="">หลักการและเหตุผล :</label>
                    <div class="col-5">
                        <textarea name="integra_detail" style="margin-top: 15px;" id="" cols="30" rows="5" class="form-control" disabled><?php echo $project['Rationale']; ?></textarea>
                    </div>
                </div>
                <hr>
                <div class="row container ">
                    <label class="col-3" for="">วัตถุประสงค์ :</label>
                    <div class="col-5">
                        <?php
                        $o = 0;
                        $objects = $this->db->get_where('objective', ['Project_id' => $PID])->result_array();
                        foreach ($objects as $object) {
                            $o += 1;
                            echo $o . '.' . $object['Objective_name'];
                            echo '<br>';
                        }
                        ?>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">ตัวชี้วัดความสำเร็จระดับโครงการ</label>
                    <div class="col-9">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="text-center">ตัวชี้วัดความสำเร็จ</th>
                                    <th class="text-center">หน่วยนับ</th>
                                    <th class="text-center">ค่าเป้าหมาย</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $project_indic_success = $this->db->get_where('project_indic_success', ['Project_id' => $PID])->result_array();
                                foreach ($project_indic_success as $project_indic_success_detail) {

                                ?>
                                    <tr>
                                        <td><?php echo $project_indic_success_detail['Indic_success']; ?></td>
                                        <td><?php echo $project_indic_success_detail['Unit']; ?></td>
                                        <td><?php echo $project_indic_success_detail['Cost']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">กลุ่มเป้าหมาย :</label>
                    <div class="col-7">
                        <?php echo $project['Target_group']; ?>
                    </div>
                    <div class="col-2">

                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">ขั้นตอนการดำเนินการ</label>
                    <div class="col-9">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width:50%">ขั้นตอนการดำเนินการ/รายการกิจกรรม</th>
                                    <th class="text-center">เริ่มต้น</th>
                                    <th class="text-center">สิ้นสุด</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php $work_steps = $this->db->get_where('work_step', ['Project_id' => $PID])->result_array();
                                foreach ($work_steps as $work_step) {

                                ?>
                                    <tr class="work_step">
                                        <td><?php echo $work_step['Step_name']; ?></td>
                                        <td><?php echo $work_step['Start']; ?></td>
                                        <td><?php echo $work_step['Stop']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">แหล่งเงิน/ประเภทงบประมาณที่ใช้ :</label>
                    <div class="col-7">

                        <?php echo $project['Source']; ?>

                    </div>
                </div>
                <?php if ($project['Source'] != "ไม่ได้ใช้งบประมาณ") { ?>
                    <div class="row container" style=" padding: 25px 0 0 0px;">
                        <label class="col-3" for="">ปริมาณการงบประมาณที่ใช้ :</label>
                        <div class="col-4">
                            <?php echo number_format($project['Butget']); ?>
                            (<?php echo $project['Butget_char']; ?>)
                        </div>
                        <div class="col-3">

                        </div>
                    </div>
                    <div class="row container" style="padding: 25px 0 0 0px;">
                        <label class="col-3" for="">แผนงาน :</label>
                        <div class="col-7">
                            <?php
                            $workplan = $this->db->get_where('tbl_workplan', ['Workplan_id' => $project['Workplan_id']])->row_array();
                            echo $workplan['Workplan_name'];
                            ?>
                        </div>
                    </div>

                    <hr>
                    <div class="row ">
                        <label for="" style="padding: 10px">ประเภทการใช้จ่าย</label>
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="border-bottom:none;"></th>
                                    <th class="text-center">ไตรมาส 1</th>
                                    <th class="text-center">ไตรมาส 2</th>
                                    <th class="text-center">ไตรมาส 3</th>
                                    <th class="text-center">ไตรมาส 4</th>
                                </tr>
                                <tr>
                                    <th class="text-center" style="border-top:none;">ประเภทรายจ่าย</th>
                                    <th class="text-center">แผนการใช้จ่าย</th>
                                    <th class="text-center">แผนการใช้จ่าย</th>
                                    <th class="text-center">แผนการใช้จ่าย</th>
                                    <th class="text-center">แผนการใช้จ่าย</th>
                                </tr>
                            </thead>
                            <?php
                            $charges_mains = $this->db->get_where('charges_main', ['Project_id' => $PID])->result_array();
                            foreach ($charges_mains as $charges_main) {
                            ?>
                                <tbody>
                                    <tr>
                                        <td colspan="5" style="background-color: #fd7e14;color:#fff;"><?php echo $charges_main['Charges_Main']; ?></td>
                                    </tr>
                                    <?php
                                    $charges_subs = $this->db->get_where('charges_sub', ['Charges_Main_id' => $charges_main['Charges_Main_id']])->result_array();
                                    foreach ($charges_subs as $charges_sub) {

                                    ?>
                                        <tr>
                                            <td><?php echo $charges_sub['Charges_Sub']; ?></td>
                                            <td class=""><?php echo $charges_sub['Quarter_one']; ?></td>
                                            <td><?php echo $charges_sub['Quarter_two']; ?></td>
                                            <td><?php echo $charges_sub['Quarter_three']; ?></td>
                                            <td><?php echo $charges_sub['Quarter_four']; ?></td>

                                        </tr>
                                    <?php } ?>
                                </tbody>
                            <?php } ?>
                        </table>
                    </div>
                <?php } ?>
                <hr>
                <div class="row container form-group">
                    <label class="col-3" for="">ประโยชน์ที่คาดว่าจะได้รับ :</label>
                    <div class="col-5">
                        <?php
                        $benefits = $this->db->get_where('benefit', ['Project_id' => $PID])->result_array();
                        $b = 0;
                        foreach ($benefits as $benefit) {
                            $b += 1;
                            echo $b . '.' . $benefit['Benefit_name'];
                            echo "<br>";
                        }
                        ?>
                    </div>
                </div>
                <div class="row container">
                    <label class="col-3" for="">เอกสาร TOR :</label>
                    <div class="col-5">
                        <?php if ($project['Tor'] == 1) {
                            echo "มี";
                        } else {
                            echo "ไม่มี";
                        } ?>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
    </section>
    <!-- /.content -->
</div>
<script>
    $("#DM1")
        .keyup(function() {
            var value = $(this).val();
            $("#DM2").val(ArabicNumberToText(value));
        })
        .keyup();
</script>
<script>
    $('#test').on('change', function() {
        //  alert( this.value ); // or $(this).val()
        if (this.value == "1") {
            $('.ic').css('display', 'block');
            $('.passport').css('display', 'none');
            $('.to').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'block');
        } else if (this.value == "2") {
            $('.to').css('display', 'block');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'block');
        } else if (this.value == "3") {
            $('.to2').css('display', 'block');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to').css('display', 'none');
            $('.to3').css('display', 'block');
        } else {
            $('.to').css('display', 'none');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'none');
        }
    });
</script>

<!-- /.content-wrapper -->