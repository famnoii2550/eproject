<script src="assets/js/thaibath.js" type="text/javascript" charset="utf-8"></script>
<style>
    #ic,
    #passport {
        display: none;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 style="display: contents;">ข้อมูลโครงการ</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <?php $uri = base64_decode($this->input->get('uri')); ?>
    <?php $ac = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array(); ?>
    <?php $status = $this->db->get_where('project', ['Project_id' => $PID])->row_array(); ?>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <div class="row" style="padding: 4px 0px 10px 8px;">
                    <div class="col-10">
                        <a href="project_list_manager?PID=<?php echo base64_encode($PID); ?>&uri=<?= base64_encode($uri); ?>">รายละเอียดโครงการ</a> <span style="padding: 0px 10px;"> / </span>
                        <u><a>เอกสารโครงการ</a></u> <span style="padding: 0px 10px;"> / </span>
                       
                            <a href="project_consider_manager?PID=<?php echo base64_encode($PID); ?>&uri=<?= base64_encode($uri); ?>">พิจารณาโครงการ</a> <span style="padding: 0px 10px;"> / </span>
                        <a href="project_review_manager?PID=<?php echo base64_encode($PID); ?>&uri=<?= base64_encode($uri); ?>">ความคิดเห็น</a>
                    </div>
                    <div class="col-2 text-right">
                        <a href="project_manager">กลับหน้าโครงการ</a>
                    </div>
                </div>
                <!-- <button class="btn btn-info" style="float:right;" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus-circle"></i> สร้างประเด็นยุทธ์ศาสตร์ใหม่</button> -->

            </div>
            <div class="card-body">
                <div class="row container mb-4">
                    <div class="col-4">
                        <label>เอกสาร TOR : </label>
                    </div>
                    <div class="col-8">
                        <?php $file_tor = $this->db->get_where('file', ['Project_id' => $PID, 'Check_type_tor' => 1])->result_array(); ?>
                            <?php foreach ($file_tor as $file_tor) { ?>
                                <span class="badge badge-primary"><a href="<?= $file_tor['Full_path']; ?>" target="_bank" style="color:#fff;"><?= $file_tor['File_name']; ?></a></span><br>
                            <?php } ?>
                            <?php if (empty($file_tor)) { ?>
                                <span class="badge badge-danger">ยังไม่มีเอกสาร TOR</span>
                            <?php } else { ?>
                                <?php foreach ($file_tor as $file_tor) { ?>
                                    <span class="badge badge-primary"><a href="<?= $file_tor['Full_path']; ?>" target="_bank" style="color:#fff;"><?= $file_tor['Full_path']; ?></a></span><br>
                                <?php } ?>
                            <?php } ?>
                    </div>
                </div>
                <div class="row container">
                    <div class="col-4">
                        <label>เอกสารที่เกี่ยวข้อง : </label>
                    </div>
                    <div class="col-8">
                        <?php $file_other = $this->db->get_where('file', ['Project_id' => $PID, 'Check_type_tor' => 2])->result_array(); ?>
                            <?php foreach ($file_other as $file_other) { ?>
                                <span class="badge badge-primary"><a href="<?= $file_other['Full_path']; ?>" target="_bank" style="color:#fff;"><?= $file_other['File_name']; ?></a></span><br>
                            <?php } ?>
                            <?php if (empty($file_other)) { ?>
                                <span class="badge badge-danger">ยังไม่มีเอกสาร TOR</span>
                            <?php } else { ?>
                                <?php foreach ($file_other as $file_other) { ?>
                                    <span class="badge badge-primary"><a href="<?= $file_other['Full_path']; ?>" target="_bank" style="color:#fff;"><?= $file_other['Full_path']; ?></a></span><br>
                                <?php } ?>
                            <?php } ?>
                    </div>
                </div>

            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<script>
    $("#DM1")
        .keyup(function() {
            var value = $(this).val();
            $("#DM2").val(ArabicNumberToText(value));
        })
        .keyup();
</script>
<script>
    $('#test').on('change', function() {
        //  alert( this.value ); // or $(this).val()
        if (this.value == "1") {
            $('.ic').css('display', 'block');
            $('.passport').css('display', 'none');
            $('.to').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'block');


        } else if (this.value == "2") {
            $('.to').css('display', 'block');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'block');


        } else if (this.value == "3") {
            $('.to2').css('display', 'block');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to').css('display', 'none');
            $('.to3').css('display', 'block');

        } else {
            $('.to').css('display', 'none');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'none');


        }
    });
</script>

<!-- /.content-wrapper -->