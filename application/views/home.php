<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>หน้าแรก</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <section class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                ข้อมูลรายปี
                            </h3>
                            <div class="card-tools">
                            </div>
                        </div>
                        <div class="card-body">
                            <canvas id="donutChart" height="100px" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                </section>
            </div>
            <div class="row">
                <section class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title pt-1">
                                ข้อมูลรายไตรมาส
                            </h3>
                            <div class="card-tools">
                                <ul class="nav nav-pills ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#chart_1" data-toggle="tab">ไตรมาส 1</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#chart_2" data-toggle="tab">ไตรมาส 2</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#chart_3" data-toggle="tab">ไตรมาส 3</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content p-0">
                                <div class="chart tab-pane active" id="chart_1" style="position: relative; height: 300px;">
                                    <canvas id="donutChart_1" style="height: 230px; min-height: 230px; display: block; width: 444px;" width="444" height="300" class="chartjs-render-monitor"></canvas>
                                </div>
                                <div class="chart tab-pane" id="chart_2" style="position: relative; height: 300px;">
                                    <canvas id="donutChart_2" height="300" style="height: 300px;"></canvas>
                                </div>
                                <div class="chart tab-pane" id="chart_3" style="position: relative; height: 300px;">
                                    <canvas id="donutChart_3" height="300" style="height: 300px;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="row">
                <section class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                ยุทธศาสตร์
                            </h3>
                            <div class="card-tools">
                            </div>
                        </div>
                        <div class="card-body">
                            <style>
                                .panel-group .panel {
                                    margin-bottom: 0;
                                    overflow: hidden;
                                    /* border-radius: 4px; */
                                }

                                .panel {
                                    background-color: #fff;
                                    /* border: 1px solid transparent; */
                                    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
                                }

                                .panel-primary {
                                    border-color: #428bca;
                                }

                                .bg-warning {
                                    border-radius: 4px;
                                }
                            </style>
                            <?php
                            $q = 1;
                            $w = 1;
                            $e = 1;
                            ?>
                            <?php $i = 1; ?>
                            <?php $r = 1; ?>
                            <?php $t = 1; ?>
                            <?php $u = 1; ?>
                            <?php
                            $o = 1;

                            $a = 1;
                            $z = 1;
                            ?>
                            <?php $Strategic_Plan = $this->db->get('strategic_plane')->result_array(); ?>

                            <?php foreach ($Strategic_Plan as  $Strategic_Plan) { ?>
                                <div class="panel-group" id="accordion_strategic">

                                    <div class="panel panel-primary bg-info py-2 mb-2" style="border-radius: 10px;">
                                        <div class="panel-heading">
                                            <h4 class="panel-title" style="text-align:left">
                                                <a data-toggle="collapse" data-parent="#accordion_strategic" href="#collapse_strategic<?php echo $q++; ?>" class="collapsed" style="color:#fff;margin-left:15px;">
                                                    แผนยุทธศาสตร์ <?php echo $w++; ?> : <?php echo $Strategic_Plan['Strategic_Plan']; ?> </a>
                                                <div class="pull-right">
                                                </div>
                                            </h4>
                                        </div>
                                        <div id="collapse_strategic<?php echo $e++; ?>" class="panel-collapse collapse" style="height: 0px;">
                                            <?php $strategic = $this->db->get_where('strategic', ['Strategic_Plan_id' => $Strategic_Plan['Strategic_Plan_id']])->result_array(); ?>
                                            <?php $y = 1; ?>

                                            <?php foreach ($strategic as $strategic) { ?>
                                                <div class="panel-body">
                                                    <div class="panel-group" id="accordion_goal<?php echo $i++; ?>">
                                                        <div class="panel panel-info">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title" style="text-align:left;border-radius: 10px;">
                                                                    <a data-toggle="collapse" data-parent="#accordion_goal<?php echo $r++; ?>" href="#collapse_goal<?php echo $t++; ?>" class="collapsed" style="margin-left:45px;">
                                                                        ประเด็นยุทธศาสตร์ที่ <?php echo $y++; ?> : <?php echo $strategic['Strategic_name']; ?></a>
                                                                    <div class="pull-right">
                                                                    </div>
                                                                </h4>
                                                            </div>

                                                            <div id="collapse_goal<?php echo $u++; ?>" class="panel-collapse collapse" style="height: 0px;">
                                                                <?php $goal = $this->db->get_where('goal', ['Strategic_id' => $Strategic_Plan['Strategic_Plan_id']])->result_array(); ?>
                                                                <?php $s = 1; ?>
                                                                <?php $p = 1; ?>
                                                                <?php foreach ($goal as $goal) { ?>
                                                                    <div class="panel-body">
                                                                        <div class="panel-group" id="accordion_inc<?php echo $o++; ?>">
                                                                            <div class="panel panel-info">
                                                                                <div class="panel-heading">
                                                                                    <h4 class="panel-title" style="text-align:left;border-radius: 10px;">
                                                                                        <a data-toggle="collapse" data-parent="#collapse_goal<?php echo $p++; ?>" href="#collapse_inc<?php echo $a++; ?>" class="collapsed" style="margin-left:80px;">
                                                                                            เป้าประสงค์ที่ <?php echo $s++; ?> : <?php echo $goal['Goal_name']; ?></a>
                                                                                        <div class="pull-right">
                                                                                        </div>
                                                                                    </h4>
                                                                                </div>
                                                                                <div id="collapse_inc<?php echo $z++; ?>" class="panel-collapse collapse" style="height: 0px;">
                                                                                    <?php $indic_p = $this->db->get_where('indic_project', ['Goal_id' => $goal['Goal_id']])->result_array(); ?>
                                                                                    <div class="panel-body">
                                                                                        <table class="table table-hover table table-bordered" style="color:#000;">
                                                                                            <tbody>
                                                                                                <?php foreach ($indic_p as $indic_p) { ?>
                                                                                                    <tr>
                                                                                                        <th width="20%" style="text-align:left">ตัวชี้วัด :</th>
                                                                                                        <td style="text-align:left"><?php echo $indic_p['Indic_project']; ?></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <th style="text-align:left">หน่วยนับ :</th>
                                                                                                        <td style="text-align:left"><?php echo $indic_p['Unit']; ?></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <th style="text-align:left">ค่าเป้าหมาย :</th>
                                                                                                        <td style="text-align:left"><?php echo $indic_p['Cost']; ?></td>
                                                                                                    </tr>
                                                                                                    <?php $tactic = $this->db->get_where('tactic', ['Goal_id' => $goal['Goal_id']])->result_array(); ?>
                                                                                                    <?php foreach ($tactic as $tactic) { ?>
                                                                                                        <tr>
                                                                                                            <th style="text-align:left">กลยุทธ์ :</th>
                                                                                                            <td style="text-align:left"><?php echo $tactic['Tactic_name']; ?> </td>
                                                                                                        </tr>
                                                                                                    <?php } ?>
                                                                                                <?php } ?>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </section>
            </div>

        </div>
        <!-- Default box -->

        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->