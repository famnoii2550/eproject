<?php
$month = array(
    '01'  => 'มกราคม', '02'  => 'กุมภาพันธ์', '03'  => 'มีนาคม',
    '04'  => 'เมษายน', '05'  => 'พฤษภาคม', '06'  => 'มิถุนายน',
    '07'  => 'กรกฎาคม', '08'  => 'สิงหาคม', '09'  => 'กันยายน',
    '10'  => 'ตุลาคม', '11'  => 'พฤศจิกายน', '12'  => 'ธันวาคม',
);
function thaiDate($date)
{
    list($date) = explode(' ', $date); // แยกวันที่ กับ เวลาออกจากกัน
    list($Y, $m, $d) = explode('-', $date); // แยกวันเป็น ปี เดือน วัน
    $Y = $Y + 543; // เปลี่ยน ค.ศ. เป็น พ.ศ.
    switch ($m) {
        case "01":
            $m = "ม.ค.";
            break;
        case "02":
            $m = "ก.พ.";
            break;
        case "03":
            $m = "มี.ค.";
            break;
        case "04":
            $m = "เม.ย.";
            break;
        case "05":
            $m = "พ.ค.";
            break;
        case "06":
            $m = "มิ.ย.";
            break;
        case "07":
            $m = "ก.ค.";
            break;
        case "08":
            $m = "ส.ค.";
            break;
        case "09":
            $m = "ก.ย.";
            break;
        case "10":
            $m = "ต.ค.";
            break;
        case "11":
            $m = "พ.ย.";
            break;
        case "12":
            $m = "ธ.ค.";
            break;
    }
    return $d . " " . $m . " " . $Y;
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>จัดการรายงาน</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <!-- <button class="btn btn-info" style="float:right;" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus-circle"></i> สร้างประเด็นยุทธ์ศาสตร์ใหม่</button> -->
                <div style="background-color: rgba(0,0,0,.05); padding:15px;">
                    <div style="display: inline-block; width:100%;">

                        <div style="display: inline-block; width: 50%;">
                            <span>แผนปฏิบัติการประจําปี</span>
                            <select name="workplan_year" id="workplan_year" class="form-control" style="width:50%; display: inline-block;">
                                <?php
                                $Date = date('Y-m-d');
                                $Date10 = date('Y', strtotime($Date . ' + 10 years'));
                                $y_budget = $this->db->get('budget_year')->row_array();
                                for ($x = date('Y'); $x <= $Date10; $x++) { ?>
                                    <option value="<?php echo $x; ?>" <?php if ($y_budget['budget_year'] == $x) {
                                                                            echo 'selected';
                                                                        } ?>> <?php echo $x + 543; ?></option>
                                <?php } ?>
                            </select>
                            <button type="button" id="show_report" class="btn btn-danger" style="margin-bottom: 3px;"><i class="fa fa-file-pdf"></i> ดูแผนปฏิบัติการ</button>
                            <!-- <input type="text" name="Fiscalyear" class="form-control" style="width:50%; display: inline-block;" id="" placeholder="ตัวอย่าง 2559"> -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
</div>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<script>
    $('#show_report').on('click', function() {
        let work_year = $('#workplan_year').val();
        window.open('workplan_report_pdf?wpy=' + work_year, '_blank');
    });
</script>