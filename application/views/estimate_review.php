<?php $quarter = base64_decode($this->input->get('quarter')); ?>
<?php $profile = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array(); ?>
<script src="assets/js/thaibath.js" type="text/javascript" charset="utf-8"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>เอกสารประเมินโครงการ ผู้รับผิดชอบโครงการ</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right"></ol>
                </div>
            </div>
        </div>
    </section>

    <?php if ($profile['Responsible'] == '1' && $profile['Account_id'] == $project['Account_id'] && $project['Status'] != '4') { ?>
        <section class="content">
            <div class="card">
                <?php $file_other = $this->db->get_where('file', ['Project_id' => $PID, 'Check_type_tor' => 2])->result_array(); ?>
                <div class="card-body">
                    <div class="row container">
                        <div class="col-12">
                            <h5>Upload เอกสารที่เกี่ยวข้องกับการประเมินโครงการ</h5>
                        </div><br><br>
                        <label class="col-3" for="">Upload เอกสารที่เกี่ยวข้อง : </label>
                        <br>
                        <span style="color:red;">(อนุญาติเฉพาะไฟล์นามสกุล .doc และ .pdf ขนาดไม่เกิน 2MB เท่านั้น !!)</span>
                        <div class="col-9">
                            <?php $uri = "estimate?PID=" . base64_encode($PID) . "&quarter=" . base64_encode($quarter); ?>
                            <form action="project_estimate_doc" method="post" enctype="multipart/form-data">
                                <input type="file" name="file_name" accept="application/pdf,.doc" required>
                                <input type="text" name="Project_id" value="<?= $PID; ?>" hidden>
                                <input type="hidden" name="uri" value="<?= $uri; ?>">
                                <span><button type="submit" class="btn btn-info">Upload</button></span>
                            </form>
                        </div>

                    </div>
                    <br>
                    <div class="row container">
                        <label class="col-3" for="">เอกสารที่เกี่ยวข้อง : </label>
                        <div class="col-9">
                            <?php if (empty($file_other)) { ?>
                                <span class="badge badge-danger">ยังไม่มีเอกสารเกี่ยวข้อง</span>
                            <?php } else { ?>
                                <?php foreach ($file_other as $file_other) { ?>
                                    <span class="badge badge-primary"><a href="<?= $file_other['Full_path']; ?>" target="_bank" style="color:#fff;"><?= $file_other['Full_path']; ?></a></span><br>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>


    <!-- Main content -->
    <section class="content">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12">
                        <h5>เอกสารประเมินโครงการ</h5>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row container">
                    <label class="col-3" for="">คำชี้แจง : </label>
                    <div class="col-6">
                        <textarea name="Explanation" id="explanation" readonly class="form-control" rows="8"><?= $estimate['Explanation']; ?></textarea>
                    </div>
                </div>
                <br>
                <div class="row container">
                    <label class="col-3" for="">ชื่อโครงการ/กิจกรรม : </label>
                    <div class="col-9">
                        <?= $project['Project_name']; ?>
                    </div>
                </div>
                <br>
                <?php $project_strategic_planeList = $this->db->get_where('project_strategic_plane', ['Project_id' => $project['Project_id']])->result_array();
                foreach ($project_strategic_planeList as $project_strategic_plane) {
                ?>
                    <?php $strategic_plane = $this->db->get_where('strategic_plane', ['Strategic_Plan_id' => $project_strategic_plane['Strategic_Plan_id']])->row_array(); ?>
                    <div class="row container">
                        <label class="col-3" for="">ตอบสนองยุทธ์ศาสตร์ : </label>
                        <div class="col-9">
                            <p><?= $strategic_plane['Strategic_Plan']; ?></p>
                            <?php $strategic = $this->db->get_where('strategic', ['Strategic_id' => $project_strategic_plane['Strategic_id']])->row_array(); ?>
                            <span style="font-weight: bold;">ประเด็นยุทธ์ศาสตร์ที่ : </span><span><?= $strategic['Strategic_name']; ?></span><br>
                            <?php $goal = $this->db->get_where('goal', ['Goal_id' => $project_strategic_plane['Goal_id']])->row_array(); ?>
                            <span style="font-weight: bold;">เป้าประสงค์ที่ : </span><span><?= $goal['Goal_name']; ?></span><br>
                            <?php $indic_project = $this->db->get_where('strategics_project', ['Project_id' => $PID])->result_array(); ?>
                            <?php foreach ($indic_project as $tactic_project) { ?>
                                <?php $tactic_project = $this->db->get_where('tactic', ['Tactic_id' => $tactic_project['Tactic_id']])->row_array(); ?>
                                <span style="font-weight: bold;">กลยุทธ์ที่: </span><span><?= $tactic_project['Tactic_name']; ?></span><br>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?><br>
                <?php $Department = $this->db->get_where('department', ['Department_id' => $project['Department_id']])->row_array(); ?>
                <div class="row container">
                    <label class="col-3" for="">ส่วนงานที่รับผิดชอบ : </label>
                    <div class="col-9">
                        <?= $Department['Department']; ?>
                    </div>
                </div>
                <br>
                <div class="row container">
                    <label class="col-3" for="">วิธีการดำเนินโครงการ : </label>
                    <div class="col-6">
                        <?= $estimate['Conducting']; ?>
                    </div>
                </div>
                <br>
                <div class="row container">
                    <label class="col-3" for="">วัตถุประสงค์ : </label>
                    <?php $objective = $this->db->get_where('objective', ['Project_id' => $PID])->result_array(); ?>
                    <div class="col-6">
                        <?php $ob = 1; ?>
                        <?php foreach ($objective as $objective) { ?>
                            <p>- <?= $objective['Objective_name']; ?></p>
                        <?php } ?>
                        <input type="radio" name="objective_check" class="objective_check" value="1" <?= ($estimate['Objective'] == 1) ? 'checked' : 'disabled'; ?>> บรรลุ &nbsp;&nbsp;
                        <input type="radio" name="objective_check" class="objective_check" value="0" <?= ($estimate['Objective'] == 0) ? 'checked' : 'disabled'; ?>> ไม่บรรลุ
                    </div>
                </div>
                <br>
                <div class="row container">
                    <label class="col-3" for="">ผลการดำเนินงาน : </label>
                    <div class="col-6">
                        <?= $estimate['Result']; ?>
                    </div>
                </div>
                <br>
                <?php $project_indic_success = $this->db->get_where('project_indic_success', ['Project_id' => $PID])->result_array(); ?>
                <div class="row container">
                    <label class="col-3" for="">ผลการดำเนินงานตามตัวชี้วัด : </label>
                    <div class="col-6">
                        <?php foreach ($project_indic_success as $p_indic_s) { ?>
                            <p>- <?= $p_indic_s['Indic_success']; ?></p>
                        <?php } ?>
                        <?= $estimate['Performance']; ?>
                    </div>
                </div>
                <br>
                <?php $charges_main = $this->db->get_where('charges_main', ['Project_id' => $PID])->row_array(); ?>
                <div class="row container">
                    <label class="col-3" for="">งบประมาณที่ใช้ดำเนินการ : </label>
                    <div class="col-6">
                        <p><?= $charges_main['Charges_Main']; ?></p>
                    </div>
                </div>
                <br>
                <?php $charges_sub = $this->db->get_where('charges_sub', ['Charges_Main_id' => $charges_main['Charges_Main_id']])->row_array(); ?>
                <div class="row container">
                    <label class="col-3" for="">ตารางการใช้เงินแต่ละไตรมาส : </label>
                    <div class="col-6">
                        <table class="table table-striped">
                            <thead>
                                <tr class="text-center">
                                    <th>ไตรมาส 1</th>
                                    <th>ไตรมาส 2</th>
                                    <th>ไตรมาส 3</th>
                                    <th>ไตรมาส 4</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-center">
                                    <td><?= (empty($charges_sub['Quarter_one'])) ? "-" : $charges_sub['Quarter_one']; ?></td>
                                    <td><?= (empty($charges_sub['Quarter_two'])) ? "-" : $charges_sub['Quarter_two']; ?></td>
                                    <td><?= (empty($charges_sub['Quarter_three'])) ? "-" : $charges_sub['Quarter_three']; ?></td>
                                    <td><?= (empty($charges_sub['Quarter_four'])) ? "-" : $charges_sub['Quarter_four']; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- <div class="row container">
                        <label class="col-3" for="">ที่ได้รับจัดสรร : </label>
                        <div class="col-6">
                            <p><?= number_format($estimate['Butget']); ?> (<?= $estimate['Butget_char']; ?>)</p>
                        </div>
                    </div> -->
                <br>
                <?php $pj_report = $this->db->get_where('project_report', ['Project_id' => $PID])->result_array(); ?>
                <div class="row container">
                    <label class="col-3" for="">ใช้จริง : </label>
                    <div class="col-6">
                        <table class="table table-striped">
                            <thead>
                                <tr class="text-center">
                                    <th>ไตรมาส 1</th>
                                    <th>ไตรมาส 2</th>
                                    <th>ไตรมาส 3</th>
                                    <th>ไตรมาส 4</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-center">
                                    <td><?= (empty($pj_report[0]['Used'])) ? "-" :  number_format($pj_report[0]['Used']); ?></td>
                                    <td><?= (empty($pj_report[1]['Used'])) ? "-" :  number_format($pj_report[1]['Used']); ?></td>
                                    <td><?= (empty($pj_report[2]['Used'])) ? "-" :  number_format($pj_report[2]['Used']); ?></td>
                                    <td><?= (empty($pj_report[3]['Used'])) ? "-" :  number_format($pj_report[3]['Used']); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                <div class="row container">
                    <label class="col-3" for="">ประโยชน์ที่ได้รับจากการดำเนินโครงการ (หลังการจัดโครงการ) : </label>
                    <div class="col-6">
                        <!-- <textarea name="Benefits" id="Benefits" rows="5" class="form-control"></textarea> -->
                        <?= $estimate['Benefits']; ?>
                    </div>
                </div>
                <br>
                <div class="row container">
                    <label class="col-3" for="">ปัญหาและอุปสรรคในการดำเนินโครงการ : </label>
                    <div class="col-6">
                        <!-- <textarea name="Problem" id="Problem" rows="5" class="form-control"></textarea> -->
                        <?= $estimate['Problem']; ?>
                    </div>
                </div>
                <br>
                <div class="row container">
                    <label class="col-3" for="">แนวทางดำเนินการแก้ไข/ข้อเสนอแนะ : </label>
                    <div class="col-6">
                        <!-- <textarea name="Improvement" id="Improvement" rows="5" class="form-control"></textarea> -->
                        <?= $estimate['Improvement']; ?>
                    </div>
                </div>
                <br>
                <?php if ($URI == 'list_project') { ?>
                    <div class="row">
                        <div class="col-12 text-center">
                            <?php if ($estimate['Flag_close'] == 0) { ?>
                                <p style="color:red;font-weight:bold;">อยู่ระหว่างรอการอนุมัติขอปิดโครงการจากเจ้าหน้าที่แผน</p>
                            <?php } elseif ($estimate['Flag_close'] == 1) { ?>
                                <p style="color:red;font-weight:bold;">อยู่ระหว่างรอการอนุมัติขอปิดโครงการจากผู้บริหาร</p>
                            <?php } ?>
                        </div>
                    </div>
                <?php } elseif ($URI == 'project_manager') { ?>
                    <?php if ($estimate['Flag_close'] == 0) { ?>
                        <div class="row">
                            <div class="col-12 text-center">
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#approved_est"><i class="fa fa-check-circle"></i> อนุมัติขอปิดโครงการ (เจ้าหน้าที่แผน)</button>
                                <!-- <button type="button" class="btn btn-danger" onclick="not_approved_est(<?= $PID; ?>);"><i class=" fa fa-times-circle"></i> ไม่อนุมัติขอปิดโครงการ (เจ้าหน้าที่แผน)</button> -->
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="approved_est" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">อนุมัติขอปิดโครงการ</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="">หมายเหตุ</label>
                                                <textarea name="note" id="note" rows="6" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">ปิดหน้าต่าง</button>
                                            <button type="button" class="btn btn-success" onclick="approved_est(<?= $PID; ?>);">บันทึกข้อมูล</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            function approved_est(pid) {
                                swal({
                                        title: "คุณแน่ใจหรือไม่?",
                                        text: "ต้องการอนุมัติขอปิดโครงการนี้ !",
                                        icon: "warning",
                                        buttons: true,
                                        dangerMode: true,
                                    })
                                    .then((willDelete) => {
                                        if (willDelete) {
                                            let note = $('#note').val();
                                            $.ajax({
                                                url: 'estimate_review_approved',
                                                type: 'GET',
                                                data: {
                                                    project_id: pid,
                                                    status_approved: 1,
                                                    note: note,
                                                },
                                                success(result) {
                                                    if (result == "success") {
                                                        swal("สำเร็จ!", 'บันทึกข้อมูลเรียบร้อยแล้ว', "success");
                                                        setTimeout(function() {
                                                            window.location.reload();
                                                        }, 2000);
                                                    } else {
                                                        swal("เกิดข้อผิดพลาด!", 'กรุณาลองใหม่อีกครั้ง !!', "error");
                                                    }
                                                }
                                            })
                                        }
                                    });
                            }

                            function not_approved_est(pid) {
                                swal({
                                        title: "คุณแน่ใจหรือไม่?",
                                        text: "ไม่อนุมัติขอปิดโครงการนี้ !",
                                        icon: "warning",
                                        buttons: true,
                                        dangerMode: true,
                                    })
                                    .then((willDelete) => {
                                        if (willDelete) {
                                            $.ajax({
                                                url: 'estimate_review_notapproved',
                                                type: 'GET',
                                                data: {
                                                    project_id: pid,
                                                },
                                                success(result) {
                                                    if (result) {
                                                        swal("สำเร็จ!", 'บันทึกข้อมูลเรียบร้อยแล้ว', "success");
                                                        setTimeout(function() {
                                                            window.location.reload();
                                                        }, 2000);
                                                    } else {
                                                        swal("เกิดข้อผิดพลาด!", 'กรุณาลองใหม่อีกครั้ง !!', "error");
                                                    }
                                                }
                                            });
                                        }
                                    });
                            }
                        </script>
                    <?php } elseif ($estimate['Flag_close'] == 2) { ?>
                        <div class="row">
                            <div class="col-12 text-center">
                                <p style="color:green;font-weight:bold;">อนุมัติขอปิดโครงการเรียบร้อยแล้ว</p>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="row">
                            <div class="col-12 text-center">
                                <p style="color:red;font-weight:bold;">อยู่ระหว่างรอการอนุมัติขอปิดโครงการจากผู้บริหาร</p>
                            </div>
                        </div>
                    <?php } ?>
                <?php } elseif ($URI == 'project') { ?>
                    <?php if ($estimate['Flag_close'] == 1) { ?>
                        <div class="row">
                            <div class="col-12 text-center">
                                <button type="button" class="btn btn-success" onclick="approved_est_director(<?= $PID; ?>);"><i class="fa fa-check-circle"></i> อนุมัติขอปิดโครงการ (ผู้บริหาร)</button>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#notapproved_est_director"><i class="fa fa-times-circle"></i> แก้ไขขอปิดโครงการ (ผู้บริหาร)</button>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="notapproved_est_director" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">แก้ไขขอปิดโครงการ</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="">หมายเหตุ</label>
                                            <textarea name="note" id="note_director" rows="6" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">ปิดหน้าต่าง</button>
                                        <button type="button" class="btn btn-success" onclick="notapproved_est_director(<?= $PID; ?>);">บันทึกข้อมูล</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            function approved_est_director(pid) {
                                swal({
                                        title: "คุณแน่ใจหรือไม่?",
                                        text: "ต้องการอนุมัติขอปิดโครงการนี้ !",
                                        icon: "warning",
                                        buttons: true,
                                        dangerMode: true,
                                    })
                                    .then((willDelete) => {
                                        if (willDelete) {
                                            $.ajax({
                                                url: 'estimate_review_approved_director',
                                                type: 'GET',
                                                data: {
                                                    project_id: pid,
                                                    status_approved: 2,
                                                },
                                                success(result) {
                                                    if (result == "success") {
                                                        swal("สำเร็จ!", 'บันทึกข้อมูลเรียบร้อยแล้ว', "success");
                                                        setTimeout(function() {
                                                            window.location.reload();
                                                        }, 2000);
                                                    } else {
                                                        swal("เกิดข้อผิดพลาด!", 'กรุณาลองใหม่อีกครั้ง !!', "error");
                                                    }
                                                }
                                            })
                                        }
                                    });
                            }

                            function notapproved_est_director(pid) {
                                let note_director = $('#note_director').val();
                                swal({
                                        title: "คุณแน่ใจหรือไม่?",
                                        text: "แก้ไขขอปิดโครงการนี้ !",
                                        icon: "warning",
                                        buttons: true,
                                        dangerMode: true,
                                    })
                                    .then((willDelete) => {
                                        if (willDelete) {
                                            $.ajax({
                                                url: 'estimate_review_notapproved_director',
                                                type: 'GET',
                                                data: {
                                                    project_id: pid,
                                                    comment: note_director,
                                                    quarter: <?= $quarter; ?>
                                                },
                                                success(result) {
                                                    if (result) {
                                                        swal("สำเร็จ!", 'บันทึกข้อมูลเรียบร้อยแล้ว', "success");
                                                        setTimeout(function() {
                                                            window.location.reload();
                                                        }, 2000);
                                                    } else {
                                                        swal("เกิดข้อผิดพลาด!", 'กรุณาลองใหม่อีกครั้ง !!', "error");
                                                    }
                                                }
                                            });
                                        }
                                    });
                            }
                        </script>
                    <?php } elseif ($estimate['Flag_close'] == 0) { ?>
                        <div class="row">
                            <div class="col-12 text-center">
                                <p style="color:red;font-weight:bold;">อยู่ระหว่างรอการอนุมัติขอปิดโครงการจากเจ้าหน้าที่แผน</p>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="row">
                            <div class="col-12 text-center">
                                <p style="color:green;font-weight:bold;">อนุมัติขอปิดโครงการเรียบร้อยแล้ว</p>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->