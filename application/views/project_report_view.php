<?php $quarter = base64_decode($this->input->get('quarter')); ?>
<?php $uri = base64_decode($this->input->get('uri')); ?>
<?php $project_report = $this->db->get_where('project_report', ['Quarter' => $quarter, 'Project_id' => $PID])->row_array(); ?>
<?php $charges_main = $this->db->get_where('charges_main', ['Project_id' => $PID])->row_array(); ?>
<?php $charges_sub = $this->db->get_where('charges_sub', ['Charges_Main_id' => $charges_main['Charges_Main_id']])->row_array(); ?>
<script src="assets/js/thaibath.js" type="text/javascript" charset="utf-8"></script>
<!-- Content Wrapper. Contains page content -->
<style>
    input[id="chk_a"][type="checkbox"][readonly] {
        pointer-events: none;
    }

    input[id="period_check"][type="checkbox"][readonly] {
        pointer-events: none;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>งานความก้าวหน้า ผู้รับผิดชอบโครงการ</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right"></ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <form action="">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-12">
                            <h5>รายงานความก้าวหน้า ไตรมาส <?= $quarter; ?></h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row container">
                        <label class="col-3" for="">ชื่อโครงการ : </label>
                        <div class="col-9">
                            <p><?= $project['Project_name']; ?></p>
                        </div>
                    </div>
                    <div class="row container">
                        <label class="col-3" for="">งบประมาณที่จัดสรร : </label>
                        <div class="col-9">
                            <p><?= number_format($project['Butget']); ?> (<?= $project['Butget_char']; ?>)</p>
                        </div>
                    </div>
                    <?php if ($quarter != 1) { ?>
                        <?php $sum_last = $this->db->get_where('project_report', ['Project_id' => $PID, 'Quarter <' => $quarter])->result_array(); ?>
                        <?php $emp = $project['Butget']; ?>
                        <?php foreach ($sum_last as $sum_last) { ?>
                            <?php $emp -= $sum_last['Used']; ?>
                        <?php } ?>
                        <div class="row container">
                            <label class="col-3" for="">งบประมาณที่จัดสรรที่เหลือใช้ : </label>
                            <div class="col-9">
                                <p><?= number_format($emp); ?> บาท</p>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="row container">
                        <label class="col-3" for="">งบไตรมาสที่ <?= $quarter; ?> : </label>
                        <div class="col-9">
                            <?php if ($quarter == 1) { ?>
                                <p><?= $charges_sub['Quarter_one']; ?></p>
                            <?php } elseif ($quarter == 2) { ?>
                                <p><?= $charges_sub['Quarter_two']; ?></p>
                            <?php } elseif ($quarter == 3) { ?>
                                <p><?= $charges_sub['Quarter_three']; ?></p>
                            <?php } else { ?>
                                <p><?= $charges_sub['Quarter_four']; ?></p>
                            <?php } ?>
                        </div>
                    </div>
                    <div>
                        <?php $project_rp = $this->db->get_where('project_report', ['Project_id' => $PID, 'Quarter' => $quarter])->row_array(); ?>
                        <div class="row container input_add form-group">
                            <label class="col-3" for="">ผลการใช้จ่าย : </label>
                            <div class="col-9">
                                <p><?= number_format($project_rp['Used']); ?> บาท</p>
                            </div>
                        </div>
                    </div>
                    <div class="row container">
                        <label class="col-3" for="">ผลตามตัวชี้วัด</label>
                        <div class="col-9">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">ตัวชี้วัด</th>
                                        <th class="text-center">เป้า</th>
                                        <th class="text-center">ผล</th>
                                        <th class="text-center">บรรลุตามตัวชี้วัด</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $Indic = $this->db->get_where('project_indic_success', ['Project_id' => $PID])->result_array(); ?>
                                    <?php foreach ($Indic as $indic) { ?>
                                        <tr>
                                            <td><?= $indic['Indic_success']; ?></td>
                                            <td class="text-center"><?= $indic['Cost']; ?></td>
                                            <?php $inc = $this->db->get_where('indic_project_report', ['Report_id' => $project_report['Report_id'], 'Indic_project_id' => $indic['project_indic_success_id']])->result_array(); ?>
                                            <?php foreach ($inc as $inc) { ?>
                                                <td class="text-center">
                                                    <input type="text" class="form-control" name="Result[]" value="<?= $inc['Result']; ?>" disabled>
                                                </td>
                                                <td class="text-center">
                                                    <?php if ($inc['Achieve'] == 0) { ?>
                                                        <!-- <input type="checkbox" name="achieve[]" id="chk_a" readonly> -->
                                                        <img src="assets/img/multiply.png">
                                                    <?php } else { ?>
                                                        <!-- <input type="checkbox" name="achieve[]" id="chk_a" checked readonly> -->
                                                        <img src="assets/img/checked.png">
                                                    <?php } ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row container">
                        <label class="col-3" for="">ขั้นตอนการดำเนินการ</label>
                        <div class="col-9">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width:50%">ขั้นตอนการดำเนินการ/รายการกิจกรรม</th>
                                        <th class="text-center">เริ่มต้น</th>
                                        <th class="text-center">สิ้นสุด</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($work_step as $work_step) { ?>
                                        <tr>
                                            <td>
                                                <p><?= $work_step['Step_name']; ?></p>
                                            </td>
                                            <td class="text-center">
                                                <div style="position: relative;">
                                                    <p><?= DateThai($work_step['Start']); ?></p>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div style="position: relative;">
                                                    <p><?= DateThai($work_step['Stop']); ?></p>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row container">
                        <label class="col-3" for="">มีการดำเนินงานตามระยะเวลาที่กำหนด :</label>
                        <div class="col-5">
                            <?php if ($project_report['Period_check'] == 0) { ?>
                                <!-- <input type="checkbox" name="period_check" id="period_check" readonly> -->
                                <img src="assets/img/multiply.png">
                            <?php } else { ?>
                                <!-- <input type="checkbox" name="period_check" id="period_check" checked readonly> -->
                                <img src="assets/img/checked.png">
                            <?php } ?>
                        </div>
                    </div><br>
                    <?php $detail = $this->db->get_where('detail', ['Report_id' => $project_report['Report_id']])->result_array(); ?>
                    <div class="row container detail_step">
                        <label class="col-3" for="">รายละเอียดความก้าวหน้า :</label>
                        <div class="col-5">
                            <?php foreach ($detail as $details) { ?>
                                <input type="text" class="form-control detail1" value="<?= $details['Detail']; ?>" disabled><br>
                            <?php } ?>
                        </div>
                    </div>
                    <br>
                    <?php $problem = $this->db->get_where('problem', ['Report_id' => $project_report['Report_id']])->result_array(); ?>
                    <div class="row container problem_step">
                        <label class="col-3" for="">ปัญหา/อุปสรรค :</label>
                        <div class="col-5">
                            <?php foreach ($problem as $problems) { ?>
                                <input type="text" class="form-control problem1" value="<?= $problems['Problem']; ?>" disabled><br>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row container" style="padding: 10px 0 0 0px;">
                        <div class="col-4">
                            <a href="<?= $uri; ?>"><button type="button" class="btn btn-danger"> ย้อนกลับ</button></a>
                        </div>
                    </div>
                    <hr>

                </div>
                <!-- /.card-body -->
            </div>
        </form>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->