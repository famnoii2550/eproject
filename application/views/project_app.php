<script src="assets/js/thaibath.js" type="text/javascript" charset="utf-8"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>จัดการข้อมูลโครงการ</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-3">
                        <h2>สร้างโครงการใหม่</h2>
                    </div>
                </div>
            </div>

            <div class="card-body">

                <div class="row container">
                    <label class="col-3" for="">ปีงบประมาณ : </label>
                    <div class="col-3">
                        <select name="year" class="form-control" required>
                            <?php
                            $Date = date('Y-m-d');
                            $Date10 = date('Y', strtotime($Date . ' + 10 years'));
                            $y_budget = $this->db->get('budget_year')->row_array();
                            for ($x = date('Y'); $x <= $Date10; $x++) { ?>
                                <option value="<?php echo $x; ?>" <?php if ($y_budget['budget_year'] == $x) {
                                                                        echo 'selected';
                                                                    } ?>> <?php echo $x + 543; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">ชื่อโครงการ : </label>
                    <div class="col-9">
                        <input type="text" class="form-control" value="" name="project_name" required>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">หน่วยงานที่รับผิดชอบโครงการ : </label>
                    <div class="col-9">
                        <?php $account_detail = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array(); ?>
                        <?php $cut = explode(",", $account_detail['Department_id']) ?>

                        <?php $department = $this->db->get_where('department', ['Department_id' => $cut[0]])->row_array(); ?>
                        <span><?php echo $department['Department']; ?> </span><br>
                        <?php if (empty($cut[1])) : ?>

                        <?php else : ?>
                            <?php $department1 = $this->db->get_where('department', ['Department_id' => $cut[1]])->row_array(); ?>
                            <span><?php echo $department1['Department']; ?> </span><br>
                        <?php endif; ?>

                        <input type="hidden" name="department_id" value="<?php echo $department['Department_id']; ?>">
                    </div>
                </div>
                <hr>
                <div>
                    <div class="row container input_add form-group">
                        <label class="col-3" for="">ผู้รับผิดชอบโครงการ : </label>
                        <div class="col-7">
                            <?php $accountList = $this->db->get('account')->result_array(); ?>
                            <select class="form-control" name="account[]" id="" required>
                                <option selected disabled>--เลือกผู้รับผิดชอบโครงการ--</option>
                                <?php foreach ($accountList as $account) { ?>
                                    <option value="<?php echo $account['Account_id'] ?>" <?php if ($account_detail['Account_id'] == $account['Account_id']) {
                                                                                                echo "selected";
                                                                                            } ?>><?php echo $account['Fname'] . " " . $account['Lname']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-2">
                            <button type="button" class="btn btn-primary" id="input_add_de"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มผู้รับผิดชอบ</button>
                        </div>
                    </div>
                </div>

                <hr>
                <div class="allstrList">

                </div>

                <script>
                    $(window).on('load', function() {
                        getStrategicPlaneList("plus");
                    });

                    function getStrategicPlaneList(type) {
                        let buttonType = 'minus';
                        if (type) {
                            buttonType = type;
                        }
                        let valueAll_str = $('[name="strategic_plane[]"]');
                        let arrStr_all = [];
                        valueAll_str.each((keyStr, valueStr) => {
                            if (valueStr.value !== "") {
                                arrStr_all.push(valueStr.value);
                            }
                        });
                        $.ajax({
                            url: "Project_strategic_plane_project_app",
                            data: {
                                buttonType: buttonType,
                                arrStr_all: arrStr_all
                            },
                            success: function(response) {
                                $('.allstrList').append(response);
                            }
                        });
                    }
                </script>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">ประเภทโครงการ :</label>
                    <div class="col-5">
                        <input type="checkbox" name="checklist_plan[]" id="checklist_out_plan" value="โครงการใหม่"> โครงการในแผน<br>
                        <input type="checkbox" name="checklist_plan[]" value="โครงการต่อเนื่อง"> โครงการนอกแผน<br>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">ลักษณะโครงการ :</label>
                    <div class="col-5">
                        <input type="radio" name="type" value="โครงการใหม่" required> โครงการใหม่<br>
                        <input type="radio" name="type" value="โครงการต่อเนื่อง" required> โครงการต่อเนื่อง<br>
                        <input type="radio" name="type" value="งานประจำ" required> งานประจำ<br>
                        <input type="radio" name="type" value="งานพัฒนา" required> งานพัฒนา<br>
                    </div>
                </div>
                <hr>
                <div id="first_integra">
                    <div class="row container">
                        <label class="col-3" for="">การบูรณาการโครงการ :</label>
                        <div class="col-5">
                            <select name="integra_name" id="test" onClick="integra_name(this);" class="form-control" required>
                                <option selected disabled>กรุณาเลือก</option>
                                <option value="บูรณาการกับการเรียนการสอน">บูรณาการกับการเรียนการสอน</option>
                                <option value="บูรณาการกับงานวิจัย">บูรณาการกับงานวิจัย</option>
                                <option value="บูรณาการกับงานทำนุบำรุงศิลปวัฒนธรรม">บูรณาการกับงานทำนุบำรุงศิลปวัฒนธรรม</option>
                                <option value="อื่นๆ">อื่นๆ</option>
                                <option value="ไม่มี">ไม่มี</option>
                            </select>
                        </div>
                        <!-- <div class="col-2">
                            <button type="button" class="btn btn-primary" id="integra_btn"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม</button>
                        </div> -->
                    </div>
                </div>
                <div class="row container ee" style="padding: 25px 0 0 0px;display:none">
                    <label class="col-3" for=""> </label>
                    <div class="col-5">
                        <input type="text" name="integra_name_text" class="form-control" placeholder="ชื่อการบูรณาการโครงการ" required>
                    </div>
                </div>
                <div class="row container gg" style="padding: 25px 0 0 0px;display:none">
                    <label class="col-3" for=""> </label>
                    <div class="col-5">
                        <textarea name="integra_detail" id="" cols="30" rows="5" class="form-control" placeholder="เรื่อง / วิชา / คณะ" required></textarea>
                    </div>
                </div>
                <div class="row container gggg" style="padding: 25px 0 0 0px;display:none">
                    <label class="col-3" for="">หลักการและเหตุผล :</label>
                    <div class="col-5">
                        <textarea name="rationale" id="" cols="30" rows="5" class="form-control" required></textarea>
                    </div>
                </div>
                <hr>
                <div id="objapp">
                    <div class="row container">
                        <label class="col-3" for="">วัตถุประสงค์ :</label>
                        <div class="col-7">
                            <input type="text" name="objective_name[]" class="form-control" value="" required>
                        </div>
                        <div class="col-2">
                            <button type="button" class="btn btn-primary" id="objapp_btn"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม</button>
                        </div>
                    </div>
                </div>

                <hr>
                <div class="row container">
                    <label class="col-3" for="">ตัวชี้วัดความสำเร็จระดับโครงการ</label>
                    <div class="col-9">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="text-center">ตัวชี้วัดความสำเร็จ</th>
                                    <th class="text-center">หน่วยนับ</th>
                                    <th class="text-center">ค่าเป้าหมาย</th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody id="trid">
                                <tr class="indic_add1">
                                    <td><input type="text" name="indic_project[]" class="form-control" value="" required></td>
                                    <td><input type="text" name="unit" class="form-control" value="" required></td>
                                    <td><input type="text" name="cost" class="form-control" value="" required></td>
                                    <td><button type="button" class="btn btn-success" id="trid_btn" onClick="indic_add1();"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม</button></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>


                <div class="row container">
                    <label class="col-3" for="">กลุ่มเป้าหมาย :</label>
                    <div class="col-7">
                        <input type="text" name="target_group" class="form-control" value="" required>
                    </div>
                    <div class="col-2">

                    </div>
                </div>


                <hr>

                <div class="row container">
                    <label class="col-3" for="">ขั้นตอนการดำเนินการ</label>
                    <div class="col-9">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width:50%">ขั้นตอนการดำเนินการ/รายการกิจกรรม</th>
                                    <th class="text-center">เริ่มต้น</th>
                                    <th class="text-center">สิ้นสุด</th>
                                    <th class="text-center" style="width: 14%;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="work_step">
                                    <td><input type="text" name="step_name[]" class="form-control" value="" required></td>
                                    <td>
                                        <div style="position: relative;">
                                            <input type="date" name="start" onchange="newYear_start(this);" class="form-control step_start" required>
                                            <div id="newYear_start" style="left: 5%;position: absolute;top: 19%;width: 78%;background-color: #fff;">YYYY-MM-DD</div>
                                        </div>
                                    </td>
                                    <td>
                                        <div style="position: relative;">
                                            <input type="date" name="stop" onchange="newYear_stop(this);" class="form-control step_stop" required>
                                            <div id="newYear_stop" style="left: 5%;position: absolute;top: 19%;width: 78%;background-color: #fff;">YYYY-MM-DD</div>
                                        </div>
                                    </td>
                                    <td><button type="button" class="btn btn-success" onClick="work_step_add();"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม</button></td>
                                </tr>

                            </tbody>

                        </table>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">แหล่งเงิน/ประเภทงบประมาณที่ใช้ :</label>
                    <div class="col-7">
                        <select name="charges_main" id="charges_main" onClick="charges_main(this);" class="form-control" required>
                            <option value="" selected disabled>กรุณาเลือก</option>
                            <option value="งบประมาณรายได้ของมหาลัย">งบประมาณรายได้ของมหาลัย</option>
                            <option value="งบประมาณเงินรายได้ของส่วนงาน">งบประมาณเงินรายได้ของส่วนงาน</option>
                            <option value="งบประมาณเงินรายได้ของแผ่นดิน">งบประมาณเงินรายได้ของแผ่นดิน</option>
                            <option value="งบอื่นๆ">งบอื่นๆ</option>
                            <option value="ไม่ได้ใช้งบประมาณ">ไม่ได้ใช้งบประมาณ</option>
                        </select>
                    </div>
                </div>
                <div class="row container aa" style="padding: 25px 0 0 0px;display:none;">
                    <label class="col-3" for=""></label>
                    <div class="col-7">
                        <input type="text" name="charges_main_text" class="form-control" value="" placeholder="กรุณาระบุ แหล่งเงิน/ประเภทงบประมาณที่ใช้" required>
                    </div>
                </div>
                <div class="row container bb" style="    padding: 25px 0 0 0px;display:none;">
                    <label class="col-3" for="">ปริมาณการงบประมาณที่ใช้ :</label>
                    <div class="col-4">
                        <input type="text" class="form-control" id="DM1" name="butget" placeholder="จำนวนเงิน" required>
                    </div>
                    <div class="col-3">
                        <input type="text" class="form-control" id="DM2" disabled>
                        <input type="hidden" name="butget_text" id="butget_text">
                    </div>
                </div>
                <div class="row container cc" style=" display:none;   padding: 25px 0 0 0px;">
                    <label class="col-3" for="">แผนงาน :</label>
                    <div class="col-7">
                        <select name="work_plan" class="form-control">
                            <option value="" selected disabled>กรุณาเลือก</option>
                            <?php $workplan = $this->db->get('tbl_workplan')->result_array();  ?>
                            <?php foreach ($workplan as $workplan) { ?>
                                <option value="<?php echo $workplan['Workplan_id']; ?>"><?php echo $workplan['Workplan_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="row dd" style="display:none;">
                    <label for="" style="padding: 10px">ประเภทการใช้จ่าย</label>
                    <table id="newtable_quarter" class="table table-striped table-bordered newtable_quarter" style="width:100%">
                        <thead>
                            <tr>
                                <th style="border-bottom:none;"></th>
                                <th class="text-center">ไตรมาส 1</th>
                                <th class="text-center">ไตรมาส 2</th>
                                <th class="text-center">ไตรมาส 3</th>
                                <th class="text-center">ไตรมาส 4</th>
                                <th class="text-center"></th>
                            </tr>
                            <tr>
                                <th class="text-center" style="border-top:none;">ประเภทรายจ่าย</th>
                                <th class="text-center">แผนการใช้จ่าย</th>
                                <th class="text-center">แผนการใช้จ่าย</th>
                                <th class="text-center">แผนการใช้จ่าย</th>
                                <th class="text-center">แผนการใช้จ่าย</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody id="quarter_add" class="quarter_add">
                            <tr>
                                <td><input type="text" name="charges_sub[]" class="form-control" value="" placeholder="งบรายจ่าย" required></td>
                                <td class="text-center"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button type="button" class="btn btn-success" onClick="newtable_quarter();"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม</button>
                                </td>
                            </tr>
                            <tr id="quarter_add_sub" class="quarter_add_sub">
                                <td><input type="text" class="form-control" name="income_butget[]" value="" placeholder="หมวดรายจ่าย" required></td>
                                <td class=""><input type="text" name="quarter_one" class="form-control auto_for" value="" required></td>
                                <td><input type="text" name="quarter_two" class="form-control auto_for" value="" required></td>
                                <td><input type="text" name="quarter_three" class="form-control auto_for" value="" required></td>
                                <td><input type="text" name="quarter_four" class="form-control auto_for" value="" required></td>
                                <td><button type="button" class="btn btn-success" onClick="quarter_add(this);" id=""><i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div class="ff">
                    <div class="row container benefit_step" style="padding: 25px 0 0 0px;">
                        <label class="col-3" for="">ประโยชน์ที่คาดว่าจะได้รับ :</label>
                        <div class="col-7">
                            <input type="text" name="benefit_name[]" class="form-control form-group benefit_name1" placeholder="กรุณาระบุ ประโยชน์ที่คาดว่าจะได้รับ" required>
                        </div>
                        <div class="col-2">
                            <button type="button" class="btn btn-primary" id="trid4_btn" onClick="benefit_name_add();"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม</button>
                        </div>
                    </div>

                    <div class="row container" style="    padding: 25px 0 0 0px;">
                        <label class="col-3" for="">เอกสาร TOR :</label>
                        <div class="col-4">
                            <input type="radio" name="tor" value="มี" class="" id="checkD1" checked onClick="javaScript:if(this.checked){document.all.zz.style.display='none';}" required> มี
                        </div>
                    </div>
                    <div class="row container" style="padding: 0px 0 0 0px;">
                        <label class="col-3" for=""></label>
                        <div class="col-4">
                            <input type="radio" name="tor" value="ไม่มี" class="" id="checkD2" onClick="javaScript:if(this.checked){document.all.zz.style.display='none';}" required> ไม่มี
                        </div>
                    </div>
                    <div class="row container" id="zz" style="padding: 10px 0 0 0px;display:none">
                        <label class="col-3" for=""></label>
                        <div class="col-7">
                            <input type="file" class="form-control">
                        </div>
                    </div>

                </div>
                <div class="row container" style="    padding: 10px 0 0 0px;">
                    <div class="col-4">
                        <button type="button" onClick="send_data_copy(this);" class="btn btn-info"><i class="fa fa-download" aria-hidden="true"></i> จัดเก็บ</button>
                        <button type="button" onClick="send_data(this);" class="btn btn-info"><i class="fa fa-paper-plane" aria-hidden="true"></i> ส่ง</button>
                    </div>
                </div>
                <hr>

            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    (function($, undefined) {

        "use strict";

        // When ready.
        $(function() {


            var $input = $(".auto_for");

            $input.on("keyup", function(event) {

                // When user select text in the document, also abort.
                var selection = window.getSelection().toString();
                if (selection !== '') {
                    return;
                }

                // When the arrow keys are pressed, abort.
                if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
                    return;
                }


                var $this = $(this);

                // Get the value.
                var input = $this.val();

                var input = input.replace(/[\D\s\._\-]+/g, "");
                input = input ? parseInt(input, 10) : 0;

                $this.val(function() {
                    return (input === 0) ? "" : input.toLocaleString("en-US");
                });
            });
        });
    })(jQuery);
</script>
<script>
    $('#integra_btn').on("click", function() {
        alert("xx");
    });



    function remove_integra(e) {
        $(e).parents('#first_integra').remove();
    }

    function planStrategic_plan() {
        let valueAll_str = $('[name="strategic_plane[]"]');
        let arrStr_all = [];
        valueAll_str.each((keyStr, valueStr) => {
            if (valueStr.value !== "") {
                arrStr_all.push(valueStr.value);
            }

        });

        event.preventDefault();
        let new_strategicPlan = '<br>';
        new_strategicPlan += '<div class="allstr">';
        new_strategicPlan += '<div class="row container new_strategicPlan">';
        new_strategicPlan += '<label class="col-3" for=""></label>';
        new_strategicPlan += '<div class="col-7">';
        new_strategicPlan += '<select name="strategic_plane[]" id="strategic_plane"  class="form-control" required>';
        new_strategicPlan += '<option selected disabled value="">--เลือกแผนยุทธ์ศาสตร์--</option>';
        <?php foreach ($strategic_planeList as $strategic_plane) { ?>
            new_strategicPlan += '<option value="<?php echo $strategic_plane['Strategic_Plan_id'] ?>"><?php echo $strategic_plane['Strategic_Plan']; ?></option>';
            // if (arrStr_all.length == 0) {
            //new_strategicPlan += '<option value="<?php echo $strategic_plane['Strategic_Plan_id'] ?>"><?php echo $strategic_plane['Strategic_Plan']; ?></option>';
            // }
            // if (arrStr_all.length > 0) {
            //     arrStr_all.forEach(element => {
            //         if(parseFloat(element) != <?php echo $strategic_plane['Strategic_Plan_id'] ?>){
            //             new_strategicPlan += '<option value="<?php echo $strategic_plane['Strategic_Plan_id'] ?>"><?php echo $strategic_plane['Strategic_Plan']; ?></option>';
            //         }
            //     });
            // }
        <?php } ?>
        new_strategicPlan += '</select>';
        new_strategicPlan += '</div>';
        new_strategicPlan += '<div class="col-2">';
        new_strategicPlan += '<button type="button" class="btn btn-danger" onclick="deleteStrategic_plan(this);"><i class="fa fa-times" aria-hidden="true"></i> ลบแผนยุทธ์ศาสตร์</button>';
        new_strategicPlan += '</div>';
        new_strategicPlan += '</div>';
        new_strategicPlan += '</div>';
        $('.allstr').last().after(new_strategicPlan);

    }

    function deleteStrategic_plan(e) {
        $(e).parents('.allstr').remove();
    }
</script>
<script>
    $("#DM1").keyup(function() {
        var value = $(this).val();
        $("#DM2").val(BAHTTEXT(value));
        $('#butget_text').val(BAHTTEXT(value));
    }).keyup();
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#input_add_de").click(function() {
            let input_account = $('[name="account[]"]');
            let accountList = [];
            input_account.each(function() {
                accountList.push($(this).val());
            });
            // alert(accountList);
            $.ajax({
                url: 'check_account',
                type: 'POST',
                data: {
                    accountList: accountList,
                },
                success(accountText) {
                    $('.input_add').last().after(accountText);
                }
            })

        });

    });

    function remove_account(e) {
        $(e).parents('#remove_account').remove();
    }


    function strategicPlan_select(e) {

        getGoalList(e);

    }

    function strategic_select(e) {
        let valueStrategic = $("#strategic").val();
        if (valueStrategic == "" || valueStrategic == null) {
            return false;
        }


        //เป้าประสงค์
        $.ajax({
            url: "search_strategicList",
            data: {
                valueStrategic: valueStrategic
            },
            success: function(getData) {
                const result = JSON.parse(getData);

                if (result.successfully == true) {
                    if (result.goalList.length > 0) {
                        let newGoalList = '<select name="goal" id="goal" class="form-control" onClick="goal_select(this);" required>';
                        newGoalList += '<option value="" selected disabled>กรุณาเลือกเป้าประสงค์</option>';

                        result.goalList.forEach(goal_detail => {
                            newGoalList += '<option value="' + goal_detail.Goal_id + '">' + goal_detail.Goal_name + '</option>';
                        });

                        newGoalList += '</select>';

                        $("#goalList").html(newGoalList);
                    }

                    if (result.goalList.length == 0) {
                        let newGoalList = '<select class="form-control" disabled>';
                        newGoalList += '<option value="" selected disabled>ไม่มีข้อมูลเป้าประสงค์</option>';
                        newGoalList += '</select>';

                        $("#goalList").html(newGoalList);
                    }
                }
            }
        });
    }

    function goal_select(e) {
        let valueGoal = $("#goal").val();

        if (valueGoal == "" || valueGoal == null) {
            return false;
        }



        //กลยุทธ์
        $.ajax({
            url: "search_tacticList",
            data: {
                valueGoal: valueGoal
            },
            success: function(getData) {
                const result = JSON.parse(getData);

                if (result.successfully == true) {

                    let newIndic_projectList = '<select name="tactic" id="tactic" class="form-control" required>';
                    newIndic_projectList += '<option value="" selected disabled>กรุณาเลือกกลยุทธ์</option>';

                    result.indic_projectList.forEach(Indic_project => {
                        newIndic_projectList += '<option value="' + Indic_project.Indic_project_id + '">' + Indic_project.Tactic_name + '</option>';
                    });

                    newIndic_projectList += '</select>';

                    $("#tacticList").html(newIndic_projectList);

                }

            }
        });
    }
</script>

<script type="text/javascript">
    //วัตถุประสงค์
    $(document).ready(function() {
        $("#objapp_btn").click(function() {
            $('#objapp').append('<span id="remove_objapp"><div class="row container" style="padding-top: 25px;"><label class="col-3" for=""></label><div class="col-7"><input type="text" name="objective_name[]" class="form-control" value="" required></div><div class="col-2"><button type="button" class="btn btn-danger" onClick="objapp_btn_delete(this);" ><i class="fa fa-times" aria-hidden="true"></i> ลบ</button></div></div></span>');
        });

    });

    function integra_name(e) {
        let check_integra = $(e).val();
        $('.ee').css('display', 'none');
        $('.gg').css('display', 'none');
        $('.gggg').css('display', 'none');

        if (check_integra == "บูรณาการกับการเรียนการสอน" || check_integra == "บูรณาการกับงานวิจัย" || check_integra == "บูรณาการกับงานทำนุบำรุงศิลปวัฒนธรรม" || check_integra == "อื่นๆ") {
            if (check_integra == "อื่นๆ") {
                $('.ee').css('display', 'flex');
            }
            $('.gg').css('display', 'flex');
            $('.gggg').css('display', 'flex');
        }
    }

    function objapp_btn_delete(e) {
        $(e).parents('#remove_objapp').remove();
    }

    function indic_add1() {
        let indic_add1 = '<tr class="indic_add1" id="indic_remove1">';
        indic_add1 += '<td><input type="text" name="indic_project[]" class="form-control" value="" required></td>';
        indic_add1 += '<td><input type="text" name="unit" class="form-control" value="" required></td>';
        indic_add1 += '<td><input type="text" name="cost" class="form-control" value="" required></td>';
        indic_add1 += '<td><button type="button" class="btn btn-danger" id="trid_btn" onClick="indic_remove1(this);"><i class="fa fa-times" aria-hidden="true"></i> ลบ</button></td>';
        indic_add1 += '</tr>';
        $('.indic_add1').last().after(indic_add1);
    }

    function indic_remove1(e) {
        $(e).parents('#indic_remove1').remove();
    }

    function work_step_add() {
        let work_step = '<tr class="work_step" id="work_stepRemove">';
        work_step += '<td><input type="text" name="step_name[]" class="form-control" value="" required></td>';
        work_step += '<td><div style="position: relative;"><input type="date" name="start" onchange="newYear_start(this);" class="form-control" value="" required><div id="newYear_start" style="left: 5%;position: absolute;top: 19%;width: 78%;background-color: #fff;">YYYY-MM-DD</div></div></td>';
        work_step += '<td><div style="position: relative;"><input type="date" name="stop" onchange="newYear_stop(this);" class="form-control" value="" required><div id="newYear_stop" style="left: 5%;position: absolute;top: 19%;width: 78%;background-color: #fff;">YYYY-MM-DD</div></div></td>';
        work_step += ' <td><button type="button" class="btn btn-danger" onClick="work_step_remove(this);"><i class="fa fa-times" aria-hidden="true"></i> ลบ</button></td>';
        work_step += '</tr>';

        $('.work_step').last().after(work_step);
    }

    function work_step_remove(e) {
        $(e).parents('#work_stepRemove').remove();
    }

    function charges_main(e) {
        let checkMain = $(e).val();
        $('.aa').css('display', 'none');
        $('.bb').css('display', 'none')
        $('.cc').css('display', 'none');
        $('.dd').css('display', 'none');

        if (checkMain == "งบประมาณรายได้ของมหาลัย" || checkMain == "งบประมาณเงินรายได้ของส่วนงาน" || checkMain == "งบอื่นๆ" || checkMain == "งบประมาณเงินรายได้ของแผ่นดิน") {
            $('.bb').css('display', 'flex');
            $('.cc').css('display', 'flex');
            if (checkMain == "งบอื่นๆ") {
                $('.aa').css('display', 'flex');
            }
            $('.dd').css('display', 'flex');
        }

    }

    function newtable_quarter() {

        let newtable_quarter = '<tbody id="quarter_add" class="quarter_add">';
        newtable_quarter += '<tr>';
        newtable_quarter += '<td><input type="text" name="charges_sub[]" class="form-control" value="" placeholder="งบรายจ่าย" required></td>';
        newtable_quarter += '<td class="text-center"></td>';
        newtable_quarter += '<td></td>';
        newtable_quarter += '<td></td>';
        newtable_quarter += '<td></td>';
        newtable_quarter += '<td><button type="button" class="btn btn-danger" onClick="newtable_quarter_remove(this);" id=""><i class="fa fa-times" aria-hidden="true"></i> ลบ</button></td>';
        newtable_quarter += '</tr>';
        newtable_quarter += '<tr id="quarter_add_sub" class="quarter_add_sub">';
        newtable_quarter += '<td><input type="text" class="form-control" name="income_butget[]" value="" placeholder="หมวดรายจ่าย" required></td>';
        newtable_quarter += '<td class=""><input type="text" name="quarter_one" class="form-control auto_for" value="" required></td>';
        newtable_quarter += '<td><input type="text" name="quarter_two" class="form-control auto_for" value="" required></td>';
        newtable_quarter += '<td><input type="text" name="quarter_three" class="form-control auto_for" value="" required></td>';
        newtable_quarter += '<td><input type="text" name="quarter_four" class="form-control auto_for" value="" required></td>';
        newtable_quarter += '<td><button type="button" class="btn btn-success" onClick="quarter_add(this);" id=""><i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม</button></td>';
        newtable_quarter += '</tr>';
        newtable_quarter += '</tbody>';
        $('.quarter_add').last().after(newtable_quarter);
    }

    function newtable_quarter_remove(e) {
        $(e).parents('#quarter_add').remove();
    }

    function quarter_add(e) {

        let quarter_add = '<tr id="quarter_add_sub" class="quarter_add_sub">';
        quarter_add += '<td><input type="text" class="form-control" name="income_butget[]" value="" placeholder="หมวดรายจ่าย" required></td>';
        quarter_add += '<td class=""><input type="text" name="quarter_one" class="form-control auto_for" value="" required></td>';
        quarter_add += '<td><input type="text" name="quarter_two" class="form-control auto_for" value="" required></td>';
        quarter_add += '<td><input type="text" name="quarter_three" class="form-control auto_for" value="" required></td>';
        quarter_add += '<td><input type="text" name="quarter_four" class="form-control auto_for" value="" required></td>';
        quarter_add += '<td><button type="button" class="btn btn-danger" onClick="quarter_remove(this);"><i class="fa fa-times" aria-hidden="true"></i> ลบ</button></td>';
        quarter_add += '</tr>';

        $(e).parents('#quarter_add_sub').after(quarter_add);

    }

    function quarter_remove(e) {
        $(e).parents('.quarter_add_sub').remove();
    }

    function quarter_add_parents(e) {
        let quarter_add_parents = '<tr class="quarter_add_parents" id="quarter_add_parentsRemove">';
        quarter_add_parents += '<td><input type="text" class="form-control" name="income_butget[]" value="" placeholder="หมวดรายจ่าย" required></td>';
        quarter_add_parents += '<td class=""><input type="text" name="quarter_one" class="form-control auto_for" value="" required></td>';
        quarter_add_parents += '<td><input type="text" name="quarter_two" class="form-control auto_for" value="" required></td>';
        quarter_add_parents += '<td><input type="text" name="quarter_three" class="form-control auto_for" value="" required></td>';
        quarter_add_parents += '<td><input type="text" name="quarter_four" class="form-control auto_for" value="" required></td>';
        quarter_add_parents += ' <td><button type="button" class="btn btn-danger" onClick="quarter_remove(this);"><i class="fa fa-times" aria-hidden="true"></i> ลบ</button></td>';
        quarter_add_parents += '</tr>';

        $(e).parents('#newtable_quarter').find('#quarter_add_parents').after(quarter_add_parents);

    }
    var counters = 2;

    function benefit_name_add() {
        // $('.benefit_name').last().after('<input type="text" name="benefit_name[]" class="form-control form-group benefit_name" placeholder="กรุณาระบุ ประโยชน์ที่คาดว่าจะได้รับ" required>');
        let benefit_step = '<div class="row container benefit_step" id="benefit_stepRemove">';
        benefit_step += '<label class="col-3" for=""></label>';
        benefit_step += '<div class="col-7"><br><input type="text" class="form-control benefit_name' + counters + '" name="benefit_name[]" placeholder="กรุณาระบุ ประโยชน์ที่คาดว่าจะได้รับ"></div>';
        benefit_step += '<div class="col-2">';
        benefit_step += '<br><button type="button" class="btn btn-danger" onClick="benefit_step_remove(this);"><i class="fa fa-times" aria-hidden="true"></i> ลบ</button>';
        benefit_step += '</div>';

        $('.benefit_step').last().after(benefit_step);
        counters++;
    }

    function benefit_step_remove(e) {
        $(e).parents('#benefit_stepRemove').remove();
    }

    function send_data(e) {
        var step_start = $('.step_start').val();
        var step_stop = $('.step_stop').val();

        let un = $('[name="unit"]').val();
        let co = $('[name="cost"]').val();
        let indic_project = $('[name="indic_project[]"]').val();

        if (indic_project == "" || un == "" || co == "") {
            swal("คำเตือน!", 'ตัวชี้วัดความสำเร็จระดับโครงการให้ครบถ้วน', "warning");
            return false;
        } else if (step_start == "" || step_stop == "") {
            swal("คำเตือน!", 'กรุณากรอกวันที่ขั้นตอนการดำเนินการ', "warning");
            return false;
        } else {
            let data = [];
            let year = $('[name="year"]').val();
            let project_name = $('[name="project_name"]').val();
            let department_id = $('[name="department_id"]').val();
            let account = $('[name="account[]"]');
            let accountList = [];
            account.each(function() {
                accountList.push($(this).val());
            });

            let strategic_plane = $('[name="strategic_plane[]"]');
            let strategic_planeList = [];
            strategic_plane.each(function() {
                let goal = $(this).parents('.allstr').find('[name="goal"]').val();
                let indic = $(this).parents('.allstr').find('[name="indic"]').val();
                let tactic = $(this).parents('.allstr').find('[name="tactic"]').val();
                strategic_planeList.push({
                    strategic_plane: $(this).val(),
                    goal: goal,
                    indic: indic,
                    tactic: tactic
                });
            });

            // let strategic =  $('[name="strategic"]').val();
            // let goal = $('[name="goal"]').val();
            // let tactic = $('[name="tactic"]').val();
            let type = $('[name="type"]:checked').val();
            let integra_name = $('[name="integra_name"]').val();
            let integra_name_text = $('[name="integra_name_text"]').val();
            let integra_detail = $('[name="integra_detail"]').val();
            let rationale = $('[name="rationale"]').val();

            let objective_name = $('[name="objective_name[]"]');
            let objective_nameList = [];
            objective_name.each(function() {
                objective_nameList.push($(this).val());
            });

            let dataAll_indic_unit_cost = [];
            let indic_project = $('[name="indic_project[]"]');
            let indic_projectList = [];
            indic_project.each(function() {
                let unit = $(this).parents('.indic_add1').find('[name="unit"]').val();
                let cost = $(this).parents('.indic_add1').find('[name="cost"]').val();
                dataAll_indic_unit_cost.push({
                    indic_project: $(this).val(),
                    unit: unit,
                    cost: cost
                });
            });


            let target_group = $('[name="target_group"]').val();

            let stepList = [];
            let step_name = $('[name="step_name[]"]');
            let step_nameList = [];
            step_name.each(function() {
                let start = $(this).parents('.work_step').find('[name="start"]').val();
                let stop = $(this).parents('.work_step').find('[name="stop"]').val();
                stepList.push({
                    step: $(this).val(),
                    start: start,
                    stop: stop
                });
            });


            let charges_main = $('[name="charges_main"]').val();
            let charges_main_text = $('[name="charges_main_text"]').val();
            let butget = $('[name="butget"]').val();
            let butget_text = $('[name="butget_text"]').val();
            let work_plan = $('[name="work_plan"]').val();


            let charges_sub = $('[name="charges_sub[]"]');
            let charges_sub_list = [];

            charges_sub.each(function() {
                let val_charges = $(this).val();
                let quarter_add_sub = $(this).parents('.quarter_add').find('.quarter_add_sub');
                let quarter_add_subList = [];
                quarter_add_sub.each(function() {
                    let val_incomeButget = {};
                    let income_butget = $(this).find('[name="income_butget[]"]').val();
                    let quarter_one = $(this).find('[name="quarter_one"]').val();
                    let quarter_two = $(this).find('[name="quarter_two"]').val();
                    let quarter_three = $(this).find('[name="quarter_three"]').val();
                    let quarter_four = $(this).find('[name="quarter_four"]').val();
                    val_incomeButget.income_butget = income_butget;
                    val_incomeButget.quarter_one = quarter_one;
                    val_incomeButget.quarter_two = quarter_two;
                    val_incomeButget.quarter_three = quarter_three;
                    val_incomeButget.quarter_four = quarter_four;
                    quarter_add_subList.push(val_incomeButget);
                });

                charges_sub_list.push({
                    charges_sub: val_charges,
                    income_butgetList: quarter_add_subList
                });

            });


            let benefit_name = $('[name="benefit_name[]"]');
            let benefit_nameList = [];
            benefit_name.each(function() {
                benefit_nameList.push($(this).val());
            });

            let tor = $('[name="tor"]:checked').val();
            let titles = $('.tactic option:selected').map(function(idx, elem) {
                return $(elem).val();
            }).get();

            data.push({
                year: year,
                project_name: project_name,
                department_id: department_id,
                accountList: accountList,
                strategic_planeList: strategic_planeList,
                titles: titles,
                // strategic_plane: strategic_plane,
                // strategic:strategic,
                // goal:goal,
                // tactic:tactic,
                type: type,
                integra_name: integra_name,
                integra_name_text: integra_name_text,
                integra_detail: integra_detail,
                rationale: rationale,
                objective_nameList: objective_nameList,
                dataAll_indic_unit_cost: dataAll_indic_unit_cost,
                target_group: target_group,
                stepList: stepList,
                charges_main: charges_main,
                charges_main_text: charges_main_text,
                butget: butget,
                butget_text: butget_text,
                work_plan: work_plan,
                charges_sub_list: charges_sub_list,
                benefit_nameList: benefit_nameList,
                tor: tor
            });
            console.log(data);
            if (titles == '') {
                swal("คำเตือน!", 'กรุณาเลือกแผนยุทธ์ศาสตร์,ประเด็นยุทธ์ศาสตร์,เป้าประสงค์,กลยุทธ์', "warning");
                return false;
            } else {
                $.ajax({
                    url: "success_project_app",
                    method: "POST",
                    data: {
                        data: data
                    },
                    success: function(getData) {
                        const result = JSON.parse(getData);
                        if (result.successfully == true) {
                            swal("Good job!", 'successfully', "success");
                            setTimeout(function() {
                                window.location.href = "list_project";
                            }, 1000);
                            console.log(result.dataList);
                        }
                    }
                });
            }
        }
    }

    function send_data_copy(e) {
        let step_start = $('.step_start').val();
        let step_stop = $('.step_stop').val();
        let data = [];
        let year = $('[name="year"]').val();
        let project_name = $('[name="project_name"]').val();
        let department_id = $('[name="department_id"]').val();
        let account = $('[name="account[]"]');
        let accountList = [];
        account.each(function() {
            accountList.push($(this).val());
        });

        let strategic_plane = $('[name="strategic_plane[]"]');
        let strategic_planeList = [];
        strategic_plane.each(function() {
            let goal = $(this).parents('.allstr').find('[name="goal"]').val();
            let indic = $(this).parents('.allstr').find('[name="indic"]').val();
            let tactic = $(this).parents('.allstr').find('[name="tactic"]').val();
            strategic_planeList.push({
                strategic_plane: $(this).val(),
                goal: goal,
                indic: indic,
                tactic: tactic
            });
        });

        // let strategic =  $('[name="strategic"]').val();
        // let goal = $('[name="goal"]').val();
        // let tactic = $('[name="tactic"]').val();
        let type = $('[name="type"]:checked').val();
        let integra_name = $('[name="integra_name"]').val();
        let integra_name_text = $('[name="integra_name_text"]').val();
        let integra_detail = $('[name="integra_detail"]').val();
        let rationale = $('[name="rationale"]').val();

        let objective_name = $('[name="objective_name[]"]');
        let objective_nameList = [];
        objective_name.each(function() {
            objective_nameList.push($(this).val());
        });

        let dataAll_indic_unit_cost = [];
        let indic_project = $('[name="indic_project[]"]');
        let indic_projectList = [];
        indic_project.each(function() {
            let unit = $(this).parents('.indic_add1').find('[name="unit"]').val();
            let cost = $(this).parents('.indic_add1').find('[name="cost"]').val();
            dataAll_indic_unit_cost.push({
                indic_project: $(this).val(),
                unit: unit,
                cost: cost
            });
        });


        let target_group = $('[name="target_group"]').val();

        let stepList = [];
        let step_name = $('[name="step_name[]"]');
        let step_nameList = [];
        step_name.each(function() {
            let start = $(this).parents('.work_step').find('[name="start"]').val();
            let stop = $(this).parents('.work_step').find('[name="stop"]').val();
            stepList.push({
                step: $(this).val(),
                start: start,
                stop: stop
            });
        });


        let charges_main = $('[name="charges_main"]').val();
        let charges_main_text = $('[name="charges_main_text"]').val();
        let butget = $('[name="butget"]').val();
        let butget_text = $('[name="butget_text"]').val();
        let work_plan = $('[name="work_plan"]').val();


        let charges_sub = $('[name="charges_sub[]"]');
        let charges_sub_list = [];

        charges_sub.each(function() {
            let val_charges = $(this).val();
            let quarter_add_sub = $(this).parents('.quarter_add').find('.quarter_add_sub');
            let quarter_add_subList = [];
            quarter_add_sub.each(function() {
                let val_incomeButget = {};
                let income_butget = $(this).find('[name="income_butget[]"]').val();
                let quarter_one = $(this).find('[name="quarter_one"]').val();
                let quarter_two = $(this).find('[name="quarter_two"]').val();
                let quarter_three = $(this).find('[name="quarter_three"]').val();
                let quarter_four = $(this).find('[name="quarter_four"]').val();
                val_incomeButget.income_butget = income_butget;
                val_incomeButget.quarter_one = quarter_one;
                val_incomeButget.quarter_two = quarter_two;
                val_incomeButget.quarter_three = quarter_three;
                val_incomeButget.quarter_four = quarter_four;
                quarter_add_subList.push(val_incomeButget);
            });

            charges_sub_list.push({
                charges_sub: val_charges,
                income_butgetList: quarter_add_subList
            });

        });


        let benefit_name = $('[name="benefit_name[]"]');
        let benefit_nameList = [];
        benefit_name.each(function() {
            benefit_nameList.push($(this).val());
        });

        let tor = $('[name="tor"]:checked').val();
        let titles = $('.tactic option:selected').map(function(idx, elem) {
            return $(elem).val();
        }).get();

        data.push({
            year: year,
            project_name: project_name,
            department_id: department_id,
            accountList: accountList,
            strategic_planeList: strategic_planeList,
            titles: titles,
            // strategic_plane: strategic_plane,
            // strategic:strategic,
            // goal:goal,
            // tactic:tactic,
            type: type,
            integra_name: integra_name,
            integra_name_text: integra_name_text,
            integra_detail: integra_detail,
            rationale: rationale,
            objective_nameList: objective_nameList,
            dataAll_indic_unit_cost: dataAll_indic_unit_cost,
            target_group: target_group,
            stepList: stepList,
            charges_main: charges_main,
            charges_main_text: charges_main_text,
            butget: butget,
            butget_text: butget_text,
            work_plan: work_plan,
            charges_sub_list: charges_sub_list,
            benefit_nameList: benefit_nameList,
            tor: tor
        });

        $.ajax({
            url: "success_project_app_copy",
            method: "POST",
            data: {
                data: data
            },
            success: function(getData) {
                const result = JSON.parse(getData);
                if (result.successfully == true) {
                    swal("Good job!", 'successfully', "success");
                    setTimeout(function() {
                        window.location.href = "list_project";
                    }, 1000);
                    console.log(result.dataList);
                }
            }
        });
        swal("Good job!", 'successfully', "success");
        setTimeout(function() {
            window.location.href = "list_project";
        }, 1000);
    }
</script>



<script type="text/javascript">
    $('#test').on('change', function() {
        //  alert( this.value ); // or $(this).val()
        if (this.value == "1") {
            $('.ee').css('display', 'none');
            $('.gg').css('display', 'flex');
        } else if (this.value == "2") {
            $('.ee').css('display', 'none');
            $('.gg').css('display', 'flex');
        } else if (this.value == "3") {
            $('.ee').css('display', 'none');
            $('.gg').css('display', 'flex');
        } else if (this.value == "4") {
            $('.ee').css('display', 'flex');
            $('.gg').css('display', 'flex');
        } else {
            $('.ee').css('display', 'none');
            $('.gg').css('display', 'none');
        }
    });
</script>

<script>
    function newYear_start(e) {
        const yearStart = event.target.value;
        const treStart = yearStart.split('-');
        let yearStartUp = parseInt(treStart[0]) + 543;
        const newYearStart = yearStartUp + "/" + treStart[1] + "/" + treStart[2];
        $(e).parents('.work_step').find('#newYear_start').html(newYearStart);

    }

    function newYear_stop(e) {
        const yearStop = event.target.value;
        const treStop = yearStop.split('-');
        let yearStopUp = parseInt(treStop[0]) + 543;
        const newYearStop = yearStopUp + "/" + treStop[1] + "/" + treStop[2];
        $(e).parents('.work_step').find('#newYear_stop').html(newYearStop);
    }
</script>