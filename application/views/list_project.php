<style>
  .not-allowed {
    cursor: not-allowed;
  }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>จัดการข้อมูลโครงการ</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <!-- <button class="btn btn-info" style="float:right;" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus-circle"></i> สร้างประเด็นยุทธ์ศาสตร์ใหม่</button> -->
        <div style="background-color: rgba(0,0,0,.05); padding:15px;">
          <form action="list_project" method="get">
            <div class="row">
              <div class="col-3">
                <span>เลือกปีงบประมาณ</span>
                <select name="year" class="form-control" style="width:35%; display: inline-block;">
                  <option value="">ทุกปี</option>
                  <?php
                  $Date = date('Y-m-d');
                  $Date10 = date('Y', strtotime($Date . ' + 10 years'));
                  for ($x = date('Y'); $x <= $Date10; $x++) { ?>
                    <option value="<?php echo $x; ?>" <?php if (isset($_GET['year']) && $_GET['year'] == $x) {
                                                        echo 'selected';
                                                      } ?>> <?php echo $x + 543; ?></option>
                  <?php } ?>

                </select>
              </div>
              <div class="col-4">
                <span>สถานะ</span>
                <select name="Status" class="form-control" style="width:60%; display: inline-block;">
                  <option value="">ทั้งหมด</option>
                  <option value="2" <?php if (isset($_GET['Status']) && $_GET['Status'] == 2) {
                                      echo 'selected';
                                    } ?>>รอหัวหน้าฝ่ายพิจารณา</option>
                  <option value="1" <?php if (isset($_GET['Status']) && $_GET['Status'] == 1) {
                                      echo 'selected';
                                    } ?>>รอเจ้าหน้าที่แผนตรวจสอบ</option>
                  <option value="3" <?php if (isset($_GET['Status']) && $_GET['Status'] == 3) {
                                      echo 'selected';
                                    } ?>>รออนุมัติจากผู้บริหาร</option>
                  <option value="11" <?php if (isset($_GET['Status']) && $_GET['Status'] == 11) {
                                        echo 'selected';
                                      } ?>>อนุมัติ</option>
                  <option value="12" <?php if (isset($_GET['Status']) && $_GET['Status'] == 12) {
                                        echo 'selected';
                                      } ?>>รอเจ้าหน้าที่แผนตรวจสอบปิดโครงการ</option>
                  <option value="10" <?php if (isset($_GET['Status']) && $_GET['Status'] == 10) {
                                        echo 'selected';
                                      } ?>>รอผู้บริหารอนุมัติปิดโครงการ</option>
                  <option value="6" <?php if (isset($_GET['Status']) && $_GET['Status'] == 6) {
                                      echo 'selected';
                                    } ?>>ปิดโครงการ / เสร็จตามระยะเวลา</option>
                  <option value="7" <?php if (isset($_GET['Status']) && $_GET['Status'] == 7) {
                                      echo 'selected';
                                    } ?>>ปิดโครงการ / ไม่เป็นไปตามระยะเวลา</option>
                  <option value="8" <?php if (isset($_GET['Status']) && $_GET['Status'] == 8) {
                                      echo 'selected';
                                    } ?>>ปิดโครงการ / ขอเลือน</option>
                  <option value="9" <?php if (isset($_GET['Status']) && $_GET['Status'] == 9) {
                                      echo 'selected';
                                    } ?>>ปิดโครงการ / ขอยกเลิก</option>
                  <option value="5" <?php if (isset($_GET['Status']) && $_GET['Status'] == 5) {
                                      echo 'selected';
                                    } ?>>แก้ไขโครงการ</option>
                  <option value="4" <?php if (isset($_GET['Status']) && $_GET['Status'] == 4) {
                                      echo 'selected';
                                    } ?>>แก้ไขเอกสารประเมินโครงการ</option>
                </select>
                <button type="submit" class="btn btn-primary" style="margin-bottom: 3px;"><i class="fa fa-search"></i> ค้นหา</button>
              </div>
            </div>
          </form>
        </div>
        <div class="card-tools">

        </div>
      </div>
      <div class="card-body">
        <table id="table" class="table table-bordered table-striped">
          <thead style="text-align:center;">
            <tr role="row">
              <th rowspan="2" colspan="1" style="width: 230px;">ชื่อโครงการ</th>
              <th rowspan="2" colspan="1" style="width: 163px;">สถานะ</th>
              <th rowspan="2" colspan="1" style="width: 123px;">สถานะการจัดซื้อจัดจ้าง</th>
              <th colspan="2" rowspan="1">เครื่องมือ</th>
              <th colspan="4" rowspan="1">รายงานความก้าวหน้า</th>
              <th style="width: 105px;">วันที่สร้าง</th>

            </tr>
            <tr role="row">
              <th rowspan="1" colspan="1" style="width: 35px;">แก้ไข</th>
              <th rowspan="1" colspan="1" style="width: 35px;">ลบ</th>
              <th rowspan="1" colspan="1" style="width: 105px;">ไตรมาส 1</th>
              <th rowspan="1" colspan="1" style="width: 83px;">ไตรมาส 2</th>
              <th rowspan="1" colspan="1" style="width: 83px;">ไตรมาส 3</th>
              <th rowspan="1" colspan="1" style="width: 105px;">ไตรมาส 4</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($project as $project) { ?>
              <?php $estimate = $this->db->get_where('estimate', ['Project_id' => $project['Project_id']])->row_array(); ?>
              <?php $cm = $this->db->order_by('Comment_id', 'desc')->get_where('comment', ['Project_id' => $project['Project_id']])->row_array(); ?>
              <?php $chk_report = $this->db->get_where('project_report', ['Project_id' => $project['Project_id']])->row_array(); ?>
              <tr>
                <td>
                  <a href="project_list?PID=<?= base64_encode($project['Project_id']); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>"><?= $project['Project_name']; ?></a>

                  <?php if ($project['Status'] == 5) { ?>
                    <span><a href="#" data-toggle="modal" data-target="#exampleModal<?= $project['Project_id']; ?>"><i class="fas fa-comment-alt"></i></a></span>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal<?= $project['Project_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">ข้อมูล</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>

                          <div class="modal-body">
                            <div>
                              <textarea name="Comment" rows="5" style="width: 100%" class="form-control" required disabled><?php echo $cm['Comment']; ?></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                </td>
                <?php if ($project['Status'] == 1) { ?>
                  <td class="text-center"><span class="badge badge-secondary">รอหัวหน้าฝ่ายอนุมัติ</span></td>
                <?php } elseif ($project['Status'] == 2) { ?>
                  <td class="text-center"><span class="badge badge-secondary">รอเจ้าหน้าที่แผนตรวจสอบ</span></td>
                <?php } elseif ($project['Status'] == 3) { ?>
                  <td class="text-center"><span class="badge badge-secondary">รอผู้บริหารอนุมัติ</span></td>
                <?php } elseif ($project['Status'] == 4) { ?>
                  <td class="text-center"><span class="badge badge-danger">ปิดโครงการ</span></td>
                <?php } elseif ($project['Status'] == 9) { ?>
                  <td class="text-center"><span class="badge badge-warning text-white">กำลังเขียนโครงการ</span></td>
                <?php } elseif ($project['Status'] == 11 && $estimate['Flag_close'] == null) { ?>
                  <td class="text-center"><span class="badge badge-primary">อนุมัติ</span></td>
                <?php } elseif ($project['Status'] == 11 && $estimate['Flag_close'] == 0) { ?>
                  <td class="text-center"><span class="badge badge-danger">รอเจ้าหน้าที่แผนตรวจสอบขอปิดโครงการ</span></td>
                <?php } elseif ($project['Status'] == 11 && $estimate['Flag_close'] == 1) { ?>
                  <td class="text-center"><span class="badge badge-danger">รอผู้บริหารตรวจสอบขอปิดโครงการ</span></td>
                <?php } elseif ($project['Status'] == 4 && $estimate['Flag_close'] == 2) { ?>
                  <td class="text-center"><span class="badge badge-danger">รอผู้บริหารตรวจสอบขอปิดโครงการ</span></td>
                <?php } elseif ($project['Status'] == 5) { ?>
                  <td class="text-center"><span class="badge badge-warning">แก้ไขโครงการ</span></td>
                <?php } ?>
                <td style="text-align:center;"><a href="" data-toggle="modal" data-target="#modal-default<?= $project['Project_id']; ?>" class="btn btn-warning btn-xs"><i class="fas fa-search"></i> ดูสถานะ </a></td>
                <?php if ($project['Status'] == 5 || $project['Status'] == 9) { ?>
                  <td style="text-align:center;">
                    <a href="edit_project_app?id=<?php echo base64_encode($project['Project_id']); ?>"><i class="fas fa-pen"></i></a>
                  </td>
                <?php } else { ?>
                  <td style="text-align:center;">
                    <i class="fas fa-ban text-danger"></i>
                  </td>
                <?php } ?>
                <td style="text-align: center;">
                  <?php if ($project['Status'] == 1 || $project['Status'] == 9) { ?>
                    <a href="#"><i class="fa fa-trash text-danger" onclick="delete_project('<?= base64_encode($project['Project_id']); ?>');"></i></a>
                  <?php } else { ?>
                    <i class="fas fa-ban text-danger"></i>
                  <?php } ?>
                </td>
                <?php $Quar = $this->db->get_where('project_report', ['Project_id' => $project['Project_id'], 'Quarter' => 1])->row_array(); ?>
                <?php $Quar2 = $this->db->get_where('project_report', ['Project_id' => $project['Project_id'], 'Quarter' => 2])->row_array(); ?>
                <?php $Quar3 = $this->db->get_where('project_report', ['Project_id' => $project['Project_id'], 'Quarter' => 3])->row_array(); ?>
                <?php $Quar4 = $this->db->get_where('project_report', ['Project_id' => $project['Project_id'], 'Quarter' => 4])->row_array(); ?>
                <?php if ($project['Status'] == 1 || $project['Status'] == 2 || $project['Status'] == 3 || $project['Status'] == 9) { ?>
                  <td style="text-align:center;">
                    <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                  </td>
                  <td style="text-align:center;">
                    <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                  </td>
                  <td style="text-align:center;">
                    <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                  </td>
                  <td style="text-align:center;">
                    <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                  </td>
                  <td>
                    <p><?= DateThai2($project['Time']); ?></p>
                  </td>
                <?php } elseif ($project['Status'] == 5 && $estimate['Flag_close'] == null) { ?>
                  <td style="text-align:center;">
                    <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                  </td>
                  <td style="text-align:center;">
                    <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                  </td>
                  <td style="text-align:center;">
                    <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                  </td>
                  <td style="text-align:center;">
                    <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                  </td>
                  <td>
                    <p><?= DateThai2($project['Time']); ?></p>
                  </td>
                <?php } elseif ($project['Status'] == 11 && $estimate['Flag_close'] == null) { ?>
                  <td style="text-align:center;">
                    <?php if (!empty($Quar)) { ?>
                      <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                    <?php } else { ?>
                      <a href="project_report?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>" class="btn btn-info btn-xs"><i class="fas fa-pen"></i> เขียนรายงาน</a>
                    <?php } ?>
                  </td>

                  <td style="text-align:center;">
                    <?php if (empty($Quar2) && !empty($Quar)) { ?>
                      <a href="project_report?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>" class="btn btn-info btn-xs"><i class="fas fa-pen"></i> เขียนรายงาน</a>
                    <?php } else { ?>
                      <?php if (empty($Quar)) { ?>
                        <a href="#" class="btn btn-default btn-xs not-allowed"><i class="fas fa-search"></i> ดูรายงาน </a>
                      <?php } else { ?>
                        <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs "><i class="fas fa-search"></i> ดูรายงาน</a>
                      <?php } ?>
                    <?php } ?>
                  </td>

                  <td style="text-align:center;">
                    <?php if (empty($Quar3) && !empty($Quar2)) { ?>
                      <a href="project_report?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(3); ?>" class="btn btn-info btn-xs"><i class="fas fa-pen"></i> เขียนรายงาน</a>
                    <?php } else { ?>
                      <?php if (empty($Quar2)) { ?>
                        <a href="#" class="btn btn-default btn-xs not-allowed"><i class="fas fa-search"></i> ดูรายงาน </a>
                      <?php } else { ?>
                        <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(3); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs "><i class="fas fa-search"></i> ดูรายงาน</a>
                      <?php } ?>
                    <?php } ?>
                  </td>

                  <td style="text-align:center;">
                    <?php if (empty($Quar4) && !empty($Quar3)) { ?>
                      <a href="project_report?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(4); ?>" class="btn btn-info btn-xs"><i class="fas fa-pen"></i> เขียนรายงาน</a>
                    <?php } else { ?>
                      <?php if (empty($Qua3)) { ?>
                        <a href="#" class="btn btn-default btn-xs not-allowed"><i class="fas fa-search"></i> ดูรายงาน </a>
                      <?php } else { ?>
                        <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(4); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs "><i class="fas fa-search"></i> ดูรายงาน</a>
                      <?php } ?>
                    <?php } ?>
                  </td>

                  <td>
                    <p><?= DateThai2($project['Time']); ?></p>
                  </td>
                <?php } elseif ($project['Status'] == 11 && ($estimate['Flag_close'] == 0 || $estimate['Flag_close'] == 1 || $estimate['Flag_close'] == 2)) { ?>
                  <?php if ($estimate['Quarter'] == 1) { ?>
                    <td style="text-align:center;">
                      <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ขอปิดโครงการ</a>
                    </td>
                    <td style="text-align:center;">
                      <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                    </td>
                    <td style="text-align:center;">
                      <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                    </td>
                    <td style="text-align:center;">
                      <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                    </td>
                  <?php } elseif ($estimate['Quarter'] == 2) { ?>
                    <td style="text-align:center;">
                      <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                    </td>
                    <td style="text-align:center;">
                      <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ขอปิดโครงการ</a>
                    </td>
                    <td style="text-align:center;">
                      <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                    </td>
                    <td style="text-align:center;">
                      <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                    </td>
                  <?php } elseif ($estimate['Quarter'] == 3) { ?>
                    <td style="text-align:center;">
                      <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                    </td>
                    <td style="text-align:center;">
                      <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                    </td>
                    <td style="text-align:center;">
                      <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(3); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ขอปิดโครงการ</a>
                    </td>
                    <td style="text-align:center;">
                      <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                    </td>
                  <?php } elseif ($estimate['Quarter'] == 4) { ?>
                    <td style="text-align:center;">
                      <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                    </td>
                    <td style="text-align:center;">
                      <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                    </td>
                    <td style="text-align:center;">
                      <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(3); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs "><i class="fas fa-search"></i> ดูรายงาน</a>
                    </td>
                    <td style="text-align:center;">
                      <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(4); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ขอปิดโครงการ</a>
                    </td>
                  <?php } ?>
                  <td>
                    <p><?= DateThai2($project['Time']); ?></p>
                  </td>

                <?php } elseif ($project['Status'] == 4) { ?>
                  <?php if ($estimate['Quarter'] == 1) { ?>
                    <td style="text-align:center;">
                      <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ปิดโครงการ</a>
                    </td>
                    <td style="text-align:center;">
                      <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                    </td>
                    <td style="text-align:center;">
                      <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                    </td>
                    <td style="text-align:center;">
                      <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                    </td>
                  <?php } elseif ($estimate['Quarter'] == 2) { ?>
                    <td style="text-align:center;">
                      <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                      <!-- <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button> -->
                    </td>
                    <td style="text-align:center;">
                      <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ปิดโครงการ</a>
                    </td>
                    <td style="text-align:center;">
                      <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                    </td>
                    <td style="text-align:center;">
                      <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                    </td>
                  <?php } elseif ($estimate['Quarter'] == 3) { ?>
                    <td style="text-align:center;">
                      <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                      <!-- <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button> -->
                    </td>
                    <td style="text-align:center;">
                      <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                      <!-- <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button> -->
                    </td>
                    <td style="text-align:center;">
                      <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(3); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ปิดโครงการ</a>
                    </td>
                    <td style="text-align:center;">
                      <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                    </td>
                  <?php } elseif ($estimate['Quarter'] == 4) { ?>
                    <td style="text-align:center;">
                      <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                      <!-- <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button> -->
                    </td>
                    <td style="text-align:center;">
                      <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                      <!-- <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button> -->
                    </td>
                    <td style="text-align:center;">
                      <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(3); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                      <!-- <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button> -->
                    </td>
                    <td style="text-align:center;">
                      <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(4); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ปิดโครงการ</a>
                    </td>
                  <?php } ?>
                  <td>
                    <p><?= DateThai2($project['Time']); ?></p>
                  </td>
                <?php } ?>
              </tr>
              <div class="modal fade" id="modal-default<?= $project['Project_id']; ?>" style="display: none;" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">สถานะการจัดซื้อจัดจ้าง</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="row my-2 mx-2 mb-4">
                        <div class="col-12">
                          <div class="form-group">
                            <textarea class="form-control" placeholder="สถานะการจัดซื้อจัดจ้าง" style="height:150px;" disabled=""><?= empty($project['purchase']) ? "" : $project['purchase']; ?></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer justify-content-end">
                      <button type="button" class="btn btn-warning" data-dismiss="modal">ปิด</button>
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  function delete_project(id_project) {
    swal({
      title: "แน่ใจหรือไม่?",
      text: "เมื่อลบโครงการแล้ว คุณจะไม่สามารถกู้คืนโครงการนี้ได้ !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        $.ajax({
          url: 'project_delete',
          type: 'GET',
          data: {
            id_project: id_project,
          },
          success(query) {
            if (query == "SUCCESS") {
              window.location.href = 'list_project';
            } else {
              swal("เกิดข้อผิดพลาด , ไม่สามารถทำรายการได้ กรุณาลองใหม่อีกครั้ง!", {
                icon: "error",
              });
            }
          }
        });

      }
    });
  }
</script>