<?php $Strategic_id = $this->input->get('Strategic_id');
$strategicName = $this->db->get_where('strategic', ['Strategic_id' => $Strategic_id])->row_array();
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo $strategicName['Strategic_name']; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">สร้างเป้าประสงค์ใหม่</h3>
            </div>
            <div class="card-body">
                <!-- <div class="form-group row">
                      <p for="" class="col-sm-2 col-lg-2 col-form-label">ชื่อแผนยุทธ์ศาสตร์ :</p>
                      <div class="col-sm-10 col-lg-6">
                          <select name="" class="form-control">
                              <option selected disabled>--- กรุณาเลือกแผนยุทธ์ศาสตร์ ---</option>
                              <option value="">แผนพัฒนาการศึกษาระดับอุดมศึกษา</option>
                          </select>
                      </div>
                  </div> -->
                <form action="" id="send_data" method="POST">
                    <?php foreach ($strategic as $key => $strategicDetail) { ?>
                        <?php $indic_tactic = $this->db->get_where('tactic', ['Goal_id' => $strategicDetail['goal_id']])->result_array(); ?>
                        <div id="first_goal">
                            <div class="form-group row">
                                <p for="" class="col-sm-2 col-lg-2 col-form-label">เป้าประสงค์ :</p>
                                <div class="col-sm-10 col-lg-6">
                                    <input type="text" class="form-control" name="Goal_name[]" required value="<?php echo $strategicDetail['goal']; ?>">
                                    <input type="hidden" name="Goal_id[]" value="<?php echo $strategicDetail['goal_id']; ?>">
                                </div>
                                <div class="form-group" style="text-align:center;">
                                    <?php if ($key == 0) { ?>
                                        <button type="button" class="btn btn-info" onClick="add_goal();" style="color:#fff;">
                                            <i class="fa fa-plus"></i> เพิ่มเป้าประสงค์
                                        </button>
                                    <?php } ?>
                                    <?php if ($key >= 1) { ?>
                                        <button type="button" class="btn btn-danger" onClick="remove_goal(this);" value="<?php echo $strategicDetail['goal_id']; ?>" style="color:#fff;">
                                            <i class="fa fa-times"></i> ลบเป้าประสงค์
                                        </button>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php foreach ($strategicDetail['indic_arr'] as $indic_key => $indic_detail) { ?>
                                <div id="first_indic">
                                    <div class="row" id="indic">
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-8">
                                            <div class="form-group row">
                                                <p for="" class="col-sm-2 col-lg-2 col-form-label">ตัวชี้วัด :</p>
                                                <div class="col-sm-10 col-lg-6">
                                                    <input type="text" class="form-control" name="Indic_project[]" required value="<?php echo $indic_detail['indic_project']; ?>">
                                                    <input type="hidden" name="Indic_project_id[]" value="<?php echo $indic_detail['Indic_project_id']; ?>">
                                                </div>

                                                <div class="form-group" style="text-align:center;">
                                                    <?php if ($indic_key == 0) { ?>
                                                        <button type="button" onClick="add_indic(this);" class="btn btn-info add_indic" id="btn_indic" style="color:#fff;">
                                                            <i class="fa fa-plus"></i> เพิ่มตัวชี้วัด
                                                        </button>
                                                    <?php } ?>
                                                    <?php if ($indic_key >= 1) { ?>
                                                        <button type="button" onClick="remove_indic(this);" value="<?php echo $indic_detail['Indic_project_id']; ?>" class="btn btn-danger add_indic" id="btn_indic" style="color:#fff;">
                                                            <i class="fa fa-times"></i> ลบตัวชี้วัด
                                                        </button>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-8">
                                            <div class="form-group row">
                                                <p for="" class="col-sm-2 col-lg-2 col-form-label">หน่วยนับ :</p>
                                                <div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Unit" required value="<?php echo $indic_detail['unit']; ?>"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2"></div>
                                    </div>
                                    <div class="row" id="Cost">
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-8">
                                            <div class="form-group row">
                                                <p for="" class="col-sm-2 col-lg-2 col-form-label">ค่าเป้าหมาย :</p>
                                                <div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Cost" required value="<?php echo $indic_detail['cost']; ?>"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2"></div>
                                    </div>



                                </div>
                            <?php } ?>
                            <?php foreach ($indic_tactic as $tactic_key => $indic_tactic) { ?>
                                <div id="first_tactic">
                                    <div class="row">
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-8">
                                            <div class="form-group row">
                                                <p for="" class="col-sm-2 col-lg-2 col-form-label">กลยุทธ์ :</p>
                                                <div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Tactic_name[]" required value="<?php echo $indic_tactic['Tactic_name']; ?>"></div>
                                                <div class="form-group" style="text-align:center;">
                                                    <?php if ($tactic_key == 0) { ?>
                                                        <button type="button" onClick="add_tactic(this);" class="btn btn-info add_tactic" id="btn_tactic" style="color:#fff;">
                                                            <i class="fa fa-plus"></i> เพิ่มกลยุทธ์
                                                        </button>
                                                    <?php } ?>
                                                    <?php if ($tactic_key >= 1) { ?>
                                                        <button type="button" onClick="remove_tactic(this);" class="btn btn-danger add_tactic" id="btn_tactic" style="color:#fff;">
                                                            <i class="fa fa-times"></i> ลบกลยุทธ์
                                                        </button>
                                                    <?php } ?>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2"></div>
                                    </div>
                                </div>
                            <?php } ?>
                            <hr>
                        </div>

                    <?php } ?>

                    <hr>

                    <div class="removeGoal"></div>
                    <div class="removeIndic"></div>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 text-right">
                            <button type="button" class="btn btn-primary" id="event_send"><i class="fa fa-download"></i> จัดเก็บ</button>
                            <a href="workplan" type="button" class="btn btn-danger text-white"><i class="fa fa-times"></i> ย้อนกลับ</a>
                        </div>
                    </div>
                </form>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
    </section>
    <!-- /.content -->
</div>

<!-- /.content-wrapper -->


<script type="text/javascript">
    //เป้าประสงค์

    function add_goal() {
        $('#first_goal').after('<div id="first_goal"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">เป้าประสงค์ :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Goal_name[]" required></div><div class="form-group" style="text-align:center;"><button type="button" onClick="remove_goal(this);" class="btn btn-danger" style="color:#fff;"><i class="fa fa-times"></i> ลบเป้าประสงค์</button></div></div><div id="first_indic"><div class="row" id="indic"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">ตัวชี้วัด :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Indic_project[]" required></div><div class="form-group" style="text-align:center;"><button type="button" onClick="add_indic(this);" class="btn btn-info add_indic" id="btn_indic" style="color:#fff;"><i class="fa fa-plus"></i> เพิ่มตัวชี้วัด</button></div></div></div><div class="col-lg-2"></div></div><div class="row"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">หน่วยนับ :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Unit" required></div></div></div><div class="col-lg-2"></div></div><div class="row"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">ค่าเป้าหมาย :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Cost" required></div></div></div><div class="col-lg-2"></div></div><div class="row"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">กลยุทธ์ :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Tactic_name" required></div></div></div><div class="col-lg-2"></div></div></div></div>');
    }

    function remove_goal(e) {
        $(e).parents('#first_goal').remove();
        $(".removeGoal").append("<input type='hidden' name='remove_goal[]' value='" + e.value + "'/>");
    }

    //ตัวชี้วัด

    function add_indic(e) {
        $(e).parents('#first_indic').find('#Cost').after('<hr><div id="first_indic"><div class="row" id="indic"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">ตัวชี้วัด :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Indic_project[]" required></div><div class="form-group" style="text-align:center;"><button type="button"  class="btn btn-danger" onClick="remove_indic(this);" id="btn_indic_remove" style="color:#fff;"><i class="fa fa-times"></i> ลบตัวชี้วัด</button></div></div></div><div class="col-lg-2"></div></div><div class="row"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">หน่วยนับ :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Unit" required></div></div></div><div class="col-lg-2"></div></div><div class="row"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">ค่าเป้าหมาย :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Cost" required></div></div></div><div class="col-lg-2"></div></div></div>');
    }

    function remove_indic(e) {
        $(e).parents('#first_indic').remove();
        $(".removeIndic").append("<input type='hidden' name='remove_indic[]' value='" + e.value + "'/>");

    }

    //กลยุทธ์
    function add_tactic(e) {
        $(e).parents('#first_tactic').after('<div id="first_tactic"><div class="row"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">กลยุทธ์ :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Tactic_name[]" required></div><div class="form-group" style="text-align:center;"><button type="button" onClick="remove_tactic(this);" class="btn btn-danger add_tactic" id="btn_tactic" style="color:#fff;"><i class="fa fa-times"></i> ลบกลยุทธ์</button></div></div><div class="col-lg-2"></div></div></div>');
    }

    function remove_tactic(e) {
        $(e).parents('#first_tactic').remove();
    }
    $('#event_send').on('click', function() {
        swal({
            title: "คุณแน่ใจหรือไม่?",
            text: "คุณต้องการบันทึกข้อมูลใช่หรือไม่ !?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                let goal_name = $('[name="Goal_name[]"]');
                let data = [];
                // ดึงค่า form ที่ต้องทำการแก้ไข
                goal_name.each(function() {
                    let Indic_project = $(this).parents('#first_goal').find('[name="Indic_project[]"]');
                    let Indic_tactic = $(this).parents('#first_goal').find('[name="Tactic_name[]"]');
                    let Indic_list = [];
                    let tactic_list = [];
                    Indic_project.each(function() {
                        let indic_value = $(this).val();
                        let unit_value = $(this).parents('#first_indic').find('[name="Unit"]').val();
                        let cost_value = $(this).parents('#first_indic').find('[name="Cost"]').val();
                        // เพิ่มค่า indic เข้าไปใน array
                        Indic_list.push({
                            indic_project_id: $(this).parents('#first_indic').find('[name="Indic_project_id[]"]').val() || null,
                            indic_project: indic_value,
                            unit: unit_value,
                            cost: cost_value,
                        });
                    });
                    // กลยุทธ์
                    Indic_tactic.each(function() {
                        tactic_list.push($(this).val());
                    });
                    // รวมค่าทั้งหมด
                    data.push({
                        goal_name: $(this).val(),
                        goal_id: $(this).parents('#first_goal').find('[name="Goal_id[]"]').val() || null,
                        indic_list: Indic_list,
                        tactic_list: tactic_list,
                    });

                    console.log(data);
                });
                // ดึงค่า goal id ที่ต้องการลบ
                let removeGoal = $(this).find('.removeGoal').find('input');
                let remove_goal = [];
                removeGoal.each(function() {
                    remove_goal.push($(this).val());
                });

                // ดึงค่า indic id ที่ต้องการลบ
                let removeIndic = $(this).find('.removeIndic').find('input');
                let remove_indic = [];
                removeIndic.each(function() {
                    remove_indic.push($(this).val());
                });
                let Strategic_id = <?php echo $Strategic_id; ?>;
                // เซ็ตค่าก่อนส่ง
                let edit_data = {};
                edit_data.dataList = data;
                edit_data.remove_goal = remove_goal;
                edit_data.remove_indic = remove_indic;
                edit_data.Strategic_id = Strategic_id;
                console.log(edit_data);

                // ส่งข้อมูล
                $.ajax({
                    url: "strategic_edit_process",
                    method: "POST",
                    data: edit_data,
                    success: function(getData) {
                        const result = JSON.parse(getData);
                        console.log(result);
                        if (result.successfully == true) {
                            swal("Good job!", 'successfully', "success");
                            setTimeout(function() {
                                window.location.href = "workplan?stp=" + btoa(<?= $stp; ?>);
                            }, 1000);
                        }
                    }
                });
            } else {
                swal("ยกเลิก", "ยกเลิกการปิดโครงการ", "error");
            }
        });
    });
</script>