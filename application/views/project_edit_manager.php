<script src="assets/js/thaibath.js" type="text/javascript" charset="utf-8"></script>
<style>
    #ic,
    #passport {
        display: none;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 style="display: contents;">ข้อมูลโครงการ</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <?php $uri = base64_decode($this->input->get('uri')); ?>
    <?php $ac = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array(); ?>
    <?php $status = $this->db->get_where('project', ['Project_id' => $PID])->row_array(); ?>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <div class="row" style="padding: 4px 0px 10px 8px;">
                    <div class="col-10">
                        <u> <a>รายละเอียดโครงการ</a> </u> <span style="padding: 0px 10px;"> / </span>
                        <a href="project_doc_manager?PID=<?php echo base64_encode($PID); ?>&uri=<?= base64_encode($uri); ?>">เอกสารโครงการ</a> <span style="padding: 0px 10px;"> / </span>
                        <a href="project_consider_manager?PID=<?php echo base64_encode($PID); ?>&uri=<?= base64_encode($uri); ?>">พิจารณาโครงการ</a> <span style="padding: 0px 10px;"> / </span>
                        <a href="project_review_manager?PID=<?php echo base64_encode($PID); ?>&uri=<?= base64_encode($uri); ?>">ความคิดเห็น</a>
                    </div>
                    <div class="col-2 text-right">
                        <a href="project_manager">กลับหน้าโครงการ</a>
                    </div>
                </div>
                <!-- <button class="btn btn-info" style="float:right;" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus-circle"></i> สร้างประเด็นยุทธ์ศาสตร์ใหม่</button> -->
                <div style="background-color: rgba(0,0,0,.05); padding:15px;">
                    <div class="row">
                        <div class="col-3">
                            <a href="project_proposal?PID=<?= base64_encode($PID); ?>" target="_bank"><img src="assets/img/pdf.png" alt="" style="width: 50px;"> แบบเสนอโครงการ</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php $project = $this->db->get_where('project', ['Project_id' => $PID])->row_array(); ?>
            <div class="card-body">
                <div class="row container">
                    <label class="col-3" for="">ปีงบประมาณ : </label>
                    <div class="col-9">
                        <span><?php echo $project['Year'] + 543; ?></span>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">ชื่อโครงการ : </label>
                    <div class="col-9">
                        <span><?php echo $project['Project_name']; ?></span>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">หน่วยงานที่รับผิดชอบโครงการ : </label>
                    <div class="col-9">
                        <?php
                        $account_detail = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
                        $department = $this->db->get_where('department', ['Department_id' => $account_detail['Department_id']])->row_array();
                        $users = $this->db->get_where('user', ['Project_id' => $PID])->result_array();
                        ?>
                        <span><?php echo $department['Department']; ?></span>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">ผู้รับผิดชอบโครงการ : </label>
                    <div class="col-9">
                        <?php
                        $i = 0;
                        foreach ($users as $user) {
                            $i += 1;
                            $accountUser = $this->db->get_where('account', ['Account_id' => $user['Account_id']])->row_array();
                        ?>
                            <span><?php echo $i . '.' . $accountUser['Fname'] . ' ' . $accountUser['Lname']; ?><br></span>
                        <?php } ?>
                    </div>
                </div>
                <hr>
                <div class="allstrList">
                    <?php if (!empty($project_strategic_plane)) { ?>
                        <?php foreach ($project_strategic_plane as $key => $project_strategic_plane) { ?>
                            <div class="allstr">
                                <div class="row container new_strategicPlan"><label class="col-3" for="">ชื่อแผนยุทธ์ศาสตร์ : </label>
                                    <div class="col-7">
                                        <select id="strategic_plane" name="strategic_plane[]" class="form-control" required>
                                            <option selected disabled>--เลือกแผนยุทธ์ศาสตร์--</option>
                                            <?php foreach ($strategic_planeList as $strategic_plane) { ?>
                                                <option value="<?php echo $strategic_plane['Strategic_Plan_id']; ?>" <?= $strategic_plane['Strategic_Plan_id'] == $project_strategic_plane['Strategic_Plan_id']  ? 'selected' : '' ?>><?php echo $strategic_plane['Strategic_Plan']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-2">
                                        <?php if ($key == 0) { ?>
                                            <button type="button" class="btn btn-primary" id="add_straregic_check"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มแผนยุทธ์ศาสตร์</button>
                                        <?php } else { ?>
                                            <button type="button" class="btn btn-danger" id="remove_straregic_check"><i class="fa fa-times" aria-hidden="true"></i> ลบแผนยุทธ์ศาสตร์</button>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div id="strategicList">
                                    <div class="row container" style=" padding: 25px 0 0 0px;">
                                        <div class="col-3"> </div>
                                        <label class="col-2 text-right" for="">ประเด็นยุทธศาสตร์ :</label>
                                        <div class="col-4">
                                            <select class="form-control load_strategic<?= $project_strategic_plane['Project_strategic_plane_id']; ?>" name="strategic" id="">
                                                <option value="" selected disabled>กรุณาเลือกประเด็นยุทธศาสตร์</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div id="goalList">
                                        <div class="row container" style="    padding: 25px 0 0 0px;">
                                            <div class="col-3"> </div>
                                            <label class="col-2 text-right" for=""> เป้าประสงค์ :</label>
                                            <div class="col-4">
                                                <select class="form-control load_goal<?= $project_strategic_plane['Project_strategic_plane_id']; ?>" name="goal" id="">
                                                    <option value="" selected disabled>กรุณาเลือกเป้าประสงค์</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- <div id="indicList"></div>  -->
                                        <div id="tacticList">
                                            <div class="row container" style="padding: 25px 0 0 0px;">
                                                <div class="col-3"> </div>
                                                <label class="col-2 text-right" for="">กลยุทธ์ :</label>
                                                <div class="col-4">
                                                    <select class="form-control load_tactic_indic<?= $project_strategic_plane['Project_strategic_plane_id']; ?>" name="tactic_indic" id="">
                                                        <option value="" selected disabled>กรุณาเลือกกลยุทธ์</option>
                                                    </select>

                                                </div>
                                            </div>

                                        </div>
                                        <script>
                                            function getTacticList(event) {
                                                let tacticGet = false;
                                                if (event) {
                                                    tacticGet = event.val();
                                                }
                                                $.ajax({
                                                    url: "<?php echo base_url('Project_ctr/tactic_project_edit'); ?>",
                                                    data: {
                                                        tacticGet: tacticGet
                                                    },
                                                    success: function(response) {
                                                        // event.parents('#indicList').find('#tacticList').html(response);
                                                        event.parents('#goalList').find('#tacticList').html(response);
                                                    }
                                                });
                                            }
                                        </script>

                                    </div>

                                    <script>
                                        $("select[name='strategic']").change(function() {
                                            getGoalList($(this));

                                        });

                                        function getGoalList(event) {
                                            let goalGet = false;
                                            if (event) {
                                                goalGet = event.val();
                                            }
                                            $.ajax({
                                                url: "<?php echo base_url('Project_ctr/goal_project_app'); ?>",
                                                data: {
                                                    goalGet: goalGet
                                                },
                                                success: function(response) {
                                                    event.parents('#strategicList').find('#goalList').html(response);
                                                }
                                            });
                                        }
                                    </script>
                                </div>
                                <script>
                                    $("button#add_straregic_check").off().on('click', function() {
                                        getStrategicPlaneList();
                                    });
                                    $("select#strategic_plane").change(function() {
                                        getStrategicList($(this));
                                    });
                                    $("button#remove_straregic_check").off().on('click', function() {
                                        $(this).parents('.allstr').remove();
                                    });

                                    function getStrategicList(event) {
                                        let strategicGet = false;
                                        if (event) {
                                            strategicGet = event.val();
                                        }
                                        $.ajax({
                                            url: "Project_ctr/strategic_project_edit",
                                            data: {
                                                strategicGet: strategicGet
                                            },
                                            beforeSend: function() {
                                                event.parents('.allstr').find('#strategicList').html('Loading...');
                                            },
                                            success: function(response) {
                                                event.parents('.allstr').find('#strategicList').html(response);
                                            }
                                        });
                                    }


                                    $(window).on('load', function() {
                                        getStrategic_strategic_plane<?= $project_strategic_plane['Project_strategic_plane_id']; ?>();
                                    });

                                    $(window).on('load', function() {
                                        getStrategic_goal<?= $project_strategic_plane['Project_strategic_plane_id']; ?>();
                                    });

                                    $(window).on('load', function() {
                                        getStrategic_tactic<?= $project_strategic_plane['Project_strategic_plane_id']; ?>();
                                    });


                                    function getStrategic_strategic_plane<?= $project_strategic_plane['Project_strategic_plane_id']; ?>() {

                                        var search = '<?= $project_strategic_plane['Strategic_Plan_id']; ?>';
                                        var search_strategic_id = '<?= $project_strategic_plane['Strategic_id']; ?>';

                                        console.log(search);

                                        if (search == 0 || search == '') {
                                            $('.load_strategic<?= $project_strategic_plane['Project_strategic_plane_id']; ?>').html('');
                                        } else {
                                            $.ajax({
                                                type: 'POST',
                                                data: {
                                                    search: search,
                                                    search_strategic_id: search_strategic_id,
                                                },
                                                url: 'strategic_search_ajax_edit',

                                                success: function(data) {
                                                    // console.log(data);

                                                    $('.load_strategic<?= $project_strategic_plane['Project_strategic_plane_id']; ?>').html(data);
                                                }
                                            });
                                        }
                                    }

                                    function getStrategic_goal<?= $project_strategic_plane['Project_strategic_plane_id']; ?>() {

                                        var search = '<?= $project_strategic_plane['Strategic_id']; ?>';
                                        var search_goal_id = '<?= $project_strategic_plane['Goal_id']; ?>';

                                        // console.log(id_invoice_main);

                                        if (search == 0 || search == '') {
                                            $('.load_goal<?= $project_strategic_plane['Project_strategic_plane_id']; ?>').html('');
                                        } else {
                                            $.ajax({
                                                type: 'POST',
                                                data: {
                                                    search_goal: search,
                                                    search_goal_id: search_goal_id,
                                                },
                                                url: 'goal_search_ajax_edit',

                                                success: function(data) {
                                                    // console.log(data);

                                                    $('.load_goal<?= $project_strategic_plane['Project_strategic_plane_id']; ?>').html(data);
                                                }
                                            });
                                        }
                                    }

                                    function getStrategic_tactic<?= $project_strategic_plane['Project_strategic_plane_id']; ?>() {

                                        var search = '<?= $project_strategic_plane['Goal_id']; ?>';
                                        var search_tactic_id = '<?= $project_strategic_plane['Indic_project_id']; ?>';

                                        // console.log(id_invoice_main);

                                        if (search == 0 || search == '') {
                                            $('.load_tactic_indic<?= $project_strategic_plane['Project_strategic_plane_id']; ?>').html('');
                                        } else {
                                            $.ajax({
                                                type: 'POST',
                                                data: {
                                                    search_tactic: search,
                                                    search_tactic_id: search_tactic_id,
                                                },
                                                url: 'tactic_search_ajax_edit',

                                                success: function(data) {
                                                    // console.log(data);

                                                    $('.load_tactic_indic<?= $project_strategic_plane['Project_strategic_plane_id']; ?>').html(data);
                                                }
                                            });
                                        }
                                    }
                                </script>
                            </div>
                        <?php } ?>
                    <?php } else { ?>

                        <div class="allstr">
                            <div class="row container new_strategicPlan"><label class="col-3" for="">ชื่อแผนยุทธ์ศาสตร์ : </label>
                                <div class="col-7">
                                    <select id="strategic_plane" name="strategic_plane[]" class="form-control" required>
                                        <option selected disabled>--เลือกแผนยุทธ์ศาสตร์--</option>
                                        <?php foreach ($strategic_planeList as $strategic_plane) { ?>
                                            <option value="<?php echo $strategic_plane['Strategic_Plan_id']; ?>"><?php echo $strategic_plane['Strategic_Plan']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-2">
                                    <button type="button" class="btn btn-primary" id="add_straregic_check"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มแผนยุทธ์ศาสตร์</button>
                                </div>
                            </div>
                            <div id="strategicList"></div>
                        </div>

                    <?php } ?>
                </div>

                <script>
                    function getStrategicPlaneList(type) {
                        let buttonType = 'minus';

                        let valueAll_str = $('[name="strategic_plane[]"]');
                        let arrStr_all = [];
                        valueAll_str.each((keyStr, valueStr) => {
                            if (valueStr.value !== "") {
                                arrStr_all.push(valueStr.value);
                            }
                        });
                        $.ajax({
                            url: "Project_strategic_plane_project_edit",
                            data: {
                                buttonType: buttonType,
                                arrStr_all: arrStr_all
                            },
                            success: function(response) {
                                $('.allstrList').append(response);
                            }
                        });
                    }
                </script>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">ลักษณะโครงการ :</label>
                    <div class="col-5">
                        <?php echo $project['Type']; ?>
                    </div>

                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">การบูรณาการโครงการ :</label>
                    <div class="col-5">
                        <?php echo $project['Integra_name']; ?>

                        <?php if ($project['Integra_name'] != "ไม่มี") { ?>
                            <textarea name="integra_detail" style="margin-top: 15px;" id="" cols="30" rows="5" class="form-control" disabled><?php echo $project['Integra_detail']; ?></textarea>
                        <?php } ?>
                    </div>
                </div>

                <div class="row container">
                    <label class="col-3" for="">หลักการและเหตุผล :</label>
                    <div class="col-5">
                        <textarea name="integra_detail" style="margin-top: 15px;" id="" cols="30" rows="5" class="form-control" disabled><?php echo $project['Rationale']; ?></textarea>
                    </div>
                </div>

                <hr>
                <div class="row container ">
                    <label class="col-3" for="">วัตถุประสงค์ :</label>
                    <div class="col-5">
                        <?php
                        $o = 0;
                        $objects = $this->db->get_where('objective', ['Project_id' => $PID])->result_array();
                        foreach ($objects as $object) {
                            $o += 1;
                            echo $o . '.' . $object['Objective_name'];
                            echo '<br>';
                        }

                        ?>
                    </div>

                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">ตัวชี้วัดความสำเร็จระดับโครงการ</label>
                    <div class="col-9">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="text-center">ตัวชี้วัดความสำเร็จ</th>
                                    <th class="text-center">หน่วยนับ</th>
                                    <th class="text-center">ค่าเป้าหมาย</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $project_indic_success = $this->db->get_where('project_indic_success', ['Project_id' => $PID])->result_array();
                                foreach ($project_indic_success as $project_indic_success_detail) {

                                ?>
                                    <tr>
                                        <td><?php echo $project_indic_success_detail['Indic_success']; ?></td>
                                        <td><?php echo $project_indic_success_detail['Unit']; ?></td>
                                        <td><?php echo $project_indic_success_detail['Cost']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">กลุ่มเป้าหมาย :</label>
                    <div class="col-7">
                        <?php echo $project['Target_group']; ?>
                    </div>
                    <div class="col-2">

                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">ขั้นตอนการดำเนินการ</label>
                    <div class="col-9">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width:50%">ขั้นตอนการดำเนินการ/รายการกิจกรรม</th>
                                    <th class="text-center">เริ่มต้น</th>
                                    <th class="text-center">สิ้นสุด</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php $work_steps = $this->db->get_where('work_step', ['Project_id' => $PID])->result_array();
                                foreach ($work_steps as $work_step) {

                                ?>
                                    <tr class="work_step">
                                        <td><?php echo $work_step['Step_name']; ?></td>
                                        <td><?php echo $work_step['Start']; ?></td>
                                        <td><?php echo $work_step['Stop']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="row container">
                    <label class="col-3" for="">แหล่งเงิน/ประเภทงบประมาณที่ใช้ :</label>
                    <div class="col-7">

                        <?php echo $project['Source']; ?>

                    </div>
                </div>
                <?php if ($project['Source'] != "ไม่ได้ใช้งบประมาณ") { ?>
                    <div class="row container" style=" padding: 25px 0 0 0px;">
                        <label class="col-3" for="">ปริมาณการงบประมาณที่ใช้ :</label>
                        <div class="col-4">
                            <?php echo number_format($project['Butget']); ?>
                            (<?php echo $project['Butget_char']; ?>)
                        </div>
                        <div class="col-3">

                        </div>
                    </div>
                    <div class="row container" style="padding: 25px 0 0 0px;">
                        <label class="col-3" for="">แผนงาน :</label>
                        <div class="col-7">
                            <?php
                            $workplan = $this->db->get_where('tbl_workplan', ['Workplan_id' => $project['Workplan_id']])->row_array();
                            echo $workplan['Workplan_name'];
                            ?>
                        </div>
                    </div>

                    <hr>
                    <div class="row ">
                        <label for="" style="padding: 10px">ประเภทการใช้จ่าย</label>
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="border-bottom:none;"></th>
                                    <th class="text-center">ไตรมาส 1</th>
                                    <th class="text-center">ไตรมาส 2</th>
                                    <th class="text-center">ไตรมาส 3</th>
                                    <th class="text-center">ไตรมาส 4</th>
                                </tr>
                                <tr>
                                    <th class="text-center" style="border-top:none;">ประเภทรายจ่าย</th>
                                    <th class="text-center">แผนการใช้จ่าย</th>
                                    <th class="text-center">แผนการใช้จ่าย</th>
                                    <th class="text-center">แผนการใช้จ่าย</th>
                                    <th class="text-center">แผนการใช้จ่าย</th>
                                </tr>
                            </thead>
                            <?php
                            $charges_mains = $this->db->get_where('charges_main', ['Project_id' => $PID])->result_array();
                            foreach ($charges_mains as $charges_main) {

                            ?>
                                <tbody>
                                    <tr>
                                        <td colspan="5" style="background-color: #fd7e14;color:#fff;"><?php echo $charges_main['Charges_Main']; ?></td>
                                    </tr>
                                    <?php
                                    $charges_subs = $this->db->get_where('charges_sub', ['Charges_Main_id' => $charges_main['Charges_Main_id']])->result_array();
                                    foreach ($charges_subs as $charges_sub) {

                                    ?>
                                        <tr>
                                            <td><?php echo $charges_sub['Charges_Sub']; ?></td>
                                            <td class=""><?php echo $charges_sub['Quarter_one']; ?></td>
                                            <td><?php echo $charges_sub['Quarter_two']; ?></td>
                                            <td><?php echo $charges_sub['Quarter_three']; ?></td>
                                            <td><?php echo $charges_sub['Quarter_four']; ?></td>

                                        </tr>
                                    <?php } ?>
                                </tbody>
                            <?php } ?>
                        </table>



                    </div>
                <?php } ?>
                <hr>


                <div class="row container form-group">
                    <label class="col-3" for="">ประโยชน์ที่คาดว่าจะได้รับ :</label>
                    <div class="col-5">
                        <?php
                        $benefits = $this->db->get_where('benefit', ['Project_id' => $PID])->result_array();
                        $b = 0;
                        foreach ($benefits as $benefit) {
                            $b += 1;
                            echo $b . '.' . $benefit['Benefit_name'];
                            echo "<br>";
                        }
                        ?>
                    </div>
                </div>


                <div class="row container">
                    <label class="col-3" for="">เอกสาร TOR :</label>
                    <div class="col-5">
                        <?php if ($project['Tor'] == 1) {
                            echo "มี";
                        } else {
                            echo "ไม่มี";
                        } ?>
                    </div>
                </div>
                <div class="row container">
                    <div class="col-12">
                        <button type="button" class="btn btn-warning" onclick="send_data();">แก้ไขข้อมูล</button>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
    </section>
    <!-- /.content -->
</div>
<script>
    function send_data() {
        let data = [];
        let id_main = '<?= $PID; ?>)';
        let strategic_plane = $('[name="strategic_plane[]"]');
        let strategic_planeList = [];
        strategic_plane.each(function() {
            let goal = $(this).parents('.allstr').find('[name="goal"]').val();
            let indic = $(this).parents('.allstr').find('[name="tactic_indic"]').val();
            let strategic_id = $(this).parents('.allstr').find('[name="strategic"]').val();
            // let tactic = $(this).parents('.allstr').find('[name="tactic"]').val();
            strategic_planeList.push({
                strategic_plane: $(this).val(),
                strategic_id: strategic_id,
                goal: goal,
                indic: indic,
                // tactic: tactic
            });
        });
        let titles = $('[name="tactic_indic"] option:selected').map(function(idx, elem) {
            return $(elem).val();
        }).get();
        console.log(strategic_planeList);
        data.push({
            id_main: id_main,
            strategic_planeList: strategic_planeList,
            titles: titles,
        });
        $.ajax({
            url: "success_project_edit_manager",
            method: "POST",
            data: {
                data: data
            },
            success: function(getData) {
               
                    swal("Good job!", 'successfully', "success");
                    setTimeout(function() {
                        window.location.href = "project_manager";
                    }, 1000);
                    // console.log(result.dataList);
               
            }
        });
    }
</script>
<script>
    $("#DM1")
        .keyup(function() {
            var value = $(this).val();
            $("#DM2").val(ArabicNumberToText(value));
        })
        .keyup();
</script>
<script>
    $('#test').on('change', function() {
        //  alert( this.value ); // or $(this).val()
        if (this.value == "1") {
            $('.ic').css('display', 'block');
            $('.passport').css('display', 'none');
            $('.to').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'block');


        } else if (this.value == "2") {
            $('.to').css('display', 'block');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'block');


        } else if (this.value == "3") {
            $('.to2').css('display', 'block');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to').css('display', 'none');
            $('.to3').css('display', 'block');

        } else {
            $('.to').css('display', 'none');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'none');


        }
    });
</script>

<!-- /.content-wrapper -->