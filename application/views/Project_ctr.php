<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Project_ctr extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Dynamic_dependent_model');
        $this->load->model('Search_model');
    }
    // เจ้าหน้าที่แผน
    public function project_manager()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $year           = $this->input->get('year');
            $Department     = $this->input->get('Department_id');
            $Status         = $this->input->get('Status');

            if ($Department == '' && $Status == '') {
                $data['projects']       = $this->Search_model->project_list($year);
            } else {
                $data['projects']       = $this->Search_model->search_All($year, $Department, $Status);
            }

            $this->load->view('option/header');
            $this->load->view('project_manager', $data);
            $this->load->view('option/footer');
        }
    }

    public function index()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $year           = $this->input->get('year');
            $Department     = $this->input->get('Department_id');
            $Status         = $this->input->get('Status');

            if ($Department == '' && $Status == '') {
                $data['projects']       = $this->Search_model->project_list($year);
            } else {
                $data['projects']       = $this->Search_model->search_All($year, $Department, $Status);
            }

            $this->load->view('option/header');
            $this->load->view('project', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_only()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $this->load->view('option/header');
            $this->load->view('project_only');
            $this->load->view('option/footer');
        }
    }

    public function project_list()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_list', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_NotApp()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $year                   = $this->input->get('year');
            $acc                    = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            $account                = $acc['Account_id'];
            if ($year == '') {
                $data['projects']       = $this->Search_model->project_notApp($account);
            } else {
                $data['projects']       = $this->Search_model->project_notApp_search($year, $account);
            }
            $this->load->view('option/header');
            $this->load->view('project_notApp', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_notAppAll()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $Department             = $this->input->get('department');
            $year                   = $this->input->get('year');
            // echo ($Department);
            // exit();
            if ($Department == '') {
                $data['projects']       = $this->Search_model->project_notAppAll($year);
            } else {
                $data['projects']       = $this->Search_model->project_notAppAll_search($Department, $year);
            }

            $this->load->view('option/header');
            $this->load->view('project_notAppAll', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $this->load->view('option/header');
            $this->load->view('project_app');
            $this->load->view('option/footer');
        }
    }

    public function project_doc()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_doc', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_doc_tor()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID    = $this->input->post('Project_id');

            // Set preference
            $config['upload_path']     = 'uploads/tor/';
            $config['allowed_types']   = '*';
            $config['max_size']        = '99999'; // max_size in kb
            $config['file_name']       = time() . $_FILES['file_name']['name'];

            //Load upload library
            $this->load->library('upload', $config);
            $this->upload->initialize($config);


            // Get data about the file
            $uploadData = $this->upload->data();
            $data = array(
                'Project_id'        => $PID,
                'File_name'         => $uploadData['file_name'],
                'Full_path'         => 'uploads/tor/' . $uploadData['file_name'],
                'Check_type_tor'    => '1',
            );

            if ($this->db->insert('file', $data)) {
                $this->session->set_flashdata('save_ss2', 'เอกสาร TOR ได้บันทึกเรียบร้อยแล้ว.');
                redirect('project_doc?PID=' . base64_encode($PID));
            } else {
                $this->session->set_flashdata('del_ss2', 'เกิดข้อผิดพลาดในการเพิ่มเอกสาร TOR กรุณาลองใหม่อีกครั้ง 1 !!.');
                redirect('project_doc?PID=' . base64_encode($PID));
            }
        }
    }

    public function project_consider()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_consider', $data);
            $this->load->view('option/footer');
        }
    }

    public function project_review()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data['PID'] = base64_decode($this->input->get('PID'));

            $this->load->view('option/header');
            $this->load->view('project_review', $data);
            $this->load->view('option/footer');
        }
    }

    function fetch_state()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            if ($this->input->post('PROVINCE_ID')) {
                echo $this->Dynamic_dependent_model->fetch_state($this->input->post('PROVINCE_ID'));
            }
        }
    }

    function fetch_city()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            if ($this->input->post('AMPHUR_ID')) {
                echo $this->Dynamic_dependent_model->fetch_city($this->input->post('AMPHUR_ID'));
            }
        }
    }

    public function search_strategic_planeList()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $strategic_plane = $this->input->get('valueStrategic_plane');
            $strategics = $this->db->get_where('strategic', ['Strategic_Plan_id' => $strategic_plane])->result_array();
            $strategicList = [];
            foreach ($strategics as $key => $strategic) {
                $strategicList[$key]['Strategic_id'] = $strategic['Strategic_id'];
                $strategicList[$key]['Strategic_name'] = $strategic['Strategic_name'];
            }

            $result = [];
            $result['successfully'] = true;
            $result['strategicList'] = $strategicList;
            echo json_encode($result);
        }
    }

    public function search_strategicList()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $strategic = $this->input->get('valueStrategic');
            $goals = $this->db->get_where('goal', ['Strategic_id' => $strategic])->result_array();
            $goalList = [];
            foreach ($goals as $key => $goal) {
                $goalList[$key]['Goal_id'] = $goal['Goal_id'];
                $goalList[$key]['Goal_name'] = $goal['Goal_name'];
            }

            $result = [];
            $result['successfully'] = true;
            $result['goalList'] = $goalList;
            echo json_encode($result);
        }
    }

    public function search_tacticList()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $goal = $this->input->get('valueGoal');
            $indic_projects = $this->db->get_where('indic_project', ['Goal_id' => $goal])->result_array();
            $indic_projectList = [];
            foreach ($indic_projects as $key => $indic_project) {
                $indic_projectList[$key]['Indic_project_id'] = $indic_project['Indic_project_id'];
                $indic_projectList[$key]['Tactic_name'] = $indic_project['Tactic_name'];
            }

            $result = [];
            $result['successfully'] = true;
            $result['indic_projectList'] = $indic_projectList;
            echo json_encode($result);
        }
    }

    public function success_project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $data = $this->input->post();
            $account_id = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array();
            $insert = [];
            foreach ($data['data'] as $dataDetail) {
                $insert['Year'] = $dataDetail['year'];
                $insert['Project_name'] = $dataDetail['project_name'];
                $insert['Type'] = $dataDetail['type'];
                $insert['Integra_name'] = $dataDetail['integra_name'];
                if ($dataDetail['integra_name'] == "อื่นๆ") {
                    $insert['Integra_name'] = $dataDetail['integra_name_text'];
                }
                $insert['Integra_detail'] = $dataDetail['integra_detail'];

                $insert['Rationale'] = $dataDetail['rationale'];
                $insert['Target_group'] = $dataDetail['target_group'];
                $insert['Source'] = $dataDetail['charges_main'];
                if ($dataDetail['charges_main'] == "งบอื่นๆ") {
                    $insert['Source'] = $dataDetail['charges_main_text'];
                }
                $insert['Butget'] = $dataDetail['butget'];
                $insert['Butget_char'] = $dataDetail['butget_text'];
                if ($dataDetail['butget_text'] == "บาทถ้วน") {
                    $insert['Butget_char'] = "";
                }
                $insert['Workplan_id'] = $dataDetail['work_plan'];
                if ($dataDetail['tor'] == "มี") {
                    $insert['Tor'] = 1;
                } else {
                    $insert['Tor'] = 0;
                }
                $insert['Account_id'] = $account_id['Account_id'];
                $insert['Department_id'] = $dataDetail['department_id'];

                $project = $this->db->insert('project', $insert);
                $project_id = $this->db->insert_id();
            }
            // ผู้รับผิดชอบโครงการ
            foreach ($data['data'][0]['accountList'] as $account) {
                $data_user = [
                    'Account_id' => $account,
                    'Project_id' =>  $project_id
                ];
                $checkuser = $this->db->get_where('user', $data_user)->row_array();
                if (!empty($checkuser)) {
                    continue;
                }
                $this->db->insert('user', $data_user);
            }
            //ชื่อแผนยุทธ์ศาสตร์
            foreach ($data['data'][0]['strategic_planeList'] as $strategic_plane) {
                $Strategic_id = $this->db->get_where('strategic', ['Strategic_Plan_id' => $strategic_plane['strategic_plane']])->row_array();
                $data_strategic_plane = [
                    'Project_id' =>  $project_id,
                    'Strategic_Plan_id' => $strategic_plane['strategic_plane'],
                    'Strategic_id' => $Strategic_id['Strategic_id'],
                    'Goal_id' => $strategic_plane['goal'],
                    'Indic_project_id' => $strategic_plane['indic']
                ];
                $this->db->insert('project_strategic_plane', $data_strategic_plane);
            }
            //วัตถุประสงค์
            foreach ($data['data'][0]['objective_nameList'] as $objective_name) {
                $data_objective = [
                    'Project_id' =>  $project_id,
                    'Objective_name' => $objective_name
                ];
                $this->db->insert('objective', $data_objective);
            }
            //ตัวชี้วัดความสำเร็จระดับโครงการ
            foreach ($data['data'][0]['dataAll_indic_unit_cost'] as $dataIndic_unit_cost) {
                $data_indic_unit_cost = [
                    'Project_id' => $project_id,
                    'Indic_success' => $dataIndic_unit_cost['indic_project'],
                    'Unit' => $dataIndic_unit_cost['unit'],
                    'Cost' => $dataIndic_unit_cost['cost'],
                ];
                $this->db->insert('project_indic_success', $data_indic_unit_cost);
            }
            //ขั้นตอนการดำเนินการ
            foreach ($data['data'][0]['stepList'] as $data_stepList) {
                $data_step = [
                    'Step_name' => $data_stepList['step'],
                    'Start' => $data_stepList['start'],
                    'Stop' => $data_stepList['stop'],
                    'Project_id' => $project_id,
                ];
                $this->db->insert('work_step', $data_step);
            }
            //ประเภทการใช้จ่าย
            if ($data['data'][0]['charges_main'] != "ไม่ได้ใช้งบประมาณ") {

                foreach ($data['data'][0]['charges_sub_list'] as $charges) {
                    $data_charges = [
                        'Charges_Main' => $charges['charges_sub'],
                        'Project_id' => $project_id,
                    ];
                    $this->db->insert('charges_main', $data_charges);
                    $charges_id = $this->db->insert_id();
                    foreach ($charges['income_butgetList'] as $income_butget) {
                        $data_charges_sub = [
                            'Charges_Sub' => $income_butget['income_butget'],
                            'Quarter_one' => $income_butget['quarter_one'],
                            'Quarter_two' => $income_butget['quarter_two'],
                            'Quarter_three' => $income_butget['quarter_three'],
                            'Quarter_four' => $income_butget['quarter_four'],
                            'Charges_Main_id' => $charges_id,
                        ];
                        $this->db->insert('charges_sub', $data_charges_sub);
                    }
                }
            }

            //ประโยชน์ที่คาดว่าจะได้รับ
            foreach ($data['data'][0]['benefit_nameList'] as $benefit_name) {
                $data_benefit_name = [
                    'Project_id' => $project_id,
                    'Benefit_name' => $benefit_name
                ];
                $this->db->insert('benefit', $data_benefit_name);
            }

            $result = [];
            $result['successfully'] = true;
            $result['dataList'] = $data;

            echo json_encode($result);
        }
    }

    public function project_status_pass_director()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID        = $this->input->post('Project_id');
            $data = array(
                'Status'        => 11,
            );

            $this->db->where('Project_id', $PID);
            $success = $this->db->update('project', $data);

            echo $success;
        }
    }

    public function project_status_pass_supervisor()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID        = $this->input->post('Project_id');
            $data = array(
                'Status'        => 3,
            );

            $this->db->where('Project_id', $PID);
            $success = $this->db->update('project', $data);

            echo $success;
        }
    }

    public function project_status_pass()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID        = $this->input->post('Project_id');
            $data = array(
                'Status'        => 2,
            );

            $this->db->where('Project_id', $PID);
            $success = $this->db->update('project', $data);

            echo $success;
        }
    }

    public function project_status_not()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID            = $this->input->post('PID');
            $Comment        = $this->input->post('Comment');
            $Account_id     = $this->input->post('Account_id');
            $data = array(
                'Status'        => 0,

            );

            $this->db->where('Project_id', $PID);
            if ($this->db->update('project', $data)) {
                $data2 = array(
                    'Project_id'    => $PID,
                    'Comment'       => $Comment,
                    'Account_id'    => $Account_id,
                    'Time'          => date('Y-m-d H:i:s'),
                );
                $success = $this->db->insert('comment', $data2);
                if ($success > 0) {
                    echo "<script>";
                    echo "alert('ทำรายการเรียบร้อยแล้ว');";
                    echo "window.location='project';";
                    echo "</script>";
                } else {
                    echo "<script>";
                    echo "alert('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !!');";
                    echo "window.location='project';";
                    echo "</script>";
                }
            }
        }
    }

    public function project_status_padding()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $PID            = $this->input->post('PID');
            $Comment        = $this->input->post('Comment');
            $Account_id     = $this->input->post('Account_id');
            $data = array(
                'Status'        => 5,

            );

            $this->db->where('Project_id', $PID);
            if ($this->db->update('project', $data)) {
                $data2 = array(
                    'Project_id'    => $PID,
                    'Comment'       => $Comment,
                    'Account_id'    => $Account_id,
                    'Time'          => date('Y-m-d H:i:s'),
                );
                $success = $this->db->insert('comment', $data2);
                if ($success > 0) {
                    echo "<script>";
                    echo "alert('ทำรายการเรียบร้อยแล้ว');";
                    echo "window.location='project';";
                    echo "</script>";
                } else {
                    echo "<script>";
                    echo "alert('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !!');";
                    echo "window.location='project';";
                    echo "</script>";
                }
            }
        }
    }

    public function project_delete()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $D_PID  = base64_decode($this->input->get('D_PID'));

            $this->db->where('Project_id', $D_PID);
            if ($this->db->delete('project')) {
                echo "<script>";
                echo "alert('คุณได้ทำการลบโครงการเรียบร้อยแล้ว');";
                echo "window.location='project';";
                echo "</script>";
            } else {
                echo "<script>";
                echo "alert('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !!');";
                echo "window.location='project';";
                echo "</script>";
            }
        }
    }

    public function strategic_plane_project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $buttonType = $this->input->get('buttonType');
            $arrStr_all = [];
            if (isset($_GET['arrStr_all'])) {
                $arrStr_all = $this->input->get('arrStr_all');
            }

            $data['type'] = $buttonType;
            $strategic_planeList = $this->db->get_where('strategic_plane', ['Status' => 0])->result_array();
            foreach ($strategic_planeList as $key => $strategicPlane) {
                foreach ($arrStr_all as $strKey => $arrStr) {

                    if (intval($strategicPlane['Strategic_Plan_id']) == intval($arrStr)) {
                        unset($strategic_planeList[$key]);
                    }
                }
            }
            $data['strategic_planeList'] = $strategic_planeList;
            $data['test'] = $arrStr_all;
            $this->load->view('option/strategic_plane_project_app', $data);
        }
    }

    public function strategic_project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $strategicGet = $this->input->get('strategicGet');
            $strategicGetList = [];
            if (isset($strategicGet) && !empty($strategicGet) && $strategicGet != "false") {
                $strategicGetList = $this->db->get_where('strategic', ['Strategic_Plan_id' => $strategicGet])->result_array();
            }
            $data['strategicGet'] = $strategicGet;
            $data['strategicGetList'] = $strategicGetList;
            $this->load->view('option/strategic_project_app', $data);
        }
    }

    public function goal_project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $goalGet = $this->input->get('goalGet');
            $goalGetList = [];
            if (isset($goalGet) && !empty($goalGet) && $goalGet != "false") {
                $goalGetList = $this->db->get_where('goal', ['Strategic_id' => $goalGet])->result_array();
            }
            $data['goalGet'] = $goalGet;
            $data['goalGetList'] = $goalGetList;
            $this->load->view('option/goal_project_app', $data);
        }
    }

    public function indic_project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $indicGet = $this->input->get('indicGet');
            $indicGetList = [];
            if (isset($indicGet) && !empty($indicGet) && $indicGet != "false") {
                $indicGetList = $this->db->get_where('indic_project', ['Goal_id' => $indicGet])->result_array();
            }
            $data['indicGet'] = $indicGet;
            $data['indicGetList'] = $indicGetList;
            $this->load->view('option/indic_project_app', $data);
        }
    }

    public function tactic_project_app()
    {
        if ($this->session->userdata('Username') == "") {
            redirect('login');
        } else {
            $tacticGet = $this->input->get('tacticGet');
            $tacticGetList = [];
            if (isset($tacticGet) && !empty($tacticGet) && $tacticGet != "false") {
                $tacticGetList = $this->db->get_where('indic_project', ['Indic_project_id' => $tacticGet])->result_array();
            }
            $data['tacticGet'] = $tacticGet;
            $data['tacticGetList'] = $tacticGetList;
            $this->load->view('option/tactic_project_app', $data);
        }
    }
}
