<?php $Strategic_id = $this->input->get('Strategic_id'); ?>
<?php $checkData_goal = $this->db->get_where('goal', ['Strategic_id' => $Strategic_id])->row_array(); ?>

<?php if (!empty($checkData_goal)) {
    redirect("strategic_edit?Strategic_id=" . $Strategic_id . '&stp=' . base64_encode($stp));
}
?>



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>เป้าประสงค์</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">สร้างเป้าประสงค์ใหม่</h3>
            </div>
            <div class="card-body">
                <!-- <div class="form-group row">
                      <p for="" class="col-sm-2 col-lg-2 col-form-label">ชื่อแผนยุทธ์ศาสตร์ :</p>
                      <div class="col-sm-10 col-lg-6">
                          <select name="" class="form-control">
                              <option selected disabled>--- กรุณาเลือกแผนยุทธ์ศาสตร์ ---</option>
                              <option value="">แผนพัฒนาการศึกษาระดับอุดมศึกษา</option>
                          </select>
                      </div>
                  </div> -->
                <form action="strategic_add_process" id="send_data" method="POST">
                    <div id="first_goal">

                        <div class="form-group row">
                            <p for="" class="col-sm-2 col-lg-2 col-form-label">เป้าประสงค์ :</p>
                            <div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Goal_name[]" required></div>
                            <div class="form-group" style="text-align:center;"><button type="button" class="btn btn-info" onClick="add_goal();" style="color:#fff;"><i class="fa fa-plus"></i> เพิ่มเป้าประสงค์</button></div>
                        </div>

                        <div id="first_indic">
                            <div class="row" id="indic">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-8">
                                    <div class="form-group row">
                                        <p for="" class="col-sm-2 col-lg-2 col-form-label">ตัวชี้วัด :</p>
                                        <div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Indic_project[]" required></div>
                                        <div class="form-group" style="text-align:center;"><button type="button" onClick="add_indic(this);" class="btn btn-info add_indic" id="btn_indic" style="color:#fff;"><i class="fa fa-plus"></i> เพิ่มตัวชี้วัด</button></div>
                                    </div>
                                </div>
                                <div class="col-lg-2"></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-8">
                                    <div class="form-group row">
                                        <p for="" class="col-sm-2 col-lg-2 col-form-label">หน่วยนับ :</p>
                                        <div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Unit" required></div>
                                    </div>
                                </div>
                                <div class="col-lg-2"></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-8">
                                    <div class="form-group row">
                                        <p for="" class="col-sm-2 col-lg-2 col-form-label">ค่าเป้าหมาย :</p>
                                        <div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Cost" required></div>
                                    </div>
                                </div>
                                <div class="col-lg-2"></div>
                            </div>
                            <div id="first_tactic">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-8">
                                        <div class="form-group row">
                                            <p for="" class="col-sm-2 col-lg-2 col-form-label">กลยุทธ์ :</p>
                                            <div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Tactic_name[]" required></div>
                                            <div class="form-group" style="text-align:center;"><button type="button" onClick="add_tactic(this);" class="btn btn-info add_tactic" id="btn_tactic" style="color:#fff;"><i class="fa fa-plus"></i> เพิ่มกลยุทธ์</button></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 text-right">
                            <button type="button" class="btn btn-primary" id="event_send"><i class="fa fa-download"></i> จัดเก็บ</button>
                            <a href="workplan" type="button" class="btn btn-danger text-white"><i class="fa fa-times"></i> ย้อนกลับ</a>
                        </div>
                    </div>
                </form>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<script type="text/javascript">
    //เป้าประสงค์

    function add_goal() {
        $('#first_goal').after('<div id="first_goal"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">เป้าประสงค์ :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Goal_name[]" required></div><div class="form-group" style="text-align:center;"><button type="button" onClick="remove_goal(this);" class="btn btn-danger" style="color:#fff;"><i class="fa fa-times"></i> ลบเป้าประสงค์</button></div></div><div id="first_indic"><div class="row" id="indic"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">ตัวชี้วัด :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Indic_project[]" required></div><div class="form-group" style="text-align:center;"><button type="button" onClick="add_indic(this);" class="btn btn-info add_indic" id="btn_indic" style="color:#fff;"><i class="fa fa-plus"></i> เพิ่มตัวชี้วัด</button></div></div></div><div class="col-lg-2"></div></div><div class="row"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">หน่วยนับ :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Unit" required></div></div></div><div class="col-lg-2"></div></div><div class="row"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">ค่าเป้าหมาย :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Cost" required></div></div></div></div><div id="first_tactic"><div class="row"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">กลยุทธ์ :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Tactic_name[]" required></div><div class="form-group" style="text-align:center;"><button type="button" onClick="add_tactic(this);" class="btn btn-info add_tactic" id="btn_tactic" style="color:#fff;"><i class="fa fa-plus"></i> เพิ่มกลยุทธ์</button></div></div></div><div class="col-lg-2"></div></div></div></div></div>');
    }

    function remove_goal(e) {
        $(e).parents('#first_goal').remove();
    }

    //ตัวชี้วัด

    function add_indic(e) {
        $(e).parents('#first_indic').after('<hr><div id="first_indic"><div class="row" id="indic"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">ตัวชี้วัด :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Indic_project[]" required></div><div class="form-group" style="text-align:center;"><button type="button" class="btn btn-danger" onClick="remove_indic(this);" id="btn_indic_remove" style="color:#fff;"><i class="fa fa-times"></i> ลบตัวชี้วัด</button></div></div></div><div class="col-lg-2"></div></div><div class="row"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">หน่วยนับ :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Unit" required></div></div></div><div class="col-lg-2"></div></div><div class="row"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">ค่าเป้าหมาย :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Cost" required></div></div></div></div></div>');
    }

    function remove_indic(e) {
        $(e).parents('#first_indic').remove();
    }

    //กลยุทธ์
    function add_tactic(e) {
        $(e).parents('#first_tactic').after('<div id="first_tactic"><div class="row"><div class="col-lg-2"></div><div class="col-lg-8"><div class="form-group row"><p for="" class="col-sm-2 col-lg-2 col-form-label">กลยุทธ์ :</p><div class="col-sm-10 col-lg-6"><input type="text" class="form-control" name="Tactic_name[]" required></div><div class="form-group" style="text-align:center;"><button type="button" onClick="remove_tactic(this);" class="btn btn-danger add_tactic" id="btn_tactic" style="color:#fff;"><i class="fa fa-times"></i> ลบกลยุทธ์</button></div></div><div class="col-lg-2"></div></div></div>');
    }

    function remove_tactic(e) {
        $(e).parents('#first_tactic').remove();
    }

    $('#event_send').on('click', function() {
        swal({
                title: "คุณแน่ใจหรือไม่?",
                text: "คุณต้องการบันทึกข้อมูลใช่หรือไม่ !?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    //   e.preventDefault();
                    let goal_name = $('[name="Goal_name[]"]');
                    let data = [];
                    goal_name.each(function() {
                        let Indic_project = $(this).parents('#first_goal').find('[name="Indic_project[]"]');
                        let Indic_tactic = $(this).parents('#first_goal').find('[name="Tactic_name[]"]');
                        let Indic_list = [];
                        let tactic_list = [];
                        Indic_project.each(function() {
                            let indic_value = $(this).val();
                            let unit_value = $(this).parents('#first_indic').find('[name="Unit"]').val();
                            let cost_value = $(this).parents('#first_indic').find('[name="Cost"]').val();
                            Indic_list.push({
                                indic_project: indic_value,
                                unit: unit_value,
                                cost: cost_value,
                            });
                        });

                        Indic_tactic.each(function() {
                            tactic_list.push($(this).val());
                        });

                        data.push({
                            goal_name: $(this).val(),
                            indic_list: Indic_list,
                            tactic_list: tactic_list,
                        });

                    });
                    console.log(data);



                    $.ajax({
                        url: "strategic_add_process",
                        method: "POST",
                        data: {
                            dataList: data,
                            Strategic_id: <?php echo $Strategic_id; ?>
                        },
                        success: function(getData) {
                            const result = JSON.parse(getData);
                            if (result.successfully == true) {
                                swal("Good job!", 'successfully', "success");
                                setTimeout(function() {
                                    window.location.href = "workplan?stp=" + btoa(<?= $stp; ?>);
                                }, 2000);
                            }
                        }
                    });
                } else {
                    swal("ยกเลิก", "ยกเลิกการปิดโครงการ", "error");
                }
            });
    });
</script>