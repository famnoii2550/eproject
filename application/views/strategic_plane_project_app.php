<div class="allstr">
                    <div class="row container new_strategicPlan">
                        <label class="col-3" for="">ชื่อแผนยุทธ์ศาสตร์ : </label>
                        <div class="col-7">
                            <select id="strategic_plane"  name="strategic_plane[]" class="form-control" required>
                                <option selected disabled>--เลือกแผนยุทธ์ศาสตร์--</option>
                                <?php foreach ($strategic_planeList as $strategic_plane) { ?>
                                    <option value="<?php echo $strategic_plane['Strategic_Plan_id']; ?>"><?php echo $strategic_plane['Strategic_Plan']; ?></option>
                                <?php } ?>
                            </select>

                        </div>
                        <div class="col-2">
                            <?php if($type == "plus"){ ?>
                                <button type="button" class="btn btn-primary" id="add_straregic_check"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มแผนยุทธ์ศาสตร์</button>
                            <?php }else{ ?>
                                <button type="button" class="btn btn-danger" id="remove_straregic_check"><i class="fa fa-times" aria-hidden="true"></i> ลบแผนยุทธ์ศาสตร์</button>
                            <?php } ?>
                        </div>
                    </div>
                     <div id="goalList">
                     </div>       
                     <script>
                        $("button#add_straregic_check").off().on('click',function(){
                            getStrategicPlaneList();
                        });
                        $("select#strategic_plane").change(function(){
                            getGoalList($(this));
                        });
                        $("button#remove_straregic_check").off().on('click',function(){
                            $(this).parents('.allstr').remove();
                        });
                        function getGoalList(event){
                            let goalGet = false;
                            if (event) {
                                goalGet = event.val();
                            }
                            $.ajax({
                                url:"<?php echo base_url('Project_ctr/goal_project_app'); ?>",
                                data:{goalGet:goalGet},
                                beforeSend:function(){
                                    event.parents('.allstr').find('#goalList').html('Loading...');
                                },
                                success:function(response){
                                    event.parents('.allstr').find('#goalList').html(response);
                                }
                            });
                        }

                        
                    </script>        
