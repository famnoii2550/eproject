  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<section class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1>แผนปฎิบัติการ</h1>
  				</div>
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
  					</ol>
  				</div>
  			</div>
  		</div><!-- /.container-fluid -->
  	</section>

  	<!-- Main content -->
  	<section class="content">
  		<?php $strategic = $this->db->get_where('strategic', ['Strategic_id' => $SID])->row_array();
			$strategic_plane = $this->db->get_where('strategic_plane', ['Strategic_Plan_id' => $strategic['Strategic_Plan_id']])->row_array();
			?>
  		<!-- Default box -->
  		<div class="card">
  			<div class="card-header">
  				<h3 class="card-title"><?php echo $strategic_plane['Strategic_Plan']; ?></h3>
  				<div class="card-tools">
  				</div>
  			</div>
  			<div class="card-body">

  				<table class="table table-bordered table-hover">
  					<thead>
  						<tr class="bg-secondary">
  							<th colspan="4">ประเด็นยุทธศาสตร์ : <?php echo $strategic['Strategic_name']; ?></th>
  						</tr>
  					</thead>
  					<?php
						$goals = $this->db->get_where('goal', ['Strategic_id' => $SID])->result_array();
						foreach ($goals as $goal) {

						?>
  						<?php $tactic = $this->db->get_where('tactic', ['Goal_id' => $goal['Goal_id']])->result_array(); ?>

  						<thead>
  							<tr style="background-color:#fd7e14;color:#fff;">
  								<th colspan="4">เป้าประสงค์ : <?php echo $goal['Goal_name']; ?></th>
  							</tr>
  						</thead>
  						<tbody id="project">
  							<tr>
  								<th>ตัวชี้วัด</th>
  								<th>หน่วยนับ</th>
  								<th>ค่าเป้าหมาย</th>
  							</tr>
  							<?php
								$indic_projects = $this->db->get_where('indic_project', ['Goal_id' => $goal['Goal_id']])->result_array();
								foreach ($indic_projects as $indic_project) {
								?>
  								<tr>
  									<th><?php echo $indic_project['Indic_project']; ?></th>
  									<th><?php echo $indic_project['Unit']; ?></th>
  									<th><?php echo $indic_project['Cost']; ?></th>
  								</tr>
  							<?php } ?>
  							<tr class="bg-light">
  								<th colspan="3">กลยุทธ์</th>
  							</tr>
  							<?php foreach ($tactic as $tactics) { ?>
  								<tr>
  									<th colspan="3"><?= $tactics['Tactic_name']; ?></th>
  								</tr>
  							<?php } ?>
  						</tbody>
  					<?php } ?>
  				</table>
  			</div>
  			<!-- /.card-body -->
  		</div>
  		<!-- /.card -->

  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->