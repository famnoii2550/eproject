  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
          <div class="container-fluid">
              <div class="row mb-2">
                  <div class="col-sm-6">
                      <h1>จัดการข้อมูลประเด็นยุทธ์ศาสตร์ เป้าประสงค์ กลยุทธ์</h1>
                  </div>
                  <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                          <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                      </ol>
                  </div>
              </div>
          </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
          <!-- Default box -->
          <div class="card">
              <div class="card-header">
                  <div>
                      <form action="workplan_add" method="POST">

                          <span>แผนยุทธ์ศาสตร์ : </span>
                          <select name="strategic_plan_id" onchange="searchStrategic_plan(this);" class="form-control" style="width: 25%; display: inline-block;">
                              <option value="">-- กรุณาเลือก --</option>
                              <?php foreach ($strategic_plane as $strategic_plane_detail) { ?>
                                  <option value="<?php echo $strategic_plane_detail->Strategic_Plan_id; ?>" <?= ($strategic_plane_detail->Strategic_Plan_id == $stp) ? 'selected' : ''; ?>><?php echo $strategic_plane_detail->Strategic_Plan; ?></option>
                              <?php } ?>
                          </select>
                          <span>ชื่อประเด็นยุทธศาสตร์ : </span><input type="text" style="width: 25%; display: inline-block;" class="form-control" name="strategic_plan" id="" required>
                          <button class="btn btn-info" style="margin-bottom: 5px;"><i class="fa fa-download"></i> จัดเก็บ</button>
                      </form>
                  </div>

                  <div class="card-tools">
                      <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> -->
                  </div>
              </div>
              <div class="card-body">
                  <table id="table" class="table table-bordered table-striped" style="text-align:center;">
                      <thead>
                          <tr>
                              <th>ที่</th>
                              <th>แผนยุทธศาสตร์</th>
                              <th>ชื่อประเด็นยุทธศาสตร์</th>
                              <!-- <th>ปีงบประมาณ</th>
                              <th>วันที่ที่ผ่านมติกรรมการบริหาร</th>
                              <th>วันที่ที่ผ่านมติกรรมการประจำ</th> -->
                              <th>เครื่องมือ</th>
                          </tr>
                      </thead>
                      <tbody id="strategicList">
                          <?php $n = 1; ?>
                          <?php foreach ($workplan_list as $key => $data) { ?>
                              <?php $Strategic_Plan_id = $this->db->get_where('strategic_plane', ['Strategic_Plan_id' => $data->Strategic_Plan_id])->row_array(); ?>
                              <tr>
                                  <td><?php echo $n++; ?></td>
                                  <td style="text-align:left;"><?php echo $Strategic_Plan_id['Strategic_Plan']; ?></td>
                                  <td style="text-align:left;"><?php echo $data->Strategic_name . ' '; ?>
                                      <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#edit_strategic<?= $data->Strategic_id; ?>"><i class="fas fa-pen"></i></button>
                                      <!-- Modal -->
                                      <div class="modal fade" id="edit_strategic<?= $data->Strategic_id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                          <div class="modal-dialog modal-lg" role="document">
                                              <div class="modal-content">
                                                  <div class="modal-header">
                                                      <h5 class="modal-title" id="exampleModalLabel">แก้ไขประเด็นยุทธ์ศาสตร์</h5>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                      </button>
                                                  </div>
                                                  <form action="strategic_edit_workplant" method="POST" id="edit_form<?= $data->Strategic_id; ?>">
                                                      <div class="modal-body">
                                                          <div class="form-group">
                                                              <label>ประเด็นยุทธ์ศาสตร์</label>
                                                              <input type="hidden" name="Strategic_id" id="Strategic_id<?= $data->Strategic_id; ?>" value="<?= $data->Strategic_id; ?>">
                                                              <input type="text" name="Strategic_name" id="Strategic_name<?= $data->Strategic_id; ?>" class="form-control" value="<?= $data->Strategic_name; ?>">
                                                          </div>
                                                      </div>
                                                      <div class="modal-footer">
                                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>
                                                          <button type="button" class="btn btn-primary" onclick="MYFUNCTIONS();">แก้ไขข้อมูล</button>
                                                      </div>
                                                  </form>
                                              </div>
                                          </div>
                                      </div>
                                      <script>
                                          function MYFUNCTIONS() {
                                              swal({
                                                  title: "แน่ใจหรือไม่?",
                                                  text: "ต้องการแก้ไขประเด็นยุทธ์ศาสตร์นี้!",
                                                  icon: "warning",
                                                  buttons: ["ยกเลิก", "ยืนยัน"],
                                                  dangerMode: true,
                                              }).then((willDelete) => {
                                                  if (willDelete) {
                                                      $('#edit_form<?= $data->Strategic_id; ?>').submit();
                                                  }
                                              });
                                          }
                                      </script>
                                  </td>
                                  <td>
                                      <a href="workplan_detail?Strategic_id=<?php echo base64_encode($data->Strategic_id); ?>" class="btn btn-info btn-sm"><i class="fas fa-search"></i></a>
                                      <a href="strategic_add?Strategic_id=<?php echo $data->Strategic_id; ?>&stp=<?= base64_encode($stp); ?>" class="btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
                                      <button type="button" onclick="myDelete('<?php echo $data->Strategic_id; ?>');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                  </td>
                              </tr>
                          <?php } ?>
                      </tbody>
                  </table>
              </div>
              <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </section>
      <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <script type="text/javascript">
      function myDelete(id) {
          swal({
                  title: "แน่ใจหรือไม่?",
                  text: "หากลบข้อมูลแล้วจะนำประเด็นยุทธ์ศาสตร์กลับมาไม่ได้อีก!",
                  icon: "warning",
                  buttons: ["ยกเลิก", "ลบข้อมูล"],
                  dangerMode: true,
              })
              .then((willDelete) => {
                  if (willDelete) {
                      $.ajax({
                          url: 'strategic_delete',
                          type: 'GET',
                          data: {
                              id: id
                          },
                          success(value) {
                              if (value == "true") {
                                  swal("ทำรายการสำเร็จ!", 'คุณได้ลบข้อมูลเรียบร้อยแล้ว', "success");
                                  setTimeout(function() {
                                      window.location.href = "workplan";
                                  }, 2000);
                              } else {
                                  swal("เกิดข้อผิดพลาด!", 'ไม่สามารถลบข้อมูลได้ กรุราลองใหม่อีกครั้ง!', "error");
                              }
                          }
                      });
                      //   swal("Poof! Your imaginary file has been deleted!", {
                      //       icon: "success",
                      //   });
                  }
              });
      }
  </script>
  <script type="text/javascript">
      $(document).ready(function() {

          $("#stra1").click(function() {
              $('.modal .modal-dialog .modal-content form .modal-body #straBT').append('<?php $this->load->view('option/stra1'); ?>');
          });

      });
  </script>
  <script type="text/javascript">
      $(document).ready(function() {

          $("#btn1").click(function() {
              $('.modal .modal-dialog .modal-content form .modal-body #meas').append('<?php $this->load->view('option/measure1'); ?>');
          });

      });
  </script>
  <script type="text/javascript">
      function searchStrategic_plan(GetData) {
          window.location.href = 'workplan?stp=' + btoa(GetData.value);
      }
  </script>