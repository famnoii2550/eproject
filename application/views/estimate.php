<?php $quarter = base64_decode($this->input->get('quarter')); ?>
<?php $profile = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array(); ?>
<script src="assets/js/thaibath.js" type="text/javascript" charset="utf-8"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>เขียนเอกสารประเมินโครงการ ผู้รับผิดชอบโครงการ</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right"></ol>
                </div>
            </div>
        </div>
    </section>

    <?php if ($profile['Responsible'] == '1' && $profile['Account_id'] == $project['Account_id']) { ?>
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <a href="project_report?PID=<?= base64_encode($PID); ?>&quarter=<?= base64_encode($quarter); ?>">เขียนรายงานความก้าวหน้า</a> /
                    <a href="#"> ปิดโครงการ</a>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <h5>เขียนเอกสารประเมินโครงการ</h5>
                        </div>
                    </div>
                </div>
                <?php $file_other = $this->db->get_where('file', ['Project_id' => $PID, 'Check_type_tor' => 2])->result_array(); ?>
                <div class="card-body">
                    <div class="row container">
                        <div class="col-12">
                            <h5>Upload เอกสารที่เกี่ยวข้องกับการประเมินโครงการ</h5>
                        </div><br><br>
                        <label class="col-3" for="">Upload เอกสารที่เกี่ยวข้อง : </label>
                        <br>
                        <span style="color:red;">(อนุญาติเฉพาะไฟล์นามสกุล .doc และ .pdf ขนาดไม่เกิน 2MB เท่านั้น !!)</span>
                        <div class="col-9">
                            <?php $uri = "estimate?PID=" . base64_encode($PID) . "&quarter=" . base64_encode($quarter); ?>
                            <form action="project_estimate_doc" method="post" enctype="multipart/form-data">
                                <input type="file" name="file_name" accept="application/pdf,.doc" required>
                                <input type="text" name="Project_id" value="<?= $PID; ?>" hidden>
                                <input type="hidden" name="uri" value="<?= $uri; ?>">
                                <span><button type="submit" class="btn btn-info">Upload</button></span>
                            </form>
                        </div>

                    </div>
                    <br>
                    <div class="row container">
                        <label class="col-3" for="">เอกสารที่เกี่ยวข้อง : </label>
                        <div class="col-9">
                            <?php if (empty($file_other)) { ?>
                                <span class="badge badge-danger">ยังไม่มีเอกสารเกี่ยวข้อง</span>
                            <?php } else { ?>
                                <?php foreach ($file_other as $file_other) { ?>
                                    <span class="badge badge-primary"><a href="<?= $file_other['Full_path']; ?>" target="_bank" style="color:#fff;"><?= $file_other['Full_path']; ?></a></span><br>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>

    <!-- Main content -->
    <section class="content">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-12">
                        <h5>เขียนเอกสารประเมินโครงการ</h5>
                    </div>
                </div>
            </div>
            <form action="#" method="POST">
                <div class="card-body">
                    <div class="row container">
                        <label class="col-3" for="">คำชี้แจง : </label>
                        <div class="col-6">
                            <textarea name="Explanation" id="explanation" readonly class="form-control" rows="8">การประเมินโครงการในรอบ 12 เดือนนี้จัดทำเพื่อการศึกษา ติดตามและวิเคราะห์ถึงผลลัพธ์ของแผนงาน/โครงการตามที่มหาวิทยาลัยได้จัดสรรงบประมาณให้ ว่าสามารถดำเนินการตามที่กำหนดไว้ได้หรือไม่รวมทั้งเป็นการศึกษาปัญหาอุปสรรคของการดำเนินงานของแผน/โครงการ การหาแนวทางแก้ไขปัญหา การเตรียมความพร้อมสำหรับการดำเนินงานของแผนงาน/โครงการครั้งต่อไป และเป็นข้อมูลประกอบการตัดสินใจสำหรับผู้บริหารในการจัดสรรงบประมาณโครงการในปีงบประมาณต่อไปด้วย ในการนี้ สำนักคอมพิวเตอร์ฯ จึงใครขอความกรุณารายงานแบบประเมินผลดำเนินงาน/โครงการ ดังนี้</textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row container">
                        <label class="col-3" for="">ชื่อโครงการ/กิจกรรม : </label>
                        <div class="col-9">
                            <?= $project['Project_name']; ?>
                        </div>
                    </div>
                    <br>
                    <?php $project_strategic_planeList = $this->db->get_where('project_strategic_plane', ['Project_id' => $project['Project_id']])->result_array();
                    foreach ($project_strategic_planeList as $project_strategic_plane) {
                    ?>
                        <?php $strategic_plane = $this->db->get_where('strategic_plane', ['Strategic_Plan_id' => $project_strategic_plane['Strategic_Plan_id']])->row_array(); ?>
                        <div class="row container">
                            <label class="col-3" for="">ตอบสนองยุทธ์ศาสตร์ : </label>
                            <div class="col-9">
                                <p><?= $strategic_plane['Strategic_Plan']; ?></p>
                                <?php $strategic = $this->db->get_where('strategic', ['Strategic_id' => $project_strategic_plane['Strategic_id']])->row_array(); ?>
                                <span style="font-weight: bold;">ประเด็นยุทธ์ศาสตร์ที่ : </span><span><?= $strategic['Strategic_name']; ?></span><br>
                                <?php $goal = $this->db->get_where('goal', ['Goal_id' => $project_strategic_plane['Goal_id']])->row_array(); ?>
                                <span style="font-weight: bold;">เป้าประสงค์ที่ : </span><span><?= $goal['Goal_name']; ?></span><br>
                                <?php $indic_project = $this->db->get_where('strategics_project', ['Project_id' => $PID])->result_array(); ?>
                                <?php foreach ($indic_project as $tactic_project) { ?>
                                    <?php $tactic_project = $this->db->get_where('tactic', ['Tactic_id' => $tactic_project['Tactic_id']])->row_array(); ?>
                                    <span style="font-weight: bold;">กลยุทธ์ที่: </span><span><?= $tactic_project['Tactic_name']; ?></span><br>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                    <br>
                    <div class="row container">
                        <label class="col-3" for="">ส่วนงานที่รับผิดชอบ : </label>
                        <div class="col-9">
							<?php $dd = $this->db->get_where('department',['Department_id' => $project['Department_id']])->row_array();?>
                            <?= $dd['Department']; ?>
                        </div>
                    </div>
                    <br>
                    <div class="row container">
                        <label class="col-3" for="">วิธีการดำเนินโครงการ : </label>
                        <div class="col-6">
                            <textarea name="Conducting" id="Conducting" rows="8" class="form-control"></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row container">
                        <label class="col-3" for="">วัตถุประสงค์ : </label>
                        <?php $objective = $this->db->get_where('objective', ['Project_id' => $estimate['str_plane_pid']])->result_array(); ?>
                        <div class="col-6">
                            <?php $ob = 1; ?>
                            <?php foreach ($objective as $objective) { ?>
                                <p>- <?= $objective['Objective_name']; ?></p>
                            <?php } ?>
                            <input type="radio" name="objective_check" class="objective_check" value="1"> บรรลุ &nbsp;&nbsp;
                            <input type="radio" name="objective_check" class="objective_check" value="0"> ไม่บรรลุ
                        </div>
                    </div>
                    <br>
                    <div class="row container">
                        <label class="col-3" for="">ผลการดำเนินงาน : </label>
                        <div class="col-6">
                            <input type="radio" name="Result" id="Result" value="ดำเนินการแล้วเสร็จตามระยะเวลาที่กำหนดไว้ในโครงการ"> ดำเนินการแล้วเสร็จตามระยะเวลาที่กำหนดไว้ในโครงการ<br><br>
                            <input type="radio" name="Result" id="Result" value="ไม่เป็นไปตามระยะเวลาที่กำหนดไว้ในโครงการ"> ไม่เป็นไปตามระยะเวลาที่กำหนดไว้ในโครงการ<br><br>
                            <input type="radio" name="Result" id="Result" value="ขอเลื่อนการดำเนินงาน"> ขอเลื่อนการดำเนินงาน<br><br>
                            <input type="radio" name="Result" id="Result" value="เสนอขอยกเลิก"> เสนอขอยกเลิก<br><br>
                        </div>
                    </div>
                    <br>
                    <?php $project_indic_success = $this->db->get_where('project_indic_success', ['Project_id' => $estimate['str_plane_pid']])->result_array(); ?>
                    <div class="row container">
                        <label class="col-3" for="">ผลการดำเนินงานตามตัวชี้วัด : </label>
                        <div class="col-6">
                            <?php foreach ($project_indic_success as $p_indic_s) { ?>
                                <p>- <?= $p_indic_s['Indic_success']; ?></p>
                            <?php } ?>
                            <textarea name="Performance" id="Performance" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                    <br>
                    <?php $charges_main = $this->db->get_where('charges_main', ['Project_id' => $PID])->row_array(); ?>
                    <div class="row container">
                        <label class="col-3" for="">งบประมาณที่ใช้ดำเนินการ : </label>
                        <div class="col-6">
                            <p><?= $charges_main['Charges_Main']; ?></p>
                        </div>
                    </div>
                    <br>
                    <?php $charges_sub = $this->db->get_where('charges_sub', ['Charges_Main_id' => $charges_main['Charges_Main_id']])->row_array(); ?>
                    <div class="row container">
                        <label class="col-3" for="">ตารางการใช้เงินแต่ละไตรมาส : </label>
                        <div class="col-6">
                            <table class="table table-striped">
                                <thead>
                                    <tr class="text-center">
                                        <th>ไตรมาส 1</th>
                                        <th>ไตรมาส 2</th>
                                        <th>ไตรมาส 3</th>
                                        <th>ไตรมาส 4</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td><?= (empty($charges_sub['Quarter_one'])) ? "-" : $charges_sub['Quarter_one']; ?></td>
                                        <td><?= (empty($charges_sub['Quarter_two'])) ? "-" : $charges_sub['Quarter_two']; ?></td>
                                        <td><?= (empty($charges_sub['Quarter_three'])) ? "-" : $charges_sub['Quarter_three']; ?></td>
                                        <td><?= (empty($charges_sub['Quarter_four'])) ? "-" : $charges_sub['Quarter_four']; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- <div class="row container">
                        <label class="col-3" for="">ที่ได้รับจัดสรร : </label>
                        <div class="col-6">
                            <p><?= number_format($estimate['Butget']); ?> (<?= $estimate['Butget_char']; ?>)</p>
                        </div>
                    </div> -->
                    <br>
                    <?php $pj_report = $this->db->get_where('project_report', ['Project_id' => $PID])->result_array(); ?>
                    <div class="row container">
                        <label class="col-3" for="">ใช้จริง : </label>
                        <div class="col-6">
                            <table class="table table-striped">
                                <thead>
                                    <tr class="text-center">
                                        <th>ไตรมาส 1</th>
                                        <th>ไตรมาส 2</th>
                                        <th>ไตรมาส 3</th>
                                        <th>ไตรมาส 4</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <?php if ($quarter == "1") { ?>
                                            <td><?= (empty($pj_report[0]['Used'])) ? "<input type='number' id='Used' name='Used' class='form-control'>" :  number_format($pj_report[0]['Used']); ?></td>
                                            <td><?= (empty($pj_report[1]['Used'])) ? "-" :  number_format($pj_report[1]['Used']); ?></td>
                                            <td><?= (empty($pj_report[2]['Used'])) ? "-" :  number_format($pj_report[2]['Used']); ?></td>
                                            <td><?= (empty($pj_report[3]['Used'])) ? "-" :  number_format($pj_report[3]['Used']); ?></td>
                                        <?php } elseif ($quarter == "2") { ?>
                                            <td><?= (empty($pj_report[0]['Used'])) ? "-" :  number_format($pj_report[0]['Used']); ?></td>
                                            <td><?= (empty($pj_report[1]['Used'])) ? "<input type='number' id='Used' name='Used' class='form-control'>" :  number_format($pj_report[1]['Used']); ?></td>
                                            <td><?= (empty($pj_report[2]['Used'])) ? "-" :  number_format($pj_report[2]['Used']); ?></td>
                                            <td><?= (empty($pj_report[3]['Used'])) ? "-" :  number_format($pj_report[3]['Used']); ?></td>
                                        <?php } elseif ($quarter == "3") { ?>
                                            <td><?= (empty($pj_report[0]['Used'])) ? "-" :  number_format($pj_report[0]['Used']); ?></td>
                                            <td><?= (empty($pj_report[1]['Used'])) ? "-" :  number_format($pj_report[1]['Used']); ?></td>
                                            <td><?= (empty($pj_report[2]['Used'])) ? "<input type='number' id='Used' name='Used' class='form-control'>" :  number_format($pj_report[2]['Used']); ?></td>
                                            <td><?= (empty($pj_report[3]['Used'])) ? "-" :  number_format($pj_report[3]['Used']); ?></td>
                                        <?php } elseif ($quarter == "4") { ?>
                                            <td><?= (empty($pj_report[0]['Used'])) ? "-" :  number_format($pj_report[0]['Used']); ?></td>
                                            <td><?= (empty($pj_report[1]['Used'])) ? "-" :  number_format($pj_report[1]['Used']); ?></td>
                                            <td><?= (empty($pj_report[2]['Used'])) ? "-" :  number_format($pj_report[2]['Used']); ?></td>
                                            <td><?= (empty($pj_report[3]['Used'])) ? "<input type='number' id='Used' name='Used' class='form-control'>" :  number_format($pj_report[3]['Used']); ?></td>
                                        <?php } ?>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br>
                    <div class="row container">
                        <label class="col-3" for="">ประโยชน์ที่ได้รับจากการดำเนินโครงการ (หลังการจัดโครงการ) : </label>
                        <div class="col-6">
                            <textarea name="Benefits" id="Benefits" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row container">
                        <label class="col-3" for="">ปัญหาและอุปสรรคในการดำเนินโครงการ : </label>
                        <div class="col-6">
                            <textarea name="Problem" id="Problem" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row container">
                        <label class="col-3" for="">แนวทางดำเนินการแก้ไข/ข้อเสนอแนะ : </label>
                        <div class="col-6">
                            <textarea name="Improvement" id="Improvement" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row container" style="padding: 10px 0 0 0px;">
                        <label class="col-3" for=""></label>
                        <div class="col-4">
                            <button type="button" onclick="send_data('<?= $PID; ?>');" class="btn btn-info"><i class="fa fa-download" aria-hidden="true"></i> จัดเก็บและส่งโครงการ</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
    function send_data(pid) {
        swal({
                title: "คุณแน่ใจหรือไม่?",
                text: "ต้องการปิดโครงการที่ไตรมาสที่ <?= $quarter; ?>!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    var Used = $('#Used').val();
                    var explanation = $('#explanation').val();
                    var Conducting = $('#Conducting').val();
                    var objective_check = $("input[name='objective_check']:checked").val();
                    var Result = $('#Result').val();
                    var Benefits = $('#Benefits').val();
                    var Problem = $('#Problem').val();
                    var Performance = $('#Performance').val();
                    var Improvement = $('#Improvement').val();
                    if (explanation == '' || Conducting == '' || Result == '' || Performance == '' || Benefits == '' || Problem == '' || Improvement == '') {
                        swal("เกิดข้อผิดพลาด !", 'กรุณากรอกข้อมูลให้ครบถ้วน', "warning");
                    } else if (!$("input[name='objective_check']").is(':checked')) {
                        swal("เกิดข้อผิดพลาด !", 'กรุณากรอกข้อมูลวัตถุประสงค์', "warning");
                    } else {
                        $.ajax({
                            url: 'project_estimate',
                            type: 'POST',
                            data: {
                                pid: pid,
                                explanation: explanation,
                                Conducting: Conducting,
                                objective_check: objective_check,
                                Result: Result,
                                Benefits: Benefits,
                                Problem: Problem,
                                Improvement: Improvement,
                                Performance: Performance,
                                Used: Used,
                                quarter: <?= $quarter; ?>,
                            },
                            success(result) {
                                if (result == 'true') {
                                    swal("สำเร็จ!", 'บันทึกข้อมูลเรียบร้อยแล้ว', "success");
                                    setTimeout(function() {
                                        window.location.href = "list_project";
                                    }, 1000);
                                } else {
                                    swal("เกิดข้อผิดพลาด!", 'กรุณาลองใหม่อีกครั้ง !!', "error");
                                }
                            }
                        });
                    }
                } else {
                    swal("ยกเลิก", "ยกเลิกการปิดโครงการ", "error");
                }
            });



    }
</script>