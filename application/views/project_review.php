<script src="assets/js/thaibath.js" type="text/javascript" charset="utf-8"></script>
<style>
    #ic,
    #passport {
        display: none;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<?php
$month = array(
    '01'  => 'มกราคม', '02'  => 'กุมภาพันธ์', '03'  => 'มีนาคม',
    '04'  => 'เมษายน', '05'  => 'พฤษภาคม', '06'  => 'มิถุนายน',
    '07'  => 'กรกฎาคม', '08'  => 'สิงหาคม', '09'  => 'กันยายน',
    '10'  => 'ตุลาคม', '11'  => 'พฤศจิกายน', '12'  => 'ธันวาคม',
);
function thaiDate($datetime)
{
    list($date, $time) = explode(' ', $datetime); // แยกวันที่ กับ เวลาออกจากกัน
    list($H, $i, $s) = explode(':', $time); // แยกเวลา ออกเป็น ชั่วโมง นาที วินาที
    list($Y, $m, $d) = explode('-', $date); // แยกวันเป็น ปี เดือน วัน
    $Y = $Y + 543; // เปลี่ยน ค.ศ. เป็น พ.ศ.
    switch ($m) {
        case "01":
            $m = "ม.ค.";
            break;
        case "02":
            $m = "ก.พ.";
            break;
        case "03":
            $m = "มี.ค.";
            break;
        case "04":
            $m = "เม.ย.";
            break;
        case "05":
            $m = "พ.ค.";
            break;
        case "06":
            $m = "มิ.ย.";
            break;
        case "07":
            $m = "ก.ค.";
            break;
        case "08":
            $m = "ส.ค.";
            break;
        case "09":
            $m = "ก.ย.";
            break;
        case "10":
            $m = "ต.ค.";
            break;
        case "11":
            $m = "พ.ย.";
            break;
        case "12":
            $m = "ธ.ค.";
            break;
    }
    return $d . " " . $m . " " . $Y . "  " . $H . ":" . $i . ":" . $s;
}
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 style="display: contents;">ข้อมูลโครงการ</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <?php $uri = base64_decode($this->input->get('uri')); ?>
    <?php $ac = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array(); ?>
    <?php $status = $this->db->get_where('project', ['Project_id' => $PID])->row_array(); ?>
    <?php $comment = $this->db->order_by('Comment_id', 'DESC')->get_where('comment', ['Project_id' => $PID])->result_array(); ?>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <div class="row" style="padding: 4px 0px 10px 8px;">
                    <div class="col-10">
                        <a href="project_list?PID=<?php echo base64_encode($PID); ?>&uri=<?= base64_encode($uri); ?>">รายละเอียดโครงการ</a> <span style="padding: 0px 10px;"> / </span>
                        <a href="project_doc?PID=<?php echo base64_encode($PID); ?>&uri=<?= base64_encode($uri); ?>">เอกสารโครงการ</a> <span style="padding: 0px 10px;"> / </span>
                       
                           
                        <u><a>ความคิดเห็น</a></u>
                    </div>
                    <div class="col-2 text-right">
                        <a href="<?= $uri; ?>">กลับหน้าโครงการ</a>
                    </div>
                </div>
                <!-- <button class="btn btn-info" style="float:right;" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus-circle"></i> สร้างประเด็นยุทธ์ศาสตร์ใหม่</button> -->

            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <tbody>
                        <?php foreach ($comment as $comment) { ?>
                            <?php
                            $user = $this->db->get_where('account', ['Account_id' => $comment['Account_id']])->row_array();
                            $department = $this->db->get_where('department', ['Department_id' => $user['Department_id']])->row_array();

                            ?>
                            <tr style="background-image: linear-gradient(to bottom,#fcf8e3 0,#faf2cc 100%);background-repeat: repeat-x;">
                                <th scope="row" style="color: #848484;">
                                    ความคิดเห็นโดย: <?php echo $user['Fname'] . ' ' . $user['Lname']; ?>
                                    ตำแหน่ง: <?php if ($comment['Role'] == 'Director') { ?>
                                        ผู้บริหาร
                                    <?php } elseif ($comment['Role'] == 'Manager') { ?>
                                        เจ้าหน้าที่แผน
                                    <?php } elseif ($comment['Role'] == 'Supervisor') { ?>
                                        หัวหน้าฝ่าย
                                    <?php } ?>
                                    <?= $department['Department']; ?>
                                </th>
                                <th class="text-right" scope="row" style="color: #848484;"><?php echo thaiDate($comment['Time']); ?></th>
                            </tr>

                            <tr>
                                <th scope="row" colspan="2">
                                    ความคิดเห็น: <?php echo $comment['Comment']; ?>
                                </th>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<script>
    $("#DM1")
        .keyup(function() {
            var value = $(this).val();
            $("#DM2").val(ArabicNumberToText(value));
        })
        .keyup();
</script>
<script>
    $('#test').on('change', function() {
        //  alert( this.value ); // or $(this).val()
        if (this.value == "1") {
            $('.ic').css('display', 'block');
            $('.passport').css('display', 'none');
            $('.to').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'block');


        } else if (this.value == "2") {
            $('.to').css('display', 'block');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'block');


        } else if (this.value == "3") {
            $('.to2').css('display', 'block');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to').css('display', 'none');
            $('.to3').css('display', 'block');

        } else {
            $('.to').css('display', 'none');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'none');


        }
    });
</script>

<!-- /.content-wrapper -->