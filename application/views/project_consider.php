<script src="assets/js/thaibath.js" type="text/javascript" charset="utf-8"></script>
<style>
    #ic,
    #passport {
        display: none;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 style="display: contents;">ข้อมูลโครงการ</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <?php $uri = base64_decode($this->input->get('uri')); ?>
    <?php $ac = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array(); ?>
    <?php $this->db->where('Username', $this->session->userdata('Username')); ?>
    <?php $this->db->where('Supervisor', 1); ?>
    <?php $this->db->or_where('Manager', 1); ?>
    <?php $this->db->or_where('Director', 1); ?>
    <?php $ac_check = $this->db->get('account')->row_array(); ?>
    <?php $status = $this->db->get_where('project', ['Project_id' => $PID])->row_array(); ?>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <div class="row" style="padding: 4px 0px 10px 8px;">
                    <div class="col-10">
                        <a href="project_list?PID=<?php echo base64_encode($PID); ?>&uri=<?= base64_encode($uri); ?>">รายละเอียดโครงการ</a> <span style="padding: 0px 10px;"> / </span>
                        <a href="project_doc?PID=<?php echo base64_encode($PID); ?>&uri=<?= base64_encode($uri); ?>">เอกสารโครงการ</a> <span style="padding: 0px 10px;"> / </span>
                   
                            <u><a>พิจารณาโครงการ</a></u> <span style="padding: 0px 10px;"> / </span>
                        <a href="project_review?PID=<?php echo base64_encode($PID); ?>&uri=<?= base64_encode($uri); ?>">ความคิดเห็น</a>
                    </div>
                    <div class="col-2 text-right">
                        <a href="<?= $uri; ?>">กลับหน้าโครงการ</a>
                    </div>
                </div>
                <!-- <button class="btn btn-info" style="float:right;" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus-circle"></i> สร้างประเด็นยุทธ์ศาสตร์ใหม่</button> -->

            </div>


            <div class="card-body">
                <div class="row container">
                    <label class="col-3" for="">พิจารณาโครงการ : </label>
                    <div class="col-9">
                        <!-- เจ้าหน้าที่แผน -->
                        <?php if ($ac['Director'] == 1) { ?>
                            <?php if ($status['Status'] == 11) { ?>
                                <span class="badge badge-success">โครงการนี้ได้อนุมัติเรียบร้อยแล้ว</span>
                            <?php } elseif ($status['Status'] == 5) { ?>
                                <span class="badge badge-warning">โครงการนี้ได้ถูกนำกลับไปแก้ไข</span>
                            <?php } elseif ($status['Status'] == 4) { ?>
                                <span class="badge badge-danger">ปิดโครงการแล้ว</span>
                            <?php } elseif ($status['Status'] == 3) { ?>
                                <!-- <button type="button" class="btn btn-success" id="pass"><i class="fa fa-check"></i></button>
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit_project"><i class="fa fa-exclamation-triangle"></i></button>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#not_pass"><i class="fa fa-times"></i></button> -->
                                <span class="badge badge-secondary">รอผู้บริหารตรวจสอบ</span>
                            <?php } elseif ($status['Status'] == 2) { ?>
                                <span class="badge badge-secondary">รอเจ้าหน้าที่แผนตรวจสอบ</span>
                            <?php } elseif ($status['Status'] == 1) { ?>
                                <span class="badge badge-secondary">รอหัวหน้าฝ่ายตรวจสอบ</span>
                            <?php } else { ?>
                                <span class="badge badge-danger">โครงการไม่ผ่านการอนุมัติ</span>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <hr>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>

<!-- Modal -->
<div class="modal fade" id="edit_project" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">ข้อมูลที่ถูกแก้ไข</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="project_status_padding_director" method="POST">
                <div class="modal-body">
                    <div>
                        <input type="text" value="<?php echo $PID; ?>" name="PID" hidden>
                        <input type="text" value="<?php echo $ac['Account_id']; ?>" name="Account_id" hidden>
                        <textarea name="Comment" rows="5" style="width: 100%" class="form-control" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                    <button type="submit" class="btn btn-primary">ส่ง</button>
                </div>
            </form>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="not_pass" tabindex="-1" role="dialog" aria-labelledby="not_pass" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">เนื่องจาก</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="project_status_notpass" method="POST">
                <div class="modal-body">
                    <div>
                        <input type="text" value="<?php echo $PID; ?>" name="PID" hidden>
                        <input type="text" value="<?php echo $ac['Account_id']; ?>" name="Account_id" hidden>
                        <textarea name="Comment" rows="5" style="width: 100%" class="form-control" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                    <button type="submit" class="btn btn-primary">ส่ง</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script>
    $("#DM1")
        .keyup(function() {
            var value = $(this).val();
            $("#DM2").val(ArabicNumberToText(value));
        })
        .keyup();
</script>

<script type="text/javascript">
    $('#pass').click(function() {
        $.ajax({
            type: 'POST',
            url: 'project_status_pass_director',
            data: {
                Project_id: <?php echo $PID; ?>,
                Account_id: <?= $ac['Account_id']; ?>
            },
            success: function(success) {
                if (success > 0) {
                    alert('อนุมัติโครงการเรียบร้อยแล้ว.');
                    window.location = 'project_consider?PID=<?= base64_encode($PID); ?>';
                } else {
                    alert('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
                    window.location = 'project_consider?PID=<?= base64_encode($PID); ?>';
                }
            }
        });
    });
</script>
<script>
    $('#test').on('change', function() {
        //  alert( this.value ); // or $(this).val()
        if (this.value == "1") {
            $('.ic').css('display', 'block');
            $('.passport').css('display', 'none');
            $('.to').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'block');


        } else if (this.value == "2") {
            $('.to').css('display', 'block');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'block');


        } else if (this.value == "3") {
            $('.to2').css('display', 'block');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to').css('display', 'none');
            $('.to3').css('display', 'block');

        } else {
            $('.to').css('display', 'none');
            $('.ic').css('display', 'none');
            $('.passport').css('display', 'none');
            $('.to2').css('display', 'none');
            $('.to3').css('display', 'none');


        }
    });
</script>

<!-- /.content-wrapper -->