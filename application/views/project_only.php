  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
          <div class="container-fluid">
              <div class="row mb-2">
                  <div class="col-sm-6">
                      <h1>จัดการข้อมูลโครงการ</h1>
                  </div>
                  <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                          <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                      </ol>
                  </div>
              </div>
          </div><!-- /.container-fluid -->
      </section>
      <?php $account_detail = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array(); ?>
      <!-- Main content -->
      <section class="content">
          <!-- Default box -->
          <div class="card">
              <div class="card-header">
                  <!-- <button class="btn btn-info" style="float:right;" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus-circle"></i> สร้างประเด็นยุทธ์ศาสตร์ใหม่</button> -->
                  <div style="background-color: rgba(0,0,0,.05); padding:15px;">
                      <form action="budget_add_com" method="post">
                          <div class="row">
                              <div class="col-3">
                                  <span>เลือกปีงบประมาณ</span>
                                  <select name="" id="" class="form-control" style="width:35%; display: inline-block;">
                                      <?php
                                        $Date = date('Y-m-d');
                                        $Date10 = date('Y', strtotime($Date . ' + 10 years'));
                                        for ($x = date('Y'); $x <= $Date10; $x++) { ?>
                                          <option value="<?php echo $x; ?>"> <?php echo $x+543; ?></option>
                                      <?php } ?>

                                  </select>
                              </div>
                              <div class="col-6">
                                  <span>สถานะ</span>
                                  <select name="" id="" class="form-control" style="width:60%; display: inline-block;">
                                      <option value="">ทั้งหมด</option>
                                      <option value="">รอหัวหน้าฝ่ายพิจารณา</option>
                                      <option value="">รอเจ้าหน้าที่แผนตรวจสอบ</option>
                                      <option value="">รออนุมัติจากผู้บริหาร</option>
                                      <option value="">อนุมัติ</option>
                                      <option value="">รอเจ้าหน้าที่แผนตรวจสอบปิดโครงการ</option>
                                      <option value="">รอผู้บริหารอนุมัติปิดโครงการ</option>
                                      <option value="">ปิดโครงการ / เสร็จตามระยะเวลา</option>
                                      <option value="">ปิดโครงการ / ไม่เป็นไปตามระยะเวลา</option>
                                      <option value="">ปิดโครงการ / ขอเลือน</option>
                                      <option value="">ปิดโครงการ / ขอยกเลิก</option>
                                      <option value="">แก้ไขโครงการ</option>
                                      <option value="">แก้ไขเอกสารประเมินโครงการ</option>
                                  </select>
                                  <button type="submit" class="btn btn-primary" style="margin-bottom: 3px;">ยืนยัน</button>
                              </div>
                          </div>
                      </form>
                  </div>
                  <div class="card-tools">
                      <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> -->

                  </div>
              </div>
              <div class="card-body">
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                          <tr>
                              <th style="border-bottom:none;"></th>
                              <th style="border-bottom:none;"></th>
                              <th class="text-center" colspan="4">ดูรายงานความก้าวหน้า</th>
                          </tr>
                          <tr>
                              <th class="text-center" style="border-top:none;">ชื่อโครงการ</th>
                              <th class="text-center" style="border-top:none;">สถานะ</th>
                              <th class="text-center" style="width: 100px;">ไตรมาส 1</th>
                              <th class="text-center" style="width: 100px;">ไตรมาส 2</th>
                              <th class="text-center" style="width: 100px;">ไตรมาส 3</th>
                              <th class="text-center" style="width: 100px;">ไตรมาส 4</th>
                          </tr>
                      </thead>
                      <tbody>
                      <?php $projects = $this->db->get('project')->result_array(); ?>
                      <?php foreach ($projects as $project) {?>
                          <tr>
                              <td><a href="project_list"><?php echo $project['Project_name']; ?></a></td>
                              <?php if($project['Status'] == 1){?>
                              <td class="text-center"><span class="badge badge-secondary" style="font-size: 13px;">รอเจ้าหน้าที่แผนตรวจสอบ</span></td>
                              <?php }elseif($project['Status'] == 2){ ?>
                                <td class="text-center"><span class="badge badge-secondary" style="font-size: 13px;">รอหัวหน้าฝ่ายอนุมัติ</span></td>
                              <?php }elseif($project['Status'] == 3){ ?>
                                <td class="text-center"><span class="badge badge-secondary" style="font-size: 13px;">รอผู้บริหารอนุมัติ</span></td>
                              <?php }elseif($project['Status'] == 4){ ?>
                                <td class="text-center"><span class="badge badge-danger" style="font-size: 13px;">ปิดโครงการ</span></td>
                              <?php }elseif($project['Status'] == 5){ ?>
                                <td class="text-center"><span class="badge badge-warning" style="font-size: 13px;">แก้ไขโครงการ</span></td>
                              <?php }elseif($project['Status'] == 11){ ?>
                                <td class="text-center"><span class="badge badge-primary" style="font-size: 13px;">อนุมัติ</span></td>
                              <?php } ?>

                              <td><button type="button" class="btn btn-default" disabled><i class="fa fa-eye" aria-hidden="true"></i> ดูรายงาน</button></td>
                              <td><button type="button" class="btn btn-default" disabled><i class="fa fa-eye" aria-hidden="true"></i> ดูรายงาน</button></td>
                              <td><button type="button" class="btn btn-default" disabled><i class="fa fa-eye" aria-hidden="true"></i> ดูรายงาน</button></td>
                              <td><button type="button" class="btn btn-default" disabled><i class="fa fa-eye" aria-hidden="true"></i> ดูรายงาน</button></td>
                          </tr>
                      <?php } ?>
                      </tbody>
                  </table>
              </div>
              <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </section>
      <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title">ข้อมูลปีงบประมาณ</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                  </button>
              </div>
              <div class="modal-body text-center">
                  <form action="budget_edit_com" method="post">
                      <!-- <div class="row my-2 mx-2 mb-4"> -->
                      <div class="form-group">
                          <input type="hidden" name="Strategic_Plan_id" value="<?php echo $budget['Strategic_Plan_id']; ?>">
                          <label for="recipient-name" class="col-form-label">ปีงบประมาณ: <span><?php echo $budget['Fiscalyear']; ?></span></label>
                      </div>
                      <div class="form-group">
                          <label for="recipient-name" class="col-form-label">ชื่อแผนงาน</label>
                          <input type="text" class="form-control" name="Strategic_Plan" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Strategic_Plan']; ?>">
                      </div>
                      <div class="form-group">
                          <label for="recipient-name" class="col-form-label">ผ่านมติกรรมการบริหาย</label>
                          <div>
                              <table class="table table-striped table-bordered">
                                  <thead>
                                      <th>ครั้งที่</th>
                                      <th>วันที่</th>
                                  </thead>
                                  <tbody>
                                      <td><input type="text" class="form-control" name="Director_of_time" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Director_of_time']; ?>"></td>
                                      <td><input type="date" class="form-control" name="Director_of_date" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Director_of_date']; ?>"></td>
                                  </tbody>
                              </table>
                          </div>

                      </div>
                      <div class="form-group">
                          <label for="recipient-name" class="col-form-label">ผ่านมติกรรมการประจำ</label>
                          <div>
                              <table class="table table-striped table-bordered text-center">
                                  <thead>
                                      <th>ครั้งที่</th>
                                      <th>วันที่</th>
                                  </thead>
                                  <tbody>
                                      <td><input type="text" class="form-control" name="Ref_of_time" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Ref_of_time']; ?>"></td>
                                      <td><input type="date" class="form-control" name="Ref_of_date" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Ref_of_date']; ?>"></td>
                                  </tbody>
                              </table>
                          </div>

                      </div>
                      <!-- </div> -->

              </div>
              <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                  <button type="submit" class="btn btn-warning">บันทึก</button>
              </div>
          </div>
          </form>
          <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
  </div>
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">แผนยุทธศาสตร์</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="workplan_add" method="POST">
                  <div class="modal-body">
                      <div class="form-group">
                          <label for="">ชื่อแผนยุทธ์ศาสตร์</label>
                          <input type="text" class="form-control" name="strategic_plan">
                      </div>
                      <div id="straBT">
                          <div class="form-group">
                              <label for="">ประเด็นยุทธ์ศาสตร์</label>
                              <input type="text" class="form-control" name="strategic_name">
                          </div>
                          <div class="form-group">
                              <label for="">เป้าประสงค์ที่</label>
                              <input type="text" class="form-control" name="goal_name">
                          </div>
                          <div id="meas">
                              <div class="form-group">
                                  <label for="">ตัวชี้วัด</label>
                                  <input type="text" class="form-control" name="#">
                              </div>
                          </div>
                          <div class="form-group" style="text-align:center;">
                              <button type="button" class="btn btn-info" id="btn1" style="color:#fff;"><i class="fa fa-plus"></i> เพิ่มตัวชี้วัด</button>
                          </div>

                          <div class="row">
                              <div class="col-6">
                                  <div class="form-group">
                                      <label for="">หน่วยนับ</label>
                                      <input type="text" class="form-control" name="#">
                                  </div>
                              </div>
                              <div class="col-6">
                                  <div class="form-group">
                                      <label for="">ค่าเป้าหมาย</label>
                                      <input type="text" class="form-control" name="#">
                                  </div>
                              </div>
                          </div>
                          <div id="goal">
                              <div class="form-group">
                                  <label for="">กลยุทธ์</label>
                                  <input type="text" class="form-control" name="tactic_name">
                              </div>
                          </div>
                          <div class="form-group" style="text-align:center;">
                              <button type="button" class="btn btn-success" style="color:#fff;" id="btn7"><i class="fa fa-plus"></i> เพิ่มกลยุทธ์</button>
                          </div>
                      </div>
                      <hr>
                      <div class="form-group" style="text-align:center;">
                          <button type="button" class="btn btn-info" style="color:#fff;" id="stra1"><i class="fa fa-plus"></i> เพิ่มเป้าประสงค์</button>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times-circle"></i> ยกเลิก</button>
                      <button type="submit" class="btn btn-primary"><i class="fa fa-download"></i> จัดเก็บ</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
  <script>
      $(document).ready(function() {
          $('#example').DataTable();
      });
  </script>