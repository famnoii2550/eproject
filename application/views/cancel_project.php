  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<section class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1>รายชื่อโครงการที่ไม่อนุมัติ/ยกเลิก</h1>
  				</div>
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
  					</ol>
  				</div>
  			</div>
  		</div><!-- /.container-fluid -->
  	</section>

  	<!-- Main content -->
  	<section class="content">

  		<!-- Default box -->
  		<div class="card">
  			<div class="card-header">
  				<h3 class="card-title">รายชื่อโครงการที่ไม่อนุมัติ/ยกเลิก</h3>
  				<? echo $data; ?>
  				<div class="card-tools">
  					<!-- <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> -->
  				</div>
  			</div>
  			<div class="card-body">
  				<div class="row">
  					<div class="col-6 text-right row">
  						เลือกปีงบประมาณ :
  						<select class="form-control">
  							<option value="2563">2563</option>
  							<option value="2562">2562</option>
  							<option value="2561">2561</option>
  							<option value="2560">2560</option>
  							<option value="2559">2559</option>
  							<option value="2558">2558</option>
  							<option value="2557">2557</option>
  						</select>
  					</div>
  				</div>
  				<table id="table" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example1" aria-describedby="dataTables-example1_info">
  					<thead>
  						<tr role="row">
  							<th rowspan="1" colspan="1" style="width: 0px;">ชื่อโครงการ</th>
  							<th rowspan="1" colspan="1" style="width: 0px;">สถานะ</th>
  							<th rowspan="1" colspan="1" style="width: 0px;">เนื่องจาก</th>
  						</tr>
  					</thead>
  					<tbody id="project">
  						<tr class="odd">
  							<td style="text-align:left" class=" ">
  								<a href="/home/data_project_detail">โครงการปรับปรุงระบบตรวจจับการบุกรุกและป้องกันระบบเครือข่าย</a>
  							</td>
  							<td class=" ">
  								<span class="badge badge-danger">ไม่อนุมัติ</span> </td>
  							<td class=" ">

  								เนื่องจากอุปกรณ์มีราคาสูง และอุปกรณ์ที่ใช้งานอยู่ในปัจจุบันยังใช้งานได้ไม่เต็มประสิทธิภาพ รวมทั้งลิงก์อินเทอร์เน็ต ณ ปัจจุบันยังไม่ถึง 10 GB ดังนั้น คกก.จึงมีความเห็นว่ายังไม่มีความจำเป็นต้องเปลี่ยนอุปกรณ์ในตอนนี้
  							</td>
  						</tr>
  						<tr class="even">
  							<td style="text-align:left" class=" ">
  								โครงการจัดหาระบบบริหารจัดการและรักษาความปลอดภัยสำหรับเครื่องคอมพิวเตอร์แม่ข่ายเสมือน
  							</td>
  							<td class=" ">
  								<span class="badge badge-danger">ไม่อนุมัติ</span> </td>
  							<td class=" ">

  								จากหลักการและเหตุผลที่ให้มา เรื่องของการป้องกันความปลอดภัยให้กับเครื่องแม่ข่ายเสมือน เทคโนโลยีที่นำมาใช้อาจจะมีราคาสูงมากเกินไป สามารถใช้ VM antivirus ที่มีประสิทธิภาพดี ทดแทนได้หรือไม่ ในโครงการนอกแผน
  							</td>
  						</tr>
  						<tr class="odd">
  							<td style="text-align:left" class=" ">
  								โครงการปรับปรุงและเพิ่มประสิทธิภาพระบบรักษาความปลอดภัยเว็บไซต์
  							</td>
  							<td class=" ">
  								<span class="badge badge-danger">ไม่อนุมัติ</span> </td>
  							<td class=" ">

  								ตามมติที่ประชุมคกก.บริหารครั้งที่ 6/2562
  							</td>
  						</tr>
  						<tr class="even">
  							<td style="text-align:left" class=" ">
  								โครงการปรับปรุงระบบควบคุมและกระจายโหลดการทำงานสำหรับเว็บเซิร์ฟเวอร์
  							</td>
  							<td class=" ">
  								<span class="badge badge-danger">ไม่อนุมัติ</span> </td>
  							<td class=" ">

  								ตามมติที่ประชุมคกก.บริหารครั้งที่ 6/2562
  							</td>
  						</tr>
  						<tr class="odd">
  							<td style="text-align:left" class=" ">
  								โครงการพัฒนาศักยภาพบุคลากรด้านการพัฒนาซอฟต์แวร์
  							</td>
  							<td class=" ">
  								<span class="badge badge-danger">ไม่อนุมัติ</span> </td>
  							<td class=" ">

  								อนุมัติในหลักการแต่เนื่องจากกำหนดให้เป็นโครงการนอกแผน ดังนั้นจึงขอให้ผู้รับผิดชอบนำเสนอโครงการเข้าระบบหลังจากที่มีรายละเอียดต่าง ๆ ชัดเจนแล้ว ไม่จำเป็นต้องเสนอในขณะนี้
  							</td>
  						</tr>
  						<tr class="even">
  							<td style="text-align:left" class=" ">
  								โครงการการให้บริการศูนย์ให้ความช่วยเหลือด้านเทคโนโลยีสารสนเทศ
  							</td>
  							<td class=" ">
  								<span class="badge badge-danger">ไม่อนุมัติ</span> </td>
  							<td class=" ">

  								ยกเลิกโครงการ
  							</td>
  						</tr>
  					</tbody>
  				</table>
  			</div>
  			<!-- /.card-body -->
  		</div>
  		<!-- /.card -->

  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->