<footer class="main-footer">
    <!-- <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.1
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap 4 -->
<script src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="assets/plugins/chart.js/Chart.min.js"></script>
<!-- DataTables -->
<script src="assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- AdminLTE App -->
<script src="assets/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="assets/js/demo.js"></script>
<script src="assets/js/demo.js"></script>
<script src="assets/js/BAHTTEXT.js"></script>
<script src="assets/js/BAHTTEXT.min.js"></script>
<script>
    $(document).ready(function() {
        $('#strategic').change(function() {
            var provinces_id = $('#strategic').val();
            if (provinces_id != '') {
                $.ajax({
                    url: 'fetch_state',
                    method: "POST",
                    data: {
                        PROVINCE_ID: provinces_id
                    },
                    success: function(data) {
                        $('#GOAL').html(data);
                    }
                })
            }
            console.log(provinces_id);
        });
        $('#GOAL').change(function() {
            var amphures_id = $('#GOAL').val();
            if (amphures_id != '') {
                $.ajax({
                    url: 'fetch_city',
                    method: 'POST',
                    data: {
                        AMPHUR_ID: amphures_id
                    },
                    success: function(data) {
                        $('#TACTIC').html(data);
                    }
                })
            }
            console.log(amphures_id);
        });
    });
</script>

<script>
    $(function() {
        var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
        var donutChart = new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        })
        var donutData = {
            labels: [
                'ยกเลิกโครงการ',
                'กำลังดำเนินโครงการ',
                'ไม่เป็นไปตามระยะเวลาที่กำหนด',
                'ขอเลื่อนการดำเนินการ',
                'ดำเนินการแล้วเสร็จตามเวลาที่กำหนด',
            ],
            datasets: [{
                data: [0, 1, 3, 0, 25],
                backgroundColor: ['#FF0F00', '#FF9E01', '#CD0D74', '#5bb2c2', '#00CC00'],
            }]
        }
        var donutOptions = {
            maintainAspectRatio: false,
            responsive: true,
        }
        var donutChart = new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        })
    })

    $(function() {
        var donutChartCanvas_1 = $('#donutChart_1').get(0).getContext('2d')
        var donutChart_1 = new Chart(donutChartCanvas_1, {
            type: 'doughnut',
            data: donutData_1,
            options: donutOptions_1
        })
        var donutData_1 = {
            labels: [
                'ตรงตามระยะเวลาที่กำหนด',
                'ไม่ตรงตามระยะเวลาที่กำหนด',
            ],
            datasets: [{
                data: [19, 8],
                backgroundColor: ['#CD0D74', '#5bb2c2'],
            }]
        }
        var donutOptions_1 = {
            maintainAspectRatio: false,
            responsive: true,
        }
        var donutChart_1 = new Chart(donutChartCanvas_1, {
            type: 'doughnut',
            data: donutData_1,
            options: donutOptions_1
        })
    })

    $(function() {
        var donutChartCanvas = $('#donutChart_2').get(0).getContext('2d')
        var donutChart_2 = new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        })
        var donutData = {
            labels: [
                'ตรงตามระยะเวลาที่กำหนด',
                'ไม่ตรงตามระยะเวลาที่กำหนด',
            ],
            datasets: [{
                data: [15, 10],
                backgroundColor: ['#CD0D74', '#5bb2c2'],
            }]
        }
        var donutOptions = {
            maintainAspectRatio: false,
            responsive: true,
        }
        var donutChart = new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        })
    })

    $(function() {
        var donutChartCanvas = $('#donutChart_3').get(0).getContext('2d')
        var donutChart_3 = new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        })
        var donutData = {
            labels: [
                'ตรงตามระยะเวลาที่กำหนด',
                'ไม่ตรงตามระยะเวลาที่กำหนด',
            ],
            datasets: [{
                data: [13, 8],
                backgroundColor: ['#CD0D74', '#5bb2c2'],
            }]
        }
        var donutOptions = {
            maintainAspectRatio: false,
            responsive: true,
        }
        var donutChart = new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        })
    })

    $(function() {
        $("#table").DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "oLanguage": {
                "sEmptyTable": "ไม่มีข้อมูลในตาราง",
                "sInfo": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                "sInfoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
                "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
                "sInfoPostFix": "",
                "sInfoThousands": ",",
                "sLengthMenu": "แสดง _MENU_ แถว",
                "sLoadingRecords": "กำลังโหลดข้อมูล...",
                "sProcessing": "กำลังดำเนินการ...",
                "sSearch": "ค้นหา: ",
                "sZeroRecords": "ไม่พบข้อมูล",
                "oPaginate": {
                    "sFirst": "หน้าแรก",
                    "sPrevious": "ก่อนหน้า",
                    "sNext": "ถัดไป",
                    "sLast": "หน้าสุดท้าย"
                },
                "oAria": {
                    "sSortAscending": ": เปิดใช้งานการเรียงข้อมูลจากน้อยไปมาก",
                    "sSortDescending": ": เปิดใช้งานการเรียงข้อมูลจากมากไปน้อย"
                }
            }
        });
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });
</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    <?php if ($suss = $this->session->flashdata('save_ss2')) : ?>
        swal("Good job!", '<?php echo $suss; ?>', "success");
    <?php endif; ?>
    <?php if ($error = $this->session->flashdata('del_ss2')) : ?>
        swal("เกิดข้อผิดพลาด !", '<?php echo $error; ?>', "error");
    <?php endif; ?>
</script>


</body>

</html>