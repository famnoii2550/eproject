<div id="first_tactic">
    <div class="row container" style="padding: 25px 0 0 0px;">
        <div class="col-3"> </div>
        <label class="col-2 text-right" for="">กลยุทธ์ :</label>
        <div class="col-4">
            <select class="form-control tactic" name="Tactic_name[]">
                <option value="" selected disabled>กรุณาเลือกกลยุทธ์</option>
                <?php foreach ($tacticGetList as $tacticGetDetail) { ?>
                    <option value="<?php echo $tacticGetDetail['Tactic_id']; ?>"><?php echo $tacticGetDetail['Tactic_name']; ?></option>
                <?php } ?>
            </select>
            <input type="hidden" name="goal_id" value="<?php echo $tacticGet; ?>">
        </div>
        <div class="col-2">
            <button type="button" onClick="add_tactic(this);" class="btn btn-info add_tactic" id="btn_tactic" style="color:#fff;"><i class="fa fa-plus"></i> เพิ่มกลยุทธ์</button>
        </div>
    </div>
</div>
<div id="indicList"></div>

<script>
    $("select[name='Tactic_name[]']").change(function() {

        getIndicList($(this));
    });

    function getIndicList(event) {
        let indicGet = false;
        let goalGet = $("input[name=goal_id]").val();
        let titles = $('.tactic option:selected').map(function(idx, elem) {
            return $(elem).val();
        }).get();

        console.log(titles);
        event.preventDefault();
        if (event) {
            indicGet = event.val();
        }

        $.ajax({
            url: "<?php echo base_url('Project_ctr/indic_project_app'); ?>",
            data: {
                indicGet: indicGet,
                goalGet: goalGet,
            },
            success: function(response) {
                event.parents('#tacticList').find('#indicList').html(response);
            }
        });
    }
</script>
<script>
    //กลยุทธ์
    function add_tactic(e) {
        $(e).parents('#first_tactic').after('<div id="first_tactic"><div class="row container" style="padding: 25px 0 0 0px;"><div class="col-3"> </div><label class="col-2 text-right" for="">กลยุทธ์ :</label><div class="col-4"><select class="form-control tactic" name="tactic[]"><option value="" selected disabled>กรุณาเลือกกลยุทธ์</option><?php foreach ($tacticGetList as $tacticGetDetail) { ?><option value="<?php echo $tacticGetDetail['Tactic_id']; ?>"><?php echo $tacticGetDetail['Tactic_name']; ?></option><?php } ?></select><input type="hidden" name="goal_id" value="<?php echo $tacticGet; ?>"></div><div class="col-2"><button type="button" onClick="remove_tactic(this);" class="btn btn-danger add_tactic" id="btn_tactic" style="color:#fff;"><i class="fa fa-times"></i> ลบกลยุทธ์</button></div></div></div>');
    }

    function remove_tactic(e) {
        $(e).parents('#first_tactic').remove();
    }
</script>