<div class="row container" style="    padding: 25px 0 0 0px;">
    <div class="col-3"> </div>
        <label class="col-2 text-right" for="">ประเด็นยุทธศาสตร์ :</label>
    <div class="col-4">
        <select class="form-control" name="strategic">
            <option value="" selected disabled>กรุณาเลือกประเด็นยุทธศาสตร์</option>
            <?php foreach ($strategicGetList as $strategicGetDetail) { ?>
            <option value="<?php echo $strategicGetDetail['Strategic_id']; ?>"><?php echo $strategicGetDetail['Strategic_name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div id="goalList"></div> 

<script>


    $("select[name='strategic']").change(function(){
        getGoalList($(this));

    });
    function getGoalList(event){
        let goalGet = false;
        if (event) {
            goalGet = event.val();
        }
        $.ajax({
            url:"<?php echo base_url('Project_ctr/goal_project_app'); ?>",
            data:{goalGet:goalGet},
            success:function(response){
                event.parents('#strategicList').find('#goalList').html(response);
            }
        });
    }
	
</script>
