<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ระบบบริหารและประเมินผลโครงการ</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="assets/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="assets/css/adminlte.min.css">
	<!-- Google Font: Source Sans Pro -->


	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

	<!-- jQuery -->
	<script src="assets/plugins/jquery/jquery.min.js"></script>
</head>

<body class="hold-transition sidebar-mini">
	<?php
	function DateThai2($date)
	{
		$strYear = date("Y", strtotime($date)) + 543;
		$strMonth = date("n", strtotime($date));
		$strDay = date("j", strtotime($date));
		$strHour = date("H", strtotime($date));
		$strMinute = date("i", strtotime($date));
		$strSeconds = date("s", strtotime($date));

		$strMonthCut = array("", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
		$strMonthThai = $strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear $strHour:$strMinute น.";
	}

	function DateThai($date)
	{
		$strYear = date("Y", strtotime($date)) + 543;
		$strMonth = date("n", strtotime($date));
		$strDay = date("j", strtotime($date));
		$strHour = date("H", strtotime($date));
		$strMinute = date("i", strtotime($date));
		$strSeconds = date("s", strtotime($date));

		$strMonthCut = array("", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
		$strMonthThai = $strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
	}
	?>
	<?php $uri = $this->input->get('uri'); ?>
	<!-- Site wrapper -->
	<div class="wrapper">
		<!-- Navbar -->
		<nav class="main-header navbar navbar-expand navbar-light navbar-warning">
			<!-- Left navbar links -->
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
				</li>
				<!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="../../index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
			</ul>

			<!-- Right navbar links -->
			<ul class="navbar-nav ml-auto">
				<!-- Notifications Dropdown Menu -->
				<li class="nav-item">
					<a data-toggle="modal" data-target="#profile_modal" class="nav-link">
						<span>ข้อมูลส่วนตัว</span>
					</a>
				</li>
				<li class="nav-item">
					<a href="Logout" class="nav-link">
						<span>ออกจากระบบ <i class="fas fa-sign-out-alt ml-2"></i></span>
					</a>
				</li>
			</ul>
		</nav>
		<!-- /.navbar -->
		<?php $account_detail = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array(); ?>
		<div class="modal fade" id="profile_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="exampleModalLabel">แก้ไขบัญชีผู้ใช้งาน</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="edit_profile" method="POST">
						<div class="modal-body">
							<input type="hidden" name="id" value="<?php echo $account_detail['Account_id']; ?>">
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Username: <span style="color:red;">*</span></label>
								<input type="text" class="form-control" id="recipient-name" disabled value="<?php echo $account_detail['Username']; ?>" required>
							</div>

							<div class="form-group">
								<label for="recipient-name" class="col-form-label">ชื่อ: <span style="color:red;">*</span></label>
								<input type="text" name="first_name" class="form-control" id="recipient-name" disabled value="<?php echo $account_detail['Fname']; ?>" required>
							</div>

							<div class="form-group">
								<label for="recipient-name" class="col-form-label">นามสกุล: <span style="color:red;">*</span></label>
								<input type="text" name="last_name" class="form-control" id="recipient-name" disabled value="<?php echo $account_detail['Lname']; ?>" required>
							</div>




							<!-- <div class="form-group">
                <label for="recipient-name" class="col-form-label">เบอร์โทรศัพท์: <span style="color:red;">*</span></label>
                <input type="text" name="tel" class="form-control" id="recipient-name" value="<?php echo $account_detail['Tel']; ?>" required>
              </div> -->

							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Email: <span style="color:red;">*</span></label>
								<input type="email" name="email" class="form-control" id="recipient-name" disabled value="<?php echo $account_detail['Email']; ?>" required>
							</div>

							<!-- <div class="form-group">
								<label for="recipient-name" class="col-form-label">สถานะ: </label>
								<div class="col-12">
									<div style="">
										<div class="col-12">
											<input type="radio" name="director_responsible" id="add_director" onload="checkResponsible<?php echo $account_detail['Account_id']; ?>();" onChange="add_checkDirector_user<?php echo $account_detail['Account_id']; ?>(this);" <?php if ($account_detail['Director'] == 1) {
																																																																				echo "checked";
																																																																			} ?> value="director">
											<div style="display: inline-block;">ผู้บริหาร</div>
										</div>
									</div>
								</div>

								<div class="col-12">
									<div style="">
										<div class="col-12">
											<input type="radio" name="director_responsible" id="add_responsible" onChange="add_checkResponsible_user<?php echo $account_detail['Account_id']; ?>(this);" <?php if ($account_detail['Responsible'] == 1) {
																																																				echo "checked";
																																																			} ?> value="responsible">
											<div style="display: inline-block;">สิทธิ์ของผู้ใช้ในหน่วยงาน</div>
										</div>
									</div>
								</div>

								<div class="col-12 add_none_user<?php echo $account_detail['Account_id']; ?>">
									<div style="display:flex; justify-content: space-around;">
										<div class="col-4">
											<input type="checkbox" class="single-checkbox_user<?php echo $account_detail['Account_id']; ?>" name="manager" id="add_manager" <?php if ($account_detail['Manager'] == 1) {
																																												echo "checked";
																																											} ?>>
											<div style="display: inline-block;">เจ้าหน้าที่แผน</div>
										</div>

										<div class="col-4">
											<input type="checkbox" class="single-checkbox_user<?php echo $account_detail['Account_id']; ?>" name="supervisor" id="add_supervisor" <?php if ($account_detail['Supervisor'] == 1) {
																																														echo "checked";
																																													} ?>>
											<div style="display: inline-block;">หัวหน้าฝ่าย</div>
										</div>
									</div>
								</div>

								<div class="col-12 add_none_user<?php echo $account_detail['Account_id']; ?>">
									<div style="display:flex; justify-content: space-around;">
										<div class="col-4">
											<input type="checkbox" class="single-checkbox_user<?php echo $account_detail['Account_id']; ?>" name="supplies" id="add_supplies" <?php if ($account_detail['Supplies'] == 1) {
																																													echo "checked";
																																												} ?>>
											<div style="display: inline-block;">นักวิชาการพัสดุ</div>
										</div>
										<div class="col-4">
											<input type="checkbox" class="single-checkbox_user<?php echo $account_detail['Account_id']; ?>" name="admin" id="add_admin" <?php if ($account_detail['Admin'] == 1) {
																																											echo "checked";
																																										} ?>>
											<div style="display: inline-block;">ผู้ดูแลระบบ</div>
										</div>
									</div>
								</div>
							</div>

							<script type="text/javascript">
								function add_checkDirector_user<?php echo $account_detail['Account_id']; ?>(check) {
									if (check.checked == true) {
										$("#add_director").attr('checked', true);
										$(".add_none_user<?php echo $account_detail['Account_id']; ?>").css('display', 'none');
										$("#add_manager").prop('checked', false);
										$("#add_supervisor").prop('checked', false);
										$("#add_supplies").prop('checked', false);
										$("#add_admin").prop('checked', false);
									}

								}

								function add_checkResponsible_user<?php echo $account_detail['Account_id']; ?>(check) {
									if (check.checked == true) {
										$("#add_responsible").attr('checked', true);
										$(".add_none_user<?php echo $account_detail['Account_id']; ?>").css('display', 'block');
									}
								}

								<?php if ($account_detail['Director'] == 1) { ?>
									$(".add_none_user<?php echo $account_detail['Account_id']; ?>").css('display', 'none');
								<?php } ?>

								$("input.single-checkbox_user<?php echo $account_detail['Account_id']; ?>").click(function() {
									var bol = $("input.single-checkbox_user<?php echo $account_detail['Account_id']; ?>:checked").length >= 2;
									$("input.single-checkbox_user<?php echo $account_detail['Account_id']; ?>").not(":checked").attr("disabled", bol);
								});

								$(document).ready(function() {
									var bolt = $("input.single-checkbox_user<?php echo $account_detail['Account_id']; ?>:checked").length >= 2;
									$("input.single-checkbox_user<?php echo $account_detail['Account_id']; ?>").not(":checked").attr("disabled", bolt);
								});
							</script> -->

							<div class="form-group">
								<label for="recipient-name" class="col-form-label">ฝ่าย: </label>

								<select name="department_id" class="form-control">
									<?php
									$department_all = $this->db->get_where('department', ['Department_id' => $account_detail['Department_id']])->row_array(); ?>
									<option value="<?php echo $department_all['Department_id']; ?>" disabled selected><?php echo $department_all['Department']; ?></option>
								</select>



							</div>
						</div>
						<div class="modal-footer justify-content-between">
							<button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
							<!-- <button type="submit" class="btn btn-warning">บันทึก</button> -->
						</div>
					</form>
				</div>
			</div>
		</div>


		<!-- Main Sidebar Container -->
		<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color: #fd7e14 ;">
			<!-- Brand Logo -->
			<?php $profile = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array(); ?>
			<a href="#" class="brand-link navbar-warning">
				<img src="assets/img/logo.png" class="brand-image img-circle elevation-3 bg-light" style="opacity: .8">
				<span class="brand-text font-weight-light text-dark" style="font-size:12px;">ระบบบริหารและประเมินผลโครงการ</span>
			</a>

			<!-- Sidebar -->
			<div class="sidebar" style="background-color: #fd7e14">
				<!-- Sidebar user (optional) -->
				<div class="user-panel mt-3 pl-2 pb-3 mb-3 d-flex justify-content-start">
					<div class="info">
						<a href="#" class="d-block"><?php echo $profile['Fname'] . ' ' . $profile['Lname'] ?></a>
					</div>
				</div>

				<!-- Sidebar Menu -->
				<nav class="mt-2">
					<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
						<?php if ($profile['Director'] == '1') : ?>
							<li class="nav-header">ผู้บริหาร</li>
							<li class="nav-item has-treeview">
								<a href="budget" class="nav-link <?php if ($this->uri->segment(1) == 'project' || $this->uri->segment(1) == 'project_list_director' || $this->uri->segment(1) == 'project_doc_director' || $this->uri->segment(1) == 'project_consider_director' || $this->uri->segment(1) == 'project_NotApp_director' || $this->uri->segment(1) == 'project_review_director' || $uri == 'cHJvamVjdA==') {
																		echo 'active';
																	}; ?>
																	">
									<i class="nav-icon far fa-circle"></i>
									<p>
										จัดการข้อมูลโครงการ
									</p>
								</a>
								<ul class="nav nav-treeview">
									<li class="nav-item">
										<a href="project" class="nav-link <?php if ($this->uri->segment(1) == 'project' || $this->uri->segment(1) == 'project_list_director' || $this->uri->segment(1) == 'project_doc_director' || $this->uri->segment(1) == 'project_consider_director' || $this->uri->segment(1) == 'project_NotApp_director' || $this->uri->segment(1) == 'project_review_director' || $uri == 'cHJvamVjdA==') {
																				echo 'active';
																			}; ?>">
											<i class="far fa-circle nav-icon"></i>
											<p>รายชื่อโครงการ</p>
										</a>
										<a href="project_NotApp_director" class="nav-link <?php if ($this->uri->segment(1) == 'project_NotApp_director') {
																								echo 'active';
																							}; ?>">
											<i class="far fa-circle nav-icon"></i>
											<p>โครงการที่ไม่อนุมัติ</p>
										</a>

									</li>
								</ul>
							</li>
						<?php endif; ?>
						<?php if ($profile['Manager'] == '1') : ?>
							<li class="nav-header">เจ้าหน้าที่แผน</li>
							<li class="nav-item has-treeview">
								<a href="budget" class="nav-link <?php if ($this->uri->segment(1) == 'budget') {
																		echo 'active';
																	}; ?>">
									<i class="nav-icon far fa-circle"></i>
									<p>
										จัดการข้อมูลปีงบประมาณ
									</p>
								</a>
							</li>
							<li class="nav-item has-treeview">
								<a href="plane_list" class="nav-link <?php if ($this->uri->segment(1) == 'plane_list') {
																			echo 'active';
																		}; ?>">
									<i class="nav-icon far fa-circle"></i>
									<p>
										จัดการข้อมูลแผนยุทธ์ศาสตร์
									</p>
								</a>
							</li>
							<li class="nav-item has-treeview">
								<a href="workplan" class="nav-link <?php if ($this->uri->segment(1) == 'workplan') {
																		echo 'active';
																	}; ?>">
									<i class="nav-icon far fa-circle"></i>
									<p>
										จัดการข้อมูลประเด็นยุทธศาสตร์
									</p>
								</a>
							</li>
							<li class="nav-item has-treeview">
								<a href="project" class="nav-link <?php if ($this->uri->segment(1) == 'project_manager' || $this->uri->segment(1) == 'project_notAppAll_manager' || $this->uri->segment(1) == 'project_list_manager' || $this->uri->segment(1) == 'project_doc_manager' || $this->uri->segment(1) == 'project_consider_manager' || $this->uri->segment(1) == 'project_manager_review' || $uri == 'cHJvamVjdF9tYW5hZ2Vy') {
																		echo 'active';
																	}; ?>">
									<i class="nav-icon far fa-circle"></i>
									<p>
										จัดการข้อมูลโครงการ
									</p>
								</a>
								<ul class="nav nav-treeview">
									<li class="nav-item">
										<a href="project_manager" class="nav-link <?php if ($this->uri->segment(1) == 'project_manager' || $this->uri->segment(1) == 'project_notAppAll_manager' || $this->uri->segment(1) == 'project_list_manager' || $this->uri->segment(1) == 'project_doc_manager' || $this->uri->segment(1) == 'project_consider_manager' || $this->uri->segment(1) == 'project_manager_review' || $uri == 'cHJvamVjdF9tYW5hZ2Vy') {
																						echo 'active';
																					}; ?>">
											<i class="far fa-circle nav-icon"></i>
											<p>รายชื่อโครงการ</p>
										</a>
										<a href="project_notAppAll_manager" class="nav-link">
											<i class="far fa-circle nav-icon"></i>
											<p>โครงการที่ไม่อนุมัติ</p>
										</a>
										<!-- <a href="project_app" class="nav-link">
											<i class="far fa-circle nav-icon"></i>
											<p>สร้างโครงการใหม่</p>
										</a> -->
									</li>
								</ul>
							</li>
							<!-- <li class="nav-item has-treeview">
								<a href="workplan_report" class="nav-link">
									<i class="nav-icon far fa-circle"></i>
									<p>
										รายงาน
									</p>
								</a>
							</li> -->
						<?php endif; ?>
						<?php if ($profile['Supervisor'] == '1') : ?>
							<li class="nav-header">หัวหน้าฝ่าย</li>

							<li class="nav-item has-treeview">
								<a href="project" class="nav-link <?php if ($this->uri->segment(1) == 'project_supervisor' || $this->uri->segment(1) == 'project_NotApp_All' || $this->uri->segment(1) == 'project_list_supervisor' || $this->uri->segment(1) == 'project_doc_supervisor' || $this->uri->segment(1) == 'project_consider_supervisor' || $this->uri->segment(1) == 'project_supervisor_review' || $uri == 'cHJvamVjdF9zdXBlcnZpc29y') {
																		echo 'active';
																	}; ?>">
									<i class="nav-icon far fa-circle"></i>
									<p>
										จัดการข้อมูลโครงการ
									</p>
								</a>
								<ul class="nav nav-treeview">
									<li class="nav-item">
										<a href="project_supervisor" class="nav-link <?php if ($this->uri->segment(1) == 'project_supervisor' || $uri == 'cHJvamVjdF9zdXBlcnZpc29y') {
																							echo 'active';
																						}; ?>">
											<i class="far fa-circle nav-icon"></i>
											<p>รายชื่อโครงการ</p>
										</a>
										<a href="project_NotApp_All" class="nav-link <?php if ($this->uri->segment(1) == 'project_NotApp_All') {
																							echo 'active';
																						}; ?>">
											<i class="far fa-circle nav-icon"></i>
											<p>โครงการที่ไม่อนุมัติ</p>
										</a>
										<!-- <a href="project_app" class="nav-link">
											<i class="far fa-circle nav-icon"></i>
											<p>สร้างโครงการใหม่</p>
										</a> -->
									</li>
								</ul>
							</li>
						<?php endif; ?>
						<?php if ($profile['Supplies'] == '1') : ?>
							<li class="nav-header">นักวิชาการพัสดุ</li>
							<li class="nav-item has-treeview">
								<a href="#" class="nav-link <?php if ($this->uri->segment(1) == 'project_supplies') {
																echo 'active';
															}; ?>">
									<i class="nav-icon far fa-circle"></i>
									<p>
										จัดการข้อมูลโครงการ
									</p>
								</a>
								<ul class="nav nav-treeview">
									<li class="nav-item">
										<a href="project_supplies" class="nav-link <?php if ($this->uri->segment(1) == 'project_supplies') {
																						echo 'active';
																					}; ?>">
											<i class="far fa-circle nav-icon"></i>
											<p>รายชื่อโครงการ</p>
										</a>
									</li>
								</ul>
							</li>
						<?php endif; ?>
						<?php if ($profile['Responsible'] == '1') : ?>
							<li class="nav-header">ผู้รับผิดชอบโครงการ</li>

							<li class="nav-item has-treeview">
								<a href="#" class="nav-link <?php if ($this->uri->segment(1) == 'list_project' || $this->uri->segment(1) == 'project_NotApp' || $this->uri->segment(1) == 'project_app' || $this->uri->segment(1) == 'project_list' || $this->uri->segment(1) == 'project_doc' || $this->uri->segment(1) == 'project_consider' || $uri == 'bGlzdF9wcm9qZWN0' || $this->uri->segment(1) == 'project_NotApp?Role=' . base64_encode('Responsible')) {
																echo 'active';
															}; ?>
															">
									<i class="nav-icon far fa-circle"></i>
									<p>
										จัดการข้อมูลโครงการ
										<i class="right fas fa-angle-left"></i>
									</p>
								</a>
								<ul class="nav nav-treeview">
									<li class="nav-item">
										<a href="list_project" class="nav-link <?php if ($this->uri->segment(1) == 'list_project' || $this->uri->segment(1) == 'project_list' || $uri == 'bGlzdF9wcm9qZWN0') {
																					echo 'active';
																				}; ?>
																				">
											<i class="far fa-circle nav-icon"></i>
											<p>รายชื่อโครงการ</p>
										</a>
										<a href="project_NotApp?Role=<?= base64_encode('Responsible'); ?>" class="nav-link <?php if ($this->uri->segment(1) == 'project_NotApp?Role=' . base64_encode('Responsible')) {
																																echo 'active';
																															}; ?>">
											<i class="far fa-circle nav-icon"></i>
											<p>โครงการที่ไม่อนุมัติ</p>
										</a>
										<a href="project_app" class="nav-link <?php if ($this->uri->segment(1) == 'project_app') {
																					echo 'active';
																				}; ?>">
											<i class="far fa-circle nav-icon"></i>
											<p>สร้างโครงการใหม่</p>
										</a>

									</li>
								</ul>
							</li>
						<?php endif; ?>
						<?php if ($profile['Admin'] == '1') : ?>
							<li class="nav-header">ผู้ดูแลระบบ</li>

							<li class="nav-item has-treeview">
								<a href="authorization" class="nav-link <?php if ($this->uri->segment(1) == 'authorization') {
																			echo 'active';
																		}; ?>">
									<i class="nav-icon far fa-circle"></i>
									<p>
										กำหนดสิทธิผู้ใช้งาน
									</p>
								</a>
							</li>

							<li class="nav-item has-treeview">
								<a href="manager_authorization" class="nav-link <?php if ($this->uri->segment(1) == 'manager_authorization') {
																					echo 'active';
																				}; ?>">
									<i class="nav-icon far fa-circle"></i>
									<p>
										จัดการข้อมูลหน่วยงาน
									</p>
								</a>
							</li>
						<?php endif; ?>
					</ul>
				</nav>
				<!-- /.sidebar-menu -->
			</div>
			<!-- /.sidebar -->
		</aside>