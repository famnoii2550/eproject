<div class="row container" style="    padding: 25px 0 0 0px;">
    <div class="col-3"> </div>
    <label class="col-2 text-right" for="">เป้าประสงค์ :</label>
    <div class="col-4">
        <select class="form-control" name="goal">
            <option value="" selected disabled>กรุณาเลือกเป้าประสงค์</option>
            <?php foreach ($goalGetList as $goalGetDetail) { ?>
                <option value="<?php echo $goalGetDetail['Goal_id']; ?>"><?php echo $goalGetDetail['Goal_name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>

<!-- <div id="indicList"></div>  -->
<div id="tacticList"></div>
<script>
    $("select[name='goal']").change(function() {
        // getIndicList($(this));
        getTacticList($(this));
    });
    // function getIndicList(event){
    //     let indicGet = false;
    //     if (event) {
    //         indicGet = event.val();
    //     }
    //     $.ajax({
    //         url:"<?php echo base_url('Project_ctr/indic_project_app'); ?>",
    //         data:{indicGet:indicGet},
    //         success:function(response){
    //             event.parents('#goalList').find('#indicList').html(response);
    //         }
    //     });
    // }
    function getTacticList(event) {
        let tacticGet = false;
        if (event) {
            tacticGet = event.val();
        }
        $.ajax({
            url: "<?php echo base_url('Project_ctr/tactic_project_app'); ?>",
            data: {
                tacticGet: tacticGet
            },
            success: function(response) {
                // event.parents('#indicList').find('#tacticList').html(response);
                event.parents('#goalList').find('#tacticList').html(response);
            }
        });
    }
</script>