  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
          <div class="container-fluid">
              <div class="row mb-2">
                  <div class="col-sm-6">
                      <h1>จัดการข้อมูลโครงการ</h1>
                  </div>
                  <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                          <!-- <li class="breadcrumb-item"><a href="#">xxxxx</a></li>
              <li class="breadcrumb-item active">xxxx</li> -->
                      </ol>
                  </div>
              </div>
          </div><!-- /.container-fluid -->
      </section>
      <?php $account_detail = $this->db->get_where('account', ['Username' => $this->session->userdata('Username')])->row_array(); ?>
      <!-- Main content -->
      <section class="content">
          <!-- Default box -->
          <div class="card">
              <div class="card-header">
                  <!-- <button class="btn btn-info" style="float:right;" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus-circle"></i> สร้างประเด็นยุทธ์ศาสตร์ใหม่</button> -->
                  <div style="background-color: rgba(0,0,0,.05); padding:15px;">
                      <form action="project_manager" method="get">
                          <div class="row">
                              <div class="col-3">
                                  <span>เลือกปีงบประมาณ</span>
                                  <select name="year" class="form-control" style="width:35%; display: inline-block;">
                                      <option value="all_year" <?php if ($y_budget == "all_year") {
                                                                    echo "selected";
                                                                } ?>>ทุกปี</option>
                                      <?php

                                        $Date = date('Y-m-d');
                                        $Date10 = date('Y', strtotime($Date . ' + 10 years'));
                                        for ($x = date('Y'); $x <= $Date10; $x++) { ?>
                                          <option value="<?php echo $x; ?>" <?php if (isset($_GET['year']) && $_GET['year'] == $x) {
                                                                                echo 'selected';
                                                                            } else {
                                                                                if (empty($_GET['year']) && $y_budget == $x) {
                                                                                    echo 'selected';
                                                                                }
                                                                            } ?>> <?php echo $x + 543; ?></option>
                                      <?php } ?>

                                  </select>
                              </div>
                              <div class="col-4">
                                  <span>หน่วยงาน</span>
                                  <?php $department = $this->db->get('department')->result_array(); ?>
                                  <select name="Department_id" class="form-control" style="width:70%; display: inline-block;">
                                      <option value="">ทั้งหมด</option>
                                      <?php foreach ($department as $department) { ?>
                                          <option value="<?php echo $department['Department_id']; ?>" <?php if (isset($_GET['Department_id']) && $_GET['Department_id'] == $department['Department_id']) {
                                                                                                            echo 'selected';
                                                                                                        } ?>><?php echo $department['Department']; ?></option>
                                      <?php } ?>
                                  </select>
                              </div>
                              <div class="col-4">
                                  <span>สถานะ</span>
                                  <select name="Status" class="form-control" style="width:60%; display: inline-block;">
                                      <option value="">ทั้งหมด</option>
                                      <option value="2" <?php if (isset($_GET['Status']) && $_GET['Status'] == 2) {
                                                            echo 'selected';
                                                        } ?>>รอหัวหน้าฝ่ายพิจารณา</option>
                                      <option value="1" <?php if (isset($_GET['Status']) && $_GET['Status'] == 1) {
                                                            echo 'selected';
                                                        } ?>>รอเจ้าหน้าที่แผนตรวจสอบ</option>
                                      <option value="3" <?php if (isset($_GET['Status']) && $_GET['Status'] == 3) {
                                                            echo 'selected';
                                                        } ?>>รออนุมัติจากผู้บริหาร</option>
                                      <option value="11" <?php if (isset($_GET['Status']) && $_GET['Status'] == 11) {
                                                                echo 'selected';
                                                            } ?>>อนุมัติ</option>
                                      <option value="12" <?php if (isset($_GET['Status']) && $_GET['Status'] == 12) {
                                                                echo 'selected';
                                                            } ?>>รอเจ้าหน้าที่แผนตรวจสอบปิดโครงการ</option>
                                      <option value="10" <?php if (isset($_GET['Status']) && $_GET['Status'] == 10) {
                                                                echo 'selected';
                                                            } ?>>รอผู้บริหารอนุมัติปิดโครงการ</option>
                                      <option value="6" <?php if (isset($_GET['Status']) && $_GET['Status'] == 6) {
                                                            echo 'selected';
                                                        } ?>>ปิดโครงการ / เสร็จตามระยะเวลา</option>
                                      <option value="7" <?php if (isset($_GET['Status']) && $_GET['Status'] == 7) {
                                                            echo 'selected';
                                                        } ?>>ปิดโครงการ / ไม่เป็นไปตามระยะเวลา</option>
                                      <option value="8" <?php if (isset($_GET['Status']) && $_GET['Status'] == 8) {
                                                            echo 'selected';
                                                        } ?>>ปิดโครงการ / ขอเลือน</option>
                                      <option value="9" <?php if (isset($_GET['Status']) && $_GET['Status'] == 9) {
                                                            echo 'selected';
                                                        } ?>>ปิดโครงการ / ขอยกเลิก</option>
                                      <option value="5" <?php if (isset($_GET['Status']) && $_GET['Status'] == 5) {
                                                            echo 'selected';
                                                        } ?>>แก้ไขโครงการ</option>
                                      <option value="4" <?php if (isset($_GET['Status']) && $_GET['Status'] == 4) {
                                                            echo 'selected';
                                                        } ?>>แก้ไขเอกสารประเมินโครงการ</option>
                                  </select>
                                  <button type="submit" class="btn btn-primary" style="margin-bottom: 3px;"><i class="fa fa-search"></i> ค้นหา</button>
                              </div>
                          </div>
                      </form>
                  </div>
                  <div class="card-tools">

                  </div>
              </div>
              <div class="card-body">
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                          <tr>
                              <th style="border-bottom:none;"></th>
                              <th style="border-bottom:none;"></th>
                              <th style="border-bottom:none;"></th>
                              <th class="text-center" colspan="5">ดูรายงานความก้าวหน้า</th>
                          </tr>
                          <tr>
                              <th class="text-center" style="border-top:none;">ชื่อโครงการ</th>
                              <th class="text-center" style="border-top:none;">สถานะ</th>
                              <th class="text-center" style="border-top:none;">แก้ไข</th>
                              <th class="text-center" style="width: 100px;">ไตรมาส 1</th>
                              <th class="text-center" style="width: 100px;">ไตรมาส 2</th>
                              <th class="text-center" style="width: 100px;">ไตรมาส 3</th>
                              <th class="text-center" style="width: 100px;">ไตรมาส 4</th>
                              <th class="text-center" style="width: 100px;">วันที่สร้าง</th>
                          </tr>
                      </thead>
                      <tbody>

                          <?php foreach ($projects as $project) { ?>
                              <?php $estimate = $this->db->get_where('estimate', ['Project_id' => $project['Project_id']])->row_array(); ?>
                              <?php $cm = $this->db->order_by('Comment_id', 'desc')->get_where('comment', ['Project_id' => $project['Project_id']])->row_array(); ?>
                              <tr>
                                  <td>
                                      <a href="project_list_manager?PID=<?php echo base64_encode($project['Project_id']); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>"><?php echo $project['Project_name']; ?></a>
                                      <?php if ($project['Status'] == 5) { ?>
                                          <span><a href="#" data-toggle="modal" data-target="#exampleModal<?= $project['Project_id']; ?>"><i class="fas fa-comment-alt"></i></a></span>
                                          <!-- Modal -->
                                          <div class="modal fade" id="exampleModal<?= $project['Project_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                  <div class="modal-content">
                                                      <div class="modal-header">
                                                          <h5 class="modal-title" id="exampleModalLabel">ข้อมูล</h5>
                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                          </button>
                                                      </div>

                                                      <div class="modal-body">
                                                          <div>
                                                              <textarea name="Comment" rows="5" style="width: 100%" class="form-control" required disabled><?php echo $cm['Comment']; ?></textarea>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      <?php } ?>
                                  </td>
                                  <?php if ($project['Status'] == 1) { ?>
                                      <td class="text-center"><span class="badge badge-secondary" style="font-size: 13px;">รอหัวหน้าฝ่ายอนุมัติ</span></td>
                                  <?php } elseif ($project['Status'] == 2) { ?>
                                      <td class="text-center"><span class="badge badge-secondary" style="font-size: 13px;">รอเจ้าหน้าที่แผนตรวจสอบ</span></td>
                                  <?php } elseif ($project['Status'] == 3) { ?>
                                      <td class="text-center"><span class="badge badge-secondary" style="font-size: 13px;">รอผู้บริหารอนุมัติ</span></td>
                                  <?php } elseif ($project['Status'] == 4) { ?>
                                      <td class="text-center"><span class="badge badge-danger" style="font-size: 13px;">ปิดโครงการ</span></td>
                                  <?php } elseif ($project['Status'] == 11 && $estimate['Flag_close'] == null) { ?>
                                      <td class="text-center"><span class="badge badge-primary" style="font-size: 13px;">อนุมัติ</span></td>
                                  <?php } elseif ($project['Status'] == 11 && $estimate['Flag_close'] == 0) { ?>
                                      <td class="text-center"><span class="badge badge-danger" style="font-size: 13px;">รอเจ้าหน้าที่แผนตรวจสอบขอปิดโครงการ</span></td>
                                  <?php } elseif ($project['Status'] == 11 && $estimate['Flag_close'] == 1) { ?>
                                      <td class="text-center"><span class="badge badge-danger" style="font-size: 13px;">รอผู้บริหารตรวจสอบขอปิดโครงการ</span></td>
                                  <?php } elseif ($project['Status'] == 4 && $estimate['Flag_close'] == 2) { ?>
                                      <td class="text-center"><span class="badge badge-danger" style="font-size: 13px;">รอผู้บริหารตรวจสอบขอปิดโครงการ</span></td>
                                  <?php } elseif ($project['Status'] == 5) { ?>
                                      <td class="text-center"><span class="badge badge-warning" style="font-size: 13px;">แก้ไขโครงการ</span></td>
                                  <?php } ?>
                                  <td class="text-center">
                                      <a href="project_edit_manager?PID=<?= base64_encode($project['Project_id']); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-warning btn-xs text-white"><i class="fa fa-edit" aria-hidden="true"></i> แก้ไข</a>
                                  </td>
                                  <?php $Quar = $this->db->get_where('project_report', ['Project_id' => $project['Project_id'], 'Quarter' => 1])->row_array(); ?>
                                  <?php $Quar2 = $this->db->get_where('project_report', ['Project_id' => $project['Project_id'], 'Quarter' => 2])->row_array(); ?>
                                  <?php $Quar3 = $this->db->get_where('project_report', ['Project_id' => $project['Project_id'], 'Quarter' => 3])->row_array(); ?>
                                  <?php $Quar4 = $this->db->get_where('project_report', ['Project_id' => $project['Project_id'], 'Quarter' => 4])->row_array(); ?>
                                  <?php if ($project['Status'] == 1 || $project['Status'] == 2 || $project['Status'] == 3) { ?>
                                      <td style="text-align:center;">
                                          <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                      </td>
                                      <td style="text-align:center;">
                                          <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                      </td>
                                      <td style="text-align:center;">
                                          <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                      </td>
                                      <td style="text-align:center;">
                                          <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                      </td>
                                  <?php } elseif ($project['Status'] == 5 && $estimate['Flag_close'] == null) { ?>
                                      <td style="text-align:center;">
                                          <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                      </td>
                                      <td style="text-align:center;">
                                          <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                      </td>
                                      <td style="text-align:center;">
                                          <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                      </td>
                                      <td style="text-align:center;">
                                          <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                      </td>
                                  <?php } elseif ($project['Status'] == 11 && $estimate['Flag_close'] == null) { ?>
                                      <td style="text-align:center;">
                                          <?php if (!empty($Quar)) { ?>
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                                          <?php } else { ?>
                                              <button class="btn btn-default btn-xs not-allowed" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          <?php } ?>
                                      </td>

                                      <td style="text-align:center;">
                                          <?php if (!empty($Quar2)) { ?>
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs "><i class="fas fa-search"></i> ดูรายงาน</a>
                                          <?php } else { ?>
                                              <button class="btn btn-default btn-xs not-allowed" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          <?php } ?>
                                      </td>

                                      <td style="text-align:center;">
                                          <?php if (!empty($Quar3)) { ?>
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(3); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs "><i class="fas fa-search"></i> ดูรายงาน</a>
                                          <?php } else { ?>
                                              <button class="btn btn-default btn-xs not-allowed" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          <?php } ?>
                                      </td>

                                      <td style="text-align:center;">
                                          <?php if (!empty($Quar4)) { ?>
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(4); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs "><i class="fas fa-search"></i> ดูรายงาน</a>
                                          <?php } else { ?>
                                              <button class="btn btn-default btn-xs not-allowed" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          <?php } ?>
                                      </td>
                                  <?php } elseif ($project['Status'] == 11 && ($estimate['Flag_close'] == 0 || $estimate['Flag_close'] == 1 || $estimate['Flag_close'] == 2)) { ?>
                                      <?php if ($estimate['Quarter'] == 1) { ?>
                                          <td style="text-align:center;">
                                              <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ขอปิดโครงการ</a>
                                          </td>
                                          <td style="text-align:center;">
                                              <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          </td>
                                          <td style="text-align:center;">
                                              <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          </td>
                                          <td style="text-align:center;">
                                              <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          </td>
                                      <?php } elseif ($estimate['Quarter'] == 2) { ?>
                                          <td style="text-align:center;">
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                                          </td>
                                          <td style="text-align:center;">
                                              <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ขอปิดโครงการ</a>
                                          </td>
                                          <td style="text-align:center;">
                                              <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          </td>
                                          <td style="text-align:center;">
                                              <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          </td>
                                      <?php } elseif ($estimate['Quarter'] == 3) { ?>
                                          <td style="text-align:center;">
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                                          </td>
                                          <td style="text-align:center;">
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                                          </td>
                                          <td style="text-align:center;">
                                              <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(3); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ขอปิดโครงการ</a>
                                          </td>
                                          <td style="text-align:center;">
                                              <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          </td>
                                      <?php } elseif ($estimate['Quarter'] == 4) { ?>
                                          <td style="text-align:center;">
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                                          </td>
                                          <td style="text-align:center;">
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                                          </td>
                                          <td style="text-align:center;">
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(3); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs "><i class="fas fa-search"></i> ดูรายงาน</a>
                                          </td>
                                          <td style="text-align:center;">
                                              <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(4); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ขอปิดโครงการ</a>
                                          </td>
                                      <?php } ?>

                                  <?php } elseif ($project['Status'] == 4) { ?>
                                      <?php if ($estimate['Quarter'] == 1) { ?>
                                          <td style="text-align:center;">
                                              <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ปิดโครงการ</a>
                                          </td>
                                          <td style="text-align:center;">
                                              <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          </td>
                                          <td style="text-align:center;">
                                              <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          </td>
                                          <td style="text-align:center;">
                                              <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          </td>
                                      <?php } elseif ($estimate['Quarter'] == 2) { ?>
                                          <td style="text-align:center;">
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                                              <!-- <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button> -->
                                          </td>
                                          <td style="text-align:center;">
                                              <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ปิดโครงการ</a>
                                          </td>
                                          <td style="text-align:center;">
                                              <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          </td>
                                          <td style="text-align:center;">
                                              <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          </td>
                                      <?php } elseif ($estimate['Quarter'] == 3) { ?>
                                          <td style="text-align:center;">
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                                              <!-- <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button> -->
                                          </td>
                                          <td style="text-align:center;">
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                                              <!-- <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button> -->
                                          </td>
                                          <td style="text-align:center;">
                                              <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(3); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ปิดโครงการ</a>
                                          </td>
                                          <td style="text-align:center;">
                                              <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button>
                                          </td>
                                      <?php } elseif ($estimate['Quarter'] == 4) { ?>
                                          <td style="text-align:center;">
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(1); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                                              <!-- <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button> -->
                                          </td>
                                          <td style="text-align:center;">
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(2); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                                              <!-- <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button> -->
                                          </td>
                                          <td style="text-align:center;">
                                              <a href="project_report_quar_view?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(3); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-default btn-xs"><i class="fas fa-search"></i> ดูรายงาน</a>
                                              <!-- <button class="btn btn-default btn-xs" disabled=""><i class="fas fa-search"> ดูรายงาน</i></button> -->
                                          </td>
                                          <td style="text-align:center;">
                                              <a href="estimate_review?PID=<?= base64_encode($project['Project_id']); ?>&quarter=<?= base64_encode(4); ?>&uri=<?= base64_encode($this->uri->segment(1)); ?>" class="btn btn-danger btn-xs "><i class="fas fa-search"></i> ปิดโครงการ</a>
                                          </td>
                                      <?php } ?>
                                  <?php } ?>
                                  <td><?= DateThai2($project['Time']); ?></td>
                              </tr>
                          <?php } ?>
                      </tbody>
                  </table>
              </div>
              <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </section>
      <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title">ข้อมูลปีงบประมาณ</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                  </button>
              </div>
              <div class="modal-body text-center">
                  <form action="budget_edit_com" method="post">
                      <!-- <div class="row my-2 mx-2 mb-4"> -->
                      <div class="form-group">
                          <input type="hidden" name="Strategic_Plan_id" value="<?php echo $budget['Strategic_Plan_id']; ?>">
                          <label for="recipient-name" class="col-form-label">ปีงบประมาณ: <span><?php echo $budget['Fiscalyear']; ?></span></label>
                      </div>
                      <div class="form-group">
                          <label for="recipient-name" class="col-form-label">ชื่อแผนงาน</label>
                          <input type="text" class="form-control" name="Strategic_Plan" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Strategic_Plan']; ?>">
                      </div>
                      <div class="form-group">
                          <label for="recipient-name" class="col-form-label">ผ่านมติกรรมการบริหาย</label>
                          <div>
                              <table class="table table-striped table-bordered">
                                  <thead>
                                      <th>ครั้งที่</th>
                                      <th>วันที่</th>
                                  </thead>
                                  <tbody>
                                      <td><input type="text" class="form-control" name="Director_of_time" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Director_of_time']; ?>"></td>
                                      <td><input type="date" class="form-control" name="Director_of_date" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Director_of_date']; ?>"></td>
                                  </tbody>
                              </table>
                          </div>

                      </div>
                      <div class="form-group">
                          <label for="recipient-name" class="col-form-label">ผ่านมติกรรมการประจำ</label>
                          <div>
                              <table class="table table-striped table-bordered text-center">
                                  <thead>
                                      <th>ครั้งที่</th>
                                      <th>วันที่</th>
                                  </thead>
                                  <tbody>
                                      <td><input type="text" class="form-control" name="Ref_of_time" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Ref_of_time']; ?>"></td>
                                      <td><input type="date" class="form-control" name="Ref_of_date" style="display: inline-block;width: 50%;" id="" value="<?php echo $budget['Ref_of_date']; ?>"></td>
                                  </tbody>
                              </table>
                          </div>

                      </div>
                      <!-- </div> -->

              </div>
              <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                  <button type="submit" class="btn btn-warning">บันทึก</button>
              </div>
          </div>
          </form>
          <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
  </div>
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">แผนยุทธศาสตร์</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="workplan_add" method="POST">
                  <div class="modal-body">
                      <div class="form-group">
                          <label for="">ชื่อแผนยุทธ์ศาสตร์</label>
                          <input type="text" class="form-control" name="strategic_plan">
                      </div>
                      <div id="straBT">
                          <div class="form-group">
                              <label for="">ประเด็นยุทธ์ศาสตร์</label>
                              <input type="text" class="form-control" name="strategic_name">
                          </div>
                          <div class="form-group">
                              <label for="">เป้าประสงค์ที่</label>
                              <input type="text" class="form-control" name="goal_name">
                          </div>
                          <div id="meas">
                              <div class="form-group">
                                  <label for="">ตัวชี้วัด</label>
                                  <input type="text" class="form-control" name="#">
                              </div>
                          </div>
                          <div class="form-group" style="text-align:center;">
                              <button type="button" class="btn btn-info" id="btn1" style="color:#fff;"><i class="fa fa-plus"></i> เพิ่มตัวชี้วัด</button>
                          </div>

                          <div class="row">
                              <div class="col-6">
                                  <div class="form-group">
                                      <label for="">หน่วยนับ</label>
                                      <input type="text" class="form-control" name="#">
                                  </div>
                              </div>
                              <div class="col-6">
                                  <div class="form-group">
                                      <label for="">ค่าเป้าหมาย</label>
                                      <input type="text" class="form-control" name="#">
                                  </div>
                              </div>
                          </div>
                          <div id="goal">
                              <div class="form-group">
                                  <label for="">กลยุทธ์</label>
                                  <input type="text" class="form-control" name="tactic_name">
                              </div>
                          </div>
                          <div class="form-group" style="text-align:center;">
                              <button type="button" class="btn btn-success" style="color:#fff;" id="btn7"><i class="fa fa-plus"></i> เพิ่มกลยุทธ์</button>
                          </div>
                      </div>
                      <hr>
                      <div class="form-group" style="text-align:center;">
                          <button type="button" class="btn btn-info" style="color:#fff;" id="stra1"><i class="fa fa-plus"></i> เพิ่มเป้าประสงค์</button>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times-circle"></i> ยกเลิก</button>
                      <button type="submit" class="btn btn-primary"><i class="fa fa-download"></i> จัดเก็บ</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
  <script>
      $(document).ready(function() {
          $('#example').DataTable({
              "order": [
                  [2, "desc"]
              ]
          });
      });
  </script>