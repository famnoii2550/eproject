-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 09, 2020 at 10:42 AM
-- Server version: 5.5.55
-- PHP Version: 7.3.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eproicit2_eproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `Account_id` int(11) NOT NULL,
  `Department_id` varchar(255) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Fname` varchar(128) DEFAULT NULL,
  `Lname` varchar(128) DEFAULT NULL,
  `Tel` decimal(10,0) DEFAULT NULL,
  `Email` varchar(128) DEFAULT NULL,
  `Director` char(1) NOT NULL,
  `Manager` char(1) NOT NULL,
  `Supervisor` char(1) NOT NULL,
  `Supplies` char(1) NOT NULL,
  `Responsible` char(1) NOT NULL,
  `Admin` char(1) NOT NULL,
  `Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`Account_id`, `Department_id`, `Username`, `Password`, `Fname`, `Lname`, `Tel`, `Email`, `Director`, `Manager`, `Supervisor`, `Supplies`, `Responsible`, `Admin`, `Time`) VALUES
(1, '1', 'test', 'e10adc3949ba59abbe56e057f20f883e', 'สุรศักดิ์', 'ดีงาม', '659840125', 'srp@example.com', '0', '1', '1', '0', '1', '0', '2020-02-28 08:00:04'),
(10, '11', 'test2', 'e10adc3949ba59abbe56e057f20f883e', 'sad', 'sad', NULL, 'adminsadsa@example.com', '0', '1', '1', '0', '1', '0', '2020-03-06 03:26:49'),
(12, '2', 'S5804062630099', NULL, 'ชนากานต์', 'แก้วหาญ', NULL, 'chanakan_home@hotmail.com', '1', '1', '1', '0', '1', '1', '2020-05-05 14:23:54');

-- --------------------------------------------------------

--
-- Table structure for table `benefit`
--

CREATE TABLE `benefit` (
  `Benefit_id` int(11) NOT NULL,
  `Project_id` int(11) NOT NULL,
  `Benefit_name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `benefit`
--

INSERT INTO `benefit` (`Benefit_id`, `Project_id`, `Benefit_name`) VALUES
(1, 1, '1. แก้ไขปัญหา IPv4 ที่ไม่เพียงพอ'),
(2, 1, '2. เพิ่มการใช้งาน IPv6'),
(3, 1, '3. บุคคลากรมีความรู้ความเข้าใจเกี่ยวกับ IPv6');

-- --------------------------------------------------------

--
-- Table structure for table `charges_main`
--

CREATE TABLE `charges_main` (
  `Charges_Main_id` int(11) NOT NULL,
  `Charges_Main` varchar(128) NOT NULL,
  `Project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `charges_main`
--

INSERT INTO `charges_main` (`Charges_Main_id`, `Charges_Main`, `Project_id`) VALUES
(1, '1. งบลงทุน', 1);

-- --------------------------------------------------------

--
-- Table structure for table `charges_sub`
--

CREATE TABLE `charges_sub` (
  `Charges_Sub_id` int(11) NOT NULL,
  `Charges_Sub` decimal(10,2) NOT NULL,
  `Quarter_one` varchar(128) NOT NULL,
  `Quarter_two` varchar(128) NOT NULL,
  `Quarter_three` varchar(128) NOT NULL,
  `Quarter_four` varchar(128) NOT NULL,
  `Charges_Main_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `charges_sub`
--

INSERT INTO `charges_sub` (`Charges_Sub_id`, `Charges_Sub`, `Quarter_one`, `Quarter_two`, `Quarter_three`, `Quarter_four`, `Charges_Main_id`) VALUES
(1, '1.10', '100,000.00', '-', '-', '100,000.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `Comment_id` int(11) NOT NULL,
  `Project_id` int(11) NOT NULL,
  `Account_id` int(11) NOT NULL,
  `Comment` varchar(255) NOT NULL,
  `Time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `Department_id` int(11) NOT NULL,
  `Department` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`Department_id`, `Department`) VALUES
(1, 'สำนักงานผู้อำนวยการ'),
(2, 'ฝ่ายพัฒนาระบบสารสนเทศ'),
(3, 'ฝ่ายวิศวกรรมระบบเครือข่าย'),
(4, 'ฝ่ายบริการวิชาการและส่งเสริมการวิจัย'),
(5, 'ฝ่ายเทคโนโลยีสารสนเทศ วิทยาเขตปราจีนบุรี'),
(6, 'ฝ่ายเทคโนโลยีสารสนเทศ วิทยาเขตระยอง'),
(11, 'ฝ่ายบริหารธุรกิจ วิทยาเขตลำปาง');

-- --------------------------------------------------------

--
-- Table structure for table `detail`
--

CREATE TABLE `detail` (
  `Detail_id` int(11) NOT NULL,
  `Report_id` int(11) NOT NULL,
  `Detail` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `estimate`
--

CREATE TABLE `estimate` (
  `Estimate_id` int(11) NOT NULL,
  `Project_id` int(11) NOT NULL,
  `Explanation` blob NOT NULL,
  `Result` varchar(128) NOT NULL,
  `Motive` blob NOT NULL,
  `Conducting` blob NOT NULL,
  `Real_used` varchar(128) NOT NULL,
  `Benefits` blob NOT NULL,
  `Problem` blob NOT NULL,
  `Improvement` blob NOT NULL,
  `Flag_close` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `File_id` int(11) NOT NULL,
  `Project_id` int(11) NOT NULL,
  `Full_path` varchar(128) NOT NULL,
  `File_name` varchar(128) NOT NULL,
  `Check_type_tor` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`File_id`, `Project_id`, `Full_path`, `File_name`, `Check_type_tor`) VALUES
(1, 1, 'uploads/tor/1600420965d.pdf', '1600420965d.pdf', '1');

-- --------------------------------------------------------

--
-- Table structure for table `goal`
--

CREATE TABLE `goal` (
  `Goal_id` int(11) NOT NULL,
  `Strategic_id` varchar(128) DEFAULT NULL,
  `Goal_name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `goal`
--

INSERT INTO `goal` (`Goal_id`, `Strategic_id`, `Goal_name`) VALUES
(1, '1', 'มีเทคโนโลยีสารสนเทศที่สนับสนุนการบริการวิชาการ'),
(2, '2', ' ระบบเครือข่ายมีความเสถียร มั่นคงและปลอดภัยในการใช้งานตามมาตรฐานสากล'),
(3, '4', 'ระบบเครือข่ายมีความเสถียร มั่นคงและปลอดภัยในการใช้งานตามมาตรฐานสากล');

-- --------------------------------------------------------

--
-- Table structure for table `indic_metric`
--

CREATE TABLE `indic_metric` (
  `Indic_metric_id` int(11) NOT NULL,
  `Project_id` int(11) NOT NULL,
  `Indic_metric` varchar(255) NOT NULL,
  `Unit` varchar(255) NOT NULL,
  `Cost` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `indic_project`
--

CREATE TABLE `indic_project` (
  `Indic_project_id` int(11) NOT NULL,
  `Project_id` int(11) DEFAULT NULL,
  `Indic_project` varchar(128) NOT NULL,
  `Unit` varchar(128) NOT NULL,
  `Cost` varchar(128) NOT NULL,
  `Goal_id` int(11) NOT NULL,
  `Tactic_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `indic_project`
--

INSERT INTO `indic_project` (`Indic_project_id`, `Project_id`, `Indic_project`, `Unit`, `Cost`, `Goal_id`, `Tactic_name`) VALUES
(1, NULL, 'บุคลากรได้รับการฝึกอบรมการพัฒนาโมบายแอปพลิเคชัน', 'คน', 'ไม่น้อยกว่า 5', 1, 'พัฒนา Application ด้านงานบริการวิชาการให้สะดวกมากยิ่งขึ้น'),
(2, NULL, 'ค่าเฉลี่ยความพึงพอใจต่อการใช้บริการ', 'คะแนนเฉลี่ย', 'ไม่น้อยกว่า 3.51', 1, 'พัฒนา Application ด้านงานบริการวิชาการให้สะดวกมากยิ่งขึ้น'),
(3, NULL, 'เซิร์ฟเวอร์และอุปกรณ์เครือข่ายที่ใช้งาน IPv6 ไม่น้อยกว่า', 'อุปกรณ์', '10', 2, 'ปรับปรุงระบบเครือข่ายให้มีความเสถียร มั่นคง และปลอดภัยตามมาตรฐานสากล'),
(4, NULL, 'ปรับปรุงการใช้งานอินเทอร์เน็ตด้วย IPv6 ไม่น้อยกว่า', 'เครื่อง', '100', 2, 'ปรับปรุงระบบเครือข่ายให้มีความเสถียร มั่นคง และปลอดภัยตามมาตรฐานสากล'),
(5, NULL, 'อบรมให้ความรู้เกี่ยวกับการใช้งาน IPv6 แก่บุคคลากรไม่น้อยกว่า', 'คน', '15', 2, 'ปรับปรุงระบบเครือข่ายให้มีความเสถียร มั่นคง และปลอดภัยตามมาตรฐานสากล'),
(6, NULL, 'กทม : มีความเร็วในการทำงาน firewall Throughput ไม่น้อยกว่า', 'Gbps', '100', 3, 'ปรับปรุงระบบเครือข่ายให้มีความเสถียร มั่นคง และปลอดภัยตามมาตรฐานสากล'),
(7, NULL, 'กทม : สามารถตรวจสอบและป้องกันการโจมตีเครือข่ายประเภท IPS (Throughput) ได้ไม่น้อยกว่า', 'Gbps', '16.5', 3, 'ปรับปรุงระบบเครือข่ายให้มีความเสถียร มั่นคง และปลอดภัยตามมาตรฐานสากล');

-- --------------------------------------------------------

--
-- Table structure for table `indic_project_report`
--

CREATE TABLE `indic_project_report` (
  `Indic_project_report_id` int(11) NOT NULL,
  `Indic_project_id` int(11) NOT NULL,
  `Result` varchar(128) NOT NULL,
  `Achieve` varchar(128) NOT NULL,
  `Report_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `objective`
--

CREATE TABLE `objective` (
  `Objective_id` int(11) NOT NULL,
  `Project_id` varchar(8) NOT NULL,
  `Objective_name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `objective`
--

INSERT INTO `objective` (`Objective_id`, `Project_id`, `Objective_name`) VALUES
(1, '1', '1. เพื่อนำ IPv6 ไปใช้งานกับเซิร์ฟเวอร์'),
(2, '1', '2. เพื่อปรับปรุงการใช้งานอินเทอร์เน็ตในห้องเรียนให้เป็น IPv6'),
(3, '1', '3. เพื่ออบรมให้ความรู้เกี่ยวกับการใช้งาน IPv6');

-- --------------------------------------------------------

--
-- Table structure for table `problem`
--

CREATE TABLE `problem` (
  `Problem_id` int(11) NOT NULL,
  `Report_id` int(11) NOT NULL,
  `Problem` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `Project_id` int(11) NOT NULL,
  `Project_name` varchar(200) DEFAULT NULL,
  `Type` varchar(128) DEFAULT NULL,
  `Integra_name` varchar(128) DEFAULT NULL,
  `Integra_detail` text,
  `Rationale` text,
  `Target_group` varchar(128) DEFAULT NULL,
  `Source` varchar(128) DEFAULT NULL,
  `Butget` decimal(10,0) DEFAULT NULL,
  `Butget_char` varchar(128) DEFAULT NULL,
  `Tor` char(1) DEFAULT NULL,
  `Time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Workplan_id` int(11) DEFAULT NULL,
  `Account_id` int(11) DEFAULT NULL,
  `Year` varchar(100) DEFAULT NULL,
  `Status` int(100) DEFAULT '1',
  `Department_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`Project_id`, `Project_name`, `Type`, `Integra_name`, `Integra_detail`, `Rationale`, `Target_group`, `Source`, `Butget`, `Butget_char`, `Tor`, `Time`, `Workplan_id`, `Account_id`, `Year`, `Status`, `Department_id`) VALUES
(1, 'โครงการขยายการให้บริการ IPv6', 'งานพัฒนา', 'บูรณาการกับการเรียนการสอน', '-', 'เนื่องจากการเติบโตของเครือข่ายอินเตอร์เน็ตเป็นไปอย่างรวดเร็ว บริการหลากหลายประเภทและ เทคโนโลยีใหม่ ๆ ถูกนำเข้ามาใช้กับระบบเครือข่ายในมหาวิทยาลัยและไอพีเวอร์ชั่น 4 ไม่เพียงพอต่อ การใช้งาน ปัจจุบันทางสำนักคอมพิวเตอร์ฯ ได้ดำเนินการติดตั้งระบบ IPv6 ภายในมหาวิทยาลัยแล้ว แต่ทั้งนี้ก็ยังมีการใช้งานในวงจำกัด เนื่องจากอุปกรณ์บางอย่างยังไม่รองรับ รวมถึงความรู้ความเข้าใจในเรื่อง IPv6 ก็เป็นส่วนสำคัญต่อการนำ IPv6 ไปใช้งาน\n\n\nดังนั้น เพื่อขยายการใช้งาน IPv6 และแก้ไขปัญหา IPv4 ที่ไม่เพียงพอต่อการใช้งาน จึงดำเนินการปรับการใช้งานไอพีแอดเดรสของคอมพิวเตอร์เซิร์ฟเวอร์ให้ใช้เ IPv6 และใช้งาน IPv6 ในห้องเรียน รวมถึงจัดอบรมเพื่อให้ความรู้แกบุคคลากรใหนมหาลัย', 'ภายในมหาวิทยาลัย', 'งบประมาณเงินรายได้ของส่วนงาน', '100000', 'หนึ่งแสนบาทถ้วน', '0', '2020-09-10 09:29:35', 1, 1, '2021', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `project_indic_success`
--

CREATE TABLE `project_indic_success` (
  `project_indic_success_id` int(11) NOT NULL,
  `Project_id` int(11) NOT NULL,
  `Indic_success` varchar(100) NOT NULL,
  `Unit` varchar(100) NOT NULL,
  `Cost` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_indic_success`
--

INSERT INTO `project_indic_success` (`project_indic_success_id`, `Project_id`, `Indic_success`, `Unit`, `Cost`) VALUES
(1, 1, 'เซิร์ฟเวอร์และอุปกรณ์เครือข่ายที่ใช้งาน IPv6 ไม่น้อยกว่า', 'อุปกรณ์', '10'),
(2, 1, 'อบรมให้ความรู้เกี่ยวกับการใช้งาน IPv6 แก่บุคคลากรไม่น้อยกว่า', 'คน', '15'),
(3, 1, 'ปรับปรุงการใช้งานอินเทอร์เน็ตด้วย IPv6 ไม่น้อยกว่า', 'เครื่อง', '100');

-- --------------------------------------------------------

--
-- Table structure for table `project_report`
--

CREATE TABLE `project_report` (
  `Report_id` int(11) NOT NULL,
  `Used` decimal(10,2) NOT NULL,
  `Quarter` varchar(128) NOT NULL,
  `Period_check` char(1) NOT NULL,
  `Project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project_strategic_plane`
--

CREATE TABLE `project_strategic_plane` (
  `Project_strategic_plane_id` int(11) NOT NULL,
  `Project_id` int(11) NOT NULL,
  `Strategic_Plan_id` int(11) NOT NULL,
  `Strategic_id` int(11) NOT NULL,
  `Goal_id` int(11) NOT NULL,
  `Indic_project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_strategic_plane`
--

INSERT INTO `project_strategic_plane` (`Project_strategic_plane_id`, `Project_id`, `Strategic_Plan_id`, `Strategic_id`, `Goal_id`, `Indic_project_id`) VALUES
(1, 1, 1, 1, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `strategic`
--

CREATE TABLE `strategic` (
  `Strategic_id` int(11) NOT NULL,
  `Strategic_Plan_id` varchar(128) NOT NULL,
  `Strategic_name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `strategic`
--

INSERT INTO `strategic` (`Strategic_id`, `Strategic_Plan_id`, `Strategic_name`) VALUES
(1, '1', 'พัฒนางานบริการและส่งเสริมการวิจัย'),
(2, '1', 'พัฒนาระบบเครือข่าย'),
(3, '1', 'บริหารจัดการเชิงรุกอย่างมีประสิทธิภาพ'),
(4, '1', 'พัฒนาระบบเครือข่าย Network'),
(5, '2', 'พัฒนาระบบเครือข่าย IOT');

-- --------------------------------------------------------

--
-- Table structure for table `strategics_project`
--

CREATE TABLE `strategics_project` (
  `Strategic_Project_id` int(11) NOT NULL,
  `Tactic_id` varchar(128) NOT NULL,
  `Project_id` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `strategic_plane`
--

CREATE TABLE `strategic_plane` (
  `Strategic_Plan_id` int(11) NOT NULL,
  `Strategic_Plan` varchar(255) NOT NULL,
  `Fiscalyear` varchar(4) NOT NULL,
  `Director_of_time` varchar(255) NOT NULL,
  `Director_of_date` date NOT NULL,
  `Ref_of_time` varchar(255) NOT NULL,
  `Ref_of_date` date NOT NULL,
  `Status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `strategic_plane`
--

INSERT INTO `strategic_plane` (`Strategic_Plan_id`, `Strategic_Plan`, `Fiscalyear`, `Director_of_time`, `Director_of_date`, `Ref_of_time`, `Ref_of_date`, `Status`) VALUES
(1, 'แผนยุทธศาสตร์การพัฒนาสำนักคอมพิวเตอร์และเทคโนโลยีสารสนเทศ ระยะกลางแผน พ.ศ. 2563-2564', '', '1', '2020-09-10', '1', '2020-09-11', '1'),
(2, 'แผนยุทธ์ศาสตร์การพัฒนานักศึกษา', '', '', '0000-00-00', '', '0000-00-00', '1'),
(4, 'แผนยุทธ์ศาสตร์การพัฒนาสำนักงานการบัญชีและงบดุลการเงิน', '', '', '0000-00-00', '', '0000-00-00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tactic`
--

CREATE TABLE `tactic` (
  `Tactic_id` int(11) NOT NULL,
  `Goal_id` varchar(128) NOT NULL,
  `Indic_project_id` int(11) NOT NULL,
  `Tactic_name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_workplan`
--

CREATE TABLE `tbl_workplan` (
  `Workplan_id` int(11) NOT NULL,
  `Workplan_name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_workplan`
--

INSERT INTO `tbl_workplan` (`Workplan_id`, `Workplan_name`) VALUES
(1, 'แผนงานบริหารการศึกษา'),
(2, 'แผนงานจัดการศึกษาระดับอุดมศึกษา'),
(3, 'แผนงานวิจัย'),
(4, 'แผนงานบริการวิชาการ'),
(5, 'แผนงานทำนุบำรุงศิลปวัฒนธรรม');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `User_id` int(11) NOT NULL,
  `Account_id` varchar(255) NOT NULL,
  `Project_id` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`User_id`, `Account_id`, `Project_id`) VALUES
(1, '12', '1');

-- --------------------------------------------------------

--
-- Table structure for table `work_step`
--

CREATE TABLE `work_step` (
  `Step_id` int(11) NOT NULL,
  `Step_name` varchar(128) DEFAULT NULL,
  `Start` date NOT NULL,
  `Stop` date NOT NULL,
  `Project_id` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `work_step`
--

INSERT INTO `work_step` (`Step_id`, `Step_name`, `Start`, `Stop`, `Project_id`) VALUES
(1, '1. สำรวจและรวบรวมข้อมูลระบบเดิมที่ใช้งานอยู่', '2020-10-10', '2020-12-31', '1'),
(2, '2. ออกแบบ จัดทำข้อกำหนดคุณลักษณะเฉพาะ', '2021-01-01', '2021-02-01', '1'),
(3, '3. จัดซื้อ จัดจ้าง', '2021-03-01', '2021-04-30', '1'),
(4, '4. ติดตั้ง ทดสอบ และอบรม', '2021-05-01', '2021-07-31', '1'),
(5, '5. ประเมินโครงการ', '2020-08-01', '2021-09-30', '1');

-- --------------------------------------------------------

--
-- Table structure for table `work_step_project_report`
--

CREATE TABLE `work_step_project_report` (
  `Step_id` int(11) NOT NULL,
  `Step_name` varchar(128) NOT NULL,
  `Start_` date NOT NULL,
  `Stop_` date NOT NULL,
  `Report_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`Account_id`);

--
-- Indexes for table `benefit`
--
ALTER TABLE `benefit`
  ADD PRIMARY KEY (`Benefit_id`);

--
-- Indexes for table `charges_main`
--
ALTER TABLE `charges_main`
  ADD PRIMARY KEY (`Charges_Main_id`);

--
-- Indexes for table `charges_sub`
--
ALTER TABLE `charges_sub`
  ADD PRIMARY KEY (`Charges_Sub_id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`Comment_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`Department_id`);

--
-- Indexes for table `detail`
--
ALTER TABLE `detail`
  ADD PRIMARY KEY (`Detail_id`);

--
-- Indexes for table `estimate`
--
ALTER TABLE `estimate`
  ADD PRIMARY KEY (`Estimate_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`File_id`);

--
-- Indexes for table `goal`
--
ALTER TABLE `goal`
  ADD PRIMARY KEY (`Goal_id`);

--
-- Indexes for table `indic_metric`
--
ALTER TABLE `indic_metric`
  ADD PRIMARY KEY (`Indic_metric_id`);

--
-- Indexes for table `indic_project`
--
ALTER TABLE `indic_project`
  ADD PRIMARY KEY (`Indic_project_id`);

--
-- Indexes for table `indic_project_report`
--
ALTER TABLE `indic_project_report`
  ADD PRIMARY KEY (`Indic_project_report_id`);

--
-- Indexes for table `objective`
--
ALTER TABLE `objective`
  ADD PRIMARY KEY (`Objective_id`);

--
-- Indexes for table `problem`
--
ALTER TABLE `problem`
  ADD PRIMARY KEY (`Problem_id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`Project_id`);

--
-- Indexes for table `project_indic_success`
--
ALTER TABLE `project_indic_success`
  ADD PRIMARY KEY (`project_indic_success_id`);

--
-- Indexes for table `project_report`
--
ALTER TABLE `project_report`
  ADD PRIMARY KEY (`Report_id`);

--
-- Indexes for table `project_strategic_plane`
--
ALTER TABLE `project_strategic_plane`
  ADD PRIMARY KEY (`Project_strategic_plane_id`);

--
-- Indexes for table `strategic`
--
ALTER TABLE `strategic`
  ADD PRIMARY KEY (`Strategic_id`);

--
-- Indexes for table `strategics_project`
--
ALTER TABLE `strategics_project`
  ADD PRIMARY KEY (`Strategic_Project_id`);

--
-- Indexes for table `strategic_plane`
--
ALTER TABLE `strategic_plane`
  ADD PRIMARY KEY (`Strategic_Plan_id`);

--
-- Indexes for table `tactic`
--
ALTER TABLE `tactic`
  ADD PRIMARY KEY (`Tactic_id`);

--
-- Indexes for table `tbl_workplan`
--
ALTER TABLE `tbl_workplan`
  ADD PRIMARY KEY (`Workplan_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`User_id`);

--
-- Indexes for table `work_step`
--
ALTER TABLE `work_step`
  ADD PRIMARY KEY (`Step_id`);

--
-- Indexes for table `work_step_project_report`
--
ALTER TABLE `work_step_project_report`
  ADD PRIMARY KEY (`Step_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `Account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `benefit`
--
ALTER TABLE `benefit`
  MODIFY `Benefit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `charges_main`
--
ALTER TABLE `charges_main`
  MODIFY `Charges_Main_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `charges_sub`
--
ALTER TABLE `charges_sub`
  MODIFY `Charges_Sub_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `Comment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `Department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `detail`
--
ALTER TABLE `detail`
  MODIFY `Detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `estimate`
--
ALTER TABLE `estimate`
  MODIFY `Estimate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `File_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `goal`
--
ALTER TABLE `goal`
  MODIFY `Goal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `indic_metric`
--
ALTER TABLE `indic_metric`
  MODIFY `Indic_metric_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indic_project`
--
ALTER TABLE `indic_project`
  MODIFY `Indic_project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `indic_project_report`
--
ALTER TABLE `indic_project_report`
  MODIFY `Indic_project_report_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `objective`
--
ALTER TABLE `objective`
  MODIFY `Objective_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `problem`
--
ALTER TABLE `problem`
  MODIFY `Problem_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `Project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `project_indic_success`
--
ALTER TABLE `project_indic_success`
  MODIFY `project_indic_success_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `project_report`
--
ALTER TABLE `project_report`
  MODIFY `Report_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_strategic_plane`
--
ALTER TABLE `project_strategic_plane`
  MODIFY `Project_strategic_plane_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `strategic`
--
ALTER TABLE `strategic`
  MODIFY `Strategic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `strategics_project`
--
ALTER TABLE `strategics_project`
  MODIFY `Strategic_Project_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `strategic_plane`
--
ALTER TABLE `strategic_plane`
  MODIFY `Strategic_Plan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tactic`
--
ALTER TABLE `tactic`
  MODIFY `Tactic_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_workplan`
--
ALTER TABLE `tbl_workplan`
  MODIFY `Workplan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `User_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `work_step`
--
ALTER TABLE `work_step`
  MODIFY `Step_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `work_step_project_report`
--
ALTER TABLE `work_step_project_report`
  MODIFY `Step_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
